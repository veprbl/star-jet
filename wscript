#!/usr/bin/env python

top = '.'
out = 'build'

def options(ctx):
    ctx.load('compiler_cxx')

def configure(ctx):
    ctx.load('compiler_cxx')

    ctx.env.append_value('CXXFLAGS', ["-O2", "-std=c++11"])

    yoda_config, = ctx.find_program("yoda-config")
    ctx.env.append_value('INCLUDES', [ctx.cmd_and_log([yoda_config, "--includedir"]).strip()])
    ctx.env.append_value('LIBPATH', [ctx.cmd_and_log([yoda_config, "--libdir"]).strip()])
    ctx.env.LIB += ["YODA"]

    root_config, = ctx.find_program("root-config")
    ctx.env.append_value('INCLUDES', [ctx.cmd_and_log([root_config, "--incdir"]).strip()])
    ctx.env.append_value('LIBPATH', [ctx.cmd_and_log([root_config, "--libdir"]).strip()])
    ctx.env.append_value('LIB', [
        "Core",
        "RIO",
        "Hist",
        "Tree",
        "Physics",
        "EG",
        ])

    ctx.find_program("rootcling", var='ROOTCLING')

import re
classdef_regex = re.compile(r"ClassDef[^(]*\(([^,\)]*),[^\)]*\)")

from waflib.Task import Task
class gen_linkdef(Task):
    def run(self):
        classes = []
        for input_node in self.inputs:
            with open(input_node.abspath(), 'r') as fp:
                content = fp.read()
                classes += re.findall(classdef_regex, content)
        assert len(self.outputs) == 1
        linkdef_contents = """\
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
                
"""
        for class_name in classes:
            linkdef_contents += "#pragma link C++ class {}+;\n".format(class_name)
        linkdef_contents += "\n#endif"
        self.outputs[0].write(linkdef_contents);

def build(ctx):
    libs = ["StJetEvent", "StJetSkimEvent", "StUeEvent"]
    for lib in libs:
        headers = ctx.path.ant_glob("StSpinPool/{}/*.h".format(lib))
        nonlinkdef_headers = filter(lambda s: s.name.find("LinkDef") == -1, headers)
        linkdef_header = filter(lambda s: s.name.find("LinkDef") != -1, headers)
        assert(len(linkdef_header) <= 1)
        if len(linkdef_header) == 0:
            t = gen_linkdef(env=ctx.env)
            t.set_inputs(nonlinkdef_headers)
            linkdef_header = [ctx.path.make_node("{}LinkDef.h".format(lib)).get_bld()]
            t.set_outputs(linkdef_header)
            ctx.add_to_group(t)
        # put linkdef header last
        headers = nonlinkdef_headers + linkdef_header
        dict_node = [ctx.path.make_node("{}_dict.cxx".format(lib)).get_bld()]
        dict_pcm_node = [ctx.path.make_node("{}_dict_rdict.pcm".format(lib)).get_bld()]
        ctx.install_files("${PREFIX}/bin", [dict_pcm_node])
        ctx(source=headers, target=dict_node + dict_pcm_node, rule="${ROOTCLING} -f ${TGT[0]} ${CPPPATH_ST:INCPATHS} ${SRC}", features='includes', includes='.')
        sources = ctx.path.ant_glob("StSpinPool/{}/*.cxx".format(lib))
        ctx.stlib(source=sources + dict_node, target=lib, includes='.')
    ctx.program(
            source="StJetPlots/make_jet_plots.cxx",
            target="make_jet_plots",
            includes=".",
            use=libs
            )
