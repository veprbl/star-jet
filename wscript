#!/usr/bin/env python

import os.path

top = '.'
out = 'build'

def options(ctx):
    ctx.load('compiler_cxx')

    ctx.add_option('--with-pdf-reweighting', action='store_true', default=False, help='Enable support for PDF reweighting in make_jet_plots')
    ctx.add_option('--without-jet2parquet', action='store_true', default=False, help='Disable building jet2parquet')


def configure(ctx):
    ctx.load('compiler_cxx')

    ctx.env.append_value('CXXFLAGS', ["-O2"])

    yoda_config, = ctx.find_program("yoda-config")
    ctx.env.append_value("INCLUDES_YODA", ctx.cmd_and_log([yoda_config, "--includedir"]).strip())
    ctx.env.append_value("LIBPATH_YODA", ctx.cmd_and_log([yoda_config, "--libdir"]).strip())
    ctx.env.append_value("LIB_YODA", "YODA")

    root_config, = ctx.find_program("root-config")
    ctx.env.append_value("INCLUDES_ROOT", ctx.cmd_and_log([root_config, "--incdir"]).strip())
    ctx.env.append_value("LIBPATH_ROOT", ctx.cmd_and_log([root_config, "--libdir"]).strip())
    ctx.env.append_value("LIB_ROOT", [
        "Core",
        "RIO",
        "Hist",
        "Tree",
        "Physics",
        "EG",
        ])
    cxxstd_flags = [flag for flag in ctx.cmd_and_log([root_config, "--cflags"]).split() if flag.startswith("-std=c++")]
    ctx.env.append_value('CXXFLAGS', cxxstd_flags)

    ctx.find_program("rootcling", var='ROOTCLING')

    ctx.env.WITH_PDF_REWEIGHTING = ctx.options.with_pdf_reweighting

    if ctx.env.WITH_PDF_REWEIGHTING:
        lhapdf_config, = ctx.find_program("lhapdf-config")
        ctx.env.append_value('INCLUDES_LHAPDF', [ctx.cmd_and_log([lhapdf_config, "--incdir"]).strip()])
        ctx.env.append_value('LIBPATH_LHAPDF', [ctx.cmd_and_log([lhapdf_config, "--libdir"]).strip()])
        ctx.env.append_value('LIB_LHAPDF', ["LHAPDF"])

    ctx.env.BUILD_JET2PARQUET = not ctx.options.without_jet2parquet

    if ctx.env.BUILD_JET2PARQUET:
        ctx.check_cfg(atleast_pkgconfig_version='0.0.0')

        ctx.check_cfg(
            package='arrow', uselib_store='ARROW',
            args=['--cflags', '--libs'],
        )
        ctx.check_cfg(
            package='parquet', uselib_store='PARQUET',
            args=['--cflags', '--libs'],
        )


import re
classdef_regex = re.compile(r"ClassDef[^(]*\(([^,\)]*),[^\)]*\)")

from waflib.Task import Task
class gen_linkdef(Task):
    def run(self):
        classes = []
        for input_node in self.inputs:
            with open(input_node.abspath(), 'r') as fp:
                content = fp.read()
                classes += re.findall(classdef_regex, content)
        assert len(self.outputs) == 1
        linkdef_contents = """\
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
                
"""
        for class_name in classes:
            linkdef_contents += "#pragma link C++ class {}+;\n".format(class_name)
        linkdef_contents += "\n#endif"
        self.outputs[0].write(linkdef_contents);

def build(ctx):
    libs = ["StJetEvent", "StJetSkimEvent", "StUeEvent"]
    for lib in libs:
        include_root = ctx.path
        headers = include_root.ant_glob("StSpinPool/{}/*.h".format(lib))
        ctx.install_files("${{PREFIX}}/include/StSpinPool/{}/".format(lib), headers)
        nonlinkdef_headers = list(filter(lambda s: s.name.find("LinkDef") == -1, headers))
        linkdef_header = list(filter(lambda s: s.name.find("LinkDef") != -1, headers))
        assert(len(linkdef_header) <= 1)
        if len(linkdef_header) == 0:
            t = gen_linkdef(env=ctx.env)
            t.set_inputs(nonlinkdef_headers)
            linkdef_header = [ctx.path.make_node("{}LinkDef.h".format(lib)).get_bld()]
            t.set_outputs(linkdef_header)
            ctx.add_to_group(t)
        # put linkdef header last
        headers = nonlinkdef_headers + linkdef_header
        dict_node = [ctx.path.make_node("{}_dict.cxx".format(lib)).get_bld()]
        dict_pcm_node = [ctx.path.make_node("{}_dict_rdict.pcm".format(lib)).get_bld()]
        ctx.install_files("${PREFIX}/bin", [dict_pcm_node])
        ctx(source=headers, target=dict_node + dict_pcm_node, rule="${ROOTCLING} -f ${TGT[0]} ${CPPPATH_ST:INCPATHS} " + " ".join([header.path_from(include_root) for header in headers]), features='includes', includes=[include_root, "${PREFIX}/include"])
        sources = ctx.path.ant_glob("StSpinPool/{}/*.cxx".format(lib))
        ctx.stlib(source=sources + dict_node, target=lib, includes='.')
    ctx.program(
            source="StJetPlots/make_jet_plots.cxx",
            target="make_jet_plots",
            includes=[include_root],
            use=libs + ["LHAPDF", "ROOT", "YODA"],
            defines=["WITH_PDF_REWEIGHTING=1"] if ctx.env.WITH_PDF_REWEIGHTING else None,
            )
    if ctx.env.BUILD_JET2PARQUET:
        ctx.program(
                source="src/jet2parquet/jet2parquet.cpp",
                target="jet2parquet",
                includes=[include_root],
                use=libs + ["ARROW", "PARQUET", "ROOT"],
                )
