// vim:et sw=2 sts=2
#pragma once

#include "common.h"

template<typename T>
class StTriggerDemotionReweighting : public T
{
public:

  template <typename ...Args>
  StTriggerDemotionReweighting(int _jp, Args ...args) : T(args...), jp(_jp) {};
  virtual ~StTriggerDemotionReweighting() {};

  bool force_trig_reweighting = false;

  void fill(const StJetPlotEvent& e, double weight) override
  {
    bool do_reweight = e.is_embedding() || force_trig_reweighting;

    if (do_reweight) {
      switch(jp) {
      case 0:
        weight *= 1 / e.jp0ps;
        break;
      case 1:
        weight *= (1 / e.jp1ps) * (1 - 1 / e.jp0ps);
        break;
      case 2:
        weight *= (1 - 1 / e.jp0ps) * (1 - 1 / e.jp1ps);
        break;
      default:
        always_assert(false);
      }
    }

    T::fill(e, weight);
  }

private:
  int jp;
};
