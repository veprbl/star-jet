// vim:et sw=2 sts=2
#pragma once

#include <TDatime.h>
#define protected public
#include <StSpinPool/StJetEvent/StJetTower.h>
#undef protected
#define private public
#include <StSpinPool/StJetEvent/StUeEvent.h>
#undef private

#include "StJetPlotEvent.h"
#include "StJetPlotContainer.h"

// There is a bug in the region UE code: region phi cut is applied before tower
// correction, so some towers don't get corrected.  This wrapper implements the
// missing correction as post processing.
template<typename T>
class StUECorrectTowersBug : public T
{
public:

  template <typename ...Args>
  StUECorrectTowersBug(Args ...args) : T(args...) {};
  virtual ~StUECorrectTowersBug() {};

  void fill(const StJetPlotEvent& orig_e, double weight) override
  {
    StJetPlotEvent e = orig_e;

    vector<StUeEvent*> all_regions = {
      new StUeEvent(*e.ueEvent_transP),
      new StUeEvent(*e.ueEvent_transM),
      new StUeEvent(*e.ueEvent_away),
      new StUeEvent(*e.ueEvent_toward),
    };
    for(StUeEvent* ue_event : all_regions) {
      ue_event->mTowers = new TClonesArray(*ue_event->mTowers);
    }

    for(StUeEvent* r1 : all_regions)
      for(StUeEvent* r2 : all_regions) {
        if (r1 == r2) continue; // towers are already corrected for tracks that fall into region
        for(TObject* _tower : (*r1->towers())) {
          StJetTower* tower = static_cast<StJetTower*>(_tower);
          if (tower->pt() < 1e-9) continue; // already corrected
          for(TObject* _track : (*r2->tracks())) {
            StJetTrack* track = static_cast<StJetTrack*>(_track);
            if ((track->exitDetectorId() == tower->detectorId()) && (track->exitTowerId() == tower->id())) {
#if 0
              std::cerr << "removed tower\tphi: " << tower->phi() << "\teta: " << tower->eta() << "\tpt: " << tower->pt() << std::endl;
              std::cerr << "by track\tphi: " << track->phi() << "\teta: " << track->eta() << "\tpt: " << track->pt() << std::endl;
#endif
              // Original from StjTowerEnergyCorrectionForTracksFraction uses
              //   tower->energy -= mFraction * track->pt * cosh(track->eta);
              // eta and phi are preserved so this should be equivalent:
              tower->mPt -= 1.0 * track->pt(); // fraction 1.0 must match to what is used in jet finder
              if (tower->pt() < 0) tower->mPt = 1e-10;
#if 0
              std::cerr << "final pt " << tower->pt() << std::endl;
#endif
            }
          }
        }
      }

    e.ueEvent_transP = all_regions[0];
    e.ueEvent_transM = all_regions[1];
    e.ueEvent_away   = all_regions[2];
    e.ueEvent_toward = all_regions[3];

    T::fill(e, weight);
  }
};
