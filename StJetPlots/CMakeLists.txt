# vim: set noexpandtab:

add_executable(make_jet_plots
	make_jet_plots.cxx
	)

target_link_libraries(make_jet_plots
	${ROOT_LIBRARIES}
	${YODA_LIBRARIES}
	StJetEvent
	StJetSkimEvent
	StUeEvent
	)

install(TARGETS make_jet_plots
        RUNTIME DESTINATION bin)
