// vim:et sw=2 sts=2
#pragma once

#include <set>

#include <TChain.h>
#include <TFile.h>

#include <StSpinPool/StJetEvent/StJetEvent.h>
#include <StSpinPool/StJetSkimEvent/StJetSkimEvent.h>
#include <StSpinPool/StJetSkimEvent/StPythiaEvent.h>
#include <StSpinPool/StJetEvent/StJetCandidate.h>
#include <StSpinPool/StJetEvent/StJetTower.h>
#include <StSpinPool/StJetEvent/StJetTrack.h>
#include <StSpinPool/StJetEvent/StJetVertex.h>
#include <StSpinPool/StJetEvent/StUeEvent.h>
#include <StSpinPool/StUeEvent/StUeOffAxisConesEvent.h>

/**
 * \brief Class to hold event data
 */
class StJetPlotEvent {
public:

  const StJetEvent& jetEvent;
  const StJetEvent& jetParticleEvent;
  const std::unordered_map<StJetCandidate*, double> &det_weight;
  const StJetSkimEvent& skimEvent;
  const StUeOffAxisConesEvent& ueEvent;
  const StUeOffAxisConesEvent& ueParticleEvent;
  const StUeEvent* ueEvent_transP;
  const StUeEvent* ueEvent_transM;
  const StUeEvent* ueEvent_away;
  const StUeEvent* ueEvent_toward;
  const std::set<int>& did_fire_triggers;
  const std::set<int>& should_fire_triggers;
  const std::set<int>& triggers;
  const TChain &chain;
  const Long64_t tree_entry;
  const Long64_t chain_entry;
  const double original_weight;
  const int runindex;
  const double jp0ps;
  const double jp1ps;

  bool is_embedding() const { return skimEvent.mcEvent(); }

  StJetPlotEvent(
      decltype(jetEvent) _jetEvent,
      decltype(jetParticleEvent) _jetParticleEvent,
      decltype(det_weight) _det_weight,
      decltype(skimEvent) _skimEvent,
      decltype(ueEvent) _ueEvent,
      decltype(ueParticleEvent) _ueParticleEvent,
      decltype(ueEvent_transP) _ueEvent_transP,
      decltype(ueEvent_transM) _ueEvent_transM,
      decltype(ueEvent_away) _ueEvent_away,
      decltype(ueEvent_toward) _ueEvent_toward,
      decltype(did_fire_triggers) _did_fire_triggers,
      decltype(should_fire_triggers) _should_fire_triggers,
      decltype(triggers) _triggers,
      decltype(chain) _chain,
      decltype(chain_entry) _chain_entry,
      decltype(tree_entry) _tree_entry,
      decltype(original_weight) _original_weight,
      decltype(runindex) _runindex,
      decltype(jp0ps) _jp0ps,
      decltype(jp1ps) _jp1ps
      )
    : jetEvent(_jetEvent)
    , jetParticleEvent(_jetParticleEvent)
    , det_weight(_det_weight)
    , skimEvent(_skimEvent)
    , ueEvent(_ueEvent)
    , ueParticleEvent(_ueParticleEvent)
    , ueEvent_transP(_ueEvent_transP)
    , ueEvent_transM(_ueEvent_transM)
    , ueEvent_away(_ueEvent_away)
    , ueEvent_toward(_ueEvent_toward)
    , did_fire_triggers(_did_fire_triggers)
    , should_fire_triggers(_should_fire_triggers)
    , triggers(_triggers)
    , chain(_chain)
    , chain_entry(_chain_entry)
    , tree_entry(_tree_entry)
    , original_weight(_original_weight)
    , runindex(_runindex)
    , jp0ps(_jp0ps)
    , jp1ps(_jp1ps)
  {};
};

static ostream& operator<<(ostream& s, const StJetPlotEvent& e)
{
  s << "runId: " << e.jetEvent.runId() << "@" << e.runindex
    << "\teventId: " << e.jetEvent.eventId()
    << "\tfile: " << e.chain.GetFile()->GetName()
    << "\ttree entry:" << e.tree_entry
    << "\tchain entry:" << e.chain_entry
    << std::endl;

  if (e.is_embedding()) {
    s << "Pythia jets:" << std::endl;
    for (TObject *_particle_jet : e.jetParticleEvent.vertex()->jets()) {
      StJetCandidate *particle_jet = dynamic_cast<StJetCandidate*>(_particle_jet);
      s << "phi: " << particle_jet->phi()
        << "\teta: " << particle_jet->eta()
        << "\tpt: " << particle_jet->pt()
        << "\tE: " << particle_jet->E()
        << std::endl;
      for (TObject *_particle : particle_jet->particles()) {
        StJetParticle *particle = dynamic_cast<StJetParticle*>(_particle);
        s << "\tphi: " << particle->phi()
          << "\teta: " << particle->eta()
          << "\tpt: " << particle->pt()
          << "\tpdg: " << particle->pdg()
          << std::endl;
      }
    }
  }

  s << "Detector jets:" << std::endl;
  for (TObject *_detector_jet : e.jetEvent.vertex()->jets()) {
    StJetCandidate *detector_jet = dynamic_cast<StJetCandidate*>(_detector_jet);
    s << "phi: " << detector_jet->phi()
      << "\teta: " << detector_jet->eta()
      << "\tpt: " << detector_jet->pt()
      << "\tE: " << detector_jet->E()
      << std::endl;
    for (TObject *_tower: detector_jet->towers()) {
      StJetTower *tower = dynamic_cast<StJetTower*>(_tower);
      s << "\tphi: " << tower->phi()
        << "\teta: " << tower->eta()
        << "\tpt: " << tower->pt()
        << "\tid: " << tower->id()
        << "\tsector: " << tower->sector()
        << "\tsubsector: " << tower->subsector()
        << "\tetabin: " << tower->etabin()
        << "\tphibin: " << tower->phibin()
        << std::endl;
    }
    for (TObject *_track: detector_jet->tracks()) {
      StJetTrack *track = dynamic_cast<StJetTrack*>(_track);
      s << "\tphi: " << track->phi()
        << "\teta: " << track->eta()
        << "\tpt: " << track->pt()
        << std::endl;
    }
  }

  return s;
}
