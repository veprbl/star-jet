// vim:et sw=2 sts=2
#pragma once

#include <map>
#include <vector>

#include <YODA/AnalysisObject.h>
#include <YODA/WriterYODA.h>

#include <YODA/Counter.h>
#include <YODA/Histo1D.h>
#include <YODA/Histo2D.h>
#include <YODA/Profile1D.h>

using YODA::Counter;
using YODA::Histo1D;
using YODA::Histo2D;
using YODA::Profile1D;

/**
 * Base class for analysis that processes events and fills AnalysisObject's (histograms, profiles, etc)
 */
template<typename Evt>
class Analysis : public YODA::AnalysisObject
{
public:

  Analysis(const std::string &path, const std::string &title="")
    : YODA::AnalysisObject("Analysis", path, title)
  {
  };
  Analysis(const Analysis&) = delete;
  Analysis& operator=(const Analysis&) = delete;
  virtual ~Analysis() {};

  virtual void fill(const Evt& e, double weight) = 0;
  virtual void finalize() {};

  std::map<std::string, YODA::AnalysisObject*> aos;
  void add(YODA::AnalysisObject &ao)
  {
    if (path() != "/") {
      // TODO: perhaps should traverse all children as well
      ao.setPath(path() + ao.path());
    }
    aos.emplace(ao.path(), &ao);
  };

  template<typename AO>
  AO& ao(const std::string &path)
  {
    return *dynamic_cast<AO*>(aos.at(path));
  };

  virtual std::vector<YODA::AnalysisObject*> to_vec()
  {
    std::vector<YODA::AnalysisObject*> result;
    for(auto &p : aos) {
      result.push_back(p.second);
    }
    return result;
  }

  void writeYODA(std::ostream& file)
  {
    YODA::WriterYODA::create().write(file, to_vec());
  }

  void writeYODA(const std::string& filename)
  {
    YODA::WriterYODA::create().write(filename, to_vec());
  }

  void reset() override { throw std::logic_error("Not implemented"); };
  Analysis* newclone() const override { throw std::logic_error("Not implemented"); };
  size_t dim() const override { throw std::logic_error("Not implemented"); };
};
