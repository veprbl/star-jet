// vim:et sw=2 sts=2
#pragma once

#include <cmath>

#include "common.h"

enum {
  None = 0,
  DataTPCVertex,
  EmbeddingTPCVertex,
  EmbeddingGenVertex
};

/**
 * \brief Applies vertex reweighting to arbitrary Analysis by wrapping its fill() method
 */
template<typename T>
class StVertexReweighting : public T
{
public:

  template <typename ...Args>
  StVertexReweighting(Args ...args) : T(args...), _reweighting_type(None), _sigma(-1), _sigma_target(-1) {};
  virtual ~StVertexReweighting() {};

  void SetVertexReweightingParams(unsigned int reweighting_type, double sigma, double b, double sigma_target, double b_target)
  {
    _reweighting_type = reweighting_type;
    _sigma = sigma;
    _b = b;
    _sigma_target = sigma_target;
    _b_target = b_target;
  }

  void fill(const StJetPlotEvent& e, double weight) override
  {
    always_assert(_sigma > 0);

    bool do_reweight;
    double vertex_z;
    StJetVertex *primary_vertex = e.jetEvent.vertex();

    switch (_reweighting_type) {
    case None:
      do_reweight = false;
      break;

    case DataTPCVertex:
      do_reweight = !e.is_embedding();
      vertex_z = primary_vertex->position().z();
      break;

    case EmbeddingTPCVertex:
      do_reweight = e.is_embedding();
      vertex_z = primary_vertex->position().z();
      break;

    case EmbeddingGenVertex:
      do_reweight = e.is_embedding();
      if (do_reweight) {
        const TVector3 &generator_vertex = e.skimEvent.mcEvent()->vertex();
        vertex_z = generator_vertex.z();
      }
      break;

    default:
      always_assert(false);
    }

    auto norm = [](double sigma, double b) {
      if (std::isinf(b)) {
        return sigma * std::sqrt(2 * M_PI);
      } else {
        return b * std::exp(sqr(b) / 2 / sqr(sigma)) * M_PI * std::erfc(b / std::sqrt(2) / sigma);
      }
    };

    if (do_reweight) {
      double factor = std::exp((1/sqr(_sigma) - 1/sqr(_sigma_target)) * sqr(vertex_z) / 2)
        * (1 + sqr(vertex_z / _b))
        / (1 + sqr(vertex_z / _b_target))
        * norm(_sigma, _b)
        / norm(_sigma_target, _b_target)
        ;
      weight *= factor;
    }

    if (_reweighting_type == EmbeddingGenVertex) {
      // Overriding original_weight should be optional, but we exercise this
      // for the sake of consistency check
      StJetPlotEvent _e = e;
      const_cast<double&>(_e.original_weight) = weight;

      T::fill(_e, weight);
    } else {
      T::fill(e, weight);
    }
  }

private:

  unsigned int _reweighting_type;
  double _sigma;
  double _b;
  double _sigma_target;
  double _b_target;
};
