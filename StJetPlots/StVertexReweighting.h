// vim:et sw=2 sts=2
#pragma once

#include <cmath>

#include "common.h"

template<typename T>
class StVertexReweighting : public T
{
public:

  template <typename ...Args>
  StVertexReweighting(Args ...args) : T(args...), _sigma(-1), _sigma_target(-1), _vertex_z_cut(-1) {};
  virtual ~StVertexReweighting() {};

  void SetVertexReweightingParams(double sigma, double sigma_target, double vertex_z_cut)
  {
    _sigma = sigma;
    _sigma_target = sigma_target;
    _vertex_z_cut = vertex_z_cut;
  }

  void fill(const StJetPlotEvent& e, double weight) override
  {
    myassert(_sigma > 0);

    StJetVertex *primary_vertex = e.jetEvent.vertex();

    double vertex_z = primary_vertex->position().z();
    if (e.is_embedding()) {
      weight *= std::exp((1/sqr(_sigma) - 1/sqr(_sigma_target)) * sqr(vertex_z) / 2)
        * (_sigma * std::erf(_vertex_z_cut / std::sqrt(2) / _sigma))
        / (_sigma_target * std::erf(_vertex_z_cut / std::sqrt(2) / _sigma_target))
        ;
    }

    T::fill(e, weight);
  }

private:

  template <typename V>
  static V sqr(V x) { return x * x; }

  double _sigma;
  double _sigma_target;
  double _vertex_z_cut;
};
