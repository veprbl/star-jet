// vim:et sw=2 sts=2
#pragma once

#include "common.h"

template<typename T>
class StTriggerCut : public T
{
public:

  template <typename ...Args>
  StTriggerCut(Args ...args) : T(args...) {};
  virtual ~StTriggerCut() {};

  void SetTriggers(const std::set<int>& triggers)
  {
    mTriggers = triggers;
  }

  void fill(const StJetPlotEvent& e, double weight) override
  {
    myassert(mTriggers.size());
    bool pass = false;
    for(int trig_id : e.fired_triggers)
      if (mTriggers.count(trig_id)) {
        pass = true;
        continue;
      }
    if (pass)
      T::fill(e, weight);
  }

private:

  std::set<int> mTriggers;
};
