// vim:et sw=2 sts=2
#pragma once

#include "common.h"

/**
 * \brief Applies a triger requirement cut to arbitrary Analysis by wrapping its fill() method
 */
template<typename T>
class StTriggerCut : public T
{
public:

  template <typename ...Args>
  StTriggerCut(Args ...args) : T(args...) {};
  virtual ~StTriggerCut() {};

  void SetTriggers(const std::set<int>& triggers)
  {
    mTriggers = triggers;
  }

  void fill(const StJetPlotEvent& e, double weight) override
  {
    always_assert(mTriggers.size());
    bool pass = false;
    for(int trig_id : e.triggers)
      if (mTriggers.count(trig_id)) {
        pass = true;
        continue;
      }
    if (pass)
      T::fill(e, weight);
  }

private:

  std::set<int> mTriggers;
};
