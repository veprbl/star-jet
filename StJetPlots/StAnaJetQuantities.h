// vim:et sw=2 sts=2
#pragma once

#include <functional>

#include "constants.h"
#include "common.h"
#include "Analysis.h"
#include "StJetCut.h"

class StAnaJetQuantities : public Analysis<StJetPlotEvent>
{
public:

  template <typename ...Args>
  StAnaJetQuantities(Args ...args)
    : Analysis<StJetPlotEvent>(args...)
    , vs_jet_pt_binning{{ 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 36, 40, 44, 48, 52, 60 }}
    , vx(50, -50., 50.,   "vertex_x", "Vertex x") /* TODO: s/vertex_/v/ */
    , vy(50, -50., 50.,   "vertex_y", "Vertex y")
    , vz(50, -100., 100., "vertex_z", "Vertex z")
    , vz_raw(50, -100., 100., "vz_raw", "Vertex z")
    , gvz_vs_vz(50, -100., 100., 50, -100., 100., "gvz_vs_vz")
    , jet_pt(60, 0., 60., "jet_pt", "Inclusive jet $p_T$")
    , jet_eta(60, -1.2, 1.2, "jet_eta", "Inclusive jet $\\eta$")
    , jet_eta_det(60, -1.2, 1.2, "jet_eta_det", "Inclusive jet $\\eta_{det}$")
    , jet_phi(60, -M_PI, M_PI, "jet_phi", "Inclusive jet $\\phi$")
    , jet_rt(41, 0., 1.+1./60, "jet_rt", "Inclusive jet $R_T$")
    , jet_rt_monojets(41, 0., 1.+1./60, "jet_rt_monojets", "Inclusive jet $R_T$")
    , jet_rt_dijets(41, 0., 1.+1./60, "jet_rt_dijets", "Inclusive jet $R_T$")
    , jet_rt_vs_pt(vs_jet_pt_binning, "jet_rt_vs_pt", "Average jet $R_T$ vs jet $p_T$")
    , jet_rt_vs_eta(60, -1.0, 1.0, "jet_rt_vs_eta", "Average jet $R_T$ vs jet $\\eta$")
    , jet_rt_vs_det_eta(60, -1.0, 1.0, "jet_rt_vs_det_eta", "Average jet $R_T$ vs jet detector $\\eta$")
    , jet_tower_mult(20, 0., 20., "jet_tower_mult", "Jet tower multiplicity")
    , jet_track_mult(20, 0., 20., "jet_track_mult", "Jet track multiplicity")
    , jet_tower_z(21, 0., 1.+1./20, "jet_tower_z", "Jet tower z distribution")
    , jet_track_z(21, 0., 1.+1./20, "jet_track_z", "Jet track z distribution")
    , jet_tower_pt_vs_jet_pt(vs_jet_pt_binning, "jet_tower_pt_vs_jet_pt", "Average jet tower $p_T$ vs jet $p_T$")
    , jet_track_pt_vs_jet_pt(vs_jet_pt_binning, "jet_track_pt_vs_jet_pt", "Average jet track $p_T$ vs jet $p_T$")
    , jet_tower_mult_vs_jet_pt(vs_jet_pt_binning, "jet_tower_mult_vs_jet_pt", "Average jet tower multiplicity vs jet $p_T$")
    , jet_subtr_tower_mult_vs_jet_pt(vs_jet_pt_binning, "jet_subtr_tower_mult_vs_jet_pt", "Average jet subtracted tower multiplicity vs jet $p_T$")
    , jet_track_mult_vs_jet_pt(vs_jet_pt_binning, "jet_track_mult_vs_jet_pt", "Average jet track multiplicity vs jet $p_T$")
    , jet_ue_density_vs_jet_pt(vs_jet_pt_binning, "jet_ue_density_vs_jet_pt", "Average cone UE density vs jet $p_T$")
    , jet_ue_density_vs_jet_pt_corrected(vs_jet_pt_binning, "jet_ue_density_vs_jet_pt_corrected", "Average cone UE density vs jet $p_T$")
    , jet_ue_neut_mult_vs_jet_pt(vs_jet_pt_binning, "jet_ue_neut_mult_vs_jet_pt", "Average cone UE tower multiplicy vs jet $p_T$")
    , jet_ue_chg_mult_vs_jet_pt(vs_jet_pt_binning, "jet_ue_chg_mult_vs_jet_pt", "Average cone UE track multiplicy vs jet $p_T$")
    , jet_ue_neut_sumpt_vs_jet_pt(vs_jet_pt_binning, "jet_ue_neut_sumpt_vs_jet_pt", "Average cone UE tower $\\sum p_T$ vs jet $p_T$")
    , jet_ue_chg_sumpt_vs_jet_pt(vs_jet_pt_binning, "jet_ue_chg_sumpt_vs_jet_pt", "Average cone UE track $\\sum p_T$ vs jet $p_T$")
    , jet_pt_vs_vz(    50, -100., 100., "jet_pt_vs_vz", "Average jet $p_T$ vs vertex z")
    , jet_rt_vs_vz(    50, -100., 100., "jet_rt_vs_vz", "Average jet $R_T$ vs vertex z")
    , num_vertex_vs_vz(50, -100., 100., "num_vertex_vs_vz", "Average number of vertices vs vertex z")
    , num_jets_vs_vz(  50, -100., 100., "num_jets_vs_vz", "Average number of jets vs vertex z")
    , vpd_vz(50, -100., 100., "vpd_vz", "Vertex z")
    , vpd_vz_raw(50, -100., 100., "vpd_vz_raw", "Vertex z")
    , vpd_vz_raw_single_hit(50, -100., 100., "vpd_vz_raw_single_hit", "Vertex z")
    , vpd_vz_vs_vz(50, -100., 100., 50, -100., 100., "vpd_vz_vs_vz", "VPD vz vs reonstructed vz")
    , events_raw("events_raw")
    , events_have_jets("events_have_jets")
    , events_after_rank_cut("events_after_rank_cut")
    , events_after_det_eta_cut("events_after_det_eta_cut")
    , events_after_phys_eta_cut("events_after_phys_eta_cut")
    , events_after_jp_match_cut("events_after_jp_match_cut")
    , events_after_pt_cut("events_after_pt_cut")
    , events_after_tower_3407_cut("events_after_tower_3407_cut")
    , events_after_max_track_pt_cut("events_after_max_track_pt_cut")
    , events_after_vertex_cut("events_after_vertex_cut")
    , events_after_rt_cut("events_after_rt_cut")
    , jets_raw("jets_raw")
    , jets_after_rank_cut("jets_after_rank_cut")
    , jets_after_det_eta_cut("jets_after_det_eta_cut")
    , jets_after_phys_eta_cut("jets_after_phys_eta_cut")
    , jets_after_jp_match_cut("jets_after_jp_match_cut")
    , jets_after_pt_cut("jets_after_pt_cut")
    , jets_after_tower_3407_cut("jets_after_tower_3407_cut")
    , jets_after_max_track_pt_cut("jets_after_max_track_pt_cut")
    , jets_after_vertex_cut("jets_after_vertex_cut")
    , jets_after_rt_cut("jets_after_rt_cut")
  {
    add(vx);
    add(vy);
    add(vz);
    add(vz_raw);
    add(gvz_vs_vz);
    add(jet_pt);
    add(jet_eta);
    add(jet_eta_det);
    add(jet_phi);
    add(jet_rt);
    add(jet_rt_monojets);
    add(jet_rt_dijets);
    add(jet_rt_vs_pt);
    add(jet_rt_vs_eta);
    add(jet_rt_vs_det_eta);
    add(jet_tower_mult);
    add(jet_track_mult);
    add(jet_tower_z);
    add(jet_track_z);
    add(jet_tower_pt_vs_jet_pt);
    add(jet_track_pt_vs_jet_pt);
    add(jet_tower_mult_vs_jet_pt);
    add(jet_subtr_tower_mult_vs_jet_pt);
    add(jet_track_mult_vs_jet_pt);
    add(jet_ue_density_vs_jet_pt);
    add(jet_ue_density_vs_jet_pt_corrected);
    add(jet_ue_neut_mult_vs_jet_pt);
    add(jet_ue_chg_mult_vs_jet_pt);
    add(jet_ue_neut_sumpt_vs_jet_pt);
    add(jet_ue_chg_sumpt_vs_jet_pt);
    add(jet_pt_vs_vz);
    add(jet_rt_vs_vz);
    add(num_vertex_vs_vz);
    add(num_jets_vs_vz);
    add(vpd_vz);
    add(vpd_vz_raw);
    add(vpd_vz_raw_single_hit);
    add(vpd_vz_vs_vz);
    add(events_raw);
    add(events_have_jets);
    add(events_after_rank_cut);
    add(events_after_det_eta_cut);
    add(events_after_phys_eta_cut);
    add(events_after_jp_match_cut);
    add(events_after_pt_cut);
    add(events_after_tower_3407_cut);
    add(events_after_max_track_pt_cut);
    add(events_after_vertex_cut);
    add(events_after_rt_cut);
    add(jets_raw);
    add(jets_after_rank_cut);
    add(jets_after_det_eta_cut);
    add(jets_after_phys_eta_cut);
    add(jets_after_jp_match_cut);
    add(jets_after_pt_cut);
    add(jets_after_tower_3407_cut);
    add(jets_after_max_track_pt_cut);
    add(jets_after_vertex_cut);
    add(jets_after_rt_cut);

    const char *s = getenv("match_to_gen_vertex");
    do_match_to_gen_vertex = atoi(s ? s : "0");
  }

  bool do_match_to_gen_vertex;
  StJetCut jet_cut;
  std::vector<double> vs_jet_pt_binning;
  Histo1D vx;
  Histo1D vy;
  Histo1D vz;
  Histo1D vz_raw;
  Histo2D gvz_vs_vz;
  Histo1D jet_pt;
  Histo1D jet_eta;
  Histo1D jet_eta_det;
  Histo1D jet_phi;
  Histo1D jet_rt;
  Histo1D jet_rt_monojets;
  Histo1D jet_rt_dijets;
  Profile1D jet_rt_vs_pt;
  Profile1D jet_rt_vs_eta;
  Profile1D jet_rt_vs_det_eta;
  Histo1D jet_tower_mult;
  Histo1D jet_track_mult;
  Histo1D jet_tower_z;
  Histo1D jet_track_z;
  Profile1D jet_tower_pt_vs_jet_pt;
  Profile1D jet_track_pt_vs_jet_pt;
  Profile1D jet_tower_mult_vs_jet_pt;
  Profile1D jet_subtr_tower_mult_vs_jet_pt;
  Profile1D jet_track_mult_vs_jet_pt;
  Profile1D jet_ue_density_vs_jet_pt;
  Profile1D jet_ue_density_vs_jet_pt_corrected;
  Profile1D jet_ue_neut_mult_vs_jet_pt;
  Profile1D jet_ue_chg_mult_vs_jet_pt;
  Profile1D jet_ue_neut_sumpt_vs_jet_pt;
  Profile1D jet_ue_chg_sumpt_vs_jet_pt;
  Profile1D jet_pt_vs_vz;
  Profile1D jet_rt_vs_vz;
  Profile1D num_vertex_vs_vz;
  Profile1D num_jets_vs_vz;
  Histo1D vpd_vz;
  Histo1D vpd_vz_raw;
  Histo1D vpd_vz_raw_single_hit;
  Histo2D vpd_vz_vs_vz;
  Counter events_raw;
  Counter events_have_jets;
  Counter events_after_rank_cut;
  Counter events_after_det_eta_cut;
  Counter events_after_phys_eta_cut;
  Counter events_after_jp_match_cut;
  Counter events_after_pt_cut;
  Counter events_after_tower_3407_cut;
  Counter events_after_max_track_pt_cut;
  Counter events_after_vertex_cut;
  Counter events_after_rt_cut;
  Counter jets_raw;
  Counter jets_after_rank_cut;
  Counter jets_after_det_eta_cut;
  Counter jets_after_phys_eta_cut;
  Counter jets_after_jp_match_cut;
  Counter jets_after_pt_cut;
  Counter jets_after_tower_3407_cut;
  Counter jets_after_max_track_pt_cut;
  Counter jets_after_vertex_cut;
  Counter jets_after_rt_cut;

  void fill(const StJetPlotEvent& e, double weight) override
  {
    vpd_vz_raw.fill(e.skimEvent.vpdZvertex(), weight);
    if ((e.skimEvent.vpdEastHits() == 1) && (e.skimEvent.vpdWestHits() == 1)) {
      vpd_vz_raw_single_hit.fill(e.skimEvent.vpdZvertex(), weight);
    }

    StJetVertex *primary_vertex = e.jetEvent.vertex();
    events_raw.fill(weight);
    for (__attribute__((unused)) TObject *_jet : primary_vertex->jets()) {
      jets_raw.fill(weight);
    }
    if (!(primary_vertex->ranking() > 0)) return;
    StUeVertex *ue_primary_vertex = e.ueEvent.vertex();
    myassert(ue_primary_vertex);
    events_after_rank_cut.fill(weight);
    for (__attribute__((unused)) TObject *_jet : primary_vertex->jets()) {
      jets_after_rank_cut.fill(weight);
    }

    vz_raw.fill(primary_vertex->position().z(), weight);
    vpd_vz_vs_vz.fill(primary_vertex->position().z(), e.skimEvent.vpdZvertex(), weight);

    if (e.is_embedding()) {
      const TVector3 &generator_vertex = e.skimEvent.mcEvent()->vertex();
      gvz_vs_vz.fill(primary_vertex->position().z(), generator_vertex.z(), weight);
      if (do_match_to_gen_vertex && (fabs(primary_vertex->position().z() - generator_vertex.z()) > 1.0 /* cm */)) return;
    }

    if (primary_vertex->numberOfJets() > 0) {
      events_have_jets.fill(weight);
    }

    bool vertex_cut = fabs(primary_vertex->position().z()) < Z_VERTEX_CUT;
    bool flag = false;
    int i = 0;
    PassCutMask event_pass_mask = 0;
    for (TObject *_jet : primary_vertex->jets()) {
      StJetCandidate *jet = dynamic_cast<StJetCandidate*>(_jet);
      StUeOffAxisConesJet *uejet = static_cast<StUeOffAxisConesJet*>(ue_primary_vertex->ueJets()[i]);
      myassert(jet->pt() == uejet->pt());
      CutResult res = jet_cut(e, *jet);
      PassCutMask jet_pass_mask = res.pass_mask;
      if (res) {
        if (vertex_cut) {
          jet_pass_mask |= PASS_VERTEX_CUT;
          if (jet_cut_rt(jet)) {
            jet_pass_mask |= PASS_RT_CUT;
            jet_pt.fill(jet->pt(), weight);
            jet_eta.fill(jet->eta(), weight);
            jet_eta_det.fill(jet->detEta(), weight);
            jet_phi.fill(jet->phi(), weight);
            jet_track_mult.fill(jet->numberOfTracks(), weight);
            jet_track_mult_vs_jet_pt.fill(jet->pt(), jet->numberOfTracks());
            int tower_mult = 0, subtr_tower_mult = 0;
            for (TObject *_tower : jet->towers()) {
              StJetTower *tower = dynamic_cast<StJetTower*>(_tower);
              if (tower->pt() > 1e-5) {
                jet_tower_z.fill(tower->pt() / jet->pt(), weight);
                jet_tower_pt_vs_jet_pt.fill(jet->pt(), tower->pt(), weight);
                tower_mult++;
              }
              else
              {
                subtr_tower_mult++;
              }
            }
            jet_tower_mult.fill(tower_mult, weight);
            jet_tower_mult_vs_jet_pt.fill(jet->pt(), tower_mult);
            jet_subtr_tower_mult_vs_jet_pt.fill(jet->pt(), subtr_tower_mult);
            for (TObject *_track : jet->tracks()) {
              StJetTrack *track = dynamic_cast<StJetTrack*>(_track);
              jet_track_z.fill(track->pt() / jet->pt(), weight);
              jet_track_pt_vs_jet_pt.fill(jet->pt(), track->pt(), weight);
            }
            jet_rt_vs_pt.fill(jet->pt(), jet->rt(), weight);
            jet_rt_vs_eta.fill(jet->eta(), jet->rt(), weight);
            jet_rt_vs_det_eta.fill(jet->detEta(), jet->rt(), weight);
            jet_ue_density_vs_jet_pt.fill(jet->pt(), jet->ueDensity()["OffAxisConesR050"]);
            jet_ue_density_vs_jet_pt_corrected.fill(jet->pt() - jet->area() * jet->ueDensity()["OffAxisConesR050"], jet->ueDensity()["OffAxisConesR050"]);
            jet_ue_neut_mult_vs_jet_pt.fill(jet->pt(), get_ue_tower_mult(uejet));
            jet_ue_chg_mult_vs_jet_pt.fill(jet->pt(), get_ue_track_mult(uejet));
            jet_ue_neut_sumpt_vs_jet_pt.fill(jet->pt(), get_ue_tower_sumpt(uejet));
            jet_ue_chg_sumpt_vs_jet_pt.fill(jet->pt(), get_ue_track_sumpt(uejet));
          }
          jet_rt.fill(jet->rt(), weight);
          if (primary_vertex->jets().GetEntries() == 1) {
            jet_rt_monojets.fill(jet->rt(), weight);
          }
          if (primary_vertex->jets().GetEntries() == 2) {
            jet_rt_dijets.fill(jet->rt(), weight);
          }
        }
        if (jet_cut_rt(jet)) {
          jet_pt_vs_vz.fill(primary_vertex->position().z(), jet->pt(), weight);
          jet_rt_vs_vz.fill(primary_vertex->position().z(), jet->rt(), weight);
          flag = true;
        }
      }
      if (jet_pass_mask & PASS_DET_ETA_CUT) jets_after_det_eta_cut.fill(weight);
      if (jet_pass_mask & PASS_PHYS_ETA_CUT) jets_after_phys_eta_cut.fill(weight);
      if (jet_pass_mask & PASS_JP_MATCH_CUT) jets_after_jp_match_cut.fill(weight);
      if (jet_pass_mask & PASS_PT_CUT) jets_after_pt_cut.fill(weight);
      if (jet_pass_mask & PASS_TOWER_3407_CUT) jets_after_tower_3407_cut.fill(weight);
      if (jet_pass_mask & PASS_MAX_TRACK_PT_CUT) jets_after_max_track_pt_cut.fill(weight);
      if (jet_pass_mask & PASS_VERTEX_CUT) jets_after_vertex_cut.fill(weight);
      if (jet_pass_mask & PASS_RT_CUT) jets_after_rt_cut.fill(weight);
      event_pass_mask |= jet_pass_mask;
      i++;
    }
    if (event_pass_mask & PASS_DET_ETA_CUT) events_after_det_eta_cut.fill(weight);
    if (event_pass_mask & PASS_PHYS_ETA_CUT) events_after_phys_eta_cut.fill(weight);
    if (event_pass_mask & PASS_JP_MATCH_CUT) events_after_jp_match_cut.fill(weight);
    if (event_pass_mask & PASS_PT_CUT) events_after_pt_cut.fill(weight);
    if (event_pass_mask & PASS_TOWER_3407_CUT) events_after_tower_3407_cut.fill(weight);
    if (event_pass_mask & PASS_MAX_TRACK_PT_CUT) events_after_max_track_pt_cut.fill(weight);
    if (event_pass_mask & PASS_VERTEX_CUT) events_after_vertex_cut.fill(weight);
    if (event_pass_mask & PASS_RT_CUT) events_after_rt_cut.fill(weight);

    if (flag) {
       vx.fill(primary_vertex->position().x(), weight);
       vy.fill(primary_vertex->position().y(), weight);
       vz.fill(primary_vertex->position().z(), weight);

       num_vertex_vs_vz.fill(primary_vertex->position().z(), e.jetEvent.numberOfVertices(), weight);
       num_jets_vs_vz.fill(primary_vertex->position().z(), e.jetEvent.numberOfJets(), weight);

       vpd_vz.fill(e.skimEvent.vpdZvertex(), weight);
    }
  }

private:

  bool jet_cut_rt(StJetCandidate *jet) {
    return jet->rt() <= 0.95;
  }

};
