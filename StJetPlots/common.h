// vim:et sw=2 sts=2
#pragma once

#include <iostream>

#include <TRefArray.h>
#include <TVector3.h>

#include "../src/common/always_assert.h"

namespace {

const int TRIG_ZEROBIAS = 9300;

const int TRIG_RUN12_PP200_VPDMB = 370001;
const int TRIG_RUN12_PP200_VPDMB_nobsmd = 370011;
const int TRIG_RUN12_PP200_JP0 = 370601;
const int TRIG_RUN12_PP200_JP1 = 370611;
const int TRIG_RUN12_PP200_JP2 = 370621;

const int TRIG_RUN12_PP500_VPDMB = 380001;
const int TRIG_RUN12_PP500_VPDMB_nobsmd = 380002;
const int TRIG_RUN12_PP500_JP0 = 380401;
const int TRIG_RUN12_PP500_JP1 = 380402;
const int TRIG_RUN12_PP500_JP2 = 380403;

template <typename T>
T sqr(T x) { return x * x; }

template<typename T>
std::vector<T*> to_vec(const TRefArray &a)
{
  std::vector<T*> result;
  TIter it(a.MakeIterator());
  std::transform(it.Begin(), TIter::End(), std::back_inserter(result),
      [](TObject *obj) { return dynamic_cast<T*>(obj); });
  always_assert(result.size() == a.GetEntriesFast());
  return result;
}

double correct_to_physics_eta(const TVector3 &momentum, const TVector3 &vertex)
{
  // Front plate of BEMC or BPRS layer
  // See StEmcGeom/geometry/StEmcGeom.cxx
  const double BEMC_RADIUS = 225.405; // cm
  // EEMC preshower1 layer
  // See StEEmcUtil/EEmcGeom/EEmcGeomDefs.h
  const double EEMC_Z = 270.190; // cm
  TVector3 pos = momentum;
  pos.SetMag(BEMC_RADIUS / pos.Unit().Perp());
  always_assert(fabs(pos.Z()) < EEMC_Z);
  pos -= vertex;
  return pos.Eta();
}

unsigned int get_ue_track_mult(const StUeOffAxisConesJet *uejet) {
  return uejet->cone(0)->numberOfTracks() + uejet->cone(1)->numberOfTracks();
}

unsigned int get_ue_tower_mult(const StUeOffAxisConesJet *uejet) {
  unsigned int detector_ue_tower_mult = 0;
  for(int cone_id = 0; cone_id <= 1; cone_id++) {
    for(const TObject *_p : uejet->cone(cone_id)->towers()) {
      const StJetTower *p = dynamic_cast<const StJetTower*>(_p);
      always_assert(p);
      if (p->pt() > 0.001) { // don't count subtracted towers
        detector_ue_tower_mult++;
      }
    }
  }
  return detector_ue_tower_mult;
}

double get_ue_track_sumpt(const StUeOffAxisConesJet *uejet) {
  double detector_ue_track_pt = 0.;
  for(int cone_id = 0; cone_id <= 1; cone_id++) {
    for(const TObject *_p : uejet->cone(cone_id)->tracks()) {
      const StJetTrack *p = dynamic_cast<const StJetTrack*>(_p);
      always_assert(p);
      detector_ue_track_pt += p->pt();
    }
  }
  return detector_ue_track_pt;
}

double get_ue_tower_sumpt(const StUeOffAxisConesJet *uejet) {
  double detector_ue_tower_pt = 0.;
  for(int cone_id = 0; cone_id <= 1; cone_id++) {
    for(const TObject *_p : uejet->cone(cone_id)->towers()) {
      const StJetTower *p = dynamic_cast<const StJetTower*>(_p);
      always_assert(p);
      detector_ue_tower_pt += p->pt();
    }
  }
  return detector_ue_tower_pt;
}

template<typename T>
bool in_sector20(T &obj) {
#define _deg2rad(x) ((x) * M_PI / 180)
  return (_deg2rad(-45) < (obj).phi()) && ((obj).phi() < _deg2rad(-15)) && ((obj).eta() < 0);
#undef _deg2rad
}

void fill_hstack1d(std::map<std::pair<double, double>, Histo1D> &hstack, double cut_value, double xvalue, double weight) {
  for (auto &p : hstack) {
    const std::pair<double, double> &cut = p.first;
    Histo1D &h = p.second;
    double cut_min = cut.first;
    double cut_max = cut.second;
    if ((cut_value >= cut_min) && (cut_value < cut_max)) {
      h.fill(xvalue, weight);
    }
  }
}


}
