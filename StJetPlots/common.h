// vim:et sw=2 sts=2
#pragma once

#include <iostream>

#include <TRefArray.h>
#include <TVector3.h>

namespace {

const int TRIG_RUN12_PP200_VPDMB = 370001;
const int TRIG_RUN12_PP200_VPDMB_nobsmd = 370011;
const int TRIG_RUN12_PP200_JP0 = 370601;
const int TRIG_RUN12_PP200_JP1 = 370611;
const int TRIG_RUN12_PP200_JP2 = 370621;

const int TRIG_RUN12_PP500_VPDMB = 380001;
const int TRIG_RUN12_PP500_VPDMB_nobsmd = 380002;
const int TRIG_RUN12_PP500_JP0 = 380401;
const int TRIG_RUN12_PP500_JP1 = 380402;
const int TRIG_RUN12_PP500_JP2 = 380403;

void print_assert(const char* exp, const char* file, int line)
{
  std::cerr << "Assertion '" << exp << "' failed in '" << file << "' at line '" << line << "'" << std::endl;
  std::terminate();
}

#define myassert(exp) ((exp) ? (void)0 : print_assert(#exp, __FILE__, __LINE__))

template<typename T>
std::vector<T*> to_vec(const TRefArray &a)
{
  std::vector<T*> result;
  TIter it(a.MakeIterator());
  std::transform(it.Begin(), TIter::End(), std::back_inserter(result),
      [](TObject *obj) { return dynamic_cast<T*>(obj); });
  myassert(result.size() == a.GetEntriesFast());
  return result;
}

double correct_to_physics_eta(const TVector3 &momentum, const TVector3 &vertex)
{
  // Front plate of BEMC or BPRS layer
  // See StEmcGeom/geometry/StEmcGeom.cxx
  const double BEMC_RADIUS = 225.405; // cm
  // EEMC preshower1 layer
  // See StEEmcUtil/EEmcGeom/EEmcGeomDefs.h
  const double EEMC_Z = 270.190; // cm
  TVector3 pos = momentum;
  pos.SetMag(BEMC_RADIUS / pos.Unit().Perp());
  myassert(fabs(pos.Z()) < EEMC_Z);
  pos -= vertex;
  return pos.Eta();
}

unsigned int get_ue_track_mult(const StUeOffAxisConesJet *uejet) {
  return uejet->cone(0)->numberOfTracks() + uejet->cone(1)->numberOfTracks();
}

unsigned int get_ue_tower_mult(const StUeOffAxisConesJet *uejet) {
  unsigned int detector_ue_tower_mult = 0;
  for(int cone_id = 0; cone_id <= 1; cone_id++) {
    for(const TObject *_p : uejet->cone(cone_id)->towers()) {
      const StJetTower *p = dynamic_cast<const StJetTower*>(_p);
      myassert(p);
      if (p->pt() > 0.001) { // don't count subtracted towers
        detector_ue_tower_mult++;
      }
    }
  }
  return detector_ue_tower_mult;
}

double get_ue_track_sumpt(const StUeOffAxisConesJet *uejet) {
  double detector_ue_track_pt = 0.;
  for(int cone_id = 0; cone_id <= 1; cone_id++) {
    for(const TObject *_p : uejet->cone(cone_id)->tracks()) {
      const StJetTrack *p = dynamic_cast<const StJetTrack*>(_p);
      myassert(p);
      detector_ue_track_pt += p->pt();
    }
  }
  return detector_ue_track_pt;
}

double get_ue_tower_sumpt(const StUeOffAxisConesJet *uejet) {
  double detector_ue_tower_pt = 0.;
  for(int cone_id = 0; cone_id <= 1; cone_id++) {
    for(const TObject *_p : uejet->cone(cone_id)->towers()) {
      const StJetTower *p = dynamic_cast<const StJetTower*>(_p);
      myassert(p);
      detector_ue_tower_pt += p->pt();
    }
  }
  return detector_ue_tower_pt;
}

}
