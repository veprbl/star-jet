// vim:et sw=2 sts=2
#pragma once

#include "Analysis.h"
#include "StJetPlotEvent.h"

class StAnaPartonicSpectrum : public Analysis<StJetPlotEvent>
{
public:

  template <typename ...Args>
  StAnaPartonicSpectrum(Args ...args)
    : Analysis<StJetPlotEvent>(args...)
    , partonic_pt(200, 0., 40., "partonic_pt", "Partonic $p_T$ spectrum")
  {
    add(partonic_pt);
  }

  void fill(const StJetPlotEvent& e, double weight) override
  {
    if (e.is_embedding()) {
      partonic_pt.fill(e.skimEvent.mcEvent()->pt(), weight);
    }
  }

  Histo1D partonic_pt;
};
