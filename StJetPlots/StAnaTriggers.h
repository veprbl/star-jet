// vim:et sw=2 sts=2

#include <TH2D.h>

#include "common.h"
#include "Analysis.h"

class StAnaTriggers : public Analysis<StJetPlotEvent>
{
public:

  template <typename ...Args>
  StAnaTriggers(Args ...args)
    : Analysis<StJetPlotEvent>(args...)
    , trig_coincedence(4, 0., 4., 4, 0., 4., "trig_coincedence", "Trigger coincedence")
  {
    add(trig_coincedence);
  }

  Histo2D trig_coincedence;

  std::map<int, int> bin_map{
    {TRIG_RUN12_PP200_JP0, 0}, {TRIG_RUN12_PP200_JP1, 1}, {TRIG_RUN12_PP200_JP2, 2}, {TRIG_RUN12_PP200_VPDMB_nobsmd, 3}, {TRIG_RUN12_PP200_VPDMB, 4},
    {TRIG_RUN12_PP500_JP0, 0}, {TRIG_RUN12_PP500_JP1, 1}, {TRIG_RUN12_PP500_JP2, 2}, {TRIG_RUN12_PP500_VPDMB_nobsmd, 3}, {TRIG_RUN12_PP500_VPDMB, 4}
  };

  void fill(const StJetPlotEvent& e, double weight) override
  {
    for(int trig1 : e.fired_triggers)
    for(int trig2 : e.fired_triggers)
    {
      myassert(bin_map.count(trig1) && bin_map.count(trig2));
      if (bin_map.count(trig1) && bin_map.count(trig2)) {
        trig_coincedence.fill(bin_map[trig1], bin_map[trig2], weight);
      }
    }
  }
};
