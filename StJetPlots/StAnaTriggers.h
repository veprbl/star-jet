// vim:et sw=2 sts=2
#pragma once

#include <sstream>
#include <map>

#include <TH2D.h>

#include "common.h"
#include "Analysis.h"

/**
 * \brief A simple analysis to show a correlation between fired triggers
 */
class StAnaTriggers : public Analysis<StJetPlotEvent>
{
public:

  template <typename ...Args>
  StAnaTriggers(Args ...args)
    : Analysis<StJetPlotEvent>(args...)
    , trig_coincedence(4, 0., 4., 4, 0., 4., "trig_coincedence", "Trigger coincedence")
    , bemc_patch_id_above_jp0_threshold(18, 0., 18., "bemc_patch_id_above_jp0_threshold")
    , bemc_patch_id_above_jp1_threshold(18, 0., 18., "bemc_patch_id_above_jp1_threshold")
    , bemc_patch_id_above_jp2_threshold(18, 0., 18., "bemc_patch_id_above_jp2_threshold")
  {
    add(trig_coincedence);
    add(bemc_patch_id_above_jp0_threshold);
    add(bemc_patch_id_above_jp1_threshold);
    add(bemc_patch_id_above_jp2_threshold);
  }

  Histo2D trig_coincedence;
  Histo1D bemc_patch_id_above_jp0_threshold;
  Histo1D bemc_patch_id_above_jp1_threshold;
  Histo1D bemc_patch_id_above_jp2_threshold;

  std::map<int, int> bin_map{
    {TRIG_RUN12_PP200_JP0, 0}, {TRIG_RUN12_PP200_JP1, 1}, {TRIG_RUN12_PP200_JP2, 2}, {TRIG_RUN12_PP200_VPDMB_nobsmd, 3}, {TRIG_RUN12_PP200_VPDMB, 4},
    {TRIG_RUN12_PP500_JP0, 0}, {TRIG_RUN12_PP500_JP1, 1}, {TRIG_RUN12_PP500_JP2, 2}, {TRIG_RUN12_PP500_VPDMB_nobsmd, 3}, {TRIG_RUN12_PP500_VPDMB, 4}
  };

  std::map<int, Counter> trig_fired;

  void fill_jp_plot(const StJetPlotEvent& e, double weight, int jp, Histo1D *h1)
  {
    map<int,int> patches = e.skimEvent.barrelJetPatchesAboveTh(jp);

    for (auto p : patches) {
      int id = p.first;
      int adc __attribute__((unused)) = p.second;
      always_assert(id >= 0);
      always_assert(id < 18);
      h1->fill(id, weight);
    }
  }

  void fill(const StJetPlotEvent& e, double weight) override
  {
    for(int trig1 : e.triggers)
    for(int trig2 : e.triggers)
    {
      if (bin_map.count(trig1) && bin_map.count(trig2)) {
        trig_coincedence.fill(bin_map[trig1], bin_map[trig2], weight);
      }
    }

    for(int trig : e.did_fire_triggers)
    {
      trig_fired[trig].fill(weight);
    }

    fill_jp_plot(e, weight, 0, &bemc_patch_id_above_jp0_threshold);
    fill_jp_plot(e, weight, 1, &bemc_patch_id_above_jp1_threshold);
    fill_jp_plot(e, weight, 2, &bemc_patch_id_above_jp2_threshold);
  }

  void finalize() override
  {
    for(auto &p : trig_fired)
    {
      int trig = p.first;
      Counter &c = p.second;
      std::ostringstream path_ss;
      path_ss << "trig_fired_" << trig;
      c.setPath(path_ss.str());
      add(c);
    }
  }
};
