// vim:et sw=2 sts=2
#pragma once

#include <functional>

#include "StJetPlotEvent.h"

typedef uint32_t PassCutMask;

const PassCutMask PASS_EMPTY = 0;
const PassCutMask PASS_DET_ETA_CUT      = 1 << 0;
const PassCutMask PASS_PHYS_ETA_CUT     = 1 << 1;
const PassCutMask PASS_JP_MATCH_CUT     = 1 << 2;
const PassCutMask PASS_PT_CUT           = 1 << 3;
const PassCutMask PASS_TOWER_3407_CUT   = 1 << 4;
const PassCutMask PASS_MAX_TRACK_PT_CUT = 1 << 5;
const PassCutMask PASS_VERTEX_CUT       = 1 << 6;
const PassCutMask PASS_RT_CUT           = 1 << 7;

class CutResult {
public:

  bool success;
  PassCutMask pass_mask;

  CutResult(bool _success)
    : success(_success)
    , pass_mask(PASS_EMPTY)
  { }

  CutResult(bool _success, PassCutMask _pass_mask)
    : success(_success)
    , pass_mask(_pass_mask)
  { }

  operator bool() const {
    return success;
  }
};

CutResult operator&&(const CutResult &r1, const CutResult &r2) {
  return CutResult { r1.success && r2.success, r1.pass_mask | r2.pass_mask };
}

typedef std::function<CutResult(const StJetPlotEvent &e, const StJetCandidate &jet)> StJetCut;
StJetCut operator&&(const StJetCut &c1, const StJetCut &c2) {
  return [c1, c2] (const StJetPlotEvent &e, const StJetCandidate &jet) {
    CutResult r1 = c1(e, jet);
    if (!r1) return r1; // lazy evalutaion
    CutResult r2 = c2(e, jet);
    return r1 && r2;
  };
}

StJetCut no_cut = [] (const StJetPlotEvent &e, const StJetCandidate &jet) {
  return true;
};

StJetCut blacklist_tower_3407_cut = [] (const StJetPlotEvent &e, const StJetCandidate &jet) {
  for (TObject *_tower: jet.towers()) {
    StJetTower *tower = dynamic_cast<StJetTower*>(_tower);
    if (tower->id() == 3407) return CutResult { false };
  }
  return CutResult { true, PASS_TOWER_3407_CUT };
};

bool jp_match(const StJetPlotEvent& e, int jp, const StJetCandidate &jet) {
  myassert((jp >= 0) && (jp <= 2));
  map<int,int> patches = e.skimEvent.barrelJetPatchesAboveTh(jp);

  for (auto p : patches) {
    int id = p.first;
    int adc __attribute__((unused)) = p.second;
    float eta, phi;
    myassert(StJetCandidate::getBarrelJetPatchEtaPhi(id, eta, phi));
    double deta = jet.detEta() - eta;
    double dphi = TVector2::Phi_mpi_pi(jet.phi() - phi);
    if ((fabs(deta) < 0.6) && (fabs(dphi) < 0.6)) {
      return true;
    }
  }

  return false;
}

StJetCut primary_vertex_cut = [] (const StJetPlotEvent &e, const StJetCandidate &jet) {
  StJetVertex *primary_vertex = e.jetEvent.vertex();
  if (!(primary_vertex->ranking() > 0)) return false;
  if (fabs(primary_vertex->position().z()) >= Z_VERTEX_CUT) return false;
  return true;
};

/// This cut is supposed to simulate VPDMB trigger requirement for the vertex.
/// That requirement is effectively bigger than 30 cm due to VPD vertex
/// smearing, so when we impose 30 cm cut here we are putting a stricter
/// condition on VPD v_z.
StJetCut vpdmb_vertex_cut = [] (const StJetPlotEvent &e, const StJetCandidate &jet) {
  StJetVertex *primary_vertex = e.jetEvent.vertex();
  double vz = primary_vertex->position().z();
  double vpdvz = e.skimEvent.vpdZvertex();
  if (!e.is_embedding()) {
    if (fabs(vz - vpdvz) > 5.0) return false;
  }
  return fabs(vz) < 30;
};

StJetCut make_detector_level_cut(int jp = -1) {
  return [jp] (const StJetPlotEvent &e, const StJetCandidate &jet) {
    CutResult res = { false, PASS_EMPTY };
    if (fabs(jet.detEta()) > 0.8) return res;
    res.pass_mask |= PASS_DET_ETA_CUT;
    if (fabs(jet.eta()) > 0.8) return res;
    res.pass_mask |= PASS_PHYS_ETA_CUT;
    if ((jp >= 0) && (!jp_match(e, jp, jet))) return res;
    res.pass_mask |= PASS_JP_MATCH_CUT;
    switch (jp) {
      case -1:
      case 0:
        break;
      case 1:
        if (jet.pt() < 6. /* GeV */) return res;
        break;
      case 2:
        if (jet.pt() < 8.4 /* GeV */) return res;
        break;
      default:
        throw "Invalid _jp";
    }
    res.pass_mask |= PASS_PT_CUT;
    res.success = true;
    return res;
  };
}

StJetCut detector_level_cut = make_detector_level_cut();
StJetCut detector_level_cut_jp0 = make_detector_level_cut(0);
StJetCut detector_level_cut_jp1 = make_detector_level_cut(1);
StJetCut detector_level_cut_jp2 = make_detector_level_cut(2);

StJetCut make_trigger_simulator_cut(const std::set<int> &triggers) {
  myassert(triggers.size());
  return [triggers] (const StJetPlotEvent &e, const StJetCandidate &jet) {
    for(int trig_id : triggers) {
      if (e.simulated_triggers.count(trig_id)) return true;
    }
    return false;
  };
}

StJetCut make_trigger_cut(const std::set<int> &triggers) {
  myassert(triggers.size());
  return [triggers] (const StJetPlotEvent &e, const StJetCandidate &jet) {
    for(int trig_id : triggers) {
      if (e.fired_triggers.count(trig_id)) return true;
    }
    return false;
  };
}

StJetCut jet_rt_cut = [] (const StJetPlotEvent &e, const StJetCandidate &jet) {
    return jet.rt() <= 0.95;
};

StJetCut particle_level_cut = [] (const StJetPlotEvent &e, const StJetCandidate &jet) {
  if (fabs(jet.eta()) > 0.8) return false;
  return true;
};

StJetCut make_max_track_pt_cut(double max_track_pt) {
  return [max_track_pt] (const StJetPlotEvent &e, const StJetCandidate &jet) {
    for (TObject *_track : jet.tracks()) {
      StJetTrack *track = static_cast<StJetTrack*>(_track);
      if (track->pt() > max_track_pt) {
        return CutResult { false };
      }
    }
    return CutResult { true, PASS_MAX_TRACK_PT_CUT };
  };
}
