// vim:et sw=2 sts=2
#pragma once

/**
 * \file StJetCut.h
 * \brief Contains machinery to do cut definitions and account events that pass those cuts
 */

#include <functional>

#include <TVector2.h>

#include "../src/common/jp_match.h"

#include "StJetPlotEvent.h"

typedef uint32_t PassCutMask;

const PassCutMask PASS_EMPTY = 0;
const PassCutMask PASS_DET_ETA_CUT      = 1 << 0;
const PassCutMask PASS_PHYS_ETA_CUT     = 1 << 1;
const PassCutMask PASS_JP_MATCH_CUT     = 1 << 2;
const PassCutMask PASS_PT_CUT           = 1 << 3;
const PassCutMask PASS_TOWER_3407_CUT   = 1 << 4;
const PassCutMask PASS_MAX_TRACK_PT_CUT = 1 << 5;
const PassCutMask PASS_VERTEX_CUT       = 1 << 6;
const PassCutMask PASS_RT_CUT           = 1 << 7;

class CutResult {
public:

  bool success;
  PassCutMask pass_mask;

  CutResult(bool _success)
    : success(_success)
    , pass_mask(PASS_EMPTY)
  { }

  CutResult(bool _success, PassCutMask _pass_mask)
    : success(_success)
    , pass_mask(_pass_mask)
  { }

  operator bool() const {
    return success;
  }
};

CutResult operator!(const CutResult &r) {
  // pass_mask is currently not implemented for negated cuts
  return CutResult { !r.success, 0 };
}

CutResult operator&&(const CutResult &r1, const CutResult &r2) {
  return CutResult { r1.success && r2.success, r1.pass_mask | r2.pass_mask };
}

CutResult operator||(const CutResult &r1, const CutResult &r2) {
  return CutResult { r1.success || r2.success, r1.pass_mask | r2.pass_mask };
}

typedef std::function<CutResult(const StJetPlotEvent &e, const StJetCandidate *jet)> StJetCut;

StJetCut operator!(const StJetCut &c) {
  return [c] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    CutResult r = c(e, jet);
    return !r;
  };
}

StJetCut operator&&(const StJetCut &c1, const StJetCut &c2) {
  return [c1, c2] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    CutResult r1 = c1(e, jet);
    if (!r1) return r1; // lazy evalutaion
    CutResult r2 = c2(e, jet);
    return r1 && r2;
  };
}

StJetCut operator||(const StJetCut &c1, const StJetCut &c2) {
  return [c1, c2] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    CutResult r1 = c1(e, jet);
    if (r1) return r1; // lazy evalutaion
    CutResult r2 = c2(e, jet);
    return r1 || r2;
  };
}

StJetCut cut_from_bool(bool flag) {
  return [flag] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    return flag;
  };
}

StJetCut no_cut = cut_from_bool(true);

StJetCut is_embedding_cut = [] (const StJetPlotEvent &e, const StJetCandidate *jet) {
  return e.is_embedding();
};

StJetCut blacklist_tower_3407_cut = [] (const StJetPlotEvent &e, const StJetCandidate *jet) {
  if (!jet) return CutResult { true };
  for (TObject *_tower: jet->towers()) {
    StJetTower *tower = dynamic_cast<StJetTower*>(_tower);
    if (tower->id() == 3407) return CutResult { false };
  }
  return CutResult { true, PASS_TOWER_3407_CUT };
};

StJetCut make_primary_vertex_cut(double min_vertex_z_cut, double max_vertex_z_cut) {
  return [min_vertex_z_cut, max_vertex_z_cut] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    StJetVertex *primary_vertex = e.jetEvent.vertex();
    if (!(primary_vertex->ranking() > 0)) return false;
    if (primary_vertex->position().z() <= min_vertex_z_cut) return false;
    if (primary_vertex->position().z() >= max_vertex_z_cut) return false;
    return true;
  };
}

StJetCut two_good_vertices_cut = [] (const StJetPlotEvent &e, const StJetCandidate *jet) {
  return e.jetEvent.vertex(1)
      && (e.jetEvent.vertex(1)->ranking() > 0);
};

StJetCut make_tpcvz_simuvz_diff_max_cut(double max_dist_cut) {
  return [max_dist_cut] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    double z_tpc = e.jetEvent.vertex()->position().z();
    double z_simu = e.jetParticleEvent.vertex()->position().z();
    return fabs(z_tpc - z_simu) < max_dist_cut;
  };
}

/// This cut is supposed to simulate VPDMB trigger requirement for the vertex.
/// That requirement is effectively bigger than 30 cm due to VPD vertex
/// smearing, so when we impose 30 cm cut here we are putting a stricter
/// condition on VPD v_z.
StJetCut vpdmb_vertex_cut = [] (const StJetPlotEvent &e, const StJetCandidate *jet) {
  StJetVertex *primary_vertex = e.jetEvent.vertex();
  double vz = primary_vertex->position().z();
  double vpdvz = e.skimEvent.vpdZvertex();
  if (!e.is_embedding()) {
    if (fabs(vz - vpdvz) > 5.0) return false;
  }
  return fabs(vz) < 30;
};

StJetCut vpd_vertex_found_cut = [] (const StJetPlotEvent &e, const StJetCandidate *jet) {
  double vpdvz = e.skimEvent.vpdZvertex();
  return vpdvz != -999;
};

StJetCut vpd_vertex_not_found_cut = [] (const StJetPlotEvent &e, const StJetCandidate *jet) {
  double vpdvz = e.skimEvent.vpdZvertex();
  return vpdvz == -999;
};

StJetCut make_vpdvz_vz_diff_min_cut(double vpdvz_vz_diff_min) {
  return [vpdvz_vz_diff_min] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    StJetVertex *primary_vertex = e.jetEvent.vertex();
    double vz = primary_vertex->position().z();
    double vpdvz = e.skimEvent.vpdZvertex();
    if (vpdvz == -999) {
      return false;
    }
    return fabs(vz - vpdvz) > vpdvz_vz_diff_min;
  };
}

StJetCut make_vpdvz_vz_diff_max_cut(double vpdvz_vz_diff_max) {
  return [vpdvz_vz_diff_max] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    StJetVertex *primary_vertex = e.jetEvent.vertex();
    double vz = primary_vertex->position().z();
    double vpdvz = e.skimEvent.vpdZvertex();
    if (vpdvz == -999) {
      return false;
    }
    return fabs(vz - vpdvz) < vpdvz_vz_diff_max;
  };
}

StJetCut make_detector_level_cut(int jp = -1, float abseta_max = 0.8) {
  return [abseta_max, jp] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    if (!jet) return CutResult { true };
    CutResult res = { false, PASS_EMPTY };
    if (fabs(jet->detEta()) > abseta_max) return res;
    res.pass_mask |= PASS_DET_ETA_CUT;
    if (fabs(jet->eta()) > abseta_max) return res;
    res.pass_mask |= PASS_PHYS_ETA_CUT;
    if ((jp >= 0) && (!jp_match(e.skimEvent, jp, *jet))) return res;
    res.pass_mask |= PASS_JP_MATCH_CUT;
    switch (jp) {
      case -1:
      case 0:
        break;
      case 1:
        if (jet->pt() < 6. /* GeV */) return res;
        break;
      case 2:
        if (jet->pt() < 8.4 /* GeV */) return res;
        break;
      default:
        throw "Invalid _jp";
    }
    res.pass_mask |= PASS_PT_CUT;
    res.success = true;
    return res;
  };
}

StJetCut make_trigger_did_fire_cut(const std::set<int> &triggers) {
  always_assert(triggers.size());
  return [triggers] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    for(int trig_id : triggers) {
      if (e.did_fire_triggers.count(trig_id)) return true;
    }
    return false;
  };
}

StJetCut make_trigger_should_fire_cut(const std::set<int> &triggers) {
  always_assert(triggers.size());
  return [triggers] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    for(int trig_id : triggers) {
      if (e.should_fire_triggers.count(trig_id)) return true;
    }
    return false;
  };
}

StJetCut make_trigger_cut(const std::set<int> &triggers) {
  always_assert(triggers.size());
  return [triggers] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    for(int trig_id : triggers) {
      if (e.triggers.count(trig_id)) return true;
    }
    return false;
  };
}

StJetCut jet_rt_cut = [] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    if (!jet) return true;
    return jet->rt() <= 0.95;
};

StJetCut make_jet_phi_cut(double phi_min, double phi_max) {
  double delta_phi_over_2 = TVector2::Phi_0_2pi(phi_max - phi_min) / 2;
  double phi_mid = phi_min + delta_phi_over_2;
  return [phi_mid, delta_phi_over_2] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    if (!jet) return true;
    double phi_prime = TVector2::Phi_mpi_pi(jet->phi() - phi_mid); // offset to midpoint
    return (phi_prime >= -delta_phi_over_2) && (phi_prime < delta_phi_over_2);
  };
};

StJetCut make_jet_abseta_cut(double abseta_min, double abseta_max) {
  return [abseta_min, abseta_max] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    if (!jet) return true;
    double abseta = fabs(jet->eta());
    return (abseta >= abseta_min) && (abseta < abseta_max);
  };
};

StJetCut make_max_track_pt_cut(double max_track_pt) {
  return [max_track_pt] (const StJetPlotEvent &e, const StJetCandidate *jet) {
    if (!jet) return CutResult { true };
    for (TObject *_track : jet->tracks()) {
      StJetTrack *track = static_cast<StJetTrack*>(_track);
      if (track->pt() > max_track_pt) {
        return CutResult { false };
      }
    }
    return CutResult { true, PASS_MAX_TRACK_PT_CUT };
  };
}

StJetCut sector20_track_cut = [] (const StJetPlotEvent &e, const StJetCandidate *jet) {
  if (!jet) return CutResult { true };
  for (TObject *_track : jet->tracks()) {
    StJetTrack *track = static_cast<StJetTrack*>(_track);
    if (in_sector20(*track)) {
      return CutResult { false };
    }
  }
  return CutResult { true };
};
