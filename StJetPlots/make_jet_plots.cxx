// vim:et sw=2 sts=2

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <set>
#include <map>

#include <TFile.h> //FIXME
#include <TChain.h>
#include <TROOT.h>

#include <StSpinPool/StJetEvent/StJetEvent.h>
#include <StSpinPool/StJetSkimEvent/StJetSkimEvent.h>
#include <StSpinPool/StUeEvent/StUeOffAxisConesEvent.h>

#include "StAnaRoot.h"
#include "common.h"

using std::string;

const string JETS_EXT = ".jets.root";
const string SKIM_EXT = ".skim.root";
const string CONE_UE_EXT = ".ueCones.root";
const string REGION_UE_EXT = ".ueRegions.root";

const std::set<int> all_trig{
  TRIG_RUN12_PP200_VPDMB, TRIG_RUN12_PP200_VPDMB_nobsmd, TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP200_JP2,
  TRIG_RUN12_PP500_VPDMB, TRIG_RUN12_PP500_VPDMB_nobsmd, TRIG_RUN12_PP500_JP0, TRIG_RUN12_PP500_JP1, TRIG_RUN12_PP500_JP2
};

string replace_filename_prefix(const string& jetfile, const string &new_prefix)
{
  auto last = jetfile.size() - JETS_EXT.size();
  myassert(jetfile.substr(last) == JETS_EXT);
  return jetfile.substr(0, last) + new_prefix;
}

const char* safe_str(const char* s)
{
  return s ? s : "";
};

const char* default_str(const char* s, const char* def)
{
  return s ? s : def;
};

int main(int argc, char **argv)
{
  string img_format = "png";
  const bool do_soft_reweight = !atoi(safe_str(getenv("no_soft_reweight")));

  if (argc < 2) {
    std::cerr << "Usage:" << std::endl << argv[0] << " file1.jets.root [file2.jets.root ...]" << std::endl;
    return EXIT_FAILURE;
  }

  gROOT->Macro("./rootlogon.C");

  StJetEvent *jetEvent = 0;
  StJetEvent *jetParticleEvent = 0;
  StJetSkimEvent *skimEvent = 0;
  StUeOffAxisConesEvent *ueEvent = 0;
  StUeOffAxisConesEvent *ueParticleEvent = 0;
  StUeEvent* ueEvent_transP = 0;
  StUeEvent* ueEvent_transM = 0;
  StUeEvent* ueEvent_away = 0;
  StUeEvent* ueEvent_toward = 0;

  TFile file("output.root", "RECREATE");
  StAnaRoot analysis("");

  for(int i = 1; i < argc; i++) {
    string arg = argv[i];
    if (arg == "--tex") {
      img_format = "tex";
      continue;
    }
    string jetfile = arg;
    string skimfile = replace_filename_prefix(jetfile, SKIM_EXT);
    string coneuefile = replace_filename_prefix(jetfile, CONE_UE_EXT);
    string regionuefile = replace_filename_prefix(jetfile, REGION_UE_EXT);

    TChain jetChain("jet");
    TChain skimChain("jetSkimTree");
    TChain coneueChain("ue");
    TChain regionueChain("ue");

    jetChain.Add(jetfile.c_str());
    skimChain.Add(skimfile.c_str());
    coneueChain.Add(coneuefile.c_str());
    //regionueChain.Add(regionuefile.c_str());

    const char *detector_jet_branch_name = default_str(getenv("detector_jet_branch_name"), "AntiKtR060NHits12");
    jetChain.SetBranchAddress(detector_jet_branch_name, &jetEvent);
    const bool is_embedding = jetChain.GetBranch("AntiKtR060Particle");
    if (is_embedding) {
      jetChain.SetBranchAddress("AntiKtR060Particle", &jetParticleEvent);
      coneueChain.SetBranchAddress("AntiKtR060ParticleOffAxisConesR050", &ueParticleEvent);
    } else {
      jetParticleEvent = new StJetEvent;
      ueParticleEvent = new StUeOffAxisConesEvent;
    }
    skimChain.SetBranchAddress("skimEventBranch", &skimEvent);
    const char *detector_ue_branch_name = default_str(getenv("detector_ue_branch_name"), "AntiKtR060NHits12OffAxisConesR050");
    coneueChain.SetBranchAddress(detector_ue_branch_name, &ueEvent);
    //regionueChain.SetBranchAddress("transP_AntiKtR060NHits12", &ueEvent_transP);
    //regionueChain.SetBranchAddress("transM_AntiKtR060NHits12", &ueEvent_transM);
    //regionueChain.SetBranchAddress("away_AntiKtR060NHits12", &ueEvent_away);
    //regionueChain.SetBranchAddress("toward_AntiKtR060NHits12", &ueEvent_toward);

    Long_t nentries = jetChain.GetEntries();
    myassert(nentries == skimChain.GetEntries());
    myassert(nentries == coneueChain.GetEntries());
    //myassert(nentries == regionueChain.GetEntries());

    if (nentries == 0) {
      std::cerr << "No entries in the input chain " << jetfile << std::endl;
      return EXIT_FAILURE;
    }

    for(Long_t entry = 0; entry < nentries; entry++) {
      jetChain.GetEvent(entry);
      skimChain.GetEvent(entry);
      coneueChain.GetEvent(entry);
      //regionueChain.GetEvent(entry);

      myassert(jetEvent->runId() == skimEvent->runId());
      myassert(jetEvent->eventId() == skimEvent->eventId());
      //myassert(jetEvent->eventId() == ueEvent->eventId());
      //myassert(jetEvent->eventId() == ueEvent_transP->eventId());
      //myassert(jetEvent->eventId() == ueEvent_transM->eventId());
      //myassert(jetEvent->eventId() == ueEvent_away->eventId());
      //myassert(jetEvent->eventId() == ueEvent_toward->eventId());

      if ((is_embedding && (jetEvent->runId() == 13059026) && (jetEvent->eventId() == 337))
       || (is_embedding && (jetEvent->runId() == 13051021) && (jetEvent->eventId() == 193))
       || (is_embedding && (jetEvent->runId() == 13051087) && (jetEvent->eventId() == 151))
       || (is_embedding && (jetEvent->runId() == 13065021) && (jetEvent->eventId() == 43))
       ) {
        std::cerr << "blacklisted event id "
          << jetEvent->eventId()
          << " encountered in run "
          << jetEvent->runId()
          << " - skipping"
          << std::endl;
        continue;
      }

      myassert(is_embedding == static_cast<bool>(skimEvent->mcEvent()));

      double extra_weight = 1.;
      if (is_embedding) {
        double p[] = {
          1.21733561, -0.32633577, 0.16913723, 0.82134143
        };
        double partonic_pt = skimEvent->mcEvent()->pt();
        if (do_soft_reweight) {
          extra_weight = 1./(1. + (p[0] + p[1] * (partonic_pt - 2.) + p[2] * (partonic_pt - 2.) * (partonic_pt - 2.)) * exp(-p[3] * (partonic_pt - 2.)));
        }
      }
      std::set<int> fired_trig{};
      std::set<int> simulated_trig{};
      for (int trig_id : all_trig) {
        StJetSkimTrig *trig = skimEvent->trigger(trig_id);
        if (!trig) continue;
        if (trig->shouldFire()) {
          simulated_trig.insert(trig_id);
        }
        if ((is_embedding || trig->didFire())
            && (
              (trig_id == TRIG_RUN12_PP200_VPDMB) || (trig_id == TRIG_RUN12_PP200_VPDMB_nobsmd) ||
              (trig_id == TRIG_RUN12_PP500_VPDMB) || (trig_id == TRIG_RUN12_PP500_VPDMB_nobsmd) ||
              trig->shouldFire()))
        {
          fired_trig.insert(trig_id);
        }
      }
      if (is_embedding) {
        fired_trig.insert(TRIG_RUN12_PP200_VPDMB_nobsmd);
        fired_trig.insert(TRIG_RUN12_PP500_VPDMB_nobsmd);
      }

      StJetPlotEvent e(*jetEvent, *jetParticleEvent, *skimEvent, *ueEvent, *ueParticleEvent, ueEvent_transP, ueEvent_transM, ueEvent_away, ueEvent_toward, simulated_trig, fired_trig, jetChain, extra_weight);
      analysis.fill(e, extra_weight);
    }
  }

  file.Write();
  analysis.finalize();
  analysis.writeYODA("output.yoda");

  return EXIT_SUCCESS;
}
