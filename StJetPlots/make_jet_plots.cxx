// vim:et sw=2 sts=2

/**
 * \file make_jet_plots.cxx
 * \brief Contains entry point of the analysis code
 *
 * make_jet_plots is a main tool in the analysis. It reads the jet
 * tree files and produces files with histograms (in YODA format).
 * The purpose of this module is to define the entry point main().  It
 * consists of a loop over files and a loop over events.  Event
 * information from "jet", "skim" and "UE" trees are then combined
 * into a single StJetPlotEvent and passed down to StAnaRoot for
 * processing.
 */

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <set>
#include <map>
#include <memory>
#include <tuple>
#include <utility>
#include <vector>

#include <TChain.h>

#ifdef WITH_PDF_REWEIGHTING
#include <LHAPDF/LHAPDF.h>
#endif

#include <YODA/ReaderYODA.h>

#include <StSpinPool/StJetEvent/StJetEvent.h>
#include <StSpinPool/StJetSkimEvent/StJetSkimEvent.h>
#include <StSpinPool/StUeEvent/StUeOffAxisConesEvent.h>

#include "StAnaRoot.h"
#include "common.h"

using std::string;

const string JETS_EXT = ".jets.root";
const string SKIM_EXT = ".skim.root";
const string CONE_UE_EXT = ".ueCones.root";
const string REGION_UE_EXT = ".ueRegions.root";

string replace_filename_prefix(const string& jetfile, const string &new_prefix)
{
  auto last = jetfile.size() - JETS_EXT.size();
  always_assert(jetfile.substr(last) == JETS_EXT);
  return jetfile.substr(0, last) + new_prefix;
}

const char* safe_str(const char* s)
{
  return s ? s : "";
};

const char* default_str(const char* s, const char* def)
{
  return s ? s : def;
};

vector<string> split(const string &s, const string &sep)
{
  string::size_type prev_pos = 0, pos = 0;
  vector<string> result;

  always_assert(sep.size() != 0);

  while ((pos = s.find(sep, pos)) != string::npos) {
    result.push_back(s.substr(prev_pos, pos - prev_pos));
    pos += sep.length();
    prev_pos = pos;
  }
  result.push_back(s.substr(prev_pos));

  return result;
}


#ifdef WITH_PDF_REWEIGHTING
unique_ptr<LHAPDF::PDF> mkPDF(const char* pdf_spec)
{
  string pdf_spec_str = pdf_spec;
  size_t pos = pdf_spec_str.find(":");
  string pdf_name = (pos != string::npos) ? pdf_spec_str.substr(0, pos) : pdf_spec_str;
  string pdf_member_str = (pos != string::npos) ? pdf_spec_str.substr(pos+1) : "0";
  errno = 0;
  long pdf_member = strtol(pdf_member_str.c_str(), NULL, 10);
  always_assert(errno == 0);
  return unique_ptr<LHAPDF::PDF> { LHAPDF::mkPDF(pdf_name, pdf_member) };
}
#endif

int main(int argc, char **argv)
{
  const int start_event_index = atoi(default_str(getenv("start_event_index"), "0"));
  const bool do_soft_reweight = !atoi(safe_str(getenv("no_soft_reweight")));
  const int jp_threshold_adjust = atoi(safe_str(getenv("jp_threshold_adjust")));
  const bool trig_pretend_did_fire = atoi(default_str(getenv("trig_pretend_did_fire"), "0"));
  const bool trig_pretend_should_fire = atoi(default_str(getenv("trig_pretend_should_fire"), "0"));
  vector<tuple<string, int, float, float>> inputs;
#ifdef WITH_PDF_REWEIGHTING
  bool do_pdf_reweight = false;
  unique_ptr<LHAPDF::PDF> reference_pdf, target_pdf;
#endif

#define PDF_REWEIGHT_OPT "--pdf-reweight"

  if (argc < 2) {
    std::cerr << "Usage:" << std::endl << argv[0]
              << " [ " PDF_REWEIGHT_OPT " reference_pdf_name[:member] target_pdf_name[:member] ]"
                 " file1.jets.root [file2.jets.root ...]" << std::endl;
    return EXIT_FAILURE;
  }

  for(int i = 1; i < argc;) {
    string arg = argv[i++];
    if (arg == PDF_REWEIGHT_OPT) {
#ifdef WITH_PDF_REWEIGHTING
      always_assert(!do_pdf_reweight);
      do_pdf_reweight = true;
      reference_pdf = mkPDF(argv[i++]);
      target_pdf = mkPDF(argv[i++]);
#else
      std::cerr << argv[0] << ": Error: PDF reweighting is not supported in this build!" << std::endl;
      return EXIT_FAILURE;
#endif
    } else {
      vector<string> input = split(arg, ":");
      string &jetfile = input[0];
      int runindex = -1;
      float jp0ps = 0.;
      float jp1ps = 0.;
      if (input.size() >= 2) {
        char *endptr = NULL;
        runindex = strtol(input[1].c_str(), &endptr, 10);
        always_assert((endptr != nullptr) && (*endptr == '\x0'));
      }
      if (input.size() >= 3) {
        char *endptr = NULL;
        jp0ps = strtod(input[2].c_str(), &endptr);
        always_assert((endptr != nullptr) && (*endptr == '\x0'));
      }
      if (input.size() >= 4) {
        char *endptr = NULL;
        jp1ps = strtod(input[3].c_str(), &endptr);
        always_assert((endptr != nullptr) && (*endptr == '\x0'));
      }
      inputs.emplace_back(jetfile, runindex, jp0ps, jp1ps);
    }
  }

  StJetEvent *jetEvent = 0;
  StJetEvent *jetParticleEvent = 0;
  StJetSkimEvent *skimEvent = 0;
  StUeOffAxisConesEvent *ueEvent = 0;
  StUeOffAxisConesEvent *ueParticleEvent = 0;
  StUeEvent* ueEvent_transP = 0;
  StUeEvent* ueEvent_transM = 0;
  StUeEvent* ueEvent_away = 0;
  StUeEvent* ueEvent_toward = 0;

  const char *rt_reweight_yoda = getenv("rt_reweight_yoda");
  const char *rt_reweight_histo_name = getenv("rt_reweight_histo_name");
  std::unique_ptr<YODA::Scatter3D> rt_reweight_histo = nullptr;
  if (rt_reweight_yoda && (strlen(rt_reweight_yoda) != 0)) {
    std::vector<YODA::AnalysisObject*> aos = YODA::ReaderYODA::create().read(rt_reweight_yoda);
    for (YODA::AnalysisObject *ao : aos) {
      if (ao->path() == rt_reweight_histo_name) {
        rt_reweight_histo.reset(dynamic_cast<YODA::Scatter3D*>(ao));
      } else {
        delete ao;
      }
    }
    always_assert(rt_reweight_histo);
  }

  StAnaRoot analysis("");

  for(const tuple<string, int, float, float> &input : inputs) {
    const string &jetfile = get<0>(input);
    const int runindex = get<1>(input);
    const float jp0ps = get<2>(input);
    const float jp1ps = get<3>(input);

    string skimfile = replace_filename_prefix(jetfile, SKIM_EXT);
    string coneuefile = replace_filename_prefix(jetfile, CONE_UE_EXT);
    string regionuefile = replace_filename_prefix(jetfile, REGION_UE_EXT);

    TChain jetChain("jet");
    TChain skimChain("jetSkimTree");
    TChain coneueChain("ue");
    TChain regionueChain("ue");

    jetChain.Add(jetfile.c_str());
    skimChain.Add(skimfile.c_str());
    coneueChain.Add(coneuefile.c_str());
    //regionueChain.Add(regionuefile.c_str());

    const char *detector_jet_branch_name = default_str(getenv("detector_jet_branch_name"), "AntiKtR060NHits12");
    jetChain.SetBranchAddress(detector_jet_branch_name, &jetEvent);
    const bool is_embedding = jetChain.GetBranch(default_str(getenv("particle_jet_branch_name"), "AntiKtR060Particle"));
    if (is_embedding) {
      jetChain.SetBranchAddress(default_str(getenv("particle_jet_branch_name"), "AntiKtR060Particle"), &jetParticleEvent);
      coneueChain.SetBranchAddress(default_str(getenv("particle_ue_branch_name"), "AntiKtR060ParticleOffAxisConesR050"), &ueParticleEvent);
    } else {
      jetParticleEvent = new StJetEvent;
      ueParticleEvent = new StUeOffAxisConesEvent;
    }
    skimChain.SetBranchAddress("skimEventBranch", &skimEvent);
    const char *detector_ue_branch_name = default_str(getenv("detector_ue_branch_name"), "AntiKtR060NHits12OffAxisConesR050");
    coneueChain.SetBranchAddress(detector_ue_branch_name, &ueEvent);
    //regionueChain.SetBranchAddress("transP_AntiKtR060NHits12", &ueEvent_transP);
    //regionueChain.SetBranchAddress("transM_AntiKtR060NHits12", &ueEvent_transM);
    //regionueChain.SetBranchAddress("away_AntiKtR060NHits12", &ueEvent_away);
    //regionueChain.SetBranchAddress("toward_AntiKtR060NHits12", &ueEvent_toward);

    Long_t nentries = jetChain.GetEntries();
    always_assert(nentries == skimChain.GetEntries());
    always_assert(nentries == coneueChain.GetEntries());
    //always_assert(nentries == regionueChain.GetEntries());

    if (nentries == 0) {
      std::cerr << "No entries in the input chain " << jetfile << std::endl;
      return EXIT_FAILURE;
    }

    for(Long_t entry = 0; entry < nentries; entry++) {
      jetChain.GetEvent(entry);
      if (jetChain.GetTree()->GetReadEntry() < start_event_index) continue;
      skimChain.GetEvent(entry);
      coneueChain.GetEvent(entry);
      //regionueChain.GetEvent(entry);

      always_assert(jetEvent->runId() == skimEvent->runId());
      always_assert(jetEvent->eventId() == skimEvent->eventId());
      //always_assert(jetEvent->eventId() == ueEvent->eventId());
      //always_assert(jetEvent->eventId() == ueEvent_transP->eventId());
      //always_assert(jetEvent->eventId() == ueEvent_transM->eventId());
      //always_assert(jetEvent->eventId() == ueEvent_away->eventId());
      //always_assert(jetEvent->eventId() == ueEvent_toward->eventId());

      if ((is_embedding && (jetEvent->runId() == 13059026) && (jetEvent->eventId() == 337))
       || (is_embedding && (jetEvent->runId() == 13051021) && (jetEvent->eventId() == 193))
       || (is_embedding && (jetEvent->runId() == 13051087) && (jetEvent->eventId() == 151))
       || (is_embedding && (jetEvent->runId() == 13065021) && (jetEvent->eventId() == 43))
       ) {
        std::cerr << "blacklisted event id "
          << jetEvent->eventId()
          << " encountered in run "
          << jetEvent->runId()
          << " - skipping"
          << std::endl;
        continue;
      }

      always_assert(is_embedding == static_cast<bool>(skimEvent->mcEvent()));

      double extra_weight = 1.;
      if (is_embedding) {
        double p[] = {
          1.21733561, -0.32633577, 0.16913723, 0.82134143
        };
        double partonic_pt = skimEvent->mcEvent()->pt();
        if (do_soft_reweight) {
          extra_weight = 1./(1. + (p[0] + p[1] * (partonic_pt - 2.) + p[2] * (partonic_pt - 2.) * (partonic_pt - 2.)) * exp(-p[3] * (partonic_pt - 2.)));
        }
      }
#ifdef WITH_PDF_REWEIGHTING
      if (do_pdf_reweight) {
        always_assert(is_embedding);
        const StPythiaEvent *pythiaEvent = skimEvent->mcEvent();
        always_assert(pythiaEvent);

        double Q = pythiaEvent->pt();
        Q = std::max(Q, 1.3 /* GeV */);

        int i1 = pythiaEvent->particle(4)->GetPdgCode();
        int i2 = pythiaEvent->particle(5)->GetPdgCode();
        double x1 = pythiaEvent->x1();
        double x2 = pythiaEvent->x2();

        double reference_lumi = reference_pdf->xfxQ(i1, x1, Q) * reference_pdf->xfxQ(i2, x2, Q);
        double target_lumi = target_pdf->xfxQ(i1, x1, Q) * target_pdf->xfxQ(i2, x2, Q);

        if (reference_lumi == 0.) {
          std::cerr << "Invalid luminosity:"
                    << "\n\tQ = " << Q
                    << "\n\ti1 = " << i1
                    << "\n\ti2 = " << i2
                    << "\n\tx1 = " << x1
                    << "\n\tx2 = " << x2
                    << "\n\treference_lumi = " << reference_lumi
                    << "\n\ttarget_lumi = " << target_lumi
                    << std::endl;
          return EXIT_FAILURE;
        }

        extra_weight *= target_lumi / reference_lumi;
      }
#endif

      std::unordered_map<StJetCandidate*, double> det_weight;
      StJetVertex *primary_vertex = jetEvent->vertex();
      for (TObject *_jet : primary_vertex->jets()) {
        StJetCandidate *jet = dynamic_cast<StJetCandidate*>(_jet);
        if (rt_reweight_histo) {
          for (YODA::Point3D &p : rt_reweight_histo->points()) {
            if ((p.xMin() <= jet->pt()) && (jet->pt() < p.xMax())
                && (p.yMin() <= jet->rt()) && (jet->rt() < p.yMax())) {
              always_assert(!det_weight.count(jet));
              det_weight[jet] = p.z();
            }
          }
          if (!det_weight.count(jet)) {
            det_weight[jet] = 0.0;
          }
        } else {
          det_weight[jet] = 1.0;
        }
      }

      if (jp_threshold_adjust != 0) {
        always_assert(skimEvent->barrelJetPatchTh(0) == 20);
        always_assert(skimEvent->barrelJetPatchTh(1) == 28);
        always_assert(skimEvent->barrelJetPatchTh(2) == 36);

        for ( int jp = 0; jp <= 2; jp++) {
          skimEvent->setBarrelJetPatchTh(jp, skimEvent->barrelJetPatchTh(jp) + jp_threshold_adjust);
        }
      }

      std::set<int> did_fire_triggers{};
      std::set<int> should_fire_triggers{};
      std::set<int> triggers{};
      for (TObject *_skim_trig : *skimEvent->triggers()) {
        StJetSkimTrig *skim_trig = dynamic_cast<StJetSkimTrig*>(_skim_trig);
        int trig_id = skim_trig->trigId();
        if (!skim_trig) continue;
        if (trig_pretend_did_fire || skim_trig->didFire()) {
          did_fire_triggers.insert(trig_id);
        }
        if (trig_pretend_should_fire || skim_trig->shouldFire()) {
          should_fire_triggers.insert(trig_id);
        }
        if ((is_embedding || trig_pretend_did_fire || skim_trig->didFire())
            && (trig_pretend_should_fire ||
              (trig_id == TRIG_RUN12_PP200_VPDMB) || (trig_id == TRIG_RUN12_PP200_VPDMB_nobsmd) ||
              (trig_id == TRIG_RUN12_PP500_VPDMB) || (trig_id == TRIG_RUN12_PP500_VPDMB_nobsmd) ||
              (trig_id == TRIG_ZEROBIAS) ||
              skim_trig->shouldFire()))
        {
          triggers.insert(trig_id);
        }
      }
      if (is_embedding) {
        triggers.insert(TRIG_RUN12_PP200_VPDMB);
        triggers.insert(TRIG_RUN12_PP200_VPDMB_nobsmd);
        triggers.insert(TRIG_RUN12_PP500_VPDMB);
        triggers.insert(TRIG_RUN12_PP500_VPDMB_nobsmd);
        triggers.insert(TRIG_ZEROBIAS);
      }

      StJetPlotEvent e(*jetEvent, *jetParticleEvent, det_weight, *skimEvent, *ueEvent, *ueParticleEvent, ueEvent_transP, ueEvent_transM, ueEvent_away, ueEvent_toward, did_fire_triggers, should_fire_triggers, triggers, jetChain, jetChain.GetReadEntry(), jetChain.GetTree()->GetReadEntry(), extra_weight, runindex, jp0ps, jp1ps);
      analysis.fill(e, extra_weight);
    }
  }

  analysis.finalize();
  analysis.writeYODA("output.yoda");

  return EXIT_SUCCESS;
}
