// vim:et sw=2 sts=2
#pragma once

#include "StJetPlotContainer.h"
#include "common.h"

template<typename T>
class StEmbeddingCut : public T
{
public:

  template <typename ...Args>
  StEmbeddingCut(Args ...args) : T(args...) {};
  virtual ~StEmbeddingCut() {};

  void fill(const StJetPlotEvent& e, double weight) override
  {
    if (e.is_embedding()) {
      T::fill(e, weight);
    }
  }
};
