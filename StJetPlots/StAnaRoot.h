// vim:et sw=2 sts=2
#pragma once

/**
 * \file StAnaRoot.h
 * \brief Defines all "analyses" used in processing
 *
 * The data processing is split into "analyses" that are classes
 * inheriting from Analysis (similar to StMaker, but much simpler).
 * This is the place where these analysis are instantiated and their
 * parameters are set.
 */

#include <memory>

#include "Analysis.h"
#include "StJetPlotEvent.h"

#include "StAnaPartonicSpectrum.h"
#include "StAnaJetQuantities.h"
#include "StAnaTriggers.h"
#include "StAnaUnfolding.h"

#include "StVertexReweighting.h"
#include "StTriggerPromotionReweighting.h"
#include "StTriggerDemotionReweighting.h"

class StAnaRoot : public Analysis<StJetPlotEvent>
{
private:

  static std::string remove_dot(const std::string &s) {
    std::string result = s;
    size_t pos = result.find('.');
    if (pos != string::npos) {
      result.erase(pos, 1);
    }
    return result;
  }
public:

  template <typename ...Args>
  StAnaRoot(Args ...args)
    : Analysis<StJetPlotEvent>(args...)
    , partonic_spectrum("partonic_spectrum")
    , jet_quantities_vpdmb("jet_quantities_vpdmb")
    , jet_quantities_vpdmb_nobsmd("jet_quantities_vpdmb_nobsmd")
    , jet_quantities_vpdmb_nobsmd_jp0_simu("jet_quantities_vpdmb_nobsmd_jp0_simu")
    , jet_quantities_jp0_reweight("jet_quantities_jp0_reweight")
    , jet_quantities_jp1_reweight("jet_quantities_jp1_reweight")
    , jet_quantities_jp2_reweight("jet_quantities_jp2_reweight")
    , jet_quantities_zerobias("jet_quantities_zerobias")
    , triggers("triggers")
    , unfolding_jp0("unfolding_jp0")
    , unfolding_jp1("unfolding_jp1")
    , unfolding_jp2("unfolding_jp2")
    , unfolding_jp0_promoted(0, "unfolding_jp0_promoted")
    , unfolding_jp1_promoted(1, "unfolding_jp1_promoted")
    , unfolding_jp2_promoted(2, "unfolding_jp2_promoted")
    , unfolding_jp0_demoted(0, "unfolding_jp0_demoted")
    , unfolding_jp1_demoted(1, "unfolding_jp1_demoted")
    , unfolding_jp2_demoted(2, "unfolding_jp2_demoted")
    , unfolding_etabins{{
        { { 0, 0 }, unfolding_jp0, "unfolding_jp0", nullptr },
        { { 1, 0 }, unfolding_jp0, "unfolding_jp0", nullptr },
        { { 0, 1 }, unfolding_jp0, "unfolding_jp0", nullptr },
        { { 1, 1 }, unfolding_jp0, "unfolding_jp0", nullptr },

        { { 0, 0 }, unfolding_jp1, "unfolding_jp1", nullptr },
        { { 1, 0 }, unfolding_jp1, "unfolding_jp1", nullptr },
        { { 0, 1 }, unfolding_jp1, "unfolding_jp1", nullptr },
        { { 1, 1 }, unfolding_jp1, "unfolding_jp1", nullptr },

        { { 0, 0 }, unfolding_jp2, "unfolding_jp2", nullptr },
        { { 1, 0 }, unfolding_jp2, "unfolding_jp2", nullptr },
        { { 0, 1 }, unfolding_jp2, "unfolding_jp2", nullptr },
        { { 1, 1 }, unfolding_jp2, "unfolding_jp2", nullptr },

        { { 0, 0 }, unfolding_jp0_promoted, "unfolding_jp0_promoted", nullptr },
        { { 1, 0 }, unfolding_jp0_promoted, "unfolding_jp0_promoted", nullptr },
        { { 0, 1 }, unfolding_jp0_promoted, "unfolding_jp0_promoted", nullptr },
        { { 1, 1 }, unfolding_jp0_promoted, "unfolding_jp0_promoted", nullptr },

        { { 0, 0 }, unfolding_jp1_promoted, "unfolding_jp1_promoted", nullptr },
        { { 1, 0 }, unfolding_jp1_promoted, "unfolding_jp1_promoted", nullptr },
        { { 0, 1 }, unfolding_jp1_promoted, "unfolding_jp1_promoted", nullptr },
        { { 1, 1 }, unfolding_jp1_promoted, "unfolding_jp1_promoted", nullptr },

        { { 0, 0 }, unfolding_jp2_promoted, "unfolding_jp2_promoted", nullptr },
        { { 1, 0 }, unfolding_jp2_promoted, "unfolding_jp2_promoted", nullptr },
        { { 0, 1 }, unfolding_jp2_promoted, "unfolding_jp2_promoted", nullptr },
        { { 1, 1 }, unfolding_jp2_promoted, "unfolding_jp2_promoted", nullptr },
        }}
  {
    StJetCut extra_cuts = no_cut;
    const char *s;
    s = getenv("min_vertex_z");
    double min_vertex_z = atof(s ? s : "-60");
    s = getenv("max_vertex_z");
    double max_vertex_z = atof(s ? s : "60");
    s = getenv("jet_phi_min");
    double jet_phi_min = atof(s ? s : "NAN");
    s = getenv("jet_phi_max");
    double jet_phi_max = atof(s ? s : "NAN");
    if ((!std::isnan(jet_phi_min)) && (!std::isnan(jet_phi_max))) {
      extra_cuts = extra_cuts && make_jet_phi_cut(jet_phi_min, jet_phi_max);
    }
    s = getenv("etabin_cut_0");
    s = s ? s : "0.0";
    const char *etabin_cut_0_s = s;
    double etabin_cut_0 = atof(s);
    s = getenv("etabin_cut_1");
    s = s ? s : "0.4";
    detector_etabin_suffix[0] = "_" + remove_dot(etabin_cut_0_s) + "_" + remove_dot(s);
    particle_etabin_suffix[0] = "_" + remove_dot(etabin_cut_0_s) + "_" + remove_dot(s);
    const char *etabin_cut_1_s = s;
    double etabin_cut_1 = atof(s);
    s = getenv("etabin_cut_2");
    s = s ? s : "0.8";
    detector_etabin_suffix[1] = "_" + remove_dot(etabin_cut_1_s) + "_" + remove_dot(s);
    particle_etabin_suffix[1] = "_" + remove_dot(etabin_cut_1_s) + "_" + remove_dot(s);
    double etabin_cut_2 = atof(s);
    detector_etabin_cut[0] = make_jet_abseta_cut(etabin_cut_0, etabin_cut_1);
    particle_etabin_cut[0] = make_jet_abseta_cut(etabin_cut_0, etabin_cut_1);
    detector_etabin_cut[1] = make_jet_abseta_cut(etabin_cut_1, etabin_cut_2);
    particle_etabin_cut[1] = make_jet_abseta_cut(etabin_cut_1, etabin_cut_2);
    s = getenv("blacklist_tower_3407");
    bool blacklist_tower_3407 = atoi(s ? s : "0");
    if (blacklist_tower_3407) {
      extra_cuts = extra_cuts && blacklist_tower_3407_cut;
    }
    s = getenv("require_two_good_vertices");
    bool require_two_good_vertices = atoi(s ? s : "0");
    if (require_two_good_vertices) {
      extra_cuts = extra_cuts && two_good_vertices_cut;
    }
    s = getenv("max_track_pt");
    int max_track_pt = atoi(s ? s : "-1");
    if (max_track_pt >= 0) {
      extra_cuts = extra_cuts && make_max_track_pt_cut(max_track_pt);
    }
    s = getenv("sector20_track_cut");
    int sector20_track_cut_flag = atoi(s ? s : "0");
    if (sector20_track_cut_flag) {
      extra_cuts = extra_cuts && sector20_track_cut;
    }
    s = getenv("vpdmb_did_fire_cut");
    if (atoi(s ? s : "0")) {
      extra_cuts = extra_cuts && make_trigger_did_fire_cut({TRIG_RUN12_PP200_VPDMB_nobsmd, TRIG_RUN12_PP500_VPDMB_nobsmd});
    }
    s = getenv("vpd_vertex_found_cut");
    int vpd_vertex_found_cut_flag = atoi(s ? s : "0");
    if (vpd_vertex_found_cut_flag) {
      extra_cuts = extra_cuts && vpd_vertex_found_cut;
    }
    s = getenv("vpd_vertex_not_found_cut");
    int vpd_vertex_not_found_cut_flag = atoi(s ? s : "0");
    if (vpd_vertex_not_found_cut_flag) {
      extra_cuts = extra_cuts && vpd_vertex_not_found_cut;
    }
    s = getenv("vpdvz_vz_diff_min");
    double vpdvz_vz_diff_min = atof(s ? s : "-1");
    if (vpdvz_vz_diff_min >= 0) {
      extra_cuts = extra_cuts && make_vpdvz_vz_diff_min_cut(vpdvz_vz_diff_min);
    }
    s = getenv("vpdvz_vz_diff_max");
    double vpdvz_vz_diff_max = atof(s ? s : "-1");
    if (vpdvz_vz_diff_max >= 0) {
      extra_cuts = extra_cuts && make_vpdvz_vz_diff_max_cut(vpdvz_vz_diff_max);
    }
    s = getenv("tpcvz_simuvz_diff_max");
    double tpcvz_simuvz_diff_max = atof(s ? s : "-1");
    if (tpcvz_simuvz_diff_max >= 0) {
      extra_cuts = extra_cuts && make_tpcvz_simuvz_diff_max_cut(tpcvz_simuvz_diff_max);
    }
    s = getenv("vertex_reweight_type");
    int reweight_type;
    std::string reweight_type_s = s ? s : "data_tpc_vertex";
    if (reweight_type_s == "none") {
      reweight_type = None;
    } else if (reweight_type_s == "data_tpc_vertex") {
      reweight_type = DataTPCVertex;
    } else if (reweight_type_s == "embedding_tpc_vertex") {
      reweight_type = EmbeddingTPCVertex;
    } else if (reweight_type_s == "embedding_gen_vertex") {
      reweight_type = EmbeddingGenVertex;
    } else {
      std::cerr << "Invalid vertex_reweight_type" << std::endl;
      std::abort();
    }
    s = getenv("vertex_reweight_sigma");
    double vertex_reweight_sigma = atof(s ? s : "46.6" /* cm */);
    s = getenv("vertex_reweight_b");
    double vertex_reweight_b = atof(s ? s : "inf" /* cm */);
    s = getenv("vertex_reweight_sigma_target");
    double vertex_reweight_sigma_target = atof(s ? s : "26.5" /* cm */);
    s = getenv("vertex_reweight_b_target");
    double vertex_reweight_b_target = atof(s ? s : "inf" /* cm */);
    add(partonic_spectrum);
    add(jet_quantities_vpdmb);
    jet_quantities_vpdmb.jet_cut =
      make_trigger_cut({TRIG_RUN12_PP200_VPDMB, TRIG_RUN12_PP500_VPDMB})
      && make_detector_level_cut(-1, etabin_cut_2)
      && extra_cuts;
    add(jet_quantities_vpdmb_nobsmd);
    jet_quantities_vpdmb_nobsmd.jet_cut =
      make_trigger_cut({TRIG_RUN12_PP200_VPDMB_nobsmd, TRIG_RUN12_PP500_VPDMB_nobsmd})
      && make_detector_level_cut(-1, etabin_cut_2)
      && extra_cuts;
    add(jet_quantities_vpdmb_nobsmd_jp0_simu);
    jet_quantities_vpdmb_nobsmd_jp0_simu.jet_cut =
      make_trigger_cut({TRIG_RUN12_PP200_VPDMB_nobsmd, TRIG_RUN12_PP500_VPDMB_nobsmd})
      && make_trigger_should_fire_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0})
      && make_detector_level_cut(0, etabin_cut_2)
      && extra_cuts;
    add(jet_quantities_jp0_reweight);
    jet_quantities_jp0_reweight.jet_cut =
      make_trigger_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0})
      && make_detector_level_cut(0, etabin_cut_2)
      && extra_cuts;
    jet_quantities_jp0_reweight.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    add(jet_quantities_jp1_reweight);
    jet_quantities_jp1_reweight.jet_cut =
      make_trigger_cut({TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1})
      && make_detector_level_cut(1, etabin_cut_2)
      && extra_cuts;
    jet_quantities_jp1_reweight.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    add(jet_quantities_jp2_reweight);
    jet_quantities_jp2_reweight.jet_cut =
      make_trigger_cut({TRIG_RUN12_PP200_JP2, TRIG_RUN12_PP500_JP2})
      && make_detector_level_cut(2, etabin_cut_2)
      && extra_cuts;
    jet_quantities_jp2_reweight.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    add(jet_quantities_zerobias);
    jet_quantities_zerobias.jet_cut =
      make_trigger_cut({TRIG_ZEROBIAS})
      && make_detector_level_cut(-1, etabin_cut_2)
      && extra_cuts;
    add(triggers);
    add(unfolding_jp0);
    unfolding_jp0.detector_jet_cut =
      make_primary_vertex_cut(min_vertex_z, max_vertex_z)
      && jet_quantities_jp0_reweight.jet_cut
      && jet_rt_cut;
    unfolding_jp0.particle_jet_cut = make_jet_abseta_cut(etabin_cut_0, etabin_cut_2);
    unfolding_jp0.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    add(unfolding_jp1);
    unfolding_jp1.detector_jet_cut =
      make_primary_vertex_cut(min_vertex_z, max_vertex_z)
      && jet_quantities_jp1_reweight.jet_cut
      && jet_rt_cut;
    unfolding_jp1.particle_jet_cut = make_jet_abseta_cut(etabin_cut_0, etabin_cut_2);
    unfolding_jp1.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    add(unfolding_jp2);
    unfolding_jp2.detector_jet_cut =
      make_primary_vertex_cut(min_vertex_z, max_vertex_z)
      && jet_quantities_jp2_reweight.jet_cut
      && jet_rt_cut;
    unfolding_jp2.particle_jet_cut = make_jet_abseta_cut(etabin_cut_0, etabin_cut_2);
    unfolding_jp2.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);

    s = getenv("trig_fire_pretend_embed");
    bool trig_fire_pretend_embed = atoi(s ? s : "0");
    s = getenv("force_trig_reweighting");
    bool force_trig_reweighting = atoi(s ? s : "0");

    add(unfolding_jp0_promoted);
    unfolding_jp0_promoted.detector_jet_cut =
      make_primary_vertex_cut(min_vertex_z, max_vertex_z)
      && (is_embedding_cut || cut_from_bool(trig_fire_pretend_embed) || make_trigger_did_fire_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0}))
      && make_trigger_should_fire_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0})
      && (!make_trigger_should_fire_cut({TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1}))
      && make_detector_level_cut(0, etabin_cut_2)
      && jet_rt_cut
      && extra_cuts;
    unfolding_jp0_promoted.particle_jet_cut = make_jet_abseta_cut(etabin_cut_0, etabin_cut_2);
    unfolding_jp0_promoted.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    unfolding_jp0_promoted.force_trig_reweighting = force_trig_reweighting;
    add(unfolding_jp1_promoted);
    unfolding_jp1_promoted.detector_jet_cut =
      make_primary_vertex_cut(min_vertex_z, max_vertex_z)
      && (is_embedding_cut || cut_from_bool(trig_fire_pretend_embed) || make_trigger_did_fire_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0, TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1}))
      && make_trigger_should_fire_cut({TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1})
      && (!make_trigger_should_fire_cut({TRIG_RUN12_PP200_JP2, TRIG_RUN12_PP500_JP2}))
      && make_detector_level_cut(1, etabin_cut_2)
      && jet_rt_cut
      && extra_cuts;
    unfolding_jp1_promoted.particle_jet_cut = make_jet_abseta_cut(etabin_cut_0, etabin_cut_2);
    unfolding_jp1_promoted.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    unfolding_jp1_promoted.force_trig_reweighting = force_trig_reweighting;
    add(unfolding_jp2_promoted);
    unfolding_jp2_promoted.detector_jet_cut =
      make_primary_vertex_cut(min_vertex_z, max_vertex_z)
      && (is_embedding_cut || cut_from_bool(trig_fire_pretend_embed) || make_trigger_did_fire_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0, TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1, TRIG_RUN12_PP200_JP2, TRIG_RUN12_PP500_JP2}))
      && make_trigger_should_fire_cut({TRIG_RUN12_PP200_JP2, TRIG_RUN12_PP500_JP2})
      && make_detector_level_cut(2, etabin_cut_2)
      && jet_rt_cut
      && extra_cuts;
    unfolding_jp2_promoted.particle_jet_cut = make_jet_abseta_cut(etabin_cut_0, etabin_cut_2);
    unfolding_jp2_promoted.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    unfolding_jp2_promoted.force_trig_reweighting = force_trig_reweighting;

    add(unfolding_jp0_demoted);
    unfolding_jp0_demoted.detector_jet_cut =
      make_primary_vertex_cut(min_vertex_z, max_vertex_z)
      && (is_embedding_cut || cut_from_bool(trig_fire_pretend_embed) || make_trigger_did_fire_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0}))
      && make_trigger_should_fire_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0})
      && make_detector_level_cut(0, etabin_cut_2)
      && jet_rt_cut
      && extra_cuts;
    unfolding_jp0_demoted.particle_jet_cut = make_jet_abseta_cut(etabin_cut_0, etabin_cut_2);
    unfolding_jp0_demoted.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    unfolding_jp0_demoted.force_trig_reweighting = force_trig_reweighting;
    add(unfolding_jp1_demoted);
    unfolding_jp1_demoted.detector_jet_cut =
      make_primary_vertex_cut(min_vertex_z, max_vertex_z)
      && (is_embedding_cut || cut_from_bool(trig_fire_pretend_embed) || (
            (!make_trigger_did_fire_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0}))
            &&
            make_trigger_did_fire_cut({TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1})
            ))
      && make_trigger_should_fire_cut({TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1})
      && make_detector_level_cut(1, etabin_cut_2)
      && jet_rt_cut
      && extra_cuts;
    unfolding_jp1_demoted.particle_jet_cut = make_jet_abseta_cut(etabin_cut_0, etabin_cut_2);
    unfolding_jp1_demoted.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    unfolding_jp1_demoted.force_trig_reweighting = force_trig_reweighting;
    add(unfolding_jp2_demoted);
    unfolding_jp2_demoted.detector_jet_cut =
      make_primary_vertex_cut(min_vertex_z, max_vertex_z)
      && (is_embedding_cut || cut_from_bool(trig_fire_pretend_embed) || (
            (!make_trigger_did_fire_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0}))
            &&
            (!make_trigger_did_fire_cut({TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1}))
            &&
            make_trigger_did_fire_cut({TRIG_RUN12_PP200_JP2, TRIG_RUN12_PP500_JP2})
            ))
      && make_trigger_should_fire_cut({TRIG_RUN12_PP200_JP2, TRIG_RUN12_PP500_JP2})
      && make_detector_level_cut(2, etabin_cut_2)
      && jet_rt_cut
      && extra_cuts;
    unfolding_jp2_demoted.particle_jet_cut = make_jet_abseta_cut(etabin_cut_0, etabin_cut_2);
    unfolding_jp2_demoted.SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    unfolding_jp2_demoted.force_trig_reweighting = force_trig_reweighting;

    for (auto &elem : unfolding_etabins) {
      unsigned int detector_etabin = std::get<0>(elem).first;
      unsigned int particle_etabin = std::get<0>(elem).second;
      const StVertexReweighting<StAnaUnfolding> &unfolding = std::get<1>(elem);
      std::string path = std::get<2>(elem)
        + detector_etabin_suffix[detector_etabin]
        + particle_etabin_suffix[particle_etabin];
      std::shared_ptr<StVertexReweighting<StAnaUnfolding>> &unfolding_etabin = std::get<3>(elem);
      unfolding_etabin.reset(new StVertexReweighting<StAnaUnfolding>(path));
      add(*unfolding_etabin);
      unfolding_etabin->disable_extra_corr = true; // reduce histogram file size
      unfolding_etabin->detector_jet_cut = unfolding.detector_jet_cut && detector_etabin_cut[detector_etabin];
      unfolding_etabin->particle_jet_cut = unfolding.particle_jet_cut && particle_etabin_cut[particle_etabin];
      unfolding_etabin->SetVertexReweightingParams(reweight_type, vertex_reweight_sigma, vertex_reweight_b, vertex_reweight_sigma_target, vertex_reweight_b_target);
    }
  }

  void fill(const StJetPlotEvent& e, double weight) override
  {
    for(auto &p : aos) {
      Analysis<StJetPlotEvent> *ana = dynamic_cast<Analysis<StJetPlotEvent>*>(p.second);
      if (!ana) continue;
      ana->fill(e, weight);
    }
  };

  void finalize() override
  {
    for(auto &p : aos) {
      Analysis<StJetPlotEvent> *ana = dynamic_cast<Analysis<StJetPlotEvent>*>(p.second);
      if (!ana) continue;
      ana->finalize();
    }
  };

  StAnaPartonicSpectrum partonic_spectrum;
  StAnaJetQuantities jet_quantities_vpdmb;
  StAnaJetQuantities jet_quantities_vpdmb_nobsmd;
  StAnaJetQuantities jet_quantities_vpdmb_nobsmd_jp0_simu;
  StVertexReweighting<StAnaJetQuantities> jet_quantities_jp0_reweight;
  StVertexReweighting<StAnaJetQuantities> jet_quantities_jp1_reweight;
  StVertexReweighting<StAnaJetQuantities> jet_quantities_jp2_reweight;
  StAnaJetQuantities jet_quantities_zerobias;
  StAnaTriggers triggers;
  StVertexReweighting<StAnaUnfolding> unfolding_jp0;
  StVertexReweighting<StAnaUnfolding> unfolding_jp1;
  StVertexReweighting<StAnaUnfolding> unfolding_jp2;
  StTriggerPromotionReweighting<StVertexReweighting<StAnaUnfolding>> unfolding_jp0_promoted;
  StTriggerPromotionReweighting<StVertexReweighting<StAnaUnfolding>> unfolding_jp1_promoted;
  StTriggerPromotionReweighting<StVertexReweighting<StAnaUnfolding>> unfolding_jp2_promoted;
  StTriggerDemotionReweighting<StVertexReweighting<StAnaUnfolding>> unfolding_jp0_demoted;
  StTriggerDemotionReweighting<StVertexReweighting<StAnaUnfolding>> unfolding_jp1_demoted;
  StTriggerDemotionReweighting<StVertexReweighting<StAnaUnfolding>> unfolding_jp2_demoted;
  std::array<StJetCut, 2> detector_etabin_cut;
  std::array<std::string, 2> detector_etabin_suffix;
  std::array<StJetCut, 2> particle_etabin_cut;
  std::array<std::string, 2> particle_etabin_suffix;
  std::vector<
    std::tuple<
      std::pair<unsigned int, unsigned int>,
      StVertexReweighting<StAnaUnfolding>&,
      std::string,
      std::shared_ptr<StVertexReweighting<StAnaUnfolding>>
    >
  > unfolding_etabins;

private:

  std::vector<YODA::AnalysisObject*> to_vec() override
  {
    std::vector<YODA::AnalysisObject*> result;
    for(auto &p : aos) {
      Analysis<StJetPlotEvent> *ana = dynamic_cast<Analysis<StJetPlotEvent>*>(p.second);
      std::vector<YODA::AnalysisObject*> sub_result = ana->to_vec();
      std::copy(sub_result.begin(), sub_result.end(),
          std::back_inserter(result));
    }
    return result;
  };
};
