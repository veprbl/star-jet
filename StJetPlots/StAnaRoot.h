// vim:et sw=2 sts=2
#pragma once

#include "constants.h"
#include "Analysis.h"
#include "StJetPlotEvent.h"

#include "StAnaPartonicSpectrum.h"
#include "StAnaJetQuantities.h"
#include "StAnaTriggers.h"
#include "StAnaUnfolding.h"

#include "StTriggerCut.h"
#include "StVertexReweighting.h"

class StAnaRoot : public Analysis<StJetPlotEvent>
{
public:

  template <typename ...Args>
  StAnaRoot(Args ...args)
    : Analysis<StJetPlotEvent>(args...)
    , partonic_spectrum("partonic_spectrum")
    , jet_quantities("jet_quantities")
    , jet_quantities_jp0("jet_quantities_jp0")
    , jet_quantities_jp0_vpdmb_cut("jet_quantities_jp0_vpdmb_cut")
    , jet_quantities_jp1("jet_quantities_jp1")
    , jet_quantities_jp2("jet_quantities_jp2")
    , jet_quantities_vpdmb("jet_quantities_vpdmb")
    , jet_quantities_vpdmb_nobsmd("jet_quantities_vpdmb_nobsmd")
    , jet_quantities_vpdmb_nobsmd_jp0_simu("jet_quantities_vpdmb_nobsmd_jp0_simu")
    , jet_quantities_jp0_reweight("jet_quantities_jp0_reweight")
    , jet_quantities_jp1_reweight("jet_quantities_jp1_reweight")
    , jet_quantities_jp2_reweight("jet_quantities_jp2_reweight")
    , triggers("triggers")
    , unfolding_jp0(0, "unfolding_jp0")
    , unfolding_jp1(1, "unfolding_jp1")
    , unfolding_jp2(2, "unfolding_jp2")
  {
    StJetCut extra_cuts = no_cut;
    const char *s;
    s = getenv("blacklist_tower_3407");
    bool blacklist_tower_3407 = atoi(s ? s : "0");
    if (blacklist_tower_3407) {
      extra_cuts = extra_cuts && blacklist_tower_3407_cut;
    }
    s = getenv("max_track_pt");
    int max_track_pt = atoi(s ? s : "-1");
    if (max_track_pt >= 0) {
      extra_cuts = extra_cuts && make_max_track_pt_cut(max_track_pt);
    }
    add(partonic_spectrum);
    add(jet_quantities);
    jet_quantities.jet_cut = detector_level_cut && extra_cuts;
    add(jet_quantities_jp0);
    jet_quantities_jp0.jet_cut = detector_level_cut_jp0 && extra_cuts;
    jet_quantities_jp0.SetTriggers(std::set<int>({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0}));
    add(jet_quantities_jp0_vpdmb_cut);
    jet_quantities_jp0_vpdmb_cut.jet_cut = jet_quantities_jp0.jet_cut && vpdmb_vertex_cut;
    jet_quantities_jp0_vpdmb_cut.SetTriggers(std::set<int>({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0}));
    add(jet_quantities_jp1);
    jet_quantities_jp1.jet_cut = detector_level_cut_jp1 && extra_cuts;
    jet_quantities_jp1.SetTriggers(std::set<int>({TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1}));
    add(jet_quantities_jp2);
    jet_quantities_jp2.jet_cut = detector_level_cut_jp2 && extra_cuts;
    jet_quantities_jp2.SetTriggers(std::set<int>({TRIG_RUN12_PP200_JP2, TRIG_RUN12_PP500_JP2}));
    add(jet_quantities_vpdmb);
    jet_quantities_vpdmb.jet_cut = detector_level_cut && extra_cuts;
    jet_quantities_vpdmb.SetTriggers(std::set<int>({TRIG_RUN12_PP200_VPDMB, TRIG_RUN12_PP500_VPDMB}));
    add(jet_quantities_vpdmb_nobsmd);
    jet_quantities_vpdmb_nobsmd.jet_cut = detector_level_cut && extra_cuts && vpdmb_vertex_cut;
    jet_quantities_vpdmb_nobsmd.SetTriggers(std::set<int>({TRIG_RUN12_PP200_VPDMB_nobsmd, TRIG_RUN12_PP500_VPDMB_nobsmd}));
    add(jet_quantities_vpdmb_nobsmd_jp0_simu);
    jet_quantities_vpdmb_nobsmd_jp0_simu.jet_cut = jet_quantities_jp0.jet_cut && make_trigger_simulator_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0}) && vpdmb_vertex_cut;
    jet_quantities_vpdmb_nobsmd_jp0_simu.SetTriggers(std::set<int>({TRIG_RUN12_PP200_VPDMB_nobsmd, TRIG_RUN12_PP500_VPDMB_nobsmd}));
    add(jet_quantities_jp0_reweight);
    jet_quantities_jp0_reweight.jet_cut = detector_level_cut_jp0 && extra_cuts;
    jet_quantities_jp0_reweight.SetTriggers(std::set<int>({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0}));
    jet_quantities_jp0_reweight.SetVertexReweightingParams(26.5 /* cm */, 46.6 /* cm */, Z_VERTEX_CUT);
    add(jet_quantities_jp1_reweight);
    jet_quantities_jp1_reweight.jet_cut = detector_level_cut_jp1 && extra_cuts;
    jet_quantities_jp1_reweight.SetTriggers(std::set<int>({TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1}));
    jet_quantities_jp1_reweight.SetVertexReweightingParams(26.5 /* cm */, 46.6 /* cm */, Z_VERTEX_CUT);
    add(jet_quantities_jp2_reweight);
    jet_quantities_jp2_reweight.jet_cut = detector_level_cut_jp2 && extra_cuts;
    jet_quantities_jp2_reweight.SetTriggers(std::set<int>({TRIG_RUN12_PP200_JP2, TRIG_RUN12_PP500_JP2}));
    jet_quantities_jp2_reweight.SetVertexReweightingParams(26.5 /* cm */, 46.6 /* cm */, Z_VERTEX_CUT);
    add(triggers);
    add(unfolding_jp0);
    unfolding_jp0.detector_jet_cut =
      make_trigger_cut({TRIG_RUN12_PP200_JP0, TRIG_RUN12_PP500_JP0})
      && primary_vertex_cut
      && detector_level_cut_jp0
      && jet_rt_cut
      && extra_cuts;
    unfolding_jp0.particle_jet_cut = particle_level_cut;
    unfolding_jp0.SetVertexReweightingParams(26.5 /* cm */, 46.6 /* cm */, Z_VERTEX_CUT);
    add(unfolding_jp1);
    unfolding_jp1.detector_jet_cut =
      make_trigger_cut({TRIG_RUN12_PP200_JP1, TRIG_RUN12_PP500_JP1})
      && primary_vertex_cut
      && detector_level_cut_jp1
      && jet_rt_cut
      && extra_cuts;
    unfolding_jp1.particle_jet_cut = particle_level_cut;
    unfolding_jp1.SetVertexReweightingParams(26.5 /* cm */, 46.6 /* cm */, Z_VERTEX_CUT);
    add(unfolding_jp2);
    unfolding_jp2.detector_jet_cut =
      make_trigger_cut({TRIG_RUN12_PP200_JP2, TRIG_RUN12_PP500_JP2})
      && primary_vertex_cut
      && detector_level_cut_jp2
      && jet_rt_cut
      && extra_cuts;
    unfolding_jp2.particle_jet_cut = particle_level_cut;
    unfolding_jp2.SetVertexReweightingParams(26.5 /* cm */, 46.6 /* cm */, Z_VERTEX_CUT);
  }

  void fill(const StJetPlotEvent& e, double weight) override
  {
    for(auto &p : aos) {
      Analysis<StJetPlotEvent> *ana = dynamic_cast<Analysis<StJetPlotEvent>*>(p.second);
      if (!ana) continue;
      ana->fill(e, weight);
    }
  };

  void finalize() override
  {
    for(auto &p : aos) {
      Analysis<StJetPlotEvent> *ana = dynamic_cast<Analysis<StJetPlotEvent>*>(p.second);
      if (!ana) continue;
      ana->finalize();
    }
  };

  StAnaPartonicSpectrum partonic_spectrum;
  StAnaJetQuantities jet_quantities;
  StTriggerCut<StAnaJetQuantities> jet_quantities_jp0;
  StTriggerCut<StAnaJetQuantities> jet_quantities_jp0_vpdmb_cut;
  StTriggerCut<StAnaJetQuantities> jet_quantities_jp1;
  StTriggerCut<StAnaJetQuantities> jet_quantities_jp2;
  StTriggerCut<StAnaJetQuantities> jet_quantities_vpdmb;
  StTriggerCut<StAnaJetQuantities> jet_quantities_vpdmb_nobsmd;
  StTriggerCut<StAnaJetQuantities> jet_quantities_vpdmb_nobsmd_jp0_simu;
  StTriggerCut<StVertexReweighting<StAnaJetQuantities>> jet_quantities_jp0_reweight;
  StTriggerCut<StVertexReweighting<StAnaJetQuantities>> jet_quantities_jp1_reweight;
  StTriggerCut<StVertexReweighting<StAnaJetQuantities>> jet_quantities_jp2_reweight;
  StAnaTriggers triggers;
  StVertexReweighting<StAnaUnfolding> unfolding_jp0;
  StVertexReweighting<StAnaUnfolding> unfolding_jp1;
  StVertexReweighting<StAnaUnfolding> unfolding_jp2;

private:

  std::vector<YODA::AnalysisObject*> to_vec() override
  {
    std::vector<YODA::AnalysisObject*> result;
    for(auto &p : aos) {
      Analysis<StJetPlotEvent> *ana = dynamic_cast<Analysis<StJetPlotEvent>*>(p.second);
      std::vector<YODA::AnalysisObject*> sub_result = ana->to_vec();
      std::copy(sub_result.begin(), sub_result.end(),
          std::back_inserter(result));
    }
    return result;
  };
};
