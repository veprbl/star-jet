// vim:et sw=2 sts=2
#pragma once

#include <algorithm>
#include <array>
#include <iostream>
#include <random>
#include <set>
#include <unordered_map>
#include <utility>

#include <TVector2.h>

#include <YODA/Histo1D.h>
#include <YODA/Histo2D.h>

#include "StJetPlotEvent.h"

class JetPair : public std::pair<StJetCandidate*, StJetCandidate*>
{
public:

  JetPair(StJetCandidate* j1, StJetCandidate* j2)
    : std::pair<StJetCandidate*, StJetCandidate*>(j1, j2)
  {}

  double dR_sqr()
  {
    return sqr(TVector2::Phi_mpi_pi(first->phi() - second->phi())) + sqr(first->eta() - second->eta());
  }

  double dR()
  {
    return sqrt(dR_sqr());
  }

  static bool cmp_dR(JetPair& p1, JetPair& p2)
  {
    return p1.dR_sqr() > p2.dR_sqr();
  }

private:

  double sqr(double x) { return x * x; };
};

/**
 * \brief Fill histograms relevant to detector effect unfolding
 *
 * The main purpose of this class is to generate the detector response
 * matrix for p_T spectra of inclusive jets. In order to do that, it
 * looks at "detector level" and "particle level" jets in events from
 * embedding file. They are matched based on proximity in eta-phi
 * space to establish which detector level jet was caused by which
 * particle jet. Additionally, one various dimensional spectra are
 * filled for detector and particle jets. Together with the response
 * matrix they serve as input to a separate tool that performs the
 * unfolding.
 *
 * There is some overlap in functionality of this with
 * StAnaJetQuantities. Latter is used for QA only, this class is used
 * for QA and unfolding.
 */
class StAnaUnfolding : public Analysis<StJetPlotEvent>
{
public:

  template <typename ...Args>
  StAnaUnfolding(Args ...args)
    : Analysis<StJetPlotEvent>(args...)
    , disable_extra_corr(false)
    , is_embedding_set(false)
    , detector_jet_pt(60, 5., 60., "detector_jet_pt", "")
    , detector_jet_pt_fine(550, 5., 60., "detector_jet_pt_fine", "")
    , particle_jet_pt(60, 0., 60., "particle_jet_pt", "")
    , particle_jet_pt_fine(600, 0., 60., "particle_jet_pt_fine", "")
    , response(60, 5., 60., 60, 0., 60., "response", "")
    , response_fine(550, 5., 60., 600, 0., 60., "response_fine", "")
    , fake(60, 5., 60., "fake", "")
    , miss(60, 0., 60., "miss", "")
    , eta_corr(40, -1.2, 1.2, 40, -1.2, 1.2, "eta_corr", "")
    , eta_corr_comb(40, -1.2, 1.2, 40, -1.2, 1.2, "eta_corr_comb", "")
    , jet_neut_sumpt_corr(100, 0., 5., 100, 0., 5., "jet_neut_sumpt_corr", "")
    , jet_chg_sumpt_corr(100, 0., 5., 100, 0., 5., "jet_chg_sumpt_corr", "")
    , jet_neut_mult_corr(20, 0., 20., 20, 0., 20., "jet_neut_mult_corr", "")
    , jet_chg_mult_corr(20, 0., 20., 20, 0., 20., "jet_chg_mult_corr", "")
    , ue_sumpt_corr(100, 0., 5., 100, 0., 5., "ue_sumpt_corr", "")
    , ue_neut_sumpt_corr(100, 0., 5., 100, 0., 5., "ue_neut_sumpt_corr", "")
    , ue_chg_sumpt_corr(100, 0., 5., 100, 0., 5., "ue_chg_sumpt_corr", "")
    , ue_sumpt_corr_cone0(30, 0., 3., 30, 0., 3., "ue_sumpt_corr_cone0", "")
    , ue_sumpt_corr_cone1(30, 0., 3., 30, 0., 3., "ue_sumpt_corr_cone1", "")
    , ue_neut_mult_corr(20, 0., 20., 20, 0., 20., "ue_neut_mult_corr", "")
    , ue_chg_mult_corr(20, 0., 20., 20, 0., 20., "ue_chg_mult_corr", "")
    , jet_dr_comb(40, 0., 1., "jet_dr_comb", "")
    , jet_dr_matched(40, 0., 1., "jet_dr_matched", "")
    , matched_detector_jet_pt(60, 5., 60., "matched_detector_jet_pt", "")
    , matched_detector_jet_pt_fine(550, 5., 60., "matched_detector_jet_pt_fine", "")
    , matched_particle_jet_pt(60, 0., 60., "matched_particle_jet_pt", "")
    , matched_particle_jet_pt_fine(600, 0., 60., "matched_particle_jet_pt_fine", "")
    , pt_ratio_vs_det_pt(30, 0., 60., "pt_ratio_vs_det_pt", "")
    , pt_ratio_vs_det_pt_rt{{
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_00_02", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_02_04", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_04_06", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_06_08", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_08_10", "" }
        }}
    , pt_ratio_vs_det_pt_rt_track_cut_20{{
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_00_02_track_cut_20", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_02_04_track_cut_20", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_04_06_track_cut_20", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_06_08_track_cut_20", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_08_10_track_cut_20", "" }
        }}
    , pt_ratio_vs_det_pt_rt_track_cut_30{{
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_00_02_track_cut_30", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_02_04_track_cut_30", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_04_06_track_cut_30", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_06_08_track_cut_30", "" },
        { 30, 0., 60., "pt_ratio_vs_det_pt_rt_08_10_track_cut_30", "" }
        }}
    , det_over_part_pt_for_part_pt{{
        { { 6.9,  8.2}, { 100, 0., 2., "det_over_part_pt_for_part_pt_69_82", "" } },
        { { 8.2,  9.7}, { 100, 0., 2., "det_over_part_pt_for_part_pt_82_97", "" } },
        { { 9.7, 11.5}, { 100, 0., 2., "det_over_part_pt_for_part_pt_97_115", "" } },
        { {11.5, 13.6}, { 100, 0., 2., "det_over_part_pt_for_part_pt_115_136", "" } },
        { {13.6, 16.1}, { 100, 0., 2., "det_over_part_pt_for_part_pt_136_161", "" } },
        { {16.1, 19.0}, { 100, 0., 2., "det_over_part_pt_for_part_pt_161_190", "" } },
        { {19.0, 22.5}, { 100, 0., 2., "det_over_part_pt_for_part_pt_190_225", "" } },
        { {22.5, 26.6}, { 100, 0., 2., "det_over_part_pt_for_part_pt_225_266", "" } },
        { {26.6, 31.4}, { 100, 0., 2., "det_over_part_pt_for_part_pt_266_314", "" } },
        { {31.4, 37.2}, { 100, 0., 2., "det_over_part_pt_for_part_pt_314_372", "" } },
        { {37.2, 44.0}, { 100, 0., 2., "det_over_part_pt_for_part_pt_372_440", "" } },
        { {44.0, 52.0}, { 100, 0., 2., "det_over_part_pt_for_part_pt_440_520", "" } },
        }}
  {
    add(detector_jet_pt);
    add(detector_jet_pt_fine);

    const char *s = getenv("max_dr");
    max_dr = strtod(s ? s : "0.2", NULL);
    s = getenv("track_thin_out_fraction");
    track_thin_out_fraction = atof(s ? s : "-1");
    s = getenv("track_thin_out_use_scalar_pt");
    track_thin_out_use_scalar_pt = atoi(s ? s : "0");
    s = getenv("jet_pt_shift_sign");
    jet_pt_shift_sign = atoi(s ? s : "+1");
    always_assert(abs(jet_pt_shift_sign) == 1);
    s = getenv("jet_charged_pt_shift");
    jet_charged_pt_shift = atof(s ? s : "0");
    s = getenv("jet_neutral_pt_shift");
    jet_neutral_pt_shift = atof(s ? s : "0");
    s = getenv("ue_shift_fraction_detector");
    ue_shift_fraction_detector = atof(s ? s : "0");
    s = getenv("ue_shift_fraction_particle");
    ue_shift_fraction_particle = atof(s ? s : "0");
  }

  bool disable_extra_corr;
  bool is_embedding;
  bool is_embedding_set;
  std::mt19937 gen;
  StJetCut detector_jet_cut;
  StJetCut particle_jet_cut;
  Histo1D detector_jet_pt;
  Histo1D detector_jet_pt_fine;
  Histo1D particle_jet_pt;
  Histo1D particle_jet_pt_fine;
  Histo2D response;
  Histo2D response_fine;
  Histo1D fake;
  Histo1D miss;
  Histo2D eta_corr;
  Histo2D eta_corr_comb;
  Histo2D jet_neut_sumpt_corr;
  Histo2D jet_chg_sumpt_corr;
  Histo2D jet_neut_mult_corr;
  Histo2D jet_chg_mult_corr;
  Histo2D ue_sumpt_corr;
  Histo2D ue_neut_sumpt_corr;
  Histo2D ue_chg_sumpt_corr;
  Histo2D ue_sumpt_corr_cone0;
  Histo2D ue_sumpt_corr_cone1;
  Histo2D ue_neut_mult_corr;
  Histo2D ue_chg_mult_corr;
  Histo1D jet_dr_comb;
  Histo1D jet_dr_matched;
  Histo1D matched_detector_jet_pt;
  Histo1D matched_detector_jet_pt_fine;
  Histo1D matched_particle_jet_pt;
  Histo1D matched_particle_jet_pt_fine;
  Profile1D pt_ratio_vs_det_pt;
  std::array<Profile1D, 5> pt_ratio_vs_det_pt_rt;
  std::array<Profile1D, 5> pt_ratio_vs_det_pt_rt_track_cut_20;
  std::array<Profile1D, 5> pt_ratio_vs_det_pt_rt_track_cut_30;
  std::map<std::pair<double, double>, Histo1D> det_over_part_pt_for_part_pt;
  double max_dr;
  double track_thin_out_fraction;
  int track_thin_out_use_scalar_pt;
  int jet_pt_shift_sign;
  double jet_charged_pt_shift;
  double jet_neutral_pt_shift;
  double ue_shift_fraction_detector;
  double ue_shift_fraction_particle;

  double thin_out_tracks_adj(StJetCandidate *detector_jet, double factor) {
    std::uniform_real_distribution<> R(0., 1.);
    TVector3 final_momentum = detector_jet->momentum();
    double scalar_pt_adjustment = 0.;

    always_assert(fabs(detector_jet->momentum().Perp() - detector_jet->pt()) < 1e-5 /* GeV */);

    for (TObject *_track : detector_jet->tracks()) {
      StJetTrack *track = dynamic_cast<StJetTrack*>(_track);

      always_assert(fabs(track->momentum().Perp() - track->pt()) < 1e-5 /* GeV */);

      if (R(gen) <= factor) {
        scalar_pt_adjustment += track->pt();
        final_momentum -= track->momentum();
      }
    }

    if (track_thin_out_use_scalar_pt) {
      return scalar_pt_adjustment;
    } else {
      double vector_pt_adjustment = detector_jet->pt() - final_momentum.Perp();
      if (fabs(vector_pt_adjustment) < 1e-5 /* GeV */) {
        vector_pt_adjustment = 0;
      }
      return vector_pt_adjustment;
    }
  }

  void fill(const StJetPlotEvent& e, double weight) override
  {
    std::unordered_map<StJetCandidate*, double> pt_map;

    for (TObject *_detector_jet : e.jetEvent.vertex()->jets()) {
      StJetCandidate *detector_jet = dynamic_cast<StJetCandidate*>(_detector_jet);

      // Store corrected jet pt value
      pt_map[detector_jet] = detector_jet->pt();
      if (track_thin_out_fraction >= 0.) {
        always_assert(track_thin_out_fraction <= 1.);

        // Track thin out is going to invalidate the R_T returned by
        // rt(), so let's not allow using thin out and shifts
        // together.
        always_assert(jet_charged_pt_shift == 0);
        always_assert(jet_neutral_pt_shift == 0);

        pt_map[detector_jet] -= thin_out_tracks_adj(detector_jet, track_thin_out_fraction);
      }

      pt_map[detector_jet] += jet_pt_shift_sign * detector_jet->pt() * sqrt(
        sqr((1 - detector_jet->rt()) * jet_charged_pt_shift)
        +
        sqr(detector_jet->rt() * jet_neutral_pt_shift)
        );

      float detector_pt_corr = - ue_shift_fraction_detector * detector_jet->area() * detector_jet->ueDensity()["OffAxisConesR050"];
      pt_map[detector_jet] += detector_pt_corr;

      if (detector_jet_cut(e, detector_jet)) {
        detector_jet_pt.fill(pt_map[detector_jet], weight * e.det_weight.at(detector_jet));
        detector_jet_pt_fine.fill(pt_map[detector_jet], weight * e.det_weight.at(detector_jet));
      }
    }

    if (e.is_embedding()) {
      fill_embedding(e, weight, pt_map);
    }

    if (is_embedding_set) {
      always_assert(is_embedding == e.is_embedding());
    } else {
      is_embedding = e.is_embedding();
      is_embedding_set = true;
    }
  }

  void fill_ue_histograms(const StUeOffAxisConesJet *detector_uejet, const StUeOffAxisConesJet *particle_uejet, double weight)
  {
    ue_sumpt_corr.fill(detector_uejet->cone(0)->pt() + detector_uejet->cone(1)->pt(), particle_uejet->cone(0)->pt() + particle_uejet->cone(1)->pt(), weight);
    ue_sumpt_corr_cone0.fill(detector_uejet->cone(0)->pt(), particle_uejet->cone(0)->pt(), weight);
    ue_sumpt_corr_cone1.fill(detector_uejet->cone(1)->pt(), particle_uejet->cone(1)->pt(), weight);
    unsigned int neutral_ue_particles_mult = 0;
    unsigned int charged_ue_particles_mult = 0;
    double particle_ue_neut_pt = 0.;
    double particle_ue_chg_pt = 0.;
    for(int cone_id = 0; cone_id <= 1; cone_id++) {
      for(const TObject *_p : particle_uejet->cone(cone_id)->particles()) {
        const StJetParticle *p = dynamic_cast<const StJetParticle*>(_p);
        always_assert(p);
        double charge = TDatabasePDG::Instance()->GetParticle(p->pdg())->Charge();
        if (charge == 0) {
          neutral_ue_particles_mult++;
          particle_ue_neut_pt += p->pt();
        } else {
          charged_ue_particles_mult++;
          particle_ue_chg_pt += p->pt();
        }
      }
    }
    unsigned int particle_mult = particle_uejet->cone(0)->numberOfParticles() + particle_uejet->cone(1)->numberOfParticles();
    always_assert(particle_mult == neutral_ue_particles_mult + charged_ue_particles_mult);
    // skip values with zero pt at both detector and particle levels, to remove
    // hot spot from the plot for nicer z axis scale
    double detector_ue_tower_pt = get_ue_tower_sumpt(detector_uejet);
    double detector_ue_track_pt = get_ue_track_sumpt(detector_uejet);
    if ((detector_ue_tower_pt != 0) || (particle_ue_neut_pt != 0)) {
      ue_neut_sumpt_corr.fill(detector_ue_tower_pt, particle_ue_neut_pt, weight);
    }
    if ((detector_ue_track_pt != 0) || (particle_ue_chg_pt != 0)) {
      ue_chg_sumpt_corr.fill(detector_ue_track_pt, particle_ue_chg_pt, weight);
    }
    ue_neut_mult_corr.fill(get_ue_tower_mult(detector_uejet), neutral_ue_particles_mult, weight);
    ue_chg_mult_corr.fill(get_ue_track_mult(detector_uejet), charged_ue_particles_mult, weight);
  }

  void fill_embedding(const StJetPlotEvent& e, double weight, std::unordered_map<StJetCandidate*, double> &pt_map)
  {
    std::vector<JetPair> pairs;
    std::vector<StJetCandidate*> detector_jets, particle_jets;
    std::unordered_map<StJetCandidate*, StUeOffAxisConesJet*> jet_to_uejet_map;
    unsigned int detector_ix = 0;
    for (TObject *_detector_jet : e.jetEvent.vertex()->jets()) {
      StJetCandidate *detector_jet = dynamic_cast<StJetCandidate*>(_detector_jet);
      if (detector_jet_cut(e, detector_jet)) {
        detector_jets.push_back(detector_jet);
        for (TObject *_particle_jet : e.jetParticleEvent.vertex()->jets()) {
          StJetCandidate *particle_jet = dynamic_cast<StJetCandidate*>(_particle_jet);
          if (particle_jet_cut(e, particle_jet)) {
            JetPair p { detector_jet, particle_jet };
            pairs.push_back(p);
            jet_dr_comb.fill(p.dR(), weight * e.det_weight.at(detector_jet));
            eta_corr_comb.fill(detector_jet->eta(), particle_jet->eta(), weight * e.det_weight.at(detector_jet));
          }
        }

        StUeOffAxisConesJet *detector_uejet = static_cast<StUeOffAxisConesJet*>(e.ueEvent.vertex()->ueJets()[detector_ix]);
        always_assert(jet_to_uejet_map.count(detector_jet) == 0);
        jet_to_uejet_map[detector_jet] = detector_uejet;
      }
      detector_ix++;
    }
    unsigned int particle_ix = 0;
    for (TObject *_particle_jet : e.jetParticleEvent.vertex()->jets()) {
      StJetCandidate *particle_jet = dynamic_cast<StJetCandidate*>(_particle_jet);
      if (particle_jet_cut(e, particle_jet)) {
        float particle_pt_ue_corr = - ue_shift_fraction_particle * particle_jet->area() * particle_jet->ueDensity()["OffAxisConesR050"];
        pt_map[particle_jet] = particle_jet->pt()  + particle_pt_ue_corr;
        particle_jet_pt.fill(pt_map[particle_jet], e.original_weight);
        particle_jet_pt_fine.fill(pt_map[particle_jet], e.original_weight);
        particle_jets.push_back(particle_jet);

        StUeOffAxisConesJet *particle_uejet = static_cast<StUeOffAxisConesJet*>(e.ueParticleEvent.vertex()->ueJets()[particle_ix]);
        always_assert(jet_to_uejet_map.count(particle_jet) == 0);
        jet_to_uejet_map[particle_jet] = particle_uejet;
      }
      particle_ix++;
    }
    std::sort(pairs.begin(), pairs.end(), JetPair::cmp_dR); // sort in descending order by dR
    while(!pairs.empty())
    {
      JetPair m = pairs.back();
      std::vector<JetPair> new_pairs;
      std::remove_copy_if(pairs.begin(), pairs.end(), std::back_inserter(new_pairs),
          [&](JetPair& p) { return (p.first == m.first) || (p.second == m.second); });
      pairs = std::move(new_pairs);
      if (m.dR() >= max_dr) break;
      StJetCandidate *detector_jet = m.first;
      StJetCandidate *particle_jet = m.second;

      if (
        // Failing any of these will put a pair into an overflow bin
        // of the "response" histogram, which is safe. But we also
        // dont want to fill it into the "matched_*" histograms.
        (pt_map[particle_jet] >= response.yMin())
        && (pt_map[particle_jet] < response.yMax())
        && (pt_map[detector_jet] >= response.xMin())
        && (pt_map[detector_jet] < response.xMax())
        ) {
        detector_jets.erase(std::find(detector_jets.begin(), detector_jets.end(), detector_jet));
        particle_jets.erase(std::find(particle_jets.begin(), particle_jets.end(), particle_jet));
        response.fill(pt_map[detector_jet], pt_map[particle_jet], weight * e.det_weight.at(detector_jet));
        response_fine.fill(pt_map[detector_jet], pt_map[particle_jet], weight * e.det_weight.at(detector_jet));
        matched_detector_jet_pt.fill(pt_map[detector_jet], weight * e.det_weight.at(detector_jet));
        matched_detector_jet_pt_fine.fill(pt_map[detector_jet], weight * e.det_weight.at(detector_jet));
        matched_particle_jet_pt.fill(pt_map[particle_jet], weight * e.det_weight.at(detector_jet));
        matched_particle_jet_pt_fine.fill(pt_map[particle_jet], weight * e.det_weight.at(detector_jet));
      }

      eta_corr.fill(detector_jet->eta(), particle_jet->eta(), weight * e.det_weight.at(detector_jet));
      unsigned int detector_jet_track_mult = detector_jet->numberOfTracks();
      unsigned int detector_jet_tower_mult = 0;
      double detector_jet_tower_pt = 0.;
      double detector_jet_track_pt = 0.;
      for(const TObject *_p : detector_jet->towers()) {
        const StJetTower *p = dynamic_cast<const StJetTower*>(_p);
        always_assert(p);
        if (p->pt() > 0.001) { // don't count subtracted towers
          detector_jet_tower_mult++;
        }
        detector_jet_tower_pt += p->pt();
      }
      for(const TObject *_p : detector_jet->tracks()) {
        const StJetTrack *p = dynamic_cast<const StJetTrack*>(_p);
        always_assert(p);
        detector_jet_track_pt += p->pt();
      }
      unsigned int neutral_jet_particles_mult = 0;
      unsigned int charged_jet_particles_mult = 0;
      double particle_jet_neut_pt = 0.;
      double particle_jet_chg_pt = 0.;
      for(const TObject *_p : particle_jet->particles()) {
        const StJetParticle *p = dynamic_cast<const StJetParticle*>(_p);
        always_assert(p);
        double charge = TDatabasePDG::Instance()->GetParticle(p->pdg())->Charge();
        if (charge == 0) {
          neutral_jet_particles_mult++;
          particle_jet_neut_pt += p->pt();
        } else {
          charged_jet_particles_mult++;
          particle_jet_chg_pt += p->pt();
        }
      }
      jet_neut_sumpt_corr.fill(detector_jet_tower_pt, particle_jet_neut_pt, weight * e.det_weight.at(detector_jet));
      jet_chg_sumpt_corr.fill(detector_jet_track_pt, particle_jet_chg_pt, weight * e.det_weight.at(detector_jet));
      jet_neut_mult_corr.fill(detector_jet_tower_mult, neutral_jet_particles_mult, weight * e.det_weight.at(detector_jet));
      jet_chg_mult_corr.fill(detector_jet_track_mult, charged_jet_particles_mult, weight * e.det_weight.at(detector_jet));

      StUeOffAxisConesJet *detector_uejet = jet_to_uejet_map[detector_jet];
      StUeOffAxisConesJet *particle_uejet = jet_to_uejet_map[particle_jet];
      fill_ue_histograms(detector_uejet, particle_uejet, weight * e.det_weight.at(detector_jet));

      pt_ratio_vs_det_pt.fill(pt_map[detector_jet], pt_map[detector_jet] / pt_map[particle_jet], weight * e.det_weight.at(detector_jet));
      int rtbin = detector_jet->rt() / 0.2;
      if (rtbin == 5) rtbin = 4; // R_T = 1 case
      pt_ratio_vs_det_pt_rt[rtbin].fill(pt_map[detector_jet], pt_map[detector_jet] / pt_map[particle_jet], weight * e.det_weight.at(detector_jet));
      static auto track_pt_cut_20 = make_max_track_pt_cut(20 /* GeV */);
      static auto track_pt_cut_30 = make_max_track_pt_cut(30 /* GeV */);
      if (track_pt_cut_20(e, detector_jet)) {
        pt_ratio_vs_det_pt_rt_track_cut_20[rtbin].fill(pt_map[detector_jet], pt_map[detector_jet] / pt_map[particle_jet], weight * e.det_weight.at(detector_jet));
      }
      if (track_pt_cut_30(e, detector_jet)) {
        pt_ratio_vs_det_pt_rt_track_cut_30[rtbin].fill(pt_map[detector_jet], pt_map[detector_jet] / pt_map[particle_jet], weight * e.det_weight.at(detector_jet));
      }
      fill_hstack1d(det_over_part_pt_for_part_pt, pt_map[particle_jet], pt_map[detector_jet] / pt_map[particle_jet], weight * e.det_weight.at(detector_jet));

      jet_dr_matched.fill(m.dR(), weight * e.det_weight.at(detector_jet));
    }
    for(StJetCandidate* detector_jet : detector_jets) {
      fake.fill(pt_map[detector_jet], weight * e.det_weight.at(detector_jet));
    }
    for(StJetCandidate* particle_jet : particle_jets) {
      miss.fill(pt_map[particle_jet], e.original_weight);
    }
  }

  void finalize() override
  {
    always_assert(is_embedding_set);
    if (is_embedding) {
      add(particle_jet_pt);
      add(particle_jet_pt_fine);
      add(response);
      add(response_fine);
      add(fake);
      add(miss);
      add(eta_corr);
      add(eta_corr_comb);
      if (!disable_extra_corr) {
        add(jet_neut_sumpt_corr);
        add(jet_chg_sumpt_corr);
        add(jet_neut_mult_corr);
        add(jet_chg_mult_corr);
        add(ue_sumpt_corr);
        add(ue_neut_sumpt_corr);
        add(ue_chg_sumpt_corr);
        add(ue_sumpt_corr_cone0);
        add(ue_sumpt_corr_cone1);
        add(ue_chg_mult_corr);
        add(ue_neut_mult_corr);
        add(jet_dr_comb);
      }
      add(jet_dr_matched);
      add(matched_detector_jet_pt);
      add(matched_detector_jet_pt_fine);
      add(matched_particle_jet_pt);
      add(matched_particle_jet_pt_fine);
      add(pt_ratio_vs_det_pt);
      for(auto &p : pt_ratio_vs_det_pt_rt)
        add(p);
      for(auto &p : pt_ratio_vs_det_pt_rt_track_cut_20)
        add(p);
      for(auto &p : pt_ratio_vs_det_pt_rt_track_cut_30)
        add(p);
      for(auto &p : det_over_part_pt_for_part_pt) {
        Histo1D &h = p.second;
        add(h);
      }
    }
  }
};
