{ lib }:

rec {
  ### Implement overridable attrsets

  # This is like lib.makeExtensibleWithCustomName, but allows to
  # override from within the fixed point as well.
  enableOverride = recursive_attrs:
    let
      override = overlay: enableOverride (lib.extends overlay (self: recursive_attrs self // { inherit override; }));
    in
      lib.fix' (self: recursive_attrs (self // { inherit override; }) // { inherit override; });

  extend = lhs: rhs: lhs // (rhs lhs);
  compose = list: final: prev: builtins.foldl' extend prev (map (x: x final) list);

  add_eval_scope = attrs:
    let
      names = builtins.attrNames attrs;
    in
      {
        eval_scope = lib.genAttrs names (name: attrs.${name});
      } // attrs;

  join = builtins.concatStringsSep " ";

  wrap_condor = import <nix-batch-submit/wrap.nix>;

  ### Implement list thinout (useful for manipulating runlists)

  #
  # Calculate the division modulo
  #
  mod = x: y: assert x >= 0; assert y > 0; x - (x / y) * y;

  #
  # Generate indices with gaps
  #
  # Example (offset = -1)
  #  -1   0   1   4   5   6
  #   ^       ^   ^
  #   \--len--/   |
  #   \---stride--/
  #
  gap_index_func = { stride, len, offset }:
    assert len > 0;
    (i: (mod i len) + (i / len) * stride + offset);
  gap_indices = gaps: num: builtins.genList (gap_index_func gaps) num;

  #
  # Take elements of list *xs* with indices from *ixs*
  #
  take_at = ixs: xs: map (builtins.elemAt xs) ixs;

  #
  # Take elements of *xs* with gaps described by *gaps* (see above)
  #
  gap_take = { stride, len, offset }@gaps: xs:
    assert stride > 0;
    let
      xs_len = builtins.length xs;
      # Number of elements taken from full strides
      num_full_strides = ((xs_len - offset) / stride) * len;
      # Number of all elements left after the last stride
      num_all_leftover = mod (xs_len - offset) stride;
      # Number of usable elements left after the last stride
      num_leftover = if num_all_leftover > len then len else num_all_leftover;

      num = num_full_strides + num_leftover;
    in
      take_at (gap_indices gaps num) xs;

  tests =
    assert mod 5 5 == 0;
    assert mod 5 4 == 1;
    assert gap_indices { stride = 5; len = 3; offset = 0; } 5 == [ 0 1 2 5 6 ];
    assert gap_indices { stride = 5; len = 2; offset = -1; } 6 == [ (-1) 0 4 5 9 10 ];
    assert take_at [ 0 2 7 ] [ 1 2 3 4 5 6 7 8 ] == [ 1 3 8 ];

    assert gap_take { stride = 5; len = 1; offset = 3; } [ "A" "B" "A" "B" ] == [ "B" ];
    assert gap_take { stride = 5; len = 1; offset = 3; } [ "A" "B" "A" "B" "A" ] == [ "B" ];
    assert gap_take { stride = 5; len = 1; offset = 3; } [ "A" "B" "A" "B" 1 2 3 4 "B" ] == [ "B" "B" ];

    assert gap_take { stride = 2; len = 2; offset = 1; } [ "A" "B" "A" "B" ] == [ "B" "A" "B" ];
    assert gap_take { stride = 3; len = 3; offset = 1; } [ "A" "B" "A" "B" ] == [ "B" "A" "B" ];
    assert gap_take { stride = 4; len = 4; offset = 1; } [ "A" "B" "A" "B" ] == [ "B" "A" "B" ];

    true;
}
