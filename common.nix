rec {
  extend = lhs: rhs: lhs // (rhs lhs);
  compose = list: self: super: builtins.foldl' extend super (map (x: x self) list);

  join = builtins.concatStringsSep " ";

  wrap_condor = import <nix-batch-submit/wrap.nix>;
}
