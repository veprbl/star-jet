// vim:et sw=2 sts=2

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <string>

#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TSVDUnfold.h>

#include <RooUnfoldResponse.h>
#include <RooUnfoldBayes.h>
#include <RooUnfoldSvd.h>
#include <RooUnfoldTUnfold.h>

using std::string;

void do_unfold(TFile &response_file, int jp, TFile *output)
{
  TH2D *response = (TH2D*)response_file.Get(Form("response_jp%i", jp));
  TH1D *particle_jet_pt = (TH1D*)response_file.Get(Form("particle_jet_pt_jp%i", jp));
  TH1D *detector_jet_pt = (TH1D*)response_file.Get(Form("detector_jet_pt_jp%i", jp));

  TH1D *dummy = (TH1D*)detector_jet_pt->Clone("dummy");
  dummy->Clear();
  // fakes subtraction is not implemented, so unfolding is performed on
  // projection of the reponse - it has no fakes
  TSVDUnfold unfold_tsvdunfold(response->ProjectionX(), dummy, particle_jet_pt, response);
  int kreg = 20;
  TH1D *h_unfold_tsvdunfold = unfold_tsvdunfold.Unfold(kreg);
  TH1D *h_unfold_tsvdunfold_d = unfold_tsvdunfold.GetD();
  TH1D *h_unfold_tsvdunfold_sv = unfold_tsvdunfold.GetSV();
  h_unfold_tsvdunfold->   SetName(Form("particle_jet_pt_jp%i_tsvdunfold", jp));
  h_unfold_tsvdunfold_d-> SetName(Form("particle_jet_pt_jp%i_tsvdunfold_d", jp));
  h_unfold_tsvdunfold_sv->SetName(Form("particle_jet_pt_jp%i_tsvdunfold_sv", jp));
  RooUnfoldResponse ru_response(detector_jet_pt, particle_jet_pt, response);
  RooUnfoldBayes roounfold_bayes(&ru_response, detector_jet_pt, 4);
  roounfold_bayes.SetName(Form("particle_jet_pt_jp%i_roounfold_bayes", jp));
  RooUnfoldSvd roounfold_svd(&ru_response, detector_jet_pt, kreg);
  roounfold_svd.SetName(Form("particle_jet_pt_jp%i_roounfold_svd", jp));
  //RooUnfoldTUnfold  unfold_tunfold(&ru_response, detector_jet_pt);
  //unfold_tunfold.SetName(Form("particle_jet_pt_jp%i_unfold_tunfold"), jp);

  output->WriteTObject(h_unfold_tsvdunfold);
  output->WriteTObject(h_unfold_tsvdunfold_d);
  output->WriteTObject(h_unfold_tsvdunfold_sv);
  output->WriteTObject(particle_jet_pt);
  output->WriteTObject(roounfold_bayes.Hreco());
  output->WriteTObject(roounfold_svd.Hreco());
  //output->WriteTObject(unfold_tunfold.Hreco());
}

int main(int argc, char **argv)
{
  if (argc < 3) {
    std::cerr << "Usage:" << std::endl << argv[0] << " response.root measurement.root" << std::endl;
    return EXIT_FAILURE;
  }

  string response_filename = argv[1];
  string measurement_filename = argv[2];

  TFile response_file(response_filename.c_str(), "READ");
  TFile measurement_file(measurement_filename.c_str(), "READ");
  TFile output("output.root", "RECREATE");

  do_unfold(response_file, 0, &output);
  do_unfold(response_file, 1, &output);
  do_unfold(response_file, 2, &output);

  return EXIT_SUCCESS;
}
