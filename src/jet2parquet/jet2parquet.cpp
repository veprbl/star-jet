#include <cstdlib>
#include <iostream>
#include <string>

#include <memory>

#include <arrow/api.h>
#include <arrow/io/api.h>
#include <arrow/util/logging.h> // for ARROW_CHECK

#include <parquet/arrow/writer.h>

#include <TFile.h>
#include <TTree.h>

#include <StSpinPool/StJetEvent/StJetCandidate.h>
#include <StSpinPool/StJetEvent/StJetEvent.h>
#include <StSpinPool/StJetEvent/StJetVertex.h>
#include <StSpinPool/StJetEvent/StJetParticle.h>
#include <StSpinPool/StJetEvent/StJetTower.h>
#include <StSpinPool/StJetEvent/StJetTrack.h>
#include <StSpinPool/StJetSkimEvent/StJetSkimEvent.h>

#include "../common/jp_match.h"
#include "../common/always_assert.h"

using std::string;

const string JETS_EXT = ".jets.root";
const string SKIM_EXT = ".skim.root";
const string PARQUET_EXT = ".jets.parquet";

string replace_filename_prefix(const string& jetfile, const string &new_prefix)
{
  auto last = jetfile.size() - JETS_EXT.size();
  always_assert(jetfile.substr(last) == JETS_EXT);
  return jetfile.substr(0, last) + new_prefix;
}

#include "../common/pq_column_helper.ipp"

DEFINE_COLUMN(runNumber, arrow::UInt64Type);
DEFINE_COLUMN(number, arrow::UInt64Type);

DEFINE_COLUMN(bx48, arrow::UInt8Type);
DEFINE_COLUMN(bx7, arrow::UInt8Type);
DEFINE_COLUMN(fillId, arrow::UInt16Type);
DEFINE_COLUMN(vertexRanking, arrow::FloatType);
DEFINE_COLUMN(vertexX, arrow::FloatType);
DEFINE_COLUMN(vertexY, arrow::FloatType);
DEFINE_COLUMN(vertexZ, arrow::FloatType);
DEFINE_COLUMN(vpdEastHits, arrow::UInt8Type);
DEFINE_COLUMN(vpdTdiff, arrow::FloatType);
DEFINE_COLUMN(vpdTstart, arrow::FloatType);
DEFINE_COLUMN(vpdWestHits, arrow::UInt8Type);
DEFINE_COLUMN(vpdZvertex, arrow::FloatType);
DEFINE_COLUMN(zdcCoincidenceRate, arrow::FloatType);
DEFINE_LIST_COLUMN(eventFiredTriggers, arrow::UInt32Type);
DEFINE_LIST_COLUMN(eventShouldFiredTriggers, arrow::UInt32Type);

DEFINE_LIST_COLUMN(jetArea, arrow::FloatType);
DEFINE_LIST_COLUMN(jetDetEta, arrow::FloatType);
DEFINE_LIST_COLUMN(jetE, arrow::FloatType);
DEFINE_LIST_COLUMN(jetEta, arrow::FloatType);
DEFINE_LIST_COLUMN(jetMatchToBarrelJP0, arrow::BooleanType);
DEFINE_LIST_COLUMN(jetMatchToBarrelJP1, arrow::BooleanType);
DEFINE_LIST_COLUMN(jetMatchToBarrelJP2, arrow::BooleanType);
DEFINE_LIST_COLUMN(jetPhi, arrow::FloatType);
DEFINE_LIST_COLUMN(jetPt, arrow::FloatType);
DEFINE_LIST_COLUMN(jetRt, arrow::FloatType);

DEFINE_LISTLIST_COLUMN(jetParticleEta, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetParticlePhi, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetParticlePt, arrow::FloatType);

DEFINE_LISTLIST_COLUMN(jetTowerEta, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetTowerId, arrow::UInt16Type);
DEFINE_LISTLIST_COLUMN(jetTowerPhi, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetTowerPt, arrow::FloatType);

DEFINE_LISTLIST_COLUMN(jetTrackBeta, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetTrackCharge, arrow::Int8Type);
DEFINE_LISTLIST_COLUMN(jetTrackEta, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetTrackPhi, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetTrackPt, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetTrackdEdx, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetTracknSigmaElectron, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetTracknSigmaKaon, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetTracknSigmaPion, arrow::FloatType);
DEFINE_LISTLIST_COLUMN(jetTracknSigmaProton, arrow::FloatType);

std::shared_ptr<arrow::Schema> schema;
std::unique_ptr<parquet::arrow::FileWriter> writer;

void verify_vertex_ordering(StJetEvent *jet_event) {
  int ix = 0;
  double prev_vertex_ranking;
  for (const TObject *_vertex : *jet_event->vertices()) {
    const StJetVertex *vertex = dynamic_cast<const StJetVertex*>(_vertex);
    if ((ix != 0) && (vertex->ranking() > prev_vertex_ranking)) {
      std::cerr << "Warning: vertices are not ordered by ranking:" << std::endl
                << "\t" << "ranking[" << (ix - 1) << "] = " << prev_vertex_ranking << std::endl
                << "\t" << "ranking[" << ix << "] = " << vertex->ranking() << std::endl
        ;
    }
    ix++;
    prev_vertex_ranking = vertex->ranking();
  }
}

void write_table() {
  std::vector<std::shared_ptr<arrow::Array>> arrays;

  for(BasePqColumn *column : columns) {
    column->finish();
    arrays.push_back(column->get_array());
  }

  std::shared_ptr<arrow::Table> table = arrow::Table::Make(schema, arrays);

  int64_t chunk_size = 2*1024*1024;
  PARQUET_THROW_NOT_OK(writer->WriteTable(*table, chunk_size));
}

int main(int argc, char **argv)
{
  if ((argc < 2) || (argc > 3)) {
    std::cerr << "Usage:" << std::endl << argv[0] << " inputfile.jets.root [ outputfile.jets.parquet ]" << std::endl;
    return EXIT_FAILURE;
  }

  StJetEvent *jet_event = nullptr;
  StJetEvent *particle_jet_event = nullptr;
  StJetSkimEvent *skim_event = nullptr;

  string jet_filename = argv[1];
  string skim_filename = replace_filename_prefix(jet_filename, SKIM_EXT);
  string out_filename;
  if (argc >= 3) {
    out_filename = argv[2];
  } else {
    out_filename = replace_filename_prefix(jet_filename, PARQUET_EXT);
  }

  // Prepare to read input files
  TFile jet_file(jet_filename.c_str());
  std::unique_ptr<TTree> jet_tree { dynamic_cast<TTree*>(jet_file.Get("jet")) };
  always_assert(jet_tree.get());
  TFile skim_file(skim_filename.c_str());
  std::unique_ptr<TTree> skim_tree { dynamic_cast<TTree*>(skim_file.Get("jetSkimTree")) };
  always_assert(skim_tree.get());

  jet_tree->SetBranchAddress("AntiKtR060NHits12", &jet_event);
  const bool is_embedding = jet_tree->GetBranch("AntiKtR060Particle");
  if (is_embedding) {
    jet_tree->SetBranchAddress("AntiKtR060Particle", &particle_jet_event);
  }
  skim_tree->SetBranchAddress("skimEventBranch", &skim_event);

  Long_t nentries = jet_tree->GetEntries();
  always_assert(nentries == skim_tree->GetEntries());

  if (nentries == 0) {
    std::cerr << "Zero events found in " << jet_filename << std::endl;
    return EXIT_FAILURE;
  }

  std::vector<std::shared_ptr<arrow::Field>> fields;
  for(BasePqColumn *column : columns) {
    fields.push_back(column->get_field());
  }
  schema = arrow::schema(fields);

  // Prepare to write output file
  std::shared_ptr<arrow::io::FileOutputStream> outfile;
  outfile = std::move(arrow::io::FileOutputStream::Open(out_filename).ValueOrDie());
  {
    parquet::WriterProperties::Builder builder;
    builder.compression(parquet::Compression::GZIP);
    std::shared_ptr<parquet::WriterProperties> writer_properties = builder.build();
    PARQUET_THROW_NOT_OK(parquet::arrow::FileWriter::Open(
                           *schema,
                           arrow::default_memory_pool(),
                           outfile,
                           writer_properties,
                           &writer
                           ));
  }

  // Loop over events
  for (Long_t entry = 0; entry < nentries; entry++) {
    jet_tree->GetEntry(entry);
    skim_tree->GetEntry(entry);

    always_assert(jet_event->runId() == skim_event->runId());
    always_assert(jet_event->eventId() == skim_event->eventId());

    always_assert(is_embedding == static_cast<bool>(skim_event->mcEvent()));

    verify_vertex_ordering(jet_event);

    // TODO: add value range check

    ARROW_CHECK_OK(runNumber.builder.Append(jet_event->runId()));
    ARROW_CHECK_OK(number.builder.Append(jet_event->eventId()));

    ARROW_CHECK_OK(bx48.builder.Append(skim_event->bx48()));
    ARROW_CHECK_OK(bx7.builder.Append(skim_event->bx7()));
    ARROW_CHECK_OK(fillId.builder.Append(skim_event->fill()));
    ARROW_CHECK_OK(vpdEastHits.builder.Append(skim_event->vpdEastHits()));
    ARROW_CHECK_OK(vpdTdiff.builder.Append(skim_event->vpdTdiff()));
    ARROW_CHECK_OK(vpdTstart.builder.Append(skim_event->vpdTstart()));
    ARROW_CHECK_OK(vpdWestHits.builder.Append(skim_event->vpdWestHits()));
    ARROW_CHECK_OK(vpdZvertex.builder.Append(skim_event->vpdZvertex()));
    ARROW_CHECK_OK(zdcCoincidenceRate.builder.Append(skim_event->zdcCoincidenceRate()));

    // List columns start with an empty list
    for(BasePqColumn *column : columns) {
      if (column->is_list()) {
        ARROW_CHECK_OK(column->append_list());
      }
    }
    
    for (const TObject *o : *skim_event->triggers()) {
      const StJetSkimTrig *trig = dynamic_cast<const StJetSkimTrig*>(o);
      int trig_id = trig->trigId();
      if (trig->didFire()) {
        ARROW_CHECK_OK(eventFiredTriggers.value_builder.Append(trig_id));
      }
      if (trig->shouldFire()) {
        ARROW_CHECK_OK(eventShouldFiredTriggers.value_builder.Append(trig_id));
      }
    }

    StJetVertex *vertex = jet_event->vertex();

    ARROW_CHECK_OK(vertexRanking.builder.Append(vertex->ranking()));
    ARROW_CHECK_OK(vertexX.builder.Append(vertex->position().x()));
    ARROW_CHECK_OK(vertexY.builder.Append(vertex->position().y()));
    ARROW_CHECK_OK(vertexZ.builder.Append(vertex->position().z()));

    for (const TObject *_jet : vertex->jets()) {
      const StJetCandidate *jet = dynamic_cast<const StJetCandidate*>(_jet);

      ARROW_CHECK_OK(jetArea.value_builder.Append(jet->area()));
      ARROW_CHECK_OK(jetDetEta.value_builder.Append(jet->detEta()));
      ARROW_CHECK_OK(jetE.value_builder.Append(jet->E()));
      ARROW_CHECK_OK(jetEta.value_builder.Append(jet->eta()));
      ARROW_CHECK_OK(jetMatchToBarrelJP0.value_builder.Append(jp_match(*skim_event, 0, *jet)));
      ARROW_CHECK_OK(jetMatchToBarrelJP1.value_builder.Append(jp_match(*skim_event, 1, *jet)));
      ARROW_CHECK_OK(jetMatchToBarrelJP2.value_builder.Append(jp_match(*skim_event, 2, *jet)));
      ARROW_CHECK_OK(jetPhi.value_builder.Append(jet->phi()));
      ARROW_CHECK_OK(jetPt.value_builder.Append(jet->pt()));
      ARROW_CHECK_OK(jetRt.value_builder.Append(jet->rt()));

      ARROW_CHECK_OK(jetParticleEta.sublist_builder.Append());
      ARROW_CHECK_OK(jetParticlePhi.sublist_builder.Append());
      ARROW_CHECK_OK(jetParticlePt.sublist_builder.Append());
      for (const TObject *_particle : jet->particles()) {
        const StJetParticle *particle = dynamic_cast<const StJetParticle*>(_particle);

        ARROW_CHECK_OK(jetParticleEta.value_builder.Append(particle->eta()));
        ARROW_CHECK_OK(jetParticlePhi.value_builder.Append(particle->phi()));
        ARROW_CHECK_OK(jetParticlePt.value_builder.Append(particle->pt()));
      }

      ARROW_CHECK_OK(jetTowerId.sublist_builder.Append());
      ARROW_CHECK_OK(jetTowerEta.sublist_builder.Append());
      ARROW_CHECK_OK(jetTowerPhi.sublist_builder.Append());
      ARROW_CHECK_OK(jetTowerPt.sublist_builder.Append());
      for (const TObject *_tower : jet->towers()) {
        const StJetTower *tower = dynamic_cast<const StJetTower*>(_tower);

        ARROW_CHECK_OK(jetTowerId.value_builder.Append(tower->id()));
        ARROW_CHECK_OK(jetTowerEta.value_builder.Append(tower->eta()));
        ARROW_CHECK_OK(jetTowerPhi.value_builder.Append(tower->phi()));
        ARROW_CHECK_OK(jetTowerPt.value_builder.Append(tower->pt()));
      }

      ARROW_CHECK_OK(jetTrackBeta.sublist_builder.Append());
      ARROW_CHECK_OK(jetTrackCharge.sublist_builder.Append());
      ARROW_CHECK_OK(jetTrackEta.sublist_builder.Append());
      ARROW_CHECK_OK(jetTrackPhi.sublist_builder.Append());
      ARROW_CHECK_OK(jetTrackPt.sublist_builder.Append());
      ARROW_CHECK_OK(jetTrackdEdx.sublist_builder.Append());
      ARROW_CHECK_OK(jetTracknSigmaElectron.sublist_builder.Append());
      ARROW_CHECK_OK(jetTracknSigmaKaon.sublist_builder.Append());
      ARROW_CHECK_OK(jetTracknSigmaPion.sublist_builder.Append());
      ARROW_CHECK_OK(jetTracknSigmaProton.sublist_builder.Append());
      for (const TObject *_track : jet->tracks()) {
        const StJetTrack *track = dynamic_cast<const StJetTrack*>(_track);

        ARROW_CHECK_OK(jetTrackBeta.value_builder.Append(track->beta()));
        ARROW_CHECK_OK(jetTrackCharge.value_builder.Append(track->charge()));
        ARROW_CHECK_OK(jetTrackEta.value_builder.Append(track->eta()));
        ARROW_CHECK_OK(jetTrackPhi.value_builder.Append(track->phi()));
        ARROW_CHECK_OK(jetTrackPt.value_builder.Append(track->pt()));
        ARROW_CHECK_OK(jetTrackdEdx.value_builder.Append(track->dEdx()));
        ARROW_CHECK_OK(jetTracknSigmaElectron.value_builder.Append(track->nSigmaElectron()));
        ARROW_CHECK_OK(jetTracknSigmaKaon.value_builder.Append(track->nSigmaKaon()));
        ARROW_CHECK_OK(jetTracknSigmaPion.value_builder.Append(track->nSigmaPion()));
        ARROW_CHECK_OK(jetTracknSigmaProton.value_builder.Append(track->nSigmaProton()));
      }
    }
    
    int64_t megabytes_allocated = arrow::default_memory_pool()->bytes_allocated() / 1024. / 1024.;
    if (megabytes_allocated > 256 /* mb */) {
      write_table();
      std::cerr << megabytes_allocated << " mb\t" << entry << std::endl;
    }
  }

  write_table();
  PARQUET_THROW_NOT_OK(writer->Close());

  return EXIT_SUCCESS;
}
