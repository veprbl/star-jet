#pragma once

#include <StSpinPool/StJetEvent/StJetCandidate.h>
#include <StSpinPool/StJetSkimEvent/StJetSkimEvent.h>

#include "always_assert.h"

bool jp_match(const StJetSkimEvent& skimEvent, int jp, const StJetCandidate &jet) {
  always_assert((jp >= 0) && (jp <= 2));
  map<int,int> patches = skimEvent.barrelJetPatchesAboveTh(jp);

  for (auto p : patches) {
    int id = p.first;
    int adc __attribute__((unused)) = p.second;
    float eta, phi;
    always_assert(StJetCandidate::getBarrelJetPatchEtaPhi(id, eta, phi));
    double deta = jet.detEta() - eta;
    double dphi = TVector2::Phi_mpi_pi(jet.phi() - phi);
    if ((fabs(deta) < 0.6) && (fabs(dphi) < 0.6)) {
      return true;
    }
  }

  return false;
}
