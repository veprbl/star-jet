class BasePqColumn {
public:

  virtual arrow::Status append_list() = 0;
  virtual bool is_list() = 0;
  virtual std::shared_ptr<arrow::Field> get_field() = 0;
  virtual std::shared_ptr<arrow::Array> get_array() = 0;
  virtual void finish() = 0;
};

template <typename T>
class PqColumn : BasePqColumn {
public:

  std::shared_ptr<arrow::DataType> data_type = std::make_shared<T>();
  using builder_type = typename arrow::TypeTraits<T>::BuilderType;
  builder_type builder;
  std::shared_ptr<arrow::Field> field;
  std::shared_ptr<arrow::Array> array;

  explicit PqColumn(arrow::MemoryPool* pool, const char *name, std::vector<BasePqColumn*> &columns, bool nullable = false)
    : builder { pool }
  {
    field = arrow::field(name, data_type, nullable);
    columns.push_back(this);
  }

  arrow::Status append_list() override {
    std::terminate();
  }

  bool is_list() override {
    return false;
  }

  std::shared_ptr<arrow::Field> get_field() override {
    return field;
  }

  std::shared_ptr<arrow::Array> get_array() override {
    return array;
  }

  void finish() override {
    ARROW_CHECK_OK(builder.Finish(&array));
  }
};

template <typename T>
class PqListColumn : BasePqColumn {
public:

  std::shared_ptr<arrow::DataType> data_type = std::make_shared<T>();
  using builder_type = typename arrow::TypeTraits<T>::BuilderType;
  builder_type &value_builder;
  arrow::ListBuilder builder;
  std::shared_ptr<arrow::Field> field;
  std::shared_ptr<arrow::Array> array;

  explicit PqListColumn(arrow::MemoryPool* pool, const char *name, std::vector<BasePqColumn*> &columns)
    : value_builder(*new builder_type(pool))
    , builder { pool, std::unique_ptr<builder_type>(&value_builder) }
  {
    field = arrow::field(name, arrow::list(data_type), /* nullable = */ true);
    columns.push_back(this);
  }

  arrow::Status append_list() override {
    return builder.Append();
  }

  bool is_list() override {
    return true;
  }

  std::shared_ptr<arrow::Field> get_field() override {
    return field;
  }

  std::shared_ptr<arrow::Array> get_array() override {
    return array;
  }

  void finish() override {
    ARROW_CHECK_OK(builder.Finish(&array));
  }
};

template <typename T>
class PqListListColumn : BasePqColumn {
public:

  std::shared_ptr<arrow::DataType> data_type = std::make_shared<T>();
  using builder_type = typename arrow::TypeTraits<T>::BuilderType;
  builder_type &value_builder;
  arrow::ListBuilder &sublist_builder;
  arrow::ListBuilder builder;
  std::shared_ptr<arrow::Field> field;
  std::shared_ptr<arrow::Array> array;

  explicit PqListListColumn(arrow::MemoryPool* pool, const char *name, std::vector<BasePqColumn*> &columns)
    : value_builder(*new builder_type(pool))
    , sublist_builder(*new arrow::ListBuilder(pool, std::unique_ptr<builder_type>(&value_builder)))
    , builder { pool, std::unique_ptr<arrow::ListBuilder>(&sublist_builder) }
  {
    field = arrow::field(name, arrow::list(arrow::list(data_type)), /* nullable = */ true);
    columns.push_back(this);
  }

  arrow::Status append_list() override {
    return builder.Append();
  }

  bool is_list() override {
    return true;
  }

  std::shared_ptr<arrow::Field> get_field() override {
    return field;
  }

  std::shared_ptr<arrow::Array> get_array() override {
    return array;
  }

  void finish() override {
    ARROW_CHECK_OK(builder.Finish(&array));
  }
};

arrow::MemoryPool* pool = arrow::default_memory_pool();
std::vector<BasePqColumn*> columns;
#define DEFINE_COLUMN(name, type) \
  PqColumn<type> name { pool, #name, columns };
#define DEFINE_COLUMN_NULLABLE(name, type) \
  PqColumn<type> name { pool, #name, columns, true };
#define DEFINE_LIST_COLUMN(name, type) \
  PqListColumn<type> name { pool, #name, columns };
#define DEFINE_LISTLIST_COLUMN(name, type) \
  PqListListColumn<type> name { pool, #name, columns };
