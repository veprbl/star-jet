#pragma once

namespace {

void print_assert(const char* exp, const char* file, int line)
{
  std::cerr << "Assertion '" << exp << "' failed in '" << file << "' at line '" << line << "'" << std::endl;
  std::terminate();
}

#define always_assert(exp) ((exp) ? (void)0 : print_assert(#exp, __FILE__, __LINE__))

}
