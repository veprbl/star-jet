# This is intended to provide PDF sets that are missing in nixpkgs 20.09

{ lib, stdenv, fetchurl, lhapdf }:

let
  mkPdfSet = name: sha256:
    stdenv.mkDerivation {
      inherit name;

      src = fetchurl {
        url = "https://lhapdfsets.web.cern.ch/lhapdfsets/current/${name}.tar.gz";
        inherit sha256;
      };

      preferLocalBuild = true;

      installPhase = ''
        mkdir -p $out/${name}/
        cp * $out/${name}/
      '';

      setupHook = lhapdf.pdf_sets.CT14nlo.setupHook;
    };
in
  lib.mapAttrs mkPdfSet {
    "CT18NLO" = "04y2p6vz484l3yv6381pfavqs3xh78h3jn6bg7ncp5vywwqp44n9";
    "CT18NNLO" = "1shkah5ma0hp101aklkz2p8n9y4i4sv6zwa5ifzyj3bgz1020l5f";
    "MSHT20nlo_as120" = "10y1a6iryahrafzdqskypjrnad6xxq08gm72pa9yc61xdy6andc6";
    "MSHT20nlo_as118" = "1qwbwcq8p4hrprz4ib18mp5142b0lbyyzc1bf5a4iq5jjvi5qm93";
    "MSHT20nnlo_as118" = "1yz0003ixjg97974648qba5d37vb4fhzzmq4k9xh4c37pnc3kgyn";
    "NNPDF40_nlo_as_01180" = "0399bnxvgl2h2ini198jmzjjb179f6dpxfv5x8imlfl515llivx2";
    "NNPDF40_nnlo_as_01180" = "1bhysjkji0k7xy9njarkfvxff05kjl1byhkknxv0875p2znzkpva";
  } // lhapdf.pdf_sets
