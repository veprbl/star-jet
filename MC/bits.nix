with {
  inherit (import <nixpkgs> {})
    lib
    rivet
    yoda
    ;
};
with import ../common.nix;

{ overrides ? self: super: {} }:

let
  this = self: super: with self; {
    # TODO merge into top level bits
    _yodamerge = add: runs: mkDerivation {
      name = "${(builtins.head runs).name}-merge";

      buildInputs = [ yoda ];
      yodagzFiles = map (d: "${d}/output.yoda.gz") runs;
      passAsFile = [ "yodagzFiles" ];
      buildPhase = ''
        yodamerge${if add then " --add" else ""} -o output.yoda $(cat $yodagzFilesPath)
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm644 output.yoda.gz "$out"/output.yoda.gz
      '';

      passthru.title = (builtins.head runs).title;
    };
    yodamerge = _yodamerge false;
    yodaadd = _yodamerge true;

    currentMC = builtins.throw "currentMC is not set";
    # run currentMC as in *num_jobs* jobs with different seeds with parameters from *args*
    runMC = num_jobs: args:
      let
        runMC_seed = seed: self.currentMC (args // { inherit seed; });
        runMC_condor = seed: wrap_condor ((runMC_seed seed).overrideAttrs (_: {
          __slurm_oversubscribe = 1;
        }));
        seedlist = builtins.genList (i: 73313 + i) num_jobs;
      in
        yodamerge (map runMC_condor seedlist);

    needmode = builtins.throw "needmode is not set";
    # This is like runMC but slices partonic spectrum to generate enough statistics at high p_T
    runMC_splitbin = num_jobs: args:
      assert !(builtins.hasAttr "pt_max" args);
      assert !(builtins.hasAttr "pt_min" args);
      let
        mode0 = if needmode then { mode = "0"; } else {};
        mode1 = if needmode then { mode = "1"; } else {};
      in
        # XXX different runMC calls use same seedlist. This is probably ok because we use different cuts
        yodaadd [
          # split bin 0-10 into 0-3 and 3-10 because otherwise we need more
          # statistics to gain more precision, but if we do that we have high
          # weight events in the tails of the jet pt distribution
          (runMC num_jobs (args // { pt_min = "0"; pt_max = "3"; } // mode0))
          # pt_max=10 is high enough for soft reweighting factor to die down (at least at sqrt(s)=200 GeV)
          # so we can switch to mode1 next
          (runMC num_jobs (args // { pt_min = "3"; pt_max = "10"; } // mode0))
          # rest of the slices are pretty arbitrary
          (runMC num_jobs (args // { pt_min = "10"; pt_max = "20"; } // mode1))
          (runMC num_jobs (args // { pt_min = "20"; pt_max = "30"; } // mode1))
          (runMC num_jobs (args // { pt_min = "30"; pt_max = "40"; } // mode1))
          (runMC num_jobs (args // { pt_min = "40"; pt_max = "-1"; } // mode1))
          ];

    buildRivetAna = minimal: name: mkDerivation {
      inherit name;

      src = lib.sourceFilesBySuffices
        (./rivet-ana + ("/" + name))
        (if minimal then [ ".cc" ] else [ ".cc" ".plot" ]);

      dontSetPhases = true;
      buildInputs = [ rivet ];
      buildPhase = ''
        rivet-buildplugin Rivet${name}.so ${name}.cc
      '';
      installPhase = ''
        for f in Rivet${name}.so${lib.optionalString (!minimal) " ${name}.plot"}; do
          install -Dm644 $f $out/$f
        done
      '';
      setupHook = ./rivet-ana-hook.sh;
    };

    rivet_analyses_names = [
      "MC_DECAY_STUDY"
      "MC_JET_PARTON_MATCH"
      "MC_JET_VPDMB"
      "MC_PARTONIC_PT"
      "MC_UE"
    ];
    rivet_analyses = map (buildRivetAna true) rivet_analyses_names;
    rivet_analyses_minimal = map (buildRivetAna false) rivet_analyses_names;

    mkhtml_config = ./config.plot;
    mkhtml = yoda_files: extra_args: (super.mkhtml yoda_files extra_args).overrideAttrs (old: {
        buildInputs = (old.buildInputs or []) ++ rivet_analyses_minimal;
        });
  };
in
  import ../bits.nix { overrides = compose [ this overrides ]; }
