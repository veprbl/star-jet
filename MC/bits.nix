{ nixpkgs ? import <nixpkgs> {} }:

with {
  inherit (nixpkgs)
    lib
    callPackage
    python3
    python3Packages
    rivet
    yoda
    ;
};
with import ../common.nix { inherit (nixpkgs) lib; };

let
  this = final: prev: with final; {
    OUTPUT_DIRECTORY = "`cat ${builtins.getEnv "HOME"}/.mc_plots_output_path`";

    # TODO merge into top level bits
    _yodamerge = add: runs:
    let
      needPostProcessing = builtins.elem "MC_JET_PARTON_MATCH" rivet_analyses_names;
    in
    mkDerivation {
      name = "${(builtins.head runs).name}-merge";

      buildInputs = [ rivet ] ++ lib.optionals needPostProcessing [ python3 python3Packages.packaging python3Packages.yoda ] ++ rivet_built_analyses;
      yodagzFiles = map (d: "${d}/output.yoda.gz") runs;
      passAsFile = [ "yodagzFiles" ];
      buildPhase = ''
        rivet-merge${if add then "" else " --equiv"} -o output.yoda $(cat $yodagzFilesPath)
        ${lib.optionalString needPostProcessing
          "${rivet-ana/MC_JET_PARTON_MATCH/post-processing.py} output.yoda"}
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm644 output.yoda.gz "$out"/output.yoda.gz
      '';

      passthru.title = (builtins.head runs).title;
    };
    yodamerge = _yodamerge false;
    yodaadd = _yodamerge true;

    installOutputYodaGz = ''
      out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
      mkdir "$out_data" || true
      CA_HASH="$(sha256sum output.yoda.gz | cut -d " " -f 1)"
      DEST="$out_data/$CA_HASH"
      ( time; env; /usr/bin/hostname ) >> "$DEST".info
      cp output.yoda.gz "$DEST"
      mkdir "$out" || true
      if [ -L "$out"/output.yoda.gz ]; then
        if [ "$(readlink "$out"/output.yoda.gz)" != "$DEST" ]; then
          echo Irreproducible build detected!
          exit 1
        fi
      else
        ln -s "$DEST" "$out"/output.yoda.gz
      fi
    '';

    filter_outliers = input: mkDerivation {
      name = "${input.name}-filtered";
      src = ./filter-outliers.py;
      nativeBuildInputs = [
        python3
        python3Packages.numpy
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp "$src" ./filter-outliers.py
        ./filter-outliers.py \
          "${input}"/output.yoda.gz \
          -o output.yoda
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      '';
    };

    DEFAULT_NUM_EVENTS = 1000000;
    SQRT_S = 200. /* GeV */;

    pdf_sets = callPackage ./pdf_sets.nix {};

    currentMC = builtins.throw "currentMC is not set";
    # run currentMC as in *num_jobs* jobs with different seeds with parameters from *args*
    runMC = num_jobs: args:
      let
        _runMC = seed: submit_as_job (final.currentMC (args // { inherit seed; }));
        seedlist = builtins.genList (i: (args.seed or 73313) + i) num_jobs;
      in
        yodamerge (map _runMC seedlist);

    needmode = builtins.throw "needmode is not set";
    # This is like runMC but slices partonic spectrum to generate enough statistics at high p_T
    runMC_splitbin = num_jobs: args:
      assert !(builtins.hasAttr "pt_max" args);
      assert !(builtins.hasAttr "pt_min" args);
      let
        mode0 = if needmode then { mode = "0"; } else {};
        mode1 = if needmode then { mode = "1"; } else {};
      in
        # XXX different runMC calls use same seedlist. This is probably ok because we use different cuts
        yodaadd (map filter_outliers [
          # split bin 0-10 into 0-3 and 3-10 because otherwise we need more
          # statistics to gain more precision, but if we do that we have high
          # weight events in the tails of the jet pt distribution
          (runMC num_jobs (args // { pt_min = "0"; pt_max = "3"; } // mode0))
          # pt_max=10 is high enough for soft reweighting factor to die down (at least at sqrt(s)=200 GeV)
          # so we can switch to mode1 next
          (runMC (num_jobs * 4) (args // { pt_min = "3"; pt_max = "10"; num_events = (args.num_events or DEFAULT_NUM_EVENTS) / 4; } // mode0))
          # rest of the slices are pretty arbitrary
          (runMC num_jobs (args // { pt_min = "10"; pt_max = "15"; } // mode1))
          (runMC num_jobs (args // { pt_min = "15"; pt_max = "20"; } // mode1))
          (runMC num_jobs (args // { pt_min = "20"; pt_max = "25"; } // mode1))
          (runMC num_jobs (args // { pt_min = "25"; pt_max = "30"; } // mode1))
          (runMC num_jobs (args // { pt_min = "30"; pt_max = "35"; } // mode1))
          (runMC num_jobs (args // { pt_min = "35"; pt_max = "40"; } // mode1))
          (runMC num_jobs (args // { pt_min = "40"; pt_max = "-1"; } // mode1))
          ]);

    getRivetAnaPath = name: ./rivet-ana + ("/" + name);

    buildRivetAna = minimal: name: mkDerivation {
      inherit name;

      src = lib.sourceFilesBySuffices
        (getRivetAnaPath name)
        (if minimal then [ ".cc" ".yoda" ] else [ ".cc" ".plot" ".yoda" ]);

      dontSetPhases = true;
      buildInputs = [ rivet ];
      buildPhase = ''
        rivet-build Rivet${name}.so ${name}.cc
      '';
      installPhase = ''
        f="${name}.yoda"
        if [ -e "$f" ]; then
          install -Dm644 $f $out/$f
        fi
        for f in Rivet${name}.so${lib.optionalString (!minimal) " ${name}.plot"}; do
          install -Dm644 $f $out/$f
        done
      '';
      setupHook = ./rivet-ana-hook.sh;
    };

    rivet_analyses = [
      "MC_DECAY_STUDY"
      "MC_JET_PARTON_MATCH"
      "MC_JET_VPDMB"
      "MC_PARTONIC_PT"
      "MC_STAR_JETS:JET_R=0.5"
      "MC_STAR_JETS:JET_R=0.6"
      "STAR_2017_I1493842"
      "STAR_2021_PP200_JETS_PRELIM"
    ];
    # Remove the analysis options that follow after ":"
    rivet_analyses_names = map (ana_name: builtins.elemAt (builtins.split ":" ana_name) 0) rivet_analyses;
    rivet_built_analyses_names = lib.filter (name: builtins.pathExists (getRivetAnaPath name)) rivet_analyses_names;
    rivet_built_analyses = map (buildRivetAna true) rivet_built_analyses_names;
    rivet_built_analyses_minimal = map (buildRivetAna false) rivet_built_analyses_names;

    mkhtml_config = ./config.plot;
    mkhtml = yoda_files: extra_args: (prev.mkhtml yoda_files extra_args).overrideAttrs (old: {
        buildInputs = (old.buildInputs or []) ++ rivet_built_analyses_minimal;
        });
  };
in
  (import ../bits.nix { inherit nixpkgs; }).override this
