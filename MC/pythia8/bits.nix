with {
  inherit (import <nixpkgs> {})
    lib
    gzip
    lhapdf
    rivet
    pythia
    sacrifice
    yoda
    ;
};
with import ../../common.nix;

{ overrides ? self: super: {} }:

let
  this = self: super: with self; {
    inherit (pythia) version;

    runPythia8 = { num_events ? 100000, pt_min, pt_max, seed, tune ? "4C" }:
      mkDerivation rec {
        name = "pythia8-${pt_min}_${pt_max}";

        buildInputs = [ gzip rivet sacrifice lhapdf.pdf_sets.cteq6l1 ] ++ rivet_analyses;
        tune_code = {
          "4C" = 5;
          "AU2-CTEQ6L1" = 9;
        }.${tune};
        buildPhase = ''
          echo Running ${pt_min} ${pt_max}
          mkfifo pythia.hepmc
          run-pythia \
            --collision-energy 200 \
            -c "HardQCD:all=on" \
            -c "PhaseSpace:pTHatMin=${pt_min}" \
            -c "PhaseSpace:pTHatMax=${pt_max}" \
            -c "Random:setSeed=on" \
            -c "Random:seed=${toString seed}" \
            -c "Tune:preferLHAPDF=2" \
            -c "Tune:pp=$tune_code" \
            -n ${toString num_events} &
          rivet pythia.hepmc ${join (map (ana_name: "-a ${ana_name}") rivet_analyses_names)} -o output.yoda
          gzip output.yoda
        '';
        installPhase = ''
          install -Dm644 output.yoda.gz "$out"/output.yoda.gz
        '';
        passthru.title = builtins.replaceStrings ["_"] ["\\_"] name;
      };
    currentMC = runPythia8;
    needmode = false;
  };
in
  import ../bits.nix { overrides = compose [ this overrides ]; }
