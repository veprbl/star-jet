{ nixpkgs ? import <nixpkgs> {} }:

with {
  inherit (nixpkgs)
    lib
    gzip
    lhapdf
    pythia
    sacrifice
    yoda
    ;
};
with import ../../common.nix { inherit (nixpkgs) lib; };

let
  this = final: prev: with final; {
    inherit (pythia) pname version;

    rivet = nixpkgs.rivet.overrideAttrs (old: {
      # Rivet's '-a' option treats comma as separator between analysis name
      # list. Hence,
      #   -a ABS_ETA_BINS=0.0,0.4,0.8
      # is interpreted as
      #   -a ABS_ETA_BINS=0.0 -a 0.4 -a 0.8
      # Here we disable that behavior:
      postPatch = (old.postPatch or "") + ''
        substituteInPlace bin/rivet \
          --replace 'if "," in a:' 'if False:'
      '';
    });

    detroit_tune = [
      "PDF:pSet=17"
      "MultipartonInteractions:ecmRef=200"
      "MultipartonInteractions:bprofile=2"
      "MultipartonInteractions:pT0Ref=1.40"
      "MultipartonInteractions:ecmPow=0.135"
      "MultipartonInteractions:coreRadius=0.56"
      "MultipartonInteractions:coreFraction=0.78"
      "ColourReconnection:range=5.4"
    ];
    runPythia8 = {
      num_events ? DEFAULT_NUM_EVENTS,
      pt_min,
      pt_max,
      seed,
      tune ? "Monash 2013",
      pdf_set ? null,
      enable_had ? true,
      enable_mi ? true,
      disable_decay ? false,
      extra_tune ? []
    }:
      let _seed = { "73340" = 12; "73394" = 1; "73342" = 123; "73338" = 1234; "73332" = 321; "73353" = 32; "73404" = 23; "74079" = 2; "73990" = 3; "73839" = 4; "73470" = 5; "73675" = 6; "73613" = 7; "73318" = 8; "73314" = 9; "73676" = 234; "73410" = 240; "73350" = 236; "73326" = 237; "73405" = 238; "73395" = 239; }."${toString seed}" or seed; in
      mkDerivation rec {
        name = "pythia8-${pt_min}_${pt_max}";

        buildInputs = [
          gzip
          rivet
          sacrifice
          pdf_sets.${if pdf_set == null then "cteq6l1" else pdf_set}
        ] ++ rivet_built_analyses;
        tune_code = {
          "4C" = 5;
          "AU2-CTEQ6L1" = 9;
          "Monash 2013" = 14;
          "CUETP8S1-CTEQ6L1" = 15; # CMS UE Tune CUETP8S1-CTEQ6L1
          "A14 CTEQL1" = 19; # ATLAS A14 central tune with CTEQL1
          "Detroit" = 14; # uses Monash as a starting point
        }.${tune};
        buildPhase = ''
          echo Running ${pt_min} ${pt_max}
          mkfifo pythia.hepmc
          rivet pythia.hepmc ${join (map (ana_name: "-a ${ana_name}") rivet_analyses)} -o output.yoda &
          run-pythia \
            --collision-energy ${toString SQRT_S} \
            -c "HardQCD:all=on" \
            -c "PhaseSpace:pTHatMin=${pt_min}" \
            -c "PhaseSpace:pTHatMax=${pt_max}" \
            -c "Random:setSeed=on" \
            -c "Random:seed=${toString _seed}" \
            -c "Tune:preferLHAPDF=2" \
            -c "Tune:pp=$tune_code" ${lib.optionalString (tune == "Monash 2013") ''-c "Tune:ee=7" ''}\
            ${lib.optionalString (!enable_had) "-c \"HadronLevel:Hadronize=off\""} \
            ${lib.optionalString (!enable_mi)  "-c \"PartonLevel:MPI=off\""} \
            ${lib.optionalString disable_decay "-c \"HadronLevel:Decay=off\""} \
            ${builtins.concatStringsSep "" (map (par: "-c \"${par}\" ") ((if tune == "Detroit" then detroit_tune else []) ++ extra_tune))}\
            ${lib.optionalString (pdf_set != null) "-c \"PDF:pSet=LHAPDF6:${pdf_set}\""} \
            -n ${toString num_events} \
            || true
          # Wait for Rivet
          wait $!
          gzip output.yoda
        '';
        installPhase = ''
          ${installOutputYodaGz}
        '';
        passthru.title = builtins.replaceStrings ["_"] ["\\_"] name;
      };
    currentMC = runPythia8;
    needmode = false;
  };
in
  (import ../bits.nix { inherit nixpkgs; }).override this
