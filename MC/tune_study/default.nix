{ nixpkgs ? import <nixpkgs> {}, nobatch ? false }:

let

  inherit (import ../../common.nix { inherit (nixpkgs) lib; }) add_eval_scope join;

  overlay_pp200 = final: prev: {
    inherit nobatch;

    # disable other rivet analyses
    rivet_analyses = [
      "MC_PARTONIC_PT"
      "MC_STAR_JETS:JET_R=0.6"
      "STAR_2019_I1771348"
      "STAR_2021_PP200_JETS_PRELIM"
    ];
  };

  py6_pp200 = (import ../pythia6/bits.nix { inherit nixpkgs; }).override overlay_pp200;
  py8_pp200 = (import ../pythia8/bits.nix { inherit nixpkgs; }).override overlay_pp200;

  overlay_pp510 = final: prev: {
    inherit nobatch;

    SQRT_S = 510.0 /* GeV */;
    rivet_analyses = [
      "MC_PARTONIC_PT"
      "STAR_2012_JETXSection_510_Prelim"
    ];
  };

  py6_pp510 = (import ../pythia6/bits.nix { inherit nixpkgs; }).override overlay_pp510;
  py8_pp510 = (import ../pythia8/bits.nix { inherit nixpkgs; }).override overlay_pp510;

  py6_run = py6: num_jobs: args:
    (py6.set_title "Pythia ${py6.version} @ Perugia 2012, \\texttt{PARP(90)}$=${args.parp90 or "0.213"}$"
      (py6.runMC_splitbin num_jobs args));
  py6_run_parp91 = py6: num_jobs: args:
    (py6.set_title "Pythia ${py6.version} @ Perugia 2012, \\texttt{PARP(91)}$=${args.parp91 or "1"}$"
      (py6.runMC_splitbin num_jobs args));

  py8_run = py8: num_jobs: args:
    (py8.set_title "Pythia ${py8.version} @ ${args.tune}"
      (py8.runMC_splitbin num_jobs args));

in

(import ../bits.nix { inherit nixpkgs; }).override (final: prev: add_eval_scope (with final; {
  inherit nobatch;

  rivet_analyses =
    (overlay_pp200 final prev).rivet_analyses
    ++ (overlay_pp510 final prev).rivet_analyses;

  parp90_var = mkhtml' "--remove-options" { LegendXPos = 0.12; } [
    (py6_run py6_pp200 1 { parp90 = "0.1"; })
    (py6_run py6_pp200 1 { parp90 = "0.15"; })
    (py6_run py6_pp200 1 {})
    (py6_run py6_pp200 1 { parp90 = "0.24"; })
    (py8_run py8_pp200 1 { tune = "Monash 2013"; })

    (py6_run py6_pp510 1 { parp90 = "0.1"; })
    (py6_run py6_pp510 1 {})
    (py8_run py8_pp510 1 { tune = "Monash 2013"; })
  ];

  parp90_var_ref_starugia = mkhtml' "--remove-options -m '/MC_STAR_JETS/jet_pt.*'" { LegendXPos = 0.12; } [
    (py6_run py6_pp200 1 {})
    (py6_run py6_pp200 1 { parp90 = "0.1"; })
    (py6_run py6_pp200 1 { parp90 = "0.15"; })
    (py6_run py6_pp200 1 { parp90 = "0.24"; })
  ];

  parp91_var = mkhtml' "--remove-options" { LegendXPos = 0.12; } [
    (py6_run_parp91 py6_pp200 1 { parp91 = "1.0"; })
    (py6_run_parp91 py6_pp200 1 { parp91 = "0.5"; })
    (py6_run_parp91 py6_pp200 1 { parp91 = "0.25"; })
    (py6_run_parp91 py6_pp200 1 { parp91 = "2.0"; })
  ];

  py8_tune =
    let
      extra_tune = [
        "MultipartonInteractions:ecmRef=200"
        "MultipartonInteractions:bProfile=3"
        "MultipartonInteractions:pT0Ref=1.302"
        "MultipartonInteractions:expPow=1.841"
      ];
      appendTitle = extraTitle: plot: set_title (plot.title + extraTitle) plot;
    in
      mkhtml' "--mc-errs" { LegendXPos = 0.12; } [
        (py8_run py8_pp200 1 { tune = "Monash 2013"; })
        (py8_run py8_pp510 1 { tune = "Monash 2013"; })
        (py8_run py8_pp200 1 { tune = "CUETP8S1-CTEQ6L1"; })
        (py8_run py8_pp510 1 { tune = "CUETP8S1-CTEQ6L1"; })
        (py8_run py8_pp200 1 { tune = "A14 CTEQL1"; })
        (py8_run py8_pp510 1 { tune = "A14 CTEQL1"; })
        (appendTitle ", 200,3,1.302,1.841" (py8_run py8_pp200 1 { tune = "Monash 2013"; inherit extra_tune; }))
        (appendTitle ", 200,3,1.302,1.841" (py8_run py8_pp510 1 { tune = "Monash 2013"; inherit extra_tune; }))
        (appendTitle ", cteq6l1" (py8_run py8_pp200 1 { tune = "Monash 2013"; pdf_set = "cteq6l1"; }))
        (appendTitle ", cteq6l1" (py8_run py8_pp510 1 { tune = "Monash 2013"; pdf_set = "cteq6l1"; }))
      ];
}))
