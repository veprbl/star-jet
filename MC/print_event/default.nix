with (import ../pythia6/bits.nix {}).override (
  final: prev: assert builtins.hasAttr "rivet_analyses_names" prev; {
    nobatch = true;

    rivet_analyses = [
      "MC_PRINTEVENT"
    ];
  }
);

rec {
  default = runPythia6 { seed = 0; num_events = 10; mode = "1"; pt_min = "0"; pt_max = "-1"; };
}
