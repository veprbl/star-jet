{ nixpkgs ? import <nixpkgs> {}, nobatch ? false }:

with import ../../common.nix { inherit (nixpkgs) lib; };

let
  py6 = (import ../pythia6/bits.nix { inherit nixpkgs; }).override (final: prev: { inherit nobatch; });
  py8 = (import ../pythia8/bits.nix { inherit nixpkgs; }).override (final: prev: { inherit nobatch; });
  herwig7 = (import ../herwig7/bits.nix { inherit nixpkgs; }).override (final: prev: { inherit nobatch; });
in

(import ../bits.nix { inherit nixpkgs; }).override (final: prev: add_eval_scope (with final; {
  inherit nobatch;

  vpdmb_study_plot  = sample: title:
    mkhtml [
    ((rename sample "unbiased_" "comp_") // { title = "no bias"; })
    ((rename sample "vpdmb50_" "comp_") // { title = "vpdmb 50\\% acceptance E$||$W"; plot_options = ":LineColor=blue:LineStyle=dashed:LineDash='1.5pt 1.5pt':LineWidth=1.5pt"; })
    ((rename sample "vpdmb50ew_" "comp_") // { title = "vpdmb 50\\% acceptance E$\\&\\&$W"; plot_options = ":LineColor=blue"; })
    ((rename sample "vpdmb100_" "comp_") // { title = "vpdmb 100\\% acceptance E$||$W"; plot_options = ":LineColor=green:LineStyle=dashed:LineDash='1.5pt 1.5pt':LineWidth=1.5pt"; })
    ((rename sample "vpdmb100ew_" "comp_") // { title = "vpdmb 100\\% acceptance E$\\&\\&$W"; plot_options = ":LineColor=green"; })
    ] "--single -m '/MC_JET_VPDMB/comp_' PLOT:LegendXPos=0.3:Title='${title}'";

  plots = [
    (vpdmb_study_plot (py6.runMC_splitbin 10 {}) "Pythia ${py6.version} (Starugia 2012)")
    (vpdmb_study_plot (py6.runMC_splitbin 10 { parp90 = "0.24"; }) "Pythia ${py6.version} (Perugia 2012)")
    (vpdmb_study_plot (py8.runMC_splitbin 10 { tune = "Monash 2013"; }) "Pythia ${py8.version} (Monash 2013)")
    (vpdmb_study_plot (py8.runMC_splitbin 10 { tune = "AU2-CTEQ6L1"; }) "Pythia ${py8.version} (AU2-CTEQ6L1)")
    (vpdmb_study_plot (herwig7.runMC_splitbin 10 { }) "Herwig ${herwig7.version} (MPI disabled)")
    ];
}))
