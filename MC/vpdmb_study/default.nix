with {
  inherit (import <nixpkgs> {})
    lib
    python2
    python2Packages
    rivet
    yoda
    ;
};
with import ../../common.nix;
with import ../bits.nix {};

let
  py6 = import ../pythia6/bits.nix {};
  py8 = import ../pythia8/bits.nix {};
  herwig7 = import ../herwig7/bits.nix {};
in

rec {
  vpdmb_study_plot  = sample: title:
    mkhtml [
    ((rename sample "pt" "pt_comp") // { title = "no bias"; })
    ((rename sample "vpdmb50_pt" "pt_comp") // { title = "vpdmb 50\\% acceptance E$||$W"; plot_options = ":LineColor=blue:LineStyle=dashed:LineDash='1.5pt 1.5pt':LineWidth=1.5pt"; })
    ((rename sample "vpdmb50ew_pt" "pt_comp") // { title = "vpdmb 50\\% acceptance E$\\&\\&$W"; plot_options = ":LineColor=blue"; })
    ((rename sample "vpdmb100_pt" "pt_comp") // { title = "vpdmb 100\\% acceptance E$||$W"; plot_options = ":LineColor=green:LineStyle=dashed:LineDash='1.5pt 1.5pt':LineWidth=1.5pt"; })
    ((rename sample "vpdmb100ew_pt" "pt_comp") // { title = "vpdmb 100\\% acceptance E$\\&\\&$W"; plot_options = ":LineColor=green"; })
    ] "--single -m '/MC_JET_VPDMB/pt_comp' PLOT:LegendXPos=0.3:RatioPlotYMin=0.0:RatioPlotYMax=1.1:Title='${title}'";

  plots = [
    (vpdmb_study_plot (py6.runMC_splitbin 100 {}) "Pythia ${py6.version} (Starugia 2012)")
    (vpdmb_study_plot (py6.runMC_splitbin 100 { parp90 = "0.24"; }) "Pythia ${py6.version} (Perugia 2012)")
    (vpdmb_study_plot (py8.runMC_splitbin 100 { tune = "4C"; }) "Pythia ${py8.version} (4C)")
    (vpdmb_study_plot (py8.runMC_splitbin 100 { tune = "AU2-CTEQ6L1"; }) "Pythia ${py8.version} (AU2-CTEQ6L1)")
    (vpdmb_study_plot (herwig7.runMC_splitbin 100 { }) "Herwig ${herwig7.version} (MPI disabled)")
    ];
}
