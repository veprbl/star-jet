#!/usr/bin/env python3

import numpy as np
import yoda


def boxconv1d(v, resolution, axis=0):
    pad_width = [(0,)] * v.ndim
    pad_width[axis] = (resolution,)
    vp = np.pad(v, pad_width=pad_width)
    S = np.cumsum(vp, axis=axis)
    offset = resolution * 2
    slice_left = np.index_exp[:] * axis + np.index_exp[offset:]
    slice_right = np.index_exp[:] * axis + np.index_exp[:-offset]
    conv = S[slice_left] - S[slice_right]
    return conv


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Remove outliers.")
    parser.add_argument('input_filename')
    parser.add_argument('-o', dest="output_filename", required=True)
    args = parser.parse_args()

    aos = yoda.readYODA(args.input_filename)
    for ao in aos.values():
        if "_fine" not in ao.path(): continue
        if not isinstance(ao, yoda.Histo1D): continue
        bins = np.array(ao)
        numEntries = np.vectorize(lambda b: b.numEntries())(bins)
        cond = ((boxconv1d(numEntries, 5) == numEntries) | (boxconv1d(numEntries, 10) <= 3)) & (numEntries != 0)
        for b in bins[cond]:
            assert b.numEntries() <= 5
            b -= b
            assert b.sumW() == 0.0

    yoda.writeYODA(aos, args.output_filename)
