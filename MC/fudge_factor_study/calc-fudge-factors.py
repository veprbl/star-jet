#!/usr/bin/env python3

import argparse
import os
import math

import numpy as np
import yoda

parser = argparse.ArgumentParser()
parser.add_argument('reference', type=str)
parser.add_argument('bins', type=str, nargs='+', help="NOTE: order matters - last bin is to match reference in order to cancel reference's ff")
args = parser.parse_args()

reference = yoda.readYODA(args.reference)['/MC_PARTONIC_PT/partonic_pt']
bins = map(yoda.readYODA, args.bins)
bins = [d['/MC_PARTONIC_PT/partonic_pt'] for d in bins]

ys = []
dys = []

def xlimits(hist):
    return (np.min([b.xMin() for b in hist if b.numEntries() != 0]), np.max([b.xMax() for b in hist if b.numEntries() != 0]))

yodafiles = []

one = reference / reference
one.setPath('/MC_PARTONIC_PT/fudge_factors')
filename = "bin-{:.0f}-{:.0f}.yoda".format(*xlimits(reference))
yoda.writeYODA([one], filename)
yodafiles.append(filename)

# take ratios to reference curve
for _bin in bins:
    ratio = _bin / reference
    relevant_points = list(filter(lambda p: p.y() != 0 and not math.isnan(p.y()), ratio.points()))
    point_ys = [p.y() for p in relevant_points]
    ys.append(np.mean(point_ys))
    dys.append(np.std(point_ys))
    s = yoda.Scatter2D('/MC_PARTONIC_PT/fudge_factors')
    s.addPoints([(p.x(), p.y(), p.xErrs(), p.yErrs()) for p in relevant_points])
    filename = "bin-{:.0f}-{:.0f}.yoda".format(*xlimits(_bin))
    yoda.writeYODA([s], filename)
    yodafiles.append(filename)

factor = ys[-1] # cancel reference's ff
ys = np.array(ys) / factor
dys = np.array(dys) / factor
for y, dy, b in zip(ys, dys, args.bins):
    print(f"{y:.3f} +- {dy:.3f}\t{b}")

with open("yodafiles", "w") as fp:
    fp.write(" ".join(yodafiles))
