with {
  inherit (import <nixpkgs> {})
    lib
    python2
    python2Packages
    rivet
    yoda
    ;
};
with import ../../common.nix;
with import ../pythia6/bits.nix {
  overrides = self: super: assert builtins.hasAttr "rivet_analyses_names" super; {
    # disable other rivet analyses
    rivet_analyses_names = [
      "MC_PARTONIC_PT"
    ];
  };
};

rec {
  ckin3 = mkhtml [
    (runMC 100 { pt_min = "1.25"; pt_max = "-1"; mode = "1"; })
    (runMC 100 { pt_min = "2"; pt_max = "-1"; mode = "1"; })
    (runMC 100 { pt_min = "3"; pt_max = "-1"; mode = "1"; })
    ] "";

  embed_bin_bounds = map toString [ 2 3 4 5 7 9 11 15 20 25 35 (-1) ];
  embed_bins = builtins.genList
    (i: {
      pt_min = builtins.elemAt embed_bin_bounds i;
      pt_max = builtins.elemAt embed_bin_bounds (i+1);
      })
    (builtins.length embed_bin_bounds - 1);

  # This plot shows difference between "unbiased" sample (no partonic pt cuts)
  # to samples generated like in embedding.
  ckin_cuts_effect =
    let
      plot_bins = [
        { pt_min = "0"; pt_max = "-1"; }
        ] ++ (lib.take 7 embed_bins);
      plot_item = bin: runMC 100 ({ mode = "1"; } // bin);
    in
      mkhtml (map plot_item plot_bins) "PLOT:RatioPlotYMin=0.8:RatioPlotYMax=4.5";

  _ckin_cuts_effect2 = enable_mi:
    let
      _plot_bins = [ 
        { pt_min = "0"; pt_max = "-1"; }
        ] ++ (lib.take 7 embed_bins) ++ [
        { pt_min = "1.25"; pt_max = "-1"; }
        ];
      plot_bins = map (bin: bin // { inherit enable_mi; }) _plot_bins;
      plot_item = bin: runMC 100 ({ mode = "1"; } // bin);
    in
      mkhtml (map plot_item plot_bins) "PLOT:RatioPlotYMin=0.8:RatioPlotYMax=4.5";
  ckin_cuts_effect2 = map _ckin_cuts_effect2 [ true false ];
  ckin_cuts_effect2_with_mi = _ckin_cuts_effect2 true;
  ckin_cuts_effect2_without_mi = _ckin_cuts_effect2 false;

  calc_fudge_factors = inputs: mkDerivation {
    name = "fudge_factors";

    buildInputs = [ python2 python2Packages.numpy rivet yoda ];
    buildPhase = ''
      ${./calc-fudge-factors.py} ${join (map (d: "${d}/output.yoda.gz") inputs)} | tee fudge_factors
      rivet-cmphistos --no-ratio $(cat yodafiles) PLOT:LogY=0:XMin=2:XMax=15:XLabel="$\\hat{p}_T$":YLabel="Cross sections ratio":LegendYPos=0.45
      for i in *.dat; do
        make-plots "$i"
      done
    '';
    installPhase = ''
      install -Dm644 fudge_factors "$out"/fudge_factors
      for i in *.pdf; do
        install -Dm644 "$i" "$out"/$i
      done
    '';
  };

  fudge_factors =
    let
      reference = runMC 1600 { pt_min = "2"; pt_max = "-1"; mode = "1"; };
      bins = map (bin: runMC 100 ({ mode = "1"; } // bin)) (lib.take 7 embed_bins);
    in
      calc_fudge_factors ([ reference ] ++ bins);

  mk_soft_reweight_fit = reference: original: mkDerivation {
    name = "soft_reweight_fit";
    buildInputs = [ python2 python2Packages.numpy python2Packages.scipy rivet yoda ];
    phases = [ "buildPhase" "installPhase" ];
    buildPhase = ''
      ${./soft-reweight-fit.py} --reference ${reference}/output.yoda.gz --original ${original}/output.yoda.gz | tee fit
      rivet-cmphistos ratio.yoda fit.yoda PLOT:LogY=0:XMin=2:XMax=15:XLabel="$\\hat{p}_T$":YLabel="Cross sections ratio":LegendYPos=0.45
      for i in *.dat; do
        make-plots "$i"
      done
    '';
    installPhase = ''
      install -Dm644 fit "$out"/fit
      for i in *.pdf; do
        install -Dm644 "$i" "$out/$i"
      done
      cp -rl "${mkhtml [ original reference ] ""}"/* "$out"/
    '';
  };

  soft_reweight_fit = mk_soft_reweight_fit
    (runMC 1600 { pt_min = "2"; pt_max = "-1"; mode = "0"; } // { title = "CKIN(3)$ = 0$, $\\hat{p}_T > 2$ cut"; })
    (runMC 1600 { pt_min = "2"; pt_max = "-1"; mode = "1"; } // { title = "CKIN(3)$ = 2$"; });
}
