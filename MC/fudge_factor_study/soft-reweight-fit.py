#!/usr/bin/env python2

import argparse
import os
import math

import numpy as np
import scipy.optimize
import yoda

sqr = lambda x: x * x

parser = argparse.ArgumentParser()
parser.add_argument('--reference', type=argparse.FileType('r'))
parser.add_argument('--original', type=argparse.FileType('r'))
args = parser.parse_args()

def read(fname):
    return yoda.readYODA(fname)['/MC_PARTONIC_PT/partonic_pt']

reference = read(args.reference)
original = read(args.original)

ratio = original / reference
relevant_points = filter(lambda p: p.y != 0 and not math.isnan(p.y), ratio.points)

# original curve has an unknown fudge factor
PT_BOUND = 12 # GeV - we assume soft reweighting is not present above that scale
PT_MAX = 20 # GeV - values at high pt are not as reliable due to low statistics
ratio_asymptotics = [p.y for p in relevant_points if p.x > PT_BOUND and p.x < PT_MAX]
print ratio_asymptotics
fudge_factor = 1 / np.mean(ratio_asymptotics)
delta_fudge_factor = np.std(ratio_asymptotics) / sqr(np.mean(ratio_asymptotics))
print "Fudge factor: {} +- {}".format(fudge_factor, delta_fudge_factor)
ratio.scaleY(fudge_factor)
relevant_points = filter(lambda p: p.y != 0 and not math.isnan(p.y), ratio.points)

s = yoda.Scatter2D('/MC_PARTONIC_PT/reweight')
s.addPoints([(p.x, p.y, p.xErrs, p.yErrs) for p in relevant_points])
yoda.writeYODA([s], "ratio.yoda")
point_xs = np.array([p.x for p in relevant_points])
point_ys = np.array([p.y for p in relevant_points])

bin_size = (point_xs[1:] - point_xs[:-1])[:40]
assert np.all(np.abs(bin_size - bin_size[0]) < 1e-10)
bin_size = bin_size[0]

def f(x, p0, p1, p2, p3):
    return 1. + (p0 + p1 * (x - 2) + p2 * sqr(x - 2)) * np.exp(-p3 * (x - 2))

import scipy.integrate

@np.vectorize
def g(x, p0, p1, p2, p3):
    n = np.int(x / bin_size)
    xmin = n * bin_size
    xmax = (n + 1) * bin_size
    return (scipy.integrate.quad(lambda x: f(x, p0, p1, p2, p3), xmin, xmax) / bin_size)[0]

print g

popt, pcov = scipy.optimize.curve_fit(f, point_xs[:40], point_ys[:40])
print "f-y", f(point_xs[:40], *popt) - point_ys[:40]
print "p", popt
assert popt[3] >= 0.
assert np.abs(f(15, *popt) - 1.) < 0.01
print "-"*80

# Fit g function
popt, pcov = scipy.optimize.curve_fit(g, point_xs[:40], point_ys[:40], p0=(1.,)*4)
print "g-y", g(point_xs[:40], *popt) - point_ys[:40]
print "p", popt
assert popt[3] >= 0.
assert np.abs(g(15, *popt) - 1.) < 0.01

# Use g function fit parameters with f function
sf = yoda.Scatter2D('/MC_PARTONIC_PT/reweight')
sf.addPoints([(x, f(x, *popt), bin_size / 2, bin_size / 2) for x in point_xs])
yoda.writeYODA([sf], "fit.yoda")
