{ nixpkgs ? import <nixpkgs> {} }:

with {
  inherit (nixpkgs)
    lib
    gzip
    lhapdf
    sherpa
    ;
};
with import ../../common.nix { inherit (nixpkgs) lib; };

let
  this = final: prev: with final; {
    inherit (sherpa) pname version;

    runSherpa = {
      num_events ? DEFAULT_NUM_EVENTS,
      seed,
      pdf_set ? "cteq6l1",
      scale_var ? "pthat",
      use_ckkw ? false,
      enable_mi ? true,
      enable_shower ? true,
      enable_frag ? true,
      frag_model ? null,
      disable_decay ? false,
    }@args:
      let
        steering_file = builtins.toFile "Run.dat" ''
          (run){
            BEAM_1 2212; BEAM_ENERGY_1 ${toString (SQRT_S / 2)};
            BEAM_2 2212; BEAM_ENERGY_2 ${toString (SQRT_S / 2)};

            PDF_LIBRARY LHAPDFSherpa;
            USE_PDF_ALPHAS 1;
            ${lib.optionalString (scale_var != null) {
              "pthat" = "SCALES VAR{max(PPerp2(p[2]),PPerp2(p[3]))}";
            }.${scale_var}}

            ${lib.optionalString (!enable_mi) "MI_HANDLER Off;"}
            ${lib.optionalString (!enable_shower) "SHOWER_GENERATOR Off;"}
            ${lib.optionalString ((!enable_frag) || (frag_model != null)) "FRAGMENTATION ${if enable_frag then frag_model else "Off"};"}
            ${lib.optionalString disable_decay "DECAYMODEL Off;"}
          }(run)
          
          (processes){
            Process 93 93 -> 93 93
            Enhance_Observable VAR{max(PPerp(p[2]), PPerp(p[3]))}|10|50 {2}
            Order (*,0);
            ${lib.optionalString use_ckkw "CKKW sqr(20/E_CMS)"}
            Integration_Error 0.02;
            End process;
          }(processes)
          
          (analysis){
          BEGIN_RIVET {
            -a ${join rivet_analyses}
          } END_RIVET
          }(analysis)
        '';
        norm = mkDerivation {
          name = "sherpa-norm";

          buildInputs = [ sherpa pdf_sets.${pdf_set} ];
          buildPhase = ''
            ln -s ${steering_file} Run.dat
            Sherpa -a 0 -R 1337 -e 1 PDF_SET=${pdf_set}
          '';
          installPhase = ''
            for file in Results.db${lib.optionalString enable_mi " MIG_*.db MPI_Cross_Sections.dat"}; do
              install -Dm444 "$file" "$out/$file"
            done
          '';
        };
      in
      mkDerivation rec {
        name = "sherpa";

        buildInputs = [ gzip sherpa pdf_sets.${pdf_set} ] ++ rivet_built_analyses;
        buildPhase = ''
          ln -s ${steering_file} Run.dat
          cp -rv ${norm}/* ./
          Sherpa -A output -a Rivet -R ${toString seed} -e ${toString num_events} PDF_SET=${pdf_set}
          gzip output.yoda
        '';
        installPhase = ''
          ${installOutputYodaGz}
        '';
        passthru.title = builtins.replaceStrings ["_"] ["\\_"] name;
      };
    currentMC = runSherpa;
    runMC_splitbin = runMC;
  };
in
  (import ../bits.nix { inherit nixpkgs; }).override this
