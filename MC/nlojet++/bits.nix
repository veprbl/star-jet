{ nixpkgs ? import <nixpkgs> {} }:

with {
  inherit (nixpkgs)
    lib
    fetchgit
    boost
    fastjet
    gzip
    hepmc2
    lhapdf
    nlojet
    rivet
    yoda
    ;
};
with import ../../common.nix { inherit (nixpkgs) lib; };

let
  this = final: prev: with final; {
    inherit (nlojet) pname version;

    nlojet_hepmc = mkDerivation rec {
      name = "nlojet_hepmc";
      src = fetchgit {
        url = "https://git.sr.ht/~veprbl/nlojet_hepmc";
        rev = "60d8ab2862ded196da4a0b860f7f5da025ff6ec6";
        sha256 = "0xvk2vi7hvldksb3djrcxas6y2sbfv7vaifbwwlj06svspy007zd";
      };
      dontSetPhases = true;
      postPatch = ''
        substituteInPlace Makefile \
          --replace "c++11" "c++14"
      '';
      # TODO: yoda should be in rivet.propagatedBuildInputs
      buildInputs = [ boost fastjet hepmc2 lhapdf nlojet rivet yoda ];
      BOOST_ROOT=boost.dev;
      HEPMC_PREFIX=hepmc2;
      installPhase = ''
        mkdir "$out"
        mv .libs "$out"/lib
        mv nlojet++_hepmc.la "$out"/lib/
      '';
    };

    runNLOJet = {
      num_events ? DEFAULT_NUM_EVENTS,
      pdf_set ? "cteq6",
      njets ? null,
      contributions ? "full",
      scale_var ? null,
      xren ? null,
      xfac ? null,
      seed,
    }:
      mkDerivation rec {
        name = "nlojet++-${contributions}";
        buildInputs =
          [ nlojet nlojet_hepmc gzip pdf_sets.${pdf_set} ]
          ++ rivet_built_analyses;
        buildPhase = ''
          nlojet++ \
            --calculate -c${contributions} \
            -u ${nlojet_hepmc}/lib/nlojet++_hepmc.la \
            -s ${toString seed} \
            --max-event ${toString num_events} \
            --run_name=output \
            ${lib.optionalString (njets != null) "--njets=${toString njets}"} \
            --pdf=${pdf_set} \
            --sqrts=${toString SQRT_S} \
            ${lib.optionalString (scale_var != null) "--scale_var=${scale_var}"} \
            ${lib.optionalString (xren != null) "--xren=${toString xren}"} \
            ${lib.optionalString (xfac != null) "--xfac=${toString xfac}"} \
            ${join (map (ana_name: "--analysis=${ana_name}") rivet_analyses)}
          gzip output.yoda
        '';
        installPhase = ''
          ${installOutputYodaGz}
        '';
        passthru.title = builtins.replaceStrings ["_"] ["\\_"] name;
      };
    currentMC = runNLOJet;
    needmode = false;
  };
in
  (import ../bits.nix { inherit nixpkgs; }).override this
