{ nixpkgs ? import <nixpkgs> {}, nobatch ? false }:

let
  use_relevant_analyses = final: prev: assert builtins.hasAttr "rivet_analyses" prev; {
    inherit nobatch;

    # disable other rivet analyses
    rivet_analyses = [
      "MC_PARTONIC_PT"
      "MC_STAR_JETS"
    ];
  };

in

with import ../../common.nix { inherit (nixpkgs) lib; };

let
  inherit (nixpkgs) lib;

  py6 = (import ../pythia6/bits.nix { inherit nixpkgs; }).override use_relevant_analyses;
  py8 = (import ../pythia8/bits.nix { inherit nixpkgs; }).override use_relevant_analyses;
  herwig7 = (import ../herwig7/bits.nix { inherit nixpkgs; }).override use_relevant_analyses;

in

((import ../pythia6/bits.nix { inherit nixpkgs; }).override use_relevant_analyses).override (final: prev: add_eval_scope (with final; {
  inherit nobatch;

  default_num_mc_jobs = 1;

  make_reweight_cmp =
    { enable_had, enable_mi, pdf_q2_var ? "VINT(54)", pdf_x_var ? "PARI(33),PARI(34)", pdf_flav_var ? "MSTI(15),MSTI(16)", reweight_target_pdf ? "MMHT2014lo68cl", ... }@_args:
    let
      args = _args // { inherit enable_had enable_mi pdf_q2_var pdf_x_var pdf_flav_var; };
      tune =
        builtins.concatStringsSep ", " (
          (lib.optional (!enable_had) "no had.")
          ++ (lib.optional (!enable_mi) "no MI")
        );
      tune_sep = lib.optionalString (tune != "") "${tune}, ";
      reweight_target_pdf_tex = builtins.replaceStrings ["_"] ["\\_"] reweight_target_pdf;
    in
      mkhtml [
        (runMC_splitbin default_num_mc_jobs args // { title = "Pythia ${version} (${tune})"; })
        (runMC_splitbin default_num_mc_jobs (args // { pdf_set = reweight_target_pdf; }) // { title = "Pythia ${version} (${tune_sep}${reweight_target_pdf_tex})"; })
        (runMC_splitbin default_num_mc_jobs (args // { do_reweight = true; inherit reweight_target_pdf; }) // { title = "Pythia ${version} (${tune_sep}reweight to ${reweight_target_pdf_tex})"; })
      ] "PLOT:LegendXPos=0.01:Title='$Q^2=\$${pdf_q2_var}/$x\\leftarrow\$${pdf_x_var}/$i\\leftarrow\$${pdf_flav_var}'";

  make_tune_set = args: nixpkgs.runCommandNoCC "tune_set" {} ''
    mkdir "$out"
    ln -s ${make_reweight_cmp ({ enable_had = true; enable_mi = true; } // args)} "$out"/default
    ln -s ${make_reweight_cmp ({ enable_had = true; enable_mi = false; } // args)} "$out"/nomi
    ln -s ${make_reweight_cmp ({ enable_had = false; enable_mi = false; } // args)} "$out"/nomi_nohad
  '';

  make_var_set = args: nixpkgs.runCommandNoCC "var_set" {} ''
    mkdir "$out"
    ln -s ${make_tune_set ({ pdf_x_var = "PARI(33),PARI(34)"; pdf_flav_var = "MSTI(15),MSTI(16)"; } // args)} "$out"/54_3334_1516
    ln -s ${make_tune_set ({ pdf_x_var = "lines 3,4"; pdf_flav_var = "MSTI(13),MSTI(14)"; } // args)} "$out"/54_34_1314
    ln -s ${make_tune_set ({ pdf_x_var = "lines 5,6"; pdf_flav_var = "MSTI(15),MSTI(16)"; } // args)} "$out"/54_56_1516
  '';

  make_pdf_set = args: nixpkgs.runCommandNoCC "pdf_set" {} ''
    mkdir "$out"
    ln -s ${make_var_set ({ reweight_target_pdf = "MMHT2014lo68cl"; } // args)} "$out"/MMHT2014lo68cl
    ln -s ${make_var_set ({ reweight_target_pdf = "CT14lo"; } // args)} "$out"/CT14lo
    ln -s ${make_var_set ({ reweight_target_pdf = "NNPDF30_lo_as_0118"; } // args)} "$out"/NNPDF30_lo_as_0118
    ln -s ${make_var_set ({ reweight_target_pdf = "HERAPDF20_LO_EIG"; } // args)} "$out"/HERAPDF20_LO_EIG
  '';

  pdf_set = make_pdf_set {};

  change_pdf = mc: tune_name: args: pdf_set: with py8; let
    upcase = s:
      lib.toUpper (builtins.substring 0 1 s)
      +
      builtins.substring 1 (builtins.stringLength s - 1) s;
    pdf_set_tex =
      if pdf_set != null then
        ", " + builtins.replaceStrings ["_"] ["\\_"] pdf_set
      else
        "";
  in
    mc.runMC_splitbin default_num_mc_jobs ({ inherit pdf_set; } // args)
      // { title = "${upcase mc.pname} ${mc.version} (${tune_name}${pdf_set_tex})"; };

  # Some additional study to see variation of R_T with different PDF sets
  change_model = mkhtml' "--errs" { LegendXPos = 0.11; } [
    (set_short_title "Starugia12" (change_pdf py6 "Starugia12" {} null))
    (change_pdf py8 "Monash 2013" { tune = "Monash 2013"; } null)
    (change_pdf py8 "Detroit" { tune = "Detroit"; } null)
    (change_pdf py8 "AU2-CTEQ6L1" { tune = "AU2-CTEQ6L1"; } null)
    (change_pdf herwig7 "default" {} null)
  ];
  change_pdf_set_py6 = mkhtml' "--errs" { LegendXPos = 0.11; } [
    (set_short_title "nominal" (change_pdf py6 "Starugia12" {} null))
    (change_pdf py6 "Starugia12" {} "MMHT2014lo68cl")
    (change_pdf py6 "Starugia12" {} "CT14lo")
    (change_pdf py6 "Starugia12" {} "NNPDF30_lo_as_0118")
    (change_pdf py6 "Starugia12" {} "HERAPDF20_LO_EIG")
  ];
  change_pdf_set_py8 = mkhtml' "--errs" { LegendXPos = 0.11; } [
    (change_pdf py8 "Monash 2013" { tune = "Monash 2013"; } null)
    (change_pdf py8 "Monash 2013" { tune = "Monash 2013"; } "MMHT2014lo68cl")
    (change_pdf py8 "Monash 2013" { tune = "Monash 2013"; } "CT14lo")
    (change_pdf py8 "Monash 2013" { tune = "Monash 2013"; } "NNPDF30_lo_as_0118")
    (change_pdf py8 "Monash 2013" { tune = "Monash 2013"; } "HERAPDF20_LO_EIG")
  ];
  change_settings = mkhtml' "--errs" { LegendXPos = 0.11; RatioPlotYMin = 0; RatioPlotYMax = 2; } [
    (change_pdf py6 "Starugia12" {} null)
    (change_pdf py6 "Starugia12, no decay" { disable_decay = true; } null)
    (change_pdf py6 "Starugia12, no had." { enable_had = false; } null)
    (change_pdf py6 "Starugia12, no had., no mi." { enable_had = false; enable_mi = false; } null)
    (change_pdf py6 "Starugia12, no had., no mi., no FSR" { enable_had = false; enable_mi = false; enable_fsr = false; } null)
    (change_pdf py6 "Starugia12, no had., no mi., no FSR, no ISR" { enable_had = false; enable_mi = false; enable_fsr = false; enable_isr = false; } null)
  ];

}))
