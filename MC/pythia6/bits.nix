with {
  inherit (import <nixpkgs> {})
    lib
    gfortran
    gzip
    hepmc
    lhapdf
    rivet
    yoda
    ;
};
with import ../../common.nix;

{ overrides ? self: super: {} }:

let
  this = self: super: with self; {
    version = "6.4.28";

    xsec = mkDerivation {
      name = "xsec";
      src = lib.sourceByRegex ./. [
        "^pythia6428.f$"
        "^xsec.cpp$"
        "^Makefile$"
        ];
      dontSetPhases = true;
      buildInputs = [ gfortran lhapdf yoda hepmc rivet ];
      installPhase = ''
         install -Dm755 xsec $out/bin/xsec
      '';
    };

    runPythia6 = { num_events ? 100000, pt_min, pt_max, seed, mode, parp90 ? "-1", enable_mi ? true, disable_decay ? false }:
      mkDerivation rec {
        name = "pythia-${pt_min}_${pt_max}-${mode}${if enable_mi then "" else "-noMI"}";
        buildInputs = [ xsec lhapdf.pdf_sets.cteq6l1 gzip ] ++ rivet_analyses;
        buildPhase = ''
          echo Running ${pt_min} ${pt_max}
          xsec ${toString num_events} ${pt_min} ${pt_max} ${toString seed} ${mode} ${parp90} ${if enable_mi then "1" else "0"} ${if disable_decay then "1" else "0"}
          mv Rivet.yoda output.yoda
          gzip output.yoda
        '';
        installPhase = ''
          install -Dm644 output.yoda.gz $out/output.yoda.gz
        '';
        passthru.title = builtins.replaceStrings ["_"] ["\\_"] name;
      };
    currentMC = runPythia6;
    needmode = true;
  };
in
  import ../bits.nix { overrides = compose [ this overrides ]; }
