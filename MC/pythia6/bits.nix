{ nixpkgs ? import <nixpkgs> {} }:

with {
  inherit (nixpkgs)
    lib
    gfortran
    gzip
    hepmc2
    lhapdf
    rivet
    yoda
    ;
};
with import ../../common.nix { inherit (nixpkgs) lib; };

let
  this = final: prev: with final; {
    pname = "pythia";
    version = "6.4.28";

    pdf_code = {
      "CT14lo" = 13200;
      "HERAPDF20_LO_EIG" = 61000;
      "MMHT2014lo68cl" = 25000;
      "NNPDF30_lo_as_0118" = 262000;
    };

    xsec = mkDerivation {
      name = "xsec";
      src = lib.sourceByRegex ./. [
        "^pythia6428.f$"
        "^xsec.cpp$"
        "^Makefile$"
        ];
      FFLAGS="-O2";
      CXXFLAGS="-O2";
      dontSetPhases = true;
      buildInputs = [ gfortran (lib.getLib gfortran.cc) lhapdf yoda hepmc2 rivet ];
      installPhase = ''
         install -Dm755 xsec $out/bin/xsec
      '';
    };

    STAR = 100000;
    TUNE_A = 100;
    D6T = 109;
    PERUGIA_0 = 320;
    Z2 = 343;
    PERUGIA_2012 = 370;

    runPythia6 = {
      num_events ? DEFAULT_NUM_EVENTS,
      tune ? STAR + PERUGIA_2012,
      pt_min,
      pt_max,
      seed,
      mode,
      parp90 ? "-1",
      parp91 ? "-1",
      enable_isr ? true,
      enable_fsr ? true,
      enable_had ? true,
      enable_mi ? true,
      disable_decay ? false,
      pdf_set ? null,
      pdf_q2_var ? "VINT(54)",
      pdf_x_var ? "PARI(33),PARI(34)",
      pdf_flav_var ? "MSTI(15),MSTI(16)",
      do_reweight ? false,
      reweight_target_pdf ? "MMHT2014lo68cl",
    }:
      mkDerivation (rec {
        name = "pythia-${pt_min}_${pt_max}-${mode}${if enable_had then "" else "-noHad"}${if enable_mi then "" else "-noMI"}";
        buildInputs =
          [ xsec pdf_sets.cteq6l1 gzip ]
          ++ rivet_built_analyses
          ++ lib.optional (builtins.isString pdf_set) pdf_sets.${pdf_set}
          ++ lib.optional do_reweight pdf_sets.${reweight_target_pdf};
        XSEC_RIVET_ANALYSES = rivet_analyses;
        XSEC_PDF_Q2_VAR = {
          "VINT(52)" = 52;
          "VINT(54)" = 54;
        }.${pdf_q2_var};
        XSEC_PDF_X_VAR = {
          "lines 3,4" = 34;
          "lines 5,6" = 56;
          "PARI(33),PARI(34)" = 3334;
        }.${pdf_x_var};
        XSEC_PDF_FLAV_VAR = {
          "MSTI(13),MSTI(14)" = 1314;
          "MSTI(15),MSTI(16)" = 1516;
        }.${pdf_flav_var};
        pdf_set_arg =
          if pdf_set == null then
            "-1"
          else if builtins.isInt pdf_set then
            toString pdf_set
          else
            toString pdf_code.${pdf_set};
        buildPhase = ''
          echo Running ${pt_min} ${pt_max}
          xsec \
            ${toString num_events} \
            ${toString SQRT_S} \
            ${toString tune} \
            ${pt_min} \
            ${pt_max} \
            ${toString seed} \
            ${mode} \
            ${parp90} \
            ${parp91} \
            ${if enable_isr then "1" else "0"} \
            ${if enable_fsr then "1" else "0"} \
            ${if enable_had then "1" else "0"} \
            ${if enable_mi then "1" else "0"} \
            ${if disable_decay then "1" else "0"} \
            $pdf_set_arg \
            ${if do_reweight then "1" else "0"}
          mv Rivet.yoda output.yoda
          gzip output.yoda
        '';
        installPhase = ''
          ${installOutputYodaGz}
        '';
        passthru.title = builtins.replaceStrings ["_"] ["\\_"] name;
      } // lib.optionalAttrs do_reweight {
        XSEC_REWEIGHT_TARGET_PDF = reweight_target_pdf;
      });
    currentMC = runPythia6;
    needmode = true;
  };
in
  (import ../bits.nix { inherit nixpkgs; }).override this
