{ nixpkgs ? import <nixpkgs> {} }:

with nixpkgs;

stdenv.mkDerivation {
  name = "xsec-shell";
  buildInputs = [ gfortran (lib.getLib gfortran.cc) lhapdf yoda hepmc rivet lhapdf.pdf_sets.cteq6l1 ];
}
