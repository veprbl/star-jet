// vim:et sw=2 sts=2

#include <cstdlib>
#include <cstring>
#include <iostream>

#include <HepMC/GenEvent.h>
#include <HepMC/IO_HEPEVT.h>
#include <HepMC/PythiaWrapper.h>
#include <Rivet/AnalysisHandler.hh>

#define STR(s) s, strlen(s)

extern "C" {
  void pygive_(const char *, int);
  void pyinit_(const char*, const char*, const char*, double*, int, int, int);
  void pytune_(int*);
  void pyevnt_();
  void pyevnw_();
  void pyhepc_(int*);
  void pystat_(int*);

#if 0
  /* COMMON/PYSUBS/MSEL,MSELPD,MSUB(500),KFIN(2,-40:40),CKIN(200) */
  extern struct {
    int msel;
    int mselpd;
    int msub[500];
    int kfin[2][81];
    double ckin[200];
  } pysubs_;

  /* COMMON/PYPARS/MSTP(200),PARP(200),MSTI(200),PARI(200) */
  extern struct {
    int mstp[200];
    double parp[200];
    int msti[200];
    double pari[200];
  } pypars_;

  /* COMMON/PYDATR/MRPY(6),RRPY(100) */
  extern struct {
    int mrpy[6];
    double rrpy[100];
  } pydatr_;

  /* COMMON/PYDAT3/MDCY(500,3),MDME(8000,2),BRAT(8000),KFDP(8000,5) */
  extern struct {
    int mdcy[3][500];
    int mdme[2][8000];
    double brat[8000];
    int kfdp[5][8000];
  } pydat3_;
#endif
};

#define ckin(i) pysubs_.ckin[(i) - 1]
#define mstp(i) pypars_.mstp[(i) - 1]
#define parp(i) pypars_.parp[(i) - 1]
#define pari(i) pypars_.pari[(i) - 1]
#define mrpy(i) pydatr_.mrpy[(i) - 1]
#define mdcy(i,j) pydat3_.mdcy[(j) - 1][(i) - 1]

int one = 1;
const double MB_TO_PB = 1e9;

int main(int argc, char** argv)
{
  if (argc != 9) {
    std::cerr << "Usage " << argv[0] << " num-events ptmin ptmax seed mode parp90 enable_mi disable_decay" << std::endl;
    return EXIT_FAILURE;
  }
  mrpy(1) = atoll(argv[4]) % 900000000;
  HepMC::HEPEVT_Wrapper::set_max_number_entries(4000);
  HepMC::HEPEVT_Wrapper::set_sizeof_real(8);
  HepMC::IO_HEPEVT hepevtio;
  Rivet::AnalysisHandler rivet;
  rivet.addAnalysis("MC_DECAY_STUDY");
  rivet.addAnalysis("MC_JET_PARTON_MATCH");
  rivet.addAnalysis("MC_JET_VPDMB");
  rivet.addAnalysis("MC_PARTONIC_PT");
  rivet.addAnalysis("MC_UE");
  pygive_(STR("MSEL=1"));
  double cms = 200.;
  int tune = 370;
  pytune_(&tune);
  double parp90 = strtod(argv[6], NULL);
  if (parp90 >= 0) {
    parp(90) = parp90;
  } else {
    pygive_(STR("PARP(90)=0.213")); // adjust tune
  }
  int enable_mi = atoi(argv[7]);
  if (!enable_mi) {
    mstp(81) = 20;
  }
  mstp(128) = 2; // not store multiple copies of particles in particle record
  int mode = atoi(argv[5]);
  double pt_min = strtod(argv[2], NULL);
  double pt_max = strtod(argv[3], NULL);
  if (mode) {
    ckin(3) = pt_min;
    ckin(4) = pt_max;
  }
  int disable_decay = atoi(argv[8]);
  if (disable_decay) {
    mdcy(102, 1) = 0; // PI0 111
    mdcy(106, 1) = 0; // PI+ 211
    mdcy(109, 1) = 0; // ETA 221
    mdcy(116, 1) = 0; // K+ 321
    mdcy(112, 1) = 0; // K_SHORT 310
    mdcy(105, 1) = 0; // K_LONG 130
    mdcy(164, 1) = 0; // LAMBDA0 3122
    mdcy(167, 1) = 0; // SIGMA0 3212
    mdcy(162, 1) = 0; // SIGMA- 3112
    mdcy(169, 1) = 0; // SIGMA+ 3222
    mdcy(172, 1) = 0; // Xi- 3312
    mdcy(174, 1) = 0; // Xi0 3322
    mdcy(176, 1) = 0; // OMEGA- 3334
  }
  pyinit_("CMS", "p", "p", &cms, strlen("CMS"), strlen("p"), strlen("p"));
  long long num_events = atoll(argv[1]), num_accepted = 0;
  long long num_generated = 0;
  for(; num_accepted < num_events; num_generated++) {
    pyevnw_();
    double hard_pt = pari(17);
    if (((pt_max >= 0) && (hard_pt >= pt_max)) || (hard_pt < pt_min)) {
      if (mode) {
        std::cerr << "CKIN(3),CKIN(4)-based cuts didn't work!" << std::endl;
        std::cerr << "pt_min: " << pt_min << std::endl;
        std::cerr << "pt_max: " << pt_max << std::endl;
        std::cerr << "hard_pt: " << hard_pt << std::endl;
        return EXIT_FAILURE;
      } else {
        continue;
      }
    }
    pyhepc_(&one);
    HepMC::GenEvent* evt = hepevtio.read_next_event();
    evt->set_event_scale(hard_pt);
    evt->use_units(HepMC::Units::GEV, HepMC::Units::MM);
    evt->set_cross_section( HepMC::getPythiaCrossSection() );
    if (evt->valid_beam_particles()) {
      evt->beam_particles().first->set_status(4);
      evt->beam_particles().second->set_status(4);
    }
    evt->weights().push_back(1.0);
    num_accepted++;
    rivet.analyze(*evt);
    delete evt;
  }
  double xsec = pari(1);
  std::cerr << "xsec:\t" << xsec << std::endl;
  std::cerr << "generated " << num_generated << " events to have " << num_accepted << " accepted" << std::endl;
  rivet.setCrossSection(xsec * MB_TO_PB * num_accepted / num_generated);
  rivet.finalize();
  rivet.writeData("Rivet.yoda");
  pystat_(&one);
  return EXIT_SUCCESS;
}
