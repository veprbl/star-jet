// vim:et sw=2 sts=2

// Copyright 2020 Dmitry Kalinkin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#include <HepMC/GenEvent.h>
#include <HepMC/IO_HEPEVT.h>
#include <HepMC/PythiaWrapper.h>
#include <LHAPDF/LHAPDF.h>
#include <Rivet/AnalysisHandler.hh>

using std::vector;
using std::string;

#define STR(s) s, strlen(s)

extern "C" {
  void pygive_(const char *, int);
  void pyinit_(const char*, const char*, const char*, double*, int, int, int);
  void pytune_(int*);
  void pyevnt_();
  void pyevnw_();
  void pyhepc_(int*);
  void pystat_(int*);

#if 0
  /* COMMON/PYSUBS/MSEL,MSELPD,MSUB(500),KFIN(2,-40:40),CKIN(200) */
  extern struct {
    int msel;
    int mselpd;
    int msub[500];
    int kfin[2][81];
    double ckin[200];
  } pysubs_;

  /* COMMON/PYPARS/MSTP(200),PARP(200),MSTI(200),PARI(200) */
  extern struct {
    int mstp[200];
    double parp[200];
    int msti[200];
    double pari[200];
  } pypars_;

  /* COMMON/PYINT1/MINT(400),VINT(400) */
  extern struct {
    int mint[400];
    double vint[400];
  } pyint1_;

  /* COMMON/PYDAT1/MSTU(200),PARU(200),MSTJ(200),PARJ(200) */
  extern struct {
    int mstu[200];
    double paru[200];
    int mstj[200];
    double parj[200];
  } pydat1_;

  /* COMMON/PYDATR/MRPY(6),RRPY(100) */
  extern struct {
    int mrpy[6];
    double rrpy[100];
  } pydatr_;

  /* COMMON/PYDAT3/MDCY(500,3),MDME(8000,2),BRAT(8000),KFDP(8000,5) */
  extern struct {
    int mdcy[3][500];
    int mdme[2][8000];
    double brat[8000];
    int kfdp[5][8000];
  } pydat3_;
#endif
};

#define ckin(i) pysubs_.ckin[(i) - 1]
#define mstp(i) pypars_.mstp[(i) - 1]
#define msti(i) pypars_.msti[(i) - 1]
#define mint(i) pyint1_.mint[(i) - 1]
#define vint(i) pyint1_.vint[(i) - 1]
#define mstu(i) pydat1_.mstu[(i) - 1]
#define parp(i) pypars_.parp[(i) - 1]
#define pari(i) pypars_.pari[(i) - 1]
#define mrpy(i) pydatr_.mrpy[(i) - 1]
#define mdcy(i,j) pydat3_.mdcy[(j) - 1][(i) - 1]

int one = 1;
const double MB_TO_PB = 1e9;

vector<string> split(const string &s, const string &sep)
{
  string::size_type prev_pos = 0, pos = 0;
  vector<string> result;

  if (sep.size() == 0) std::terminate();

  while ((pos = s.find(sep, pos)) != string::npos) {
    result.push_back(s.substr(prev_pos, pos - prev_pos));
    pos += sep.length();
    prev_pos = pos;
  }
  result.push_back(s.substr(prev_pos));

  return result;
}

void test_split()
{
#define ASSERT(cond) \
  if (!(cond)) { \
    std::cerr << "Assert failed at " << __FILE__ << ":" <<  __LINE__ << std::endl; \
    std::terminate(); \
  }

  {
    auto res = split("a::bb::ccc", "::");
    ASSERT(res.size() == 3);
    ASSERT(res[0] == "a");
    ASSERT(res[1] == "bb");
    ASSERT(res[2] == "ccc");
  }

  {
    auto res = split("test", "sep");
    ASSERT(res.size() == 1);
    ASSERT(res[0] == "test");
  }

  {
    auto res = split("test", "t");
    ASSERT(res.size() == 3);
    ASSERT(res[0] == "");
    ASSERT(res[1] == "es");
    ASSERT(res[2] == "");
  }

  {
    auto res = split("", "q");
    ASSERT(res.size() == 1);
    ASSERT(res[0] == "");
  }
}

char* getenv_or_die(const char *name, std::string err_msg) {
  char *value = getenv(name);
  if (value == NULL) {
    std::cerr << err_msg << std::endl;
    std::exit(EXIT_FAILURE);
  }
  return value;
}

double get_pdf_Q2() {
  double Q2;
  std::string possible_values =
    "Possible values are:\n"
    "  52 - use VINT(52),\n"
    "  54 - use VINT(54)";
  static const unsigned int reweight_q2_var
    = atoi(getenv_or_die(
             "XSEC_PDF_Q2_VAR",
             "XSEC_PDF_Q2_VAR is not set." + possible_values
             ));
  switch(reweight_q2_var) {
  case 52:
    Q2 = vint(52); break;
  case 54:
    // "Q of the outer hard-scattering subprocess"
    // "used as scale for parton distribution function evaluation"
    Q2 = vint(54); break;
  default:
    std::cerr << "Invalid XSEC_PDF_Q2_VAR." << possible_values << std::endl;
    std::exit(EXIT_FAILURE);
  }

  if (pari(22) != vint(52)) {
    std::cerr << "assertion failed: pari(22) != vint(52)" << std::endl;
    std::abort();
  }
  if (pari(18) != vint(54)) {
    std::cerr << "assertion failed: pari(18) != vint(54)" << std::endl;
    std::abort();
  }

  return Q2;
}

std::pair<double, double> get_pdf_x1x2(const HepMC::GenEvent *evt, double sqrt_s) {
  double x1, x2;
  std::string possible_values =
    " Possible values are:\n"
    "  34 - use lines 3 and 4 of the Pythia record\n"
    "  56 - use lines 5 and 6 of the Pythia record\n"
    "  3334 - use PARI(33) and PARI(34)";
  static const unsigned int reweight_x_var
    = atoi(getenv_or_die(
             "XSEC_PDF_X_VAR",
             "XSEC_PDF_X_VAR is not set." + possible_values
             ));
  bool use_x_lines_3_4 = false, use_x_lines_5_6 = false;
  switch(reweight_x_var) {
  case 34: use_x_lines_3_4 = true; break;
  case 56: use_x_lines_5_6 = true; break;
  case 3334: x1 = pari(33); x2 = pari(34); break;
  default:
    std::cerr << "Invalid XSEC_PDF_X_VAR." << possible_values << std::endl;
    std::abort();
  }

  for (auto it = evt->particles_begin(); it != evt->particles_end(); ++it) {
    const HepMC::GenParticle *p = *it;
    double x = 2 * std::fabs(p->momentum().z()) / sqrt_s;

    if (use_x_lines_3_4 && (p->barcode() == 3)) x1 = x;
    if (use_x_lines_3_4 && (p->barcode() == 4)) x2 = x;
    if (use_x_lines_5_6 && (p->barcode() == 5)) x1 = x;
    if (use_x_lines_5_6 && (p->barcode() == 6)) x2 = x;
  }

  return std::pair<double, double>(x1, x2);
}

std::pair<unsigned int, unsigned int> get_pdf_i1i2(const HepMC::GenEvent *evt) {
  double i1, i2;
  std::string possible_values =
    " Possible values are:\n"
    "  1314 - use MSTI(13) and MSTI(14)\n"
    "  1516 - use MSTI(15) and MSTI(16)";
  static const unsigned int reweight_flav_var
    = atoi(getenv_or_die(
             "XSEC_PDF_FLAV_VAR",
             "XSEC_PDF_FLAV_VAR is not set." + possible_values
             ));
  switch(reweight_flav_var) {
  case 1314:
    // "initial-state shower initiators"
    i1 = msti(13); i2 = msti(14);
    break;
  case 1516:
    // "incoming partons to the hard interaction"
    i1 = msti(15); i2 = msti(16);
    break;
  default:
    std::cerr << "Invalid XSEC_PDF_FLAV_VAR." << possible_values << std::endl;
    std::abort();
  }

  for (auto it = evt->particles_begin(); it != evt->particles_end(); ++it) {
    const HepMC::GenParticle *p = *it;
    if ((p->barcode() <= 6) && (p->barcode() >= 3)) {
      const unsigned int index = p->barcode() - 3 + 13;
      if (msti(index) != p->pdg_id()) {
        std::cerr
          << "msti(" << index << ") = " << msti(index)
          << " does not match"
          << "PDG id = " << p->pdg_id() << " on line " << p->barcode()
          << std::endl;
        std::abort();
      }
    }
  }

  return std::pair<unsigned int, unsigned int>(i1, i2);
}

int main(int argc, char** argv)
{
  test_split();

  const char *xsec_rivet_analyses = getenv("XSEC_RIVET_ANALYSES");

  const int NUM_ARGS = 17;
  if ((argc != NUM_ARGS) || (!xsec_rivet_analyses)) {
    if (argc > NUM_ARGS) {
      std::cerr << "Error: Too many arguments" << std::endl << std::endl;
    }
    if (argc < NUM_ARGS) {
      std::cerr << "Error: Not enough arguments" << std::endl << std::endl;
    }
    if (!xsec_rivet_analyses) {
      std::cerr << "Error: Missing XSEC_RIVET_ANALYSES" << std::endl << std::endl;
    }
    std::cerr << "Usage " << argv[0] << " num-events sqrt_s tune ptmin ptmax seed mode parp90 parp91 enable_isr enable_fsr enable_had enable_mi disable_decay pdf_set do_reweight" << std::endl
              << std::endl
              << "  XSEC_RIVET_ANALYSES environment variable must contain list of Rivet analyses" << std::endl
              << "  names (separated with spaces)." << std::endl;
    return EXIT_FAILURE;
  }
  mrpy(1) = atoll(argv[6]) % 900000000;
  HepMC::HEPEVT_Wrapper::set_max_number_entries(4000);
  HepMC::HEPEVT_Wrapper::set_sizeof_real(8);
  HepMC::IO_HEPEVT hepevtio;
  Rivet::AnalysisHandler rivet;
  pygive_(STR("MSEL=1"));
  double cms = strtod(argv[2], NULL);
  int tune = atoi(argv[3]);
  int pytune = tune;
  const int STAR_TUNE = 100000;
  if (tune >= STAR_TUNE) {
    pytune = tune % STAR_TUNE;
  }
  pytune_(&pytune);
  double parp90 = strtod(argv[8], NULL);
  if (parp90 >= 0) {
    parp(90) = parp90;
  } else {
    if (tune >= STAR_TUNE) {
      pygive_(STR("PARP(90)=0.213")); // adjust tune
    }
  }
  double parp91 = strtod(argv[9], NULL);
  parp(91) = parp91;
  int enable_isr = atoi(argv[10]);
  if (!enable_isr) {
    mstp(61) = 0;
  }
  int enable_fsr = atoi(argv[11]);
  if (!enable_fsr) {
    mstp(71) = 0;
  }
  int enable_had = atoi(argv[12]);
  if (!enable_had) {
    mstp(111) = 0;
  }
  int enable_mi = atoi(argv[13]);
  if (!enable_mi) {
    mstp(81) = 20;
  }
  mstp(128) = 2; // not store multiple copies of particles in particle record
  int mode = atoi(argv[7]);
  double pt_min = strtod(argv[4], NULL);
  double pt_max = strtod(argv[5], NULL);
  if (mode) {
    ckin(3) = pt_min;
    ckin(4) = pt_max;
  }
  int disable_decay = atoi(argv[14]);
  if (disable_decay) {
    mdcy(102, 1) = 0; // PI0 111
    mdcy(106, 1) = 0; // PI+ 211
    mdcy(109, 1) = 0; // ETA 221
    mdcy(116, 1) = 0; // K+ 321
    mdcy(112, 1) = 0; // K_SHORT 310
    mdcy(105, 1) = 0; // K_LONG 130
    mdcy(164, 1) = 0; // LAMBDA0 3122
    mdcy(167, 1) = 0; // SIGMA0 3212
    mdcy(162, 1) = 0; // SIGMA- 3112
    mdcy(169, 1) = 0; // SIGMA+ 3222
    mdcy(172, 1) = 0; // Xi- 3312
    mdcy(174, 1) = 0; // Xi0 3322
    mdcy(176, 1) = 0; // OMEGA- 3334
  }
  int pdf_set = atoi(argv[15]);
  if (pdf_set >= 0) {
    mstp(51) = pdf_set;
  }

  std::unique_ptr<LHAPDF::PDF> reference_pdf, target_pdf;
  int do_reweight = atoi(argv[16]);
  double reweight_xsec_ratio = 1;
  if (do_reweight) {
    int reference_pdf_code = mstp(51);
    const char *reweight_target_pdf = getenv("XSEC_REWEIGHT_TARGET_PDF");
    const unsigned int reweight_target_pdf_member = 0;
    if (!reweight_target_pdf) {
      std::cerr << "XSEC_REWEIGHT_TARGET_PDF is not set." << std::endl;
      return EXIT_FAILURE;
    }

    reference_pdf.reset(LHAPDF::mkPDF(reference_pdf_code));
    try {
      int reweight_target_pdf_code = std::stoi(reweight_target_pdf, NULL, 10);
      target_pdf.reset(LHAPDF::mkPDF(reweight_target_pdf_code));
    } catch(std::invalid_argument) {
      target_pdf.reset(LHAPDF::mkPDF(reweight_target_pdf, reweight_target_pdf_member));
    }

    mstp(51) = target_pdf->lhapdfID();
    pyinit_("CMS", "p", "p", &cms, strlen("CMS"), strlen("p"), strlen("p"));
    for(int i = 0; i < 10000; i++) pyevnw_();
    std::cerr << "xsec for target PDF " << mstp(51) << ": " << pari(1) << std::endl;
    reweight_xsec_ratio = pari(1);
    mstp(51) = reference_pdf_code;
    pyinit_("CMS", "p", "p", &cms, strlen("CMS"), strlen("p"), strlen("p"));
    for(int i = 0; i < 10000; i++) pyevnw_();
    reweight_xsec_ratio /= pari(1);
    std::cerr << "xsec for reference PDF " << mstp(51) << ": " << pari(1) << std::endl;
  }

  vector<string> analyses_names = split(xsec_rivet_analyses, " ");
  for (const string &analysis_name : analyses_names) {
    rivet.addAnalysis(analysis_name);
  }

  pyinit_("CMS", "p", "p", &cms, strlen("CMS"), strlen("p"), strlen("p"));
  long long num_events = atoll(argv[1]), num_accepted = 0;
  long long num_generated = 0;
  for(; num_accepted < num_events; num_generated++) {
    // raise number of allowed errors proportionally to the number of events generated
    mstu(22) = std::max(10, static_cast<int>(num_generated * 1e-4));

    pyevnw_();
    double hard_pt = pari(17);
    if (((pt_max >= 0) && (hard_pt >= pt_max)) || (hard_pt < pt_min)) {
      if (mode) {
        std::cerr << "CKIN(3),CKIN(4)-based cuts didn't work!" << std::endl;
        std::cerr << "pt_min: " << pt_min << std::endl;
        std::cerr << "pt_max: " << pt_max << std::endl;
        std::cerr << "hard_pt: " << hard_pt << std::endl;
        return EXIT_FAILURE;
      } else {
        continue;
      }
    }
    pyhepc_(&one);
    HepMC::GenEvent* evt = hepevtio.read_next_event();
    evt->set_event_number(num_accepted);
    evt->set_mpi(mint(351));
    evt->set_event_scale(hard_pt);
    evt->use_units(HepMC::Units::GEV, HepMC::Units::MM);
    evt->set_cross_section( HepMC::getPythiaCrossSection() );
    if (evt->valid_beam_particles()) {
      evt->beam_particles().first->set_status(4);
      evt->beam_particles().second->set_status(4);
    }
    for (auto it = evt->particles_begin(); it != evt->particles_end(); ++it) {
      HepMC::GenParticle *p = *it;
      unsigned int num_lines = mstu(70);
      for (unsigned int line = 1; line < num_lines; line++) {
        unsigned int line_pos = mstu(70 + line);
        if (p->barcode() > line_pos) {
          // record section number into the flow (ugly!)
          p->set_flow(0, line);
        }
      }
    }

    double weight = 1.0;

    {
      double Q2 = get_pdf_Q2();

      double x1_raw, x2_raw;
      std::tie(x1_raw, x2_raw) = get_pdf_x1x2(evt, cms);
      const double x1 = std::min(x1_raw, 1.0);
      const double x2 = std::min(x2_raw, 1.0);

      int i1, i2;
      std::tie(i1, i2) = get_pdf_i1i2(evt);

      {
        const double Q2min = 1.3 * 1.3;
        Q2 = std::max(Q2, Q2min);
      }

      double p1 = NAN, p2 = NAN;
      HepMC::PdfInfo pdf_info(i1, i2, x1_raw, x2_raw, std::sqrt(Q2), p1, p2);
      evt->set_pdf_info(pdf_info);

      if (do_reweight) {
        p1 = reference_pdf->xfxQ2(i1, x1, Q2);
        p2 = reference_pdf->xfxQ2(i2, x2, Q2);

        weight *=
          (target_pdf->xfxQ2(i1, x1, Q2) * target_pdf->xfxQ2(i2, x2, Q2)) /
          (p1 * p2);
      }
    }

    evt->weights().push_back(weight);
    num_accepted++;
    rivet.analyze(*evt);
    delete evt;
  }

  double xsec = pari(1) * reweight_xsec_ratio;
  std::cerr << "xsec:\t" << xsec << std::endl;
  std::cerr << "generated " << num_generated << " events to have " << num_accepted << " accepted" << std::endl;
  rivet.setCrossSection(xsec * MB_TO_PB * num_accepted / num_generated, 0.);
  rivet.finalize();
  rivet.writeData("Rivet.yoda");
  pystat_(&one);
  return EXIT_SUCCESS;
}
