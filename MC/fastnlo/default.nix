{ nixpkgs ? import <nixpkgs> {} }:

with nixpkgs;

let
  # compatibility with pre-21.11 nixpkgs
  #fastnlo_toolkit = pkgs.fastnlo_toolkit or pkgs.fastnlo;
  fastnlo_toolkit = pkgs.callPackage ./pkgs/fastnlo_toolkit {};

  yoda_for_yodascale = bits.yoda_for_yodamerge.overrideAttrs (prev: rec {
    name = "yoda-${version}";
    version = "2.0.0";
    src = fetchurl {
      url = "https://yoda.hepforge.org/downloads?f=YODA-2.0.0.tar.bz2";
      sha256 = "0xcmm42s0q1j3sjml92i0wcw71f2qb8p5vjx3k76fcdvpvd463v8";
    };
    patches = [];
    buildInputs = prev.buildInputs ++ [ ncurses ];
    doInstallCheck = false; # missing data files in tarball
  });

  bits = import ../bits.nix { inherit nixpkgs; };

in

bits.override (final: prev: with final; {
  get_grid_file = { jet_r, etabin_cut_1_nodot, etabin_cut_2_nodot, variant }: {
    "pp200.000000__R0.5__eta05_09__NLOJet++" = fetchurl {
      url = "https://zenodo.org/record/6407316/files/run12pp200_R050.tab";
      sha256 = "149q5j1njdybrg3d03rxx5ljf0n976pf9n463rsvi3w8w6j705m2";
    };
    "pp200.000000__R0.5__eta05_09__NNLOJet" = [
      (fetchurl {
        url = "https://www.star.bnl.gov/~veprbl/1jetfc.NNLO.fnr0342-fc-v2_eta0_ptjet_v25.tab.gz";
        sha256 = "07qgfzsab04d46998q5hybp2akqhp2h6agikd1wnim9hr70rl2ys";
      })
      (fetchurl {
        url = "https://www.star.bnl.gov/~veprbl/1jetfc.NNLO.fnr0342-fc-v2_eta1_ptjet_v25.tab.gz";
        sha256 = "1shim0l4r82n8rs799a28payragp5bhyb1pvq9cacc28xdwfrqhy";
      })
    ];
    "pp510.000000__R0.5__eta05_09__NNLOJet" = [
      (fetchurl {
        url = "https://www.star.bnl.gov/~veprbl/1jetfc.NNLO.fnr1342-fc-v2_eta0_ptjet_v25.tab.gz";
        sha256 = "01msyhmqaba60qsi8fd6ib4bc8k2xihibq7cxswj4f9hwan1vd6b";
      })
      (fetchurl {
        url = "https://www.star.bnl.gov/~veprbl/1jetfc.NNLO.fnr1342-fc-v2_eta1_ptjet_v25.tab.gz";
        sha256 = "1gb8vyx446yhn2mgk890y0xl0np6vhmhxwfhr62l8iwn32l68rr7";
      })
    ];
    "pp200.000000__R0.6__eta05_09__NLOJet++" = fetchurl {
      url = "https://zenodo.org/record/6407316/files/run12pp200_R060.tab";
      sha256 = "101ivai57rh8fjx68jg6ssy0vdzqh5vz5jxjdq7yjh3dgkzrfl81";
    };
    "pp200.000000__R0.6__eta04_08__NLOJet++" = fetchurl {
      url = "https://zenodo.org/record/6413104/files/run12pp200_R060_04_08.tab";
      sha256 = "1yp51y819s6lpk3kwndbspmfkkdznrp04wvxisgby766rwj7n56m";
    };
    "pp510.000000__R0.5__eta05_09__NLOJet++" = fetchurl {
      url = "https://zenodo.org/record/6413104/files/run12pp510.tab";
      sha256 = "1w67b0yvnijfrwxlccqdpahwhxm4ifjr22ljgfmf9n04vaqmfs23";
    };
  }.${"pp${toString SQRT_S}__R${jet_r}__eta${etabin_cut_1_nodot}_${etabin_cut_2_nodot}__${variant}"};

  convolutions = { variant ? "NLOJet++", ... }@args: merge ([
    (convolution ({ jet_r = "0.5"; etabin_cut_1 = "0.5"; etabin_cut_2 = "0.9"; } // args))
  ] ++ lib.optionals ((SQRT_S == 200.) && (variant == "NLOJet++")) [
    (convolution ({ jet_r = "0.6"; etabin_cut_1 = "0.4"; etabin_cut_2 = "0.8"; } // args))
    (convolution ({ jet_r = "0.6"; etabin_cut_1 = "0.5"; etabin_cut_2 = "0.9"; } // args))
  ]);
  convolution = { pdf_set ? "CT14nlo", jet_r, etabin_cut_1, etabin_cut_2, order ? "NLO", variant ? "NLOJet++", uncertainty ? "L6" }:
    let
      ana_path = "/MC_STAR_JETS:ABS_ETA_BINS=0.0,${etabin_cut_1},${etabin_cut_2}:JET_R=${jet_r}";
      etabin_cut_1_nodot = remove_dot etabin_cut_1;
      etabin_cut_2_nodot = remove_dot etabin_cut_2;
      etabin_cut_1_float = builtins.fromJSON etabin_cut_1;
      etabin_cut_2_float = builtins.fromJSON etabin_cut_2;
      grid_files = get_grid_file { inherit jet_r etabin_cut_1_nodot etabin_cut_2_nodot variant; };
      # Capital "D" is required for multidimensional histograms by fnlo-tk-yodaout
      D = if builtins.isList grid_files then "d" else "D";
    in
      rename
      (rename
      (runCommandNoCC "fastnlo-${pdf_set}" {
        inherit pdf_set grid_files;
        nativeBuildInputs = [
          bc
          fastnlo_toolkit
          pdf_sets.${pdf_set}
          bits.yoda_for_yodamerge
        ];
        passthru.title = "${order} pQCD \\raisebox{0.1\\baselineskip}{$\\otimes$} ${builtins.replaceStrings [ "_" ] [ "\\_" ] pdf_set} ${variant}";
      } (''
        ix=0
        for grid_file in $grid_files; do
          if [[ "$grid_file" == *.gz ]]; then
            gunzip "$grid_file" --to-stdout > grid$ix.tab
          else
            cp "$grid_file" grid''${ix}.tab
          fi
          plot_ix=$(printf "%02d" $(( ix + 1 )))
          sed -i grid''${ix}.tab -e 's#provided by:#RIVET_ID=result/${D}'$plot_ix'-x01-y01#'
          fnlo-tk-yodaout grid$ix.tab "$pdf_set" ${uncertainty} ${order}
          ls -la
          cat ${order}_*_${uncertainty}_no*_no.yoda >> output.yoda
          rm ${order}_*_${uncertainty}_no*_no.yoda
          ix=$(( ix + 1 ))
        done

        # create scatter plot for the sum of the eta bins
        sed output.yoda -e 's#/result/d01-x01-y01#${ana_path}/jet_pt_fine#' > bin1.yoda
        sed output.yoda -e 's#/result/d02-x01-y01#${ana_path}/jet_pt_fine#' > bin2.yoda
        yodastack -o sum.yoda -m jet_pt \
          bin1.yoda:${toString ((etabin_cut_1_float - 0.0) / etabin_cut_2_float)} \
          bin2.yoda:${toString ((etabin_cut_2_float - etabin_cut_1_float) * 1.0 / etabin_cut_2_float)}
        '' + lib.optionalString (variant == "NNLOJet") ''
        ${yoda_for_yodascale}/bin/yodascale \
          -c "/result/d01-x01-y01 ${toString (1 / (2 * (etabin_cut_1_float - 0.0)))}x" \
          -c "/result/d02-x01-y01 ${toString (1 / (2 * (etabin_cut_2_float - etabin_cut_1_float)))}x" \
          -o output.yoda \
          output.yoda
        '' + ''
        cat sum.yoda >>output.yoda

        gzip output.yoda
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      ''))
      "/result/d01-x01-y01"
      "${ana_path}/jet_pt_fine_00_${etabin_cut_1_nodot}")
      "/result/d02-x01-y01"
      "${ana_path}/jet_pt_fine_${etabin_cut_1_nodot}_${etabin_cut_2_nodot}";
})
