@name@ () {
  addToSearchPath RIVET_REF_PATH "@out@"
  addToSearchPath RIVET_ANALYSIS_PATH "@out@"
  addToSearchPath RIVET_INFO_PATH "@out@"
}

postHooks+=(@name@)
