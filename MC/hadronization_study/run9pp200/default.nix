{ nixpkgs ? import <nixpkgs> {}, py6 ? import ../../pythia6/bits.nix { inherit nixpkgs; } }:

with import ../../bits.nix { inherit nixpkgs; };

let
  py6massfull = py6.override (final: prev: {
    rivet_analyses = [
      "STAR_2017_I1493842:MASS=full"
    ];
  });
  py6massred = py6.override (final: prev: {
    rivet_analyses = [
      "STAR_2017_I1493842:MASS=reduced"
    ];
  });

  inherit (py6) PERUGIA_0;
in

rec {
  f_ue_had = mkhtml [
    (set_title "$\\text{NLO} * f_{\\text{UE+had.}} / \\text{NLO}$"
      (yoda_ratio
        # $\text{NLO} * f_{\text{UEH}}$ from https://doi.org/10.17182/hepdata.77208.v1/t4
        (to_output ./hepdata.yoda)
        # The raw NLO from https://drupal.star.bnl.gov/STAR/blog/veprbl/dijet-nlo-theory-sqrts200-gev
        (to_output ./nlo.yoda))
    // { plot_options = ":Scale=${toString (1.6 /*=deta*/ * 1.6 /*=deta*/ * 1.e6 /*=(picobarn/microbarn)*/)}"; })
  ] "-sv PLOT:LogY=0:YMin=0.9:YMax=1.9:Title=:YLabel='$f_{\\text{UE+had.}}$'";

  f_ue_had_repro = mkhtml (f_ue_had.yoda_files ++ [
    (set_title "Perugia 0 (full mass)$/$(no Had., no MPI, reduced mass)"
      (yoda_ratio
        (rename (py6massfull.runMC_splitbin 1 { tune = PERUGIA_0; }) ":MASS=full" "")
        (rename (py6massred.runMC_splitbin 1 { tune = PERUGIA_0; enable_had = false; enable_mi = false; }) ":MASS=reduced" "")))
    (set_title "Perugia 0 (no decay, full mass)$/$(no Had., no MPI, reduced mass)"
      (yoda_ratio
        (rename (py6massfull.runMC_splitbin 1 { tune = PERUGIA_0; disable_decay = true; }) ":MASS=full" "")
        (rename (py6massred.runMC_splitbin 1 { tune = PERUGIA_0; enable_had = false; enable_mi = false; }) ":MASS=reduced" "")))
    (set_title "Starugia 2012 (no decay, full mass)$/$(no Had., no MPI, reduced mass)"
      (yoda_ratio
        (rename (py6massfull.runMC_splitbin 1 { disable_decay = true; }) ":MASS=full" "")
        (rename (py6massred.runMC_splitbin 1 { enable_had = false; enable_mi = false; }) ":MASS=reduced" "")))
    (set_title "Starugia 2012 (no decay, full mass)$/$(no Had., no MPI, full mass)"
      (yoda_ratio
        (rename (py6massfull.runMC_splitbin 1 { disable_decay = true; }) ":MASS=full" "")
        (rename (py6massfull.runMC_splitbin 1 { enable_had = false; enable_mi = false; }) ":MASS=full" "")))
  ]) "-s -m '/STAR_2017_I1493842/' PLOT:LegendXPos=0.04:LogY=0:YMin=0.9:YMax=1.9:Title=:YLabel='$f_{\\text{UE+had.}}$'";

  f_had = mkhtml [
    (set_title "Starugia 2012 (no decay, full mass)$/$(no Had., reduced mass)"
      (yoda_ratio
        (rename (py6massfull.runMC_splitbin 1 { disable_decay = true; }) ":MASS=full" "")
        (rename (py6massred.runMC_splitbin 1 { enable_had = false; }) ":MASS=reduced" "")))
    (set_title "Starugia 2012 (no decay, full mass)$/$(no Had., full mass)"
      (yoda_ratio
        (rename (py6massfull.runMC_splitbin 1 { disable_decay = true; }) ":MASS=full" "")
        (rename (py6massfull.runMC_splitbin 1 { enable_had = false; }) ":MASS=full" "")))
  ] "-s -m '/STAR_2017_I1493842/' PLOT:LegendXPos=0.04:LogY=0:YMin=0.9:YMax=1.9:Title=:YLabel='$f_{\\text{had.}}$'";

  f_ue_had_nomass = mkhtml [
    (set_title "Pythia ${py6massred.version} (Starugia 2012, no Had., no MPI)" (py6massred.runMC_splitbin 1 { enable_had = false; enable_mi = false; }))
    (set_title "Pythia ${py6massfull.version} (Starugia 2012, no decay)" (py6massfull.runMC_splitbin 1 { disable_decay = true; }))
  ] "-s PLOT:LegendXPos=0.05:RatioPlotYMin=0.5:RatioPlotYMax=1.5";
}
