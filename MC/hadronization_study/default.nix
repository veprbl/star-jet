{ nixpkgs ? import <nixpkgs> {}, nobatch ? false }:

with import ../../common.nix { inherit (nixpkgs) lib; };

let
  py6 = (import ../pythia6/bits.nix { inherit nixpkgs; }).override (final: prev: { inherit nobatch; });
  py8 = (import ../pythia8/bits.nix { inherit nixpkgs; }).override (final: prev: { inherit nobatch; });

  overlay_pp510 = final: prev: {
    inherit nobatch;

    SQRT_S = 510.0 /* GeV */;
    rivet_analyses = [
      "MC_PARTONIC_PT"
      "STAR_2012_JETXSection_510_Prelim"
    ];
  };

  py6_pp510 = (import ../pythia6/bits.nix { inherit nixpkgs; }).override overlay_pp510;
  py8_pp510 = (import ../pythia8/bits.nix { inherit nixpkgs; }).override overlay_pp510;

  f_ue_had_latex = "$f_{\\text{UE}+\\text{had.}} = \\sigma_{\\text{particle}} / \\sigma_{\\text{parton, no MI}}$";
  f_had_latex = "$f_{\\text{had.}} = \\sigma_{\\text{particle}} / \\sigma_{\\text{parton}}$";
in

(import ../bits.nix { inherit nixpkgs; }).override (final: prev: add_eval_scope (with final; {
  inherit nobatch;

  f_had_py6 = mkhtml' "-m 'MC_STAR_JETS:JET_R=0.5.*jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.5; RatioPlotYMax = 1.5; RatioPlotYLabel = "${f_had_latex}"; } [
    (set_title "Pythia ${py6.version} (Starugia 2012, no Had.)" (py6.runMC_splitbin 10 { enable_had = false; }))
    (set_title "Pythia ${py6.version} (Starugia 2012)" (py6.runMC_splitbin 10 { }))
  ];

  f_had_py6_pp510 = mkhtml' "" { LegendXPos = 0.1; RatioPlotYMin = 0.5; RatioPlotYMax = 1.5; RatioPlotYLabel = "${f_had_latex}"; } [
    (set_title "Pythia ${py6_pp510.version} (Starugia 2012, no Had.)" (py6_pp510.runMC_splitbin 10 { enable_had = false; }))
    (set_title "Pythia ${py6_pp510.version} (Starugia 2012)" (py6_pp510.runMC_splitbin 10 { }))
  ];

  f_had_py6_tune_a = mkhtml' "-m 'jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.5; RatioPlotYMax = 1.5; RatioPlotYLabel = "${f_had_latex}"; } [
    (set_title "Pythia ${py6.version} (Tune A, no Had.)" (py6.runMC_splitbin 1 { enable_had = false; tune = py6.TUNE_A; }))
    (set_title "Pythia ${py6.version} (Tune A)" (py6.runMC_splitbin 1 { tune = py6.TUNE_A; }))
  ];

  f_had_py6_perugia_0 = mkhtml' "-m 'jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.5; RatioPlotYMax = 1.5; RatioPlotYLabel = "${f_had_latex}"; } [
    (set_title "Pythia ${py6.version} (Perugia 0, no Had.)" (py6.runMC_splitbin 1 { enable_had = false; tune = py6.PERUGIA_0; }))
    (set_title "Pythia ${py6.version} (Perugia 0)" (py6.runMC_splitbin 1 { tune = py6.PERUGIA_0; }))
  ];

  f_had_py8 = mkhtml' "-m 'jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.5; RatioPlotYMax = 1.5; RatioPlotYLabel = "${f_had_latex}"; } [
    (set_title "Pythia ${py8.version} (Monash 2013, no Had.)" (py8.runMC_splitbin 1 { tune = "Monash 2013"; enable_had = false; }))
    (set_title "Pythia ${py8.version} (Monash 2013)" (py8.runMC_splitbin 1 { tune = "Monash 2013"; }))
  ];

  f_had_py8_cms = mkhtml' "-m 'jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.5; RatioPlotYMax = 1.5; RatioPlotYLabel = "${f_had_latex}"; } [
    (set_title "Pythia ${py8.version} (CUETP8S1-CTEQ6L1, no Had.)" (py8.runMC_splitbin 1 { tune = "CUETP8S1-CTEQ6L1"; enable_had = false; }))
    (set_title "Pythia ${py8.version} (CUETP8S1-CTEQ6L1)" (py8.runMC_splitbin 1 { tune = "CUETP8S1-CTEQ6L1"; }))
  ];

  f_had_py8_atlas = mkhtml' "-m 'jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.5; RatioPlotYMax = 1.5; RatioPlotYLabel = "${f_had_latex}"; } [
    (set_title "Pythia ${py8.version} (A14 CTEQL1, no Had.)" (py8.runMC_splitbin 1 { tune = "A14 CTEQL1"; enable_had = false; }))
    (set_title "Pythia ${py8.version} (A14 CTEQL1)" (py8.runMC_splitbin 1 { tune = "A14 CTEQL1"; }))
  ];

  mk_f_had = mc: tune:
    yoda_ratio
      (mc.runMC_splitbin 4 (tune))
      (mc.runMC_splitbin 4 ({ enable_had = false; } // tune));

  mk_f_ue_had = mc: tune:
    yoda_ratio
      (mc.runMC_splitbin 4 (tune))
      (mc.runMC_splitbin 4 ({ enable_had = false; enable_mi = false; } // tune));

  f_had_perugia12 =
      mkhtml' "--remove-options -m 'MC_STAR_JETS:JET_R=0.5.*jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.9; RatioPlotYMax = 1.1; LogY = 0; XMin = 5.0; YMin = 0.5; YMax = 1.5; YLabel="${f_had_latex}"; } [
        (set_title "Pythia ${py6.version} (Starugia 2012, 370)" (mk_f_had py6 { tune = 370; }))
        (set_title "Pythia ${py6.version} (Starugia 2012, 374-loCR)" (mk_f_had py6 { tune = 374; }))
        (set_title "Pythia ${py6.version} (Starugia 2012, 375-noCR)" (mk_f_had py6 { tune = 375; }))
        (set_title "Pythia ${py6.version} (Starugia 2012, 376-FL)" (mk_f_had py6 { tune = 376; }))
        (set_title "Pythia ${py6.version} (Starugia 2012, 377-FT)" (mk_f_had py6 { tune = 377; }))
      ];

  f_had_jet_r =
      mkhtml' "--no-ratio -m 'MC_STAR_JETS.*jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.9; RatioPlotYMax = 1.1; LogY = 0; XMin = 5.0; YMin = 0.5; YMax = 1.5; YLabel="${f_had_latex}"; } [
        (set_title "Pythia ${py6.version} (Starugia 2012)" (mk_f_had py6 {}))
      ];

  f_ue_had_jet_r =
      mkhtml' "--no-ratio -m 'MC_STAR_JETS.*jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.9; RatioPlotYMax = 1.1; LogY = 0; XMin = 5.0; YMin = 0.5; YMax = 1.5; YLabel="${f_ue_had_latex}"; } [
        (set_title "Pythia ${py6.version} (Starugia 2012)" (mk_f_ue_had py6 {}))
      ];

  f_had_parp90 =
      mkhtml' "--remove-options -m 'MC_STAR_JETS:JET_R=0.5.*jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.9; RatioPlotYMax = 1.1; LogY = 0; XMin = 5.0; YMin = 0.5; YMax = 1.5; YLabel="${f_had_latex}"; } [
        (set_title "Pythia ${py6.version} (Starugia 2012)" (mk_f_had py6 {}))
        (set_title "Pythia ${py6.version} (Starugia 2012, PARP(90)$ = 0.24$)" (mk_f_had py6 { parp90 = "0.24"; }))
        (set_title "Pythia ${py6.version} (Starugia 2012, PARP(90)$ = 0.19$)" (mk_f_had py6 { parp90 = "0.19"; }))
      ];

  f_ue_had_parp90 =
      mkhtml' "--remove-options -m 'MC_STAR_JETS:JET_R=0.5.*jet_pt$'" { LegendXPos = 0.1; RatioPlotYMin = 0.9; RatioPlotYMax = 1.1; LogY = 0; XMin = 5.0; YMin = 0.5; YMax = 1.5; YLabel="${f_ue_had_latex}"; } [
        (set_title "Pythia ${py6.version} (Starugia 2012)" (mk_f_ue_had py6 {}))
        (set_title "Pythia ${py6.version} (Starugia 2012, PARP(90)$ = 0.15$)" (mk_f_ue_had py6 { parp90 = "0.15"; }))
      ];

  f_had_model_change =
      mkhtml' "--no-ratio --remove-options -m 'MC_STAR_JETS:JET_R=0.5.*jet_pt'" { LegendXPos = 0.1; RatioPlotYMin = 0.9; RatioPlotYMax = 1.1; LogY = 0; XMin = 5.0; YMin = 0.5; YMax = 1.5; YLabel="${f_had_latex}"; } [
        (set_title "Pythia ${py6.version} (Starugia 2012)" (mk_f_had py6 {}))
        (set_title "Pythia ${py6.version} (Tune A)" (mk_f_had py6 { tune = py6.TUNE_A; }))
        (set_title "Pythia ${py6.version} (Perugia 0)" (mk_f_had py6 { tune = py6.PERUGIA_0; }))
        (set_title "Pythia ${py8.version} (Detroit)" (mk_f_had py8 { tune = "Detroit"; }))
        (set_title "Pythia ${py8.version} (Monash 2013)" (mk_f_had py8 { tune = "Monash 2013"; }))
        (set_title "Pythia ${py8.version} (CUETP8S1-CTEQ6L1)" (mk_f_had py8 { tune = "CUETP8S1-CTEQ6L1"; }))
        (set_title "Pythia ${py8.version} (A14 CTEQL1)" (mk_f_had py8 { tune = "A14 CTEQL1"; }))
      ];

  f_ue_had_model_change =
      mkhtml' "--no-ratio --remove-options -m 'MC_STAR_JETS:JET_R=0.5.*jet_pt$'" { LegendXPos = 0.1; RatioPlotYMin = 0.9; RatioPlotYMax = 1.1; LogY = 0; XMin = 5.0; YMin = 0.5; YMax = 1.5; YLabel="${f_ue_had_latex}"; } [
        (set_title "Pythia ${py6.version} (Starugia 2012)" (mk_f_ue_had py6 {}))
        (set_title "Pythia ${py6.version} (Tune A)" (mk_f_ue_had py6 { tune = py6.TUNE_A; }))
        (set_title "Pythia ${py6.version} (Perugia 0)" (mk_f_ue_had py6 { tune = py6.PERUGIA_0; }))
        (set_title "Pythia ${py8.version} (Detroit)" (mk_f_ue_had py8 { tune = "Detroit"; }))
        (set_title "Pythia ${py8.version} (Monash 2013)" (mk_f_ue_had py8 { tune = "Monash 2013"; }))
        (set_title "Pythia ${py8.version} (CUETP8S1-CTEQ6L1)" (mk_f_ue_had py8 { tune = "CUETP8S1-CTEQ6L1"; }))
        (set_title "Pythia ${py8.version} (A14 CTEQL1)" (mk_f_ue_had py8 { tune = "A14 CTEQL1"; }))
      ];

  run9pp200 = import ./run9pp200 { inherit nixpkgs py6; };
}))
