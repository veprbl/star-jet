# BEGIN PLOT /MC_PARTONIC_PT/partonic_pt
XLabel=Hard scale
YLabel=$d\sigma/dQ$ [pb/GeV]
XMin=0.
XMax=15.
YMax=2e11
YMin=1e5
# END PLOT
