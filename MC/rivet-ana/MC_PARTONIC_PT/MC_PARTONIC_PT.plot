# BEGIN PLOT /MC_PARTONIC_PT/partonic_pt
XLabel=$Q$, GeV
YLabel=$d\sigma/dQ$, pb/GeV
XMin=0.
XMax=15.
YMax=2e11
YMin=1e5
# END PLOT

# BEGIN PLOT /MC_PARTONIC_PT/avg_.*_vs_partonic_pt
XLabel=$Q$, GeV
LogY=0
# END PLOT

# BEGIN PLOT /MC_PARTONIC_PT/avg_lead_pt_vs_partonic_pt
YLabel=Average leading jet $p_T$, GeV
# END PLOT

# BEGIN PLOT /MC_PARTONIC_PT/avg_sublead_pt_vs_partonic_pt
YLabel=Average subleading jet $p_T$, GeV
# END PLOT

# BEGIN PLOT /MC_PARTONIC_PT/avg_numjet_vs_partonic_pt
YLabel=Average number of jets
# END PLOT
