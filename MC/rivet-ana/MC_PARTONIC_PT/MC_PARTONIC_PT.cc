// vim:et sw=2 sts=2
// -*- C++ -*-

// --- BEGIN UGLY
#include "Rivet/Config/RivetCommon.hh"
#include "Rivet/Particle.hh"
#define private public // access to _genevent_original
#include "Rivet/Event.hh"
#undef private
// --- END UGLY

#include "Rivet/Analysis.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_PARTONIC_PT : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_PARTONIC_PT);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      _h_partonic_pt = bookHisto1D("partonic_pt", 40*5, 0., 40.);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double hard_pt = event._genevent_original->event_scale();
      const double weight = event.weight();

      _h_partonic_pt->fill(hard_pt, weight);
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_partonic_pt, crossSection()/sumOfWeights());

    }

    //@}


  private:


    /// @name Histograms
    //@{

    Histo1DPtr _h_partonic_pt;

    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_PARTONIC_PT);


}
