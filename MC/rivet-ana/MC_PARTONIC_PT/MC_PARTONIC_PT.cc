// vim:et sw=2 sts=2
// -*- C++ -*-

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_PARTONIC_PT : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_PARTONIC_PT);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      const FinalState fs;
      declare(FastJets(fs, FastJets::ANTIKT, 0.6), "Jets");

      book(_h_partonic_pt, "partonic_pt", 40*5, 0., 40.);
      book(_p_avg_lead_pt_vs_partonic_pt, "avg_lead_pt_vs_partonic_pt", 40*5, 0., 40.);
      book(_p_avg_sublead_pt_vs_partonic_pt, "avg_sublead_pt_vs_partonic_pt", 40*5, 0., 40.);
      book(_p_avg_numjet_vs_partonic_pt, "avg_numjet_vs_partonic_pt", 40*5, 0., 40.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double hard_pt = event.genEvent()->event_scale();
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 5*GeV) && (Cuts::abseta < 0.8));

      _h_partonic_pt->fill(hard_pt);

      _p_avg_numjet_vs_partonic_pt->fill(hard_pt, jets.size());
      if (jets.size() >= 1) {
        double leading_jet_pt = jets[0].pt();
        _p_avg_lead_pt_vs_partonic_pt->fill(hard_pt, leading_jet_pt);
        if (jets.size() >= 2) {
          double subleading_jet_pt = jets[1].pt();
          _p_avg_sublead_pt_vs_partonic_pt->fill(hard_pt, subleading_jet_pt);
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_partonic_pt, crossSection()/sumOfWeights());
      _p_avg_lead_pt_vs_partonic_pt->scaleW(crossSection()/sumOfWeights());
      _p_avg_sublead_pt_vs_partonic_pt->scaleW(crossSection()/sumOfWeights());
      _p_avg_numjet_vs_partonic_pt->scaleW(crossSection()/sumOfWeights());

    }

    //@}


  private:


    /// @name Histograms
    //@{

    Histo1DPtr _h_partonic_pt;
    Profile1DPtr _p_avg_lead_pt_vs_partonic_pt;
    Profile1DPtr _p_avg_sublead_pt_vs_partonic_pt;
    Profile1DPtr _p_avg_numjet_vs_partonic_pt;

    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_PARTONIC_PT);


}
