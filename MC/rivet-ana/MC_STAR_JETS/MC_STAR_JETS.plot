# BEGIN PLOT /MC_STAR_JETS/num_mpi$
XLabel=number of MPI (from \texttt{MINT(351)})
YLabel=$\mathrm{d}\sigma/\mathrm{d}N$, pb
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d01-x01-y02
RatioPlot=0
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d.+-x02-.+
LogY=0
XLabel=Partonic $p_T$
XMin=2
XMax=15
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/jet_pt$
XLabel=jet $p_T$, GeV
YLabel=$\mathrm{d}^2\sigma/(\mathrm{d}p_T\mathrm{d}\eta)$, pb/GeV
XMin=0
XMax=60
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/jet_pt_ue_corr$
XLabel=jet $p_T$ - (jet area)$ * \rho_{\text{UE}}$, GeV
YLabel=$\mathrm{d}^2\sigma/(\mathrm{d}p_T\mathrm{d}\eta)$, pb/GeV
XMin=0
XMax=60
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/ue_density.*
XLabel=jet $p_T$, GeV
YLabel=average UE density, GeV
LogY=0
# END PLOT
# BEGIN PLOT /MC_STAR_JETS/ue_density_vs.*
Title=all jets
# END PLOT
# BEGIN PLOT /MC_STAR_JETS/ue_density_nonzero_vs.*
Title=jets with non-zero UE density
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/ue_sumpt_for.*
XLabel=$\sum p_T^{\text{UE}}, \text{ GeV}$
# END PLOT
# BEGIN PLOT /MC_STAR_JETS/.*_for_jet_pt_05_10
YLabel=Yield for jets with $5\text{ GeV} < p_T^{\text{jet}} < 10\text{ GeV}$
# END PLOT
# BEGIN PLOT /MC_STAR_JETS/.*_for_jet_pt_10_15
YLabel=Yield for jets with $10\text{ GeV} < p_T^{\text{jet}} < 15\text{ GeV}$
# END PLOT
# BEGIN PLOT /MC_STAR_JETS/.*_for_jet_pt_15_20
YLabel=Yield for jets with $15\text{ GeV} < p_T^{\text{jet}} < 20\text{ GeV}$
# END PLOT
# BEGIN PLOT /MC_STAR_JETS/.*_for_jet_pt_20_30
YLabel=Yield for jets with $20\text{ GeV} < p_T^{\text{jet}} < 30\text{ GeV}$
# END PLOT
# BEGIN PLOT /MC_STAR_JETS/.*_for_jet_pt_30_40
YLabel=Yield for jets with $30\text{ GeV} < p_T^{\text{jet}} < 40\text{ GeV}$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d01-x01-y02
XLabel=jet $p_T$
XMin=0
XMax=60
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d01-x01-y0[34]
XLabel=jet $\eta$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d01-x01-y0[24]
LogY=0
YLabel=average $R_T$
YMin=0.
YMax=1.
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d01-x01-y03
YLabel=$d\sigma/d\eta$, pb
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/jet_rt_at_jet_pt_.*
LogY=0
XLabel=jet $R_T$
YLabel=probability
YMax=0.8
NormalizeToSum=1
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_.*
LogY=0
XLabel=jet $p_T$
YMin=0.
YMax=0.5
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_antiproton
YLabel=average $\left( \sum_{\text{antiproton in jet}} p_{T;i} / \sum p_{T} \right)$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_gamma
YLabel=average $\left( \sum_{\gamma\text{ in jet}} p_{T;i} / \sum p_{T} \right)$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_k0l
YLabel=average $\left( \sum_{K^0_L\text{ in jet}} p_{T;i} / \sum p_{T} \right)$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_kminus
YLabel=average $\left( \sum_{K^-\text{ in jet}} p_{T;i} / \sum p_{T} \right)$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_kplus
YLabel=average $\left( \sum_{K^+\text{ in jet}} p_{T;i} / \sum p_{T} \right)$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_neutron
YLabel=average $\left( \sum_{\text{neutron in jet}} p_{T;i} / \sum p_{T} \right)$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_pi0
YLabel=average $\left( \sum_{\pi^0\text{ in jet}} p_{T;i} / \sum p_{T} \right)$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_piminus
YLabel=average $\left( \sum_{\pi^-\text{ in jet}} p_{T;i} / \sum p_{T} \right)$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_piplus
YLabel=average $\left( \sum_{\pi^+\text{ in jet}} p_{T;i} / \sum p_{T} \right)$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/rt_vs_jet_pt_proton
YLabel=average $\left( \sum_{\text{proton in jet}} p_{T;i} / \sum p_{T} \right)$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d01-x02-y01
YLabel=$R_T$
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d01-x02-y02
YLabel=Jet neutral particle multiplicity
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d01-x02-y03
YLabel=Jet charged particle multiplicity
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d02-x02-y01
YLabel=Leading jet (TransP+TransM)/2 UE density, GeV
# END PLOT

# BEGIN PLOT /MC_STAR_JETS/d02-x02-y02
YLabel=Leading jet cone UE density, GeV
# END PLOT
