// vim:et sw=2 sts=2
// -*- C++ -*-

#include <unordered_map>

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/NeutralFinalState.hh"
#include "Rivet/Projections/MergedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_STAR_JETS : public Analysis {
  private:

    constexpr double square(double x) {
      return x * x;
    }
    double Phi_mpi_pi(double phi) {
      return std::remainder(phi, 2 * M_PI);
    }
    const double REGION_AREA = 2 * M_PI / 3;
    const double UE_CONE_R = 0.5;
    const double UE_CONE_AREA = M_PI * square(UE_CONE_R);
    double max_abs_eta = -1;

  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_STAR_JETS);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      const ChargedFinalState cfs;
      const NeutralFinalState nfs;
      const MergedFinalState jfs(cfs, nfs);
      declare(jfs, "FS");

      double jet_radius = getOption<float>("JET_R", 0.6);
      FastJets fj(jfs, FastJets::ANTIKT, jet_radius, JetAlg::Muons::ALL, JetAlg::Invisibles::NONE);
      fj.useJetArea(new fastjet::AreaDefinition(
        fastjet::AreaType::active_area,
        fastjet::GhostedAreaSpec(1.5, 1, 0.04, 1.0, 0.1, 1e-100)
      ));
      declare(fj, "Jets");

      double max_pt;
      // TODO use isCompatibleWithSqrtS in Rivet 3.1.5+
      if (fuzzyEquals(sqrtS()/GeV, 200., 1e-3)) {
        max_pt = 60.;
      } else if (fuzzyEquals(sqrtS()/GeV, 510., 1e-3)) {
        max_pt = 80.;
      } else {
        max_pt = 60.;
      }

      vector<string> abs_eta_bins_s = split(getOption<string>("ABS_ETA_BINS", "0.0,0.4,0.8"), ",");
      double bin_low = -1;
      string suffix_prev;
      for(const string &val_s : abs_eta_bins_s) {
        char *endptr = NULL;
        double bin_high = strtod(val_s.c_str(), &endptr);
        if ((endptr == nullptr) || (*endptr != '\x0')) {
          throw UserError("Invalid value in ABS_ETA_BINS!");
        }
        if (bin_high < 0) {
          throw UserError("Negative value in ABS_ETA_BINS!");
        }
        string suffix = val_s;
        size_t pos = suffix.find('.');
        if (pos != string::npos) {
          suffix.erase(pos, 1);
        }
        if (bin_low >= 0) {
          Histo1DPtr _h;
          book(_h, "jet_pt_fine_" + suffix_prev + "_" + suffix, max_pt * 10, 0., max_pt);
          _h_jet_pt_fine_etabin.emplace_back(bin_low, bin_high, _h);
          Histo1DPtr _h_ue_corr;
          book(_h_ue_corr, "jet_pt_ue_corr_fine_" + suffix_prev + "_" + suffix, max_pt * 10, 0., max_pt);
          _h_jet_pt_ue_corr_fine_etabin.emplace_back(bin_low, bin_high, _h_ue_corr);
        }
        bin_low = bin_high;
        suffix_prev = suffix;
        max_abs_eta = std::max(max_abs_eta, bin_high);
      }

      book(_h_num_mpi, "num_mpi", 20, 0., 20.);

      book(_h_jet_area, "jet_area", 20, 0., 2.);
      book(_h_jet_pt, "jet_pt", max_pt * 1, 0., max_pt);
      book(_h_jet_pt_fine, "jet_pt_fine", max_pt * 10, 0., max_pt);
      book(_h_jet_pt_ue_corr, "jet_pt_ue_corr", max_pt * 1, 0., max_pt);
      book(_h_jet_pt_ue_corr_fine, "jet_pt_ue_corr_fine", max_pt * 10, 0., max_pt);
      book(_h_jet_rt_vs_jet_pt, "d01-x01-y02", max_pt * 1, 0., max_pt);
      book(_h_jet_rt_at_jet_pt_10_12, "jet_rt_at_jet_pt_10_12", 21, 0., 1. + 1. / 20);
      book(_h_jet_rt_at_jet_pt_20_22, "jet_rt_at_jet_pt_20_22", 21, 0., 1. + 1. / 20);
      book(_h_jet_rt_at_jet_pt_30_32, "jet_rt_at_jet_pt_30_32", 21, 0., 1. + 1. / 20);
      book(_h_jet_eta, "d01-x01-y03", 20, -1., 1.);
      book(_h_jet_rt_vs_jet_eta, "d01-x01-y04", 20, -1., 1.);

      book(_h_rt_vs_jet_pt_antiproton, "rt_vs_jet_pt_antiproton", max_pt * 1, 0., max_pt);
      book(_h_rt_vs_jet_pt_gamma, "rt_vs_jet_pt_gamma", max_pt * 1, 0., max_pt);
      book(_h_rt_vs_jet_pt_k0l, "rt_vs_jet_pt_k0l", max_pt * 1, 0., max_pt);
      book(_h_rt_vs_jet_pt_kplus, "rt_vs_jet_pt_kplus", max_pt * 1, 0., max_pt);
      book(_h_rt_vs_jet_pt_kminus, "rt_vs_jet_pt_kminus", max_pt * 1, 0., max_pt);
      book(_h_rt_vs_jet_pt_neutron, "rt_vs_jet_pt_neutron", max_pt * 1, 0., max_pt);
      book(_h_rt_vs_jet_pt_pi0, "rt_vs_jet_pt_pi0", max_pt * 1, 0., max_pt);
      book(_h_rt_vs_jet_pt_piplus, "rt_vs_jet_pt_piplus", max_pt * 1, 0., max_pt);
      book(_h_rt_vs_jet_pt_piminus, "rt_vs_jet_pt_piminus", max_pt * 1, 0., max_pt);
      book(_h_rt_vs_jet_pt_proton, "rt_vs_jet_pt_proton", max_pt * 1, 0., max_pt);

      book(_h_jet_hpt_rt, "d01-x02-y01", 80, 0., 40);
      book(_h_jet_ntrl_mult, "d01-x02-y02", 80, 0., 40.);
      book(_h_jet_chg_mult, "d01-x02-y03", 80, 0., 40.);

      book(_h_ue_hpt_uetrans, "d02-x02-y01", 80, 0., 40.);
      book(_h_ue_hpt_uecone, "d02-x02-y02", 80, 0., 40.);

      book(_h_ue_density_vs_jet_pt, "ue_density_vs_jet_pt", max_pt * 1, 0., max_pt);
      book(_h_ue_density_nonzero_vs_jet_pt, "ue_density_nonzero_vs_jet_pt", max_pt * 1, 0., max_pt);
      book(_h_ue_sumpt_for_jet_pt_05_10, "ue_sumpt_for_jet_pt_05_10", 40, 0., 10.);
      book(_h_ue_sumpt_for_jet_pt_10_15, "ue_sumpt_for_jet_pt_10_15", 40, 0., 10.);
      book(_h_ue_sumpt_for_jet_pt_15_20, "ue_sumpt_for_jet_pt_15_20", 40, 0., 10.);
      book(_h_ue_sumpt_for_jet_pt_20_30, "ue_sumpt_for_jet_pt_20_30", 40, 0., 10.);
      book(_h_ue_sumpt_for_jet_pt_30_40, "ue_sumpt_for_jet_pt_30_40", 40, 0., 10.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 1.5*GeV) && (Cuts::abseta < max_abs_eta));
      const FinalState& fs = apply<FinalState>(event, "FS");
      const double hard_pt = event.genEvent()->event_scale();
      const int num_mpi = event.genEvent()->mpi();
      const shared_ptr<fastjet::ClusterSequenceArea> csa = fj.clusterSeqArea();

      _h_num_mpi->fill(num_mpi);

      for(auto jet : jets) {
        double sum_cone_pt = 0.;
        for(const Particle& p : fs.particles()) {
          const double dphi_plus = Phi_mpi_pi(p.phi() + M_PI_2 - jet.phi());
          const double dphi_minus = Phi_mpi_pi(p.phi() - M_PI_2 - jet.phi());
          const double deta = p.eta() - jet.eta();
          if (
              (square(dphi_plus) + square(deta) < square(UE_CONE_R))
           || (square(dphi_minus) + square(deta) < square(UE_CONE_R))
            ) {
            sum_cone_pt += p.pt();
          }
        }
        std::unordered_map<PdgId, double> particle_sum_pt;
        double pt_neutral = 0., pt_charged = 0.;
        int num_neutral = 0, num_charged = 0;
        for(const Particle& p : jet.particles()) {
          const PdgId pid = p.pid();
          if (PID::threeCharge(pid) == 0) {
            pt_neutral += p.pt();
            num_neutral++;
          } else {
            pt_charged += p.pt();
            num_charged++;
          }
          particle_sum_pt[pid] += p.pt();
        }
        double pt_sum = pt_neutral + pt_charged;
        double rt = pt_neutral / pt_sum;

        double ue_density = sum_cone_pt / 2 / UE_CONE_AREA;
        double jet_area = csa->area(jet);
        _h_jet_area->fill(jet_area);
        _h_jet_pt->fill(jet.pt());
        _h_jet_pt_fine->fill(jet.pt());
        _h_jet_pt_ue_corr->fill(jet.pt() - ue_density * jet_area);
        _h_jet_pt_ue_corr_fine->fill(jet.pt() - ue_density * jet_area);
        for(auto &t : _h_jet_pt_fine_etabin) {
          double bin_low = std::get<0>(t);
          double bin_high = std::get<1>(t);
          Histo1DPtr h = std::get<2>(t);
          if ((jet.abseta() >= bin_low) && (jet.abseta() < bin_high)) {
            h->fill(jet.pt());
          }
        }
        for(auto &t : _h_jet_pt_ue_corr_fine_etabin) {
          double bin_low = std::get<0>(t);
          double bin_high = std::get<1>(t);
          Histo1DPtr h = std::get<2>(t);
          if ((jet.abseta() >= bin_low) && (jet.abseta() < bin_high)) {
            h->fill(jet.pt() - ue_density * jet_area);
          }
        }
        _h_jet_rt_vs_jet_pt->fill(jet.pt(), rt);
        if ((jet.pt() > 10 /* GeV */) && (jet.pt() < 12 /* GeV */)) {
          _h_jet_rt_at_jet_pt_10_12->fill(rt);
        }
        if ((jet.pt() > 20 /* GeV */) && (jet.pt() < 22 /* GeV */)) {
          _h_jet_rt_at_jet_pt_20_22->fill(rt);
        }
        if ((jet.pt() > 30 /* GeV */) && (jet.pt() < 32 /* GeV */)) {
          _h_jet_rt_at_jet_pt_30_32->fill(rt);
        }
        _h_jet_eta->fill(jet.eta());
        _h_jet_rt_vs_jet_eta->fill(jet.eta(), rt);

        _h_rt_vs_jet_pt_antiproton->fill(jet.pt(), particle_sum_pt[PID::ANTIPROTON] / pt_sum);
        _h_rt_vs_jet_pt_gamma->fill(jet.pt(), particle_sum_pt[PID::GAMMA] / pt_sum);
        _h_rt_vs_jet_pt_k0l->fill(jet.pt(), particle_sum_pt[PID::K0L] / pt_sum);
        _h_rt_vs_jet_pt_kplus->fill(jet.pt(), particle_sum_pt[PID::KPLUS] / pt_sum);
        _h_rt_vs_jet_pt_kminus->fill(jet.pt(), particle_sum_pt[PID::KMINUS] / pt_sum);
        _h_rt_vs_jet_pt_neutron->fill(jet.pt(), particle_sum_pt[PID::NEUTRON] / pt_sum);
        _h_rt_vs_jet_pt_pi0->fill(jet.pt(), particle_sum_pt[PID::PI0] / pt_sum);
        _h_rt_vs_jet_pt_piplus->fill(jet.pt(), particle_sum_pt[PID::PIPLUS] / pt_sum);
        _h_rt_vs_jet_pt_piminus->fill(jet.pt(), particle_sum_pt[PID::PIMINUS] / pt_sum);
        _h_rt_vs_jet_pt_proton->fill(jet.pt(), particle_sum_pt[PID::PROTON] / pt_sum);

        _h_jet_hpt_rt->fill(hard_pt, rt);
        _h_jet_ntrl_mult->fill(hard_pt, num_neutral);
        _h_jet_chg_mult->fill(hard_pt, num_charged);

        _h_ue_density_vs_jet_pt->fill(jet.pt(), ue_density);
        if (ue_density > 0) _h_ue_density_nonzero_vs_jet_pt->fill(jet.pt(), ue_density);
        if ((5. <= jet.pt()) && (jet.pt() < 10.)) _h_ue_sumpt_for_jet_pt_05_10->fill(sum_cone_pt);
        if ((10. <= jet.pt()) && (jet.pt() < 15.)) _h_ue_sumpt_for_jet_pt_10_15->fill(sum_cone_pt);
        if ((15. <= jet.pt()) && (jet.pt() < 20.)) _h_ue_sumpt_for_jet_pt_15_20->fill(sum_cone_pt);
        if ((20. <= jet.pt()) && (jet.pt() < 30.)) _h_ue_sumpt_for_jet_pt_20_30->fill(sum_cone_pt);
        if ((30. <= jet.pt()) && (jet.pt() < 40.)) _h_ue_sumpt_for_jet_pt_30_40->fill(sum_cone_pt);
      }

      if (jets.size() >= 1) {
        const Jet& jet = jets[0];
        double sum_trans_pt = 0.;
        double sum_cone_pt = 0.;
        for(const Particle& p : fs.particles()) {
          if (std::fabs(p.eta()) < 1.0) {
            const double dphi = Phi_mpi_pi(p.phi() - jet.phi());
            const double deta = p.eta() - jet.eta();
            if ((std::fabs(dphi) > M_PI/3) && (std::fabs(dphi) < M_PI*2/3)) {
              sum_trans_pt += p.pt();
            }
            if (
                (square(Phi_mpi_pi(p.phi() + M_PI_2 - jet.phi())) + square(deta) < square(UE_CONE_R))
             || (square(Phi_mpi_pi(p.phi() - M_PI_2 - jet.phi())) + square(deta) < square(UE_CONE_R))
              ) {
              sum_cone_pt += p.pt();
            }
          }
        }
        _h_ue_hpt_uetrans->fill(hard_pt, sum_trans_pt / (2 * REGION_AREA));
        _h_ue_hpt_uecone->fill(hard_pt, sum_cone_pt / (2 * UE_CONE_AREA));
      }
    }


    static void reset_if_sparse(Histo1DPtr h) {
      for (HistoBin1D &bin : h->bins()) {
        if (bin.numEntries() == 1) {
          bin.reset();
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_num_mpi, crossSection()/sumOfWeights());

      scale(_h_jet_area, crossSection()/sumOfWeights());
      scale(_h_jet_pt, crossSection() / sumOfWeights() / max_abs_eta / 2);
      scale(_h_jet_pt_fine, crossSection() / sumOfWeights() / max_abs_eta / 2);
      scale(_h_jet_pt_ue_corr, crossSection() / sumOfWeights() / max_abs_eta / 2);
      scale(_h_jet_pt_ue_corr_fine, crossSection() / sumOfWeights() / max_abs_eta / 2);
      for(auto &t : _h_jet_pt_fine_etabin) {
        double bin_low = std::get<0>(t);
        double bin_high = std::get<1>(t);
        Histo1DPtr h = std::get<2>(t);
        scale(h, crossSection() / sumOfWeights() / (bin_high - bin_low) / 2);
      }
      for(auto &t : _h_jet_pt_ue_corr_fine_etabin) {
        double bin_low = std::get<0>(t);
        double bin_high = std::get<1>(t);
        Histo1DPtr h = std::get<2>(t);
        scale(h, crossSection() / sumOfWeights() / (bin_high - bin_low) / 2);
      }
      scale(_h_jet_eta, crossSection()/sumOfWeights());

      // This part preferrably be done after merging different runs but before
      // adding up partonic p_T bins
      reset_if_sparse(_h_jet_rt_at_jet_pt_10_12);
      reset_if_sparse(_h_jet_rt_at_jet_pt_20_22);
      reset_if_sparse(_h_jet_rt_at_jet_pt_30_32);

      scale(_h_jet_rt_at_jet_pt_10_12, crossSection()/sumOfWeights());
      scale(_h_jet_rt_at_jet_pt_20_22, crossSection()/sumOfWeights());
      scale(_h_jet_rt_at_jet_pt_30_32, crossSection()/sumOfWeights());

      scale(_h_ue_sumpt_for_jet_pt_05_10, crossSection()/sumOfWeights());
      scale(_h_ue_sumpt_for_jet_pt_10_15, crossSection()/sumOfWeights());
      scale(_h_ue_sumpt_for_jet_pt_15_20, crossSection()/sumOfWeights());
      scale(_h_ue_sumpt_for_jet_pt_20_30, crossSection()/sumOfWeights());
      scale(_h_ue_sumpt_for_jet_pt_30_40, crossSection()/sumOfWeights());

    }

    //@}


  private:


    /// @name Histograms
    //@{
    //Histo2DPtr _h_leading_trans_vs_cone;
    Histo1DPtr _h_num_mpi;
    Histo1DPtr _h_jet_area;
    Histo1DPtr _h_jet_pt;
    Histo1DPtr _h_jet_pt_fine;
    Histo1DPtr _h_jet_pt_ue_corr;
    Histo1DPtr _h_jet_pt_ue_corr_fine;
    vector<tuple<double, double, Histo1DPtr>> _h_jet_pt_fine_etabin;
    vector<tuple<double, double, Histo1DPtr>> _h_jet_pt_ue_corr_fine_etabin;
    Profile1DPtr _h_jet_rt_vs_jet_pt;
    Histo1DPtr _h_jet_rt_at_jet_pt_10_12;
    Histo1DPtr _h_jet_rt_at_jet_pt_20_22;
    Histo1DPtr _h_jet_rt_at_jet_pt_30_32;
    Histo1DPtr _h_jet_eta;
    Profile1DPtr _h_jet_rt_vs_jet_eta;

    Profile1DPtr _h_rt_vs_jet_pt_antiproton;
    Profile1DPtr _h_rt_vs_jet_pt_gamma;
    Profile1DPtr _h_rt_vs_jet_pt_k0l;
    Profile1DPtr _h_rt_vs_jet_pt_kplus;
    Profile1DPtr _h_rt_vs_jet_pt_kminus;
    Profile1DPtr _h_rt_vs_jet_pt_neutron;
    Profile1DPtr _h_rt_vs_jet_pt_pi0;
    Profile1DPtr _h_rt_vs_jet_pt_piplus;
    Profile1DPtr _h_rt_vs_jet_pt_piminus;
    Profile1DPtr _h_rt_vs_jet_pt_proton;

    Profile1DPtr _h_jet_hpt_rt;
    Profile1DPtr _h_jet_ntrl_mult;
    Profile1DPtr _h_jet_chg_mult;

    Profile1DPtr _h_ue_hpt_uetrans;
    Profile1DPtr _h_ue_hpt_uecone;

    Profile1DPtr _h_ue_density_vs_jet_pt;
    Profile1DPtr _h_ue_density_nonzero_vs_jet_pt;
    Histo1DPtr _h_ue_sumpt_for_jet_pt_05_10;
    Histo1DPtr _h_ue_sumpt_for_jet_pt_10_15;
    Histo1DPtr _h_ue_sumpt_for_jet_pt_15_20;
    Histo1DPtr _h_ue_sumpt_for_jet_pt_20_30;
    Histo1DPtr _h_ue_sumpt_for_jet_pt_30_40;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_STAR_JETS);


}
