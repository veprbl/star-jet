# BEGIN PLOT /MC_UE/d.+-x02-.+
LogY=0
XLabel=Partonic $p_T$
XMin=2
XMax=15
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_UE/d01-x01-y01
XLabel=jet $p_T$
YLabel=$d\sigma/dp_T$ [pb/GeV]
XMin=0
XMax=60
# END PLOT

# BEGIN PLOT /MC_UE/d01-x01-y02
XLabel=jet $\eta$
YLabel=$d\sigma/d\eta$ [pb]
YMax=1e4
# END PLOT

# BEGIN PLOT /MC_UE/d01-x02-y01
YLabel=$R_T$
# END PLOT

# BEGIN PLOT /MC_UE/d01-x02-y02
YLabel=Jet neutral particle multiplicity
# END PLOT

# BEGIN PLOT /MC_UE/d01-x02-y03
YLabel=Jet charged particle multiplicity
# END PLOT

# BEGIN PLOT /MC_UE/d02-x02-y01
YLabel=Leading jet (TransP+TransM)/2 UE density [GeV]
# END PLOT

# BEGIN PLOT /MC_UE/d02-x02-y02
YLabel=Leading jet cone UE density [GeV]
# END PLOT
