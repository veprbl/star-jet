// vim:et sw=2 sts=2
// -*- C++ -*-

// --- BEGIN UGLY
#include "Rivet/Config/RivetCommon.hh"
#include "Rivet/Particle.hh"
#define private public // access to _genevent_original
#include "Rivet/Event.hh"
#undef private
// --- END UGLY

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/NeutralFinalState.hh"
#include "Rivet/Projections/MergedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_UE : public Analysis {
  private:

    double square(double x) {
      return x * x;
    }
    double Phi_mpi_pi(double phi) {
      return std::fmod(M_PI + phi, 2 * M_PI) - M_PI;
    }
    const double REGION_AREA = 2 * M_PI / 3;
    const double CONE_R = 0.5;
    const double CONE_AREA = M_PI * CONE_R * CONE_R;

  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_UE);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      const ChargedFinalState cfs;
      const NeutralFinalState nfs;
      const MergedFinalState jfs(cfs, nfs);
      declare(jfs, "FS");

      declare(FastJets(jfs, FastJets::ANTIKT, 0.6), "Jets");

      _h_jet_pt = bookHisto1D("d01-x01-y01", 60, 0., 60.);
      _h_jet_eta = bookHisto1D("d01-x01-y02", 20, -1., 1.);

      _h_jet_hpt_rt = bookProfile1D("d01-x02-y01", 80, 0., 40);
      _h_jet_ntrl_mult = bookProfile1D("d01-x02-y02", 80, 0., 40.);
      _h_jet_chg_mult = bookProfile1D("d01-x02-y03", 80, 0., 40.);

      _h_ue_hpt_uetrans = bookProfile1D("d02-x02-y01", 80, 0., 40.);
      _h_ue_hpt_uecone = bookProfile1D("d02-x02-y02", 80, 0., 40.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 1.5*GeV) && (Cuts::abseta < 0.8));
      const FinalState& fs = apply<FinalState>(event, "FS");
      const double hard_pt = event._genevent_original->event_scale();
      const double weight = event.weight();

      for(auto jet : jets) {
        _h_jet_pt->fill(jet.pt(), weight);
        _h_jet_eta->fill(jet.eta(), weight);

        double pt_neutral = 0., pt_charged = 0.;
        int num_neutral = 0, num_charged = 0;
        for(const Particle& p : jet.particles()) {
          const PdgId pid = p.pid();
          if (PID::threeCharge(pid) == 0) {
            pt_neutral += p.pt();
            num_neutral++;
          } else {
            pt_charged += p.pt();
            num_charged++;
          }
        }
        double rt = pt_neutral / (pt_neutral + pt_charged);
        _h_jet_hpt_rt->fill(hard_pt, rt, weight);
        _h_jet_ntrl_mult->fill(hard_pt, num_neutral, weight);
        _h_jet_chg_mult->fill(hard_pt, num_charged, weight);
      }

      if (jets.size() >= 1) {
        const Jet& jet = jets[0];
        double sum_trans_pt = 0.;
        double sum_cone_pt = 0.;
        for(const Particle& p : fs.particles()) {
          if (std::fabs(p.eta()) < 1.0) {
            const double dphi = Phi_mpi_pi(p.phi() - jet.phi());
            const double deta = p.eta() - jet.eta();
            if ((std::fabs(dphi) > M_PI/3) && (std::fabs(dphi) < M_PI*2/3)) {
              sum_trans_pt += p.pt();
            }
            if (
                (square(Phi_mpi_pi(p.phi() + M_PI_2 - jet.phi())) + square(deta) < square(CONE_R))
             || (square(Phi_mpi_pi(p.phi() - M_PI_2 - jet.phi())) + square(deta) < square(CONE_R))
              ) {
              sum_cone_pt += p.pt();
            }
          }
        }
        _h_ue_hpt_uetrans->fill(hard_pt, sum_trans_pt / (2 * REGION_AREA), weight);
        _h_ue_hpt_uecone->fill(hard_pt, sum_cone_pt / (2 * CONE_AREA), weight);
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_jet_pt, crossSection()/sumOfWeights());
      scale(_h_jet_eta, crossSection()/sumOfWeights());

    }

    //@}


  private:


    /// @name Histograms
    //@{
    //Histo2DPtr _h_leading_trans_vs_cone;
    Histo1DPtr _h_jet_pt;
    Histo1DPtr _h_jet_eta;

    Profile1DPtr _h_jet_hpt_rt;
    Profile1DPtr _h_jet_ntrl_mult;
    Profile1DPtr _h_jet_chg_mult;

    Profile1DPtr _h_ue_hpt_uetrans;
    Profile1DPtr _h_ue_hpt_uecone;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_UE);


}
