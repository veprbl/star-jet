// -*- C++ -*-
#include <unordered_set>
#include <unordered_map>

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class STAR_2021_PP200_JETS_PRELIM : public Analysis {
  private:

    constexpr double square(double x) {
      return x * x;
    }
    double Phi_mpi_pi(double phi) {
      return std::remainder(phi, 2 * M_PI);
    }
    const double UE_CONE_R = 0.5;
    const double UE_CONE_AREA = M_PI * square(UE_CONE_R);
    const double DELTA_ETA = 1.6;
    bool ue_subtraction;

  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(STAR_2021_PP200_JETS_PRELIM);


    /// @name Analysis methods
    ///@{

    /// Book histograms and initialise projections before the run
    void init() {

      const STAR_Hadrons hadron_fs;
      declare(hadron_fs, "HFS");

      FastJets jetfs(hadron_fs, FastJets::ANTIKT, 0.6, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      jetfs.useJetArea(new fastjet::AreaDefinition(
        fastjet::AreaType::active_area,
        fastjet::GhostedAreaSpec(1.5, 1, 0.04, 1.0, 0.1, 1e-100)
      ));
      declare(jetfs, "jets");

      book(_h["jet_pt_ue_corr"], 1, 1, 1);

      ue_subtraction = getOption<bool>("UE_SUBTR", true);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      const FinalState& hadron_fs = apply<FinalState>(event, "HFS");
      const FastJets& jetfs = apply<FastJets>(event, "jets");
      Jets jets = jetfs.jetsByPt((Cuts::pT > 5.0*GeV) && (Cuts::abseta < 0.8));
      const shared_ptr<fastjet::ClusterSequenceArea> csa = ue_subtraction ? jetfs.clusterSeqArea() : nullptr;

      for(const Jet &jet : jets) {
        double sum_cone_pt = 0.;
        for(const Particle& p : hadron_fs.particles()) {
          const double dphi_plus = Phi_mpi_pi(p.phi() + M_PI_2 - jet.phi());
          const double dphi_minus = Phi_mpi_pi(p.phi() - M_PI_2 - jet.phi());
          const double deta = p.eta() - jet.eta();
          if (
              (square(dphi_plus) + square(deta) < square(UE_CONE_R))
           || (square(dphi_minus) + square(deta) < square(UE_CONE_R))
            ) {
            sum_cone_pt += p.pt();
          }
        }

        double jet_pt = jet.pt();
        if (ue_subtraction) {
          double ue_density = sum_cone_pt / 2 / UE_CONE_AREA;
          double jet_area = csa->area(jet);
          jet_pt -= ue_density * jet_area;
        }
        _h["jet_pt_ue_corr"]->fill((jet_pt) / GeV);
      }

    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h["jet_pt_ue_corr"], crossSection() / picobarn / sumW() / DELTA_ETA);

    }

    ///@}


    /// @name Histograms
    ///@{
    map<string, Histo1DPtr> _h;
    ///@}


    /// @brief Special hadron final state finder
    class STAR_Hadrons : public FinalState {
    private:

      std::unordered_set<PdgId> star_hadrons;

    public:

      STAR_Hadrons(const Cut& c=Cuts::open())
        : FinalState(c)
        , star_hadrons {
            PID::PI0,
            PID::PIPLUS,
            PID::ETA,
            PID::KPLUS,
            PID::K0S,
            PID::K0L,
            PID::LAMBDA,
            PID::SIGMA0,
            PID::SIGMAMINUS,
            PID::SIGMAPLUS,
            PID::XIMINUS,
            PID::XI0,
            PID::OMEGAMINUS
          }
      {
        setName("STAR_Hadrons");
      }

      DEFAULT_RIVET_PROJ_CLONE(STAR_Hadrons);

    protected:

      virtual void project(const Event& e) {

        _theParticles.clear();
        std::unordered_map<int, ConstGenParticlePtr> found;
        std::unordered_set<int> decay_products;

        // Mark final state particles for use
        for (ConstGenParticlePtr p : HepMCUtils::particles(e.genEvent())) {
          if (p->status() == 1) {
            found[p->barcode()] = p;
          }
        }

        for (ConstGenParticlePtr p : HepMCUtils::particles(e.genEvent())) {
          bool hasDecayed = p->status() == 2;
          bool decayNotAllowed = star_hadrons.count(abs(p->pdg_id()));
          if (hasDecayed && decayNotAllowed) {
            // Mark the hadron for use
            found[p->barcode()] = p;
            // Mark all of its downstream particles as decay products
            ConstGenVertexPtr pv = p->end_vertex();
            if (pv) {
              for (ConstGenParticlePtr d : HepMCUtils::particles(pv, Relatives::DESCENDANTS)) {
                decay_products.insert(d->barcode());
              }
            }
          }
        }

        // Provide all marked particles except those marked as decay products
        for (const std::pair<int, ConstGenParticlePtr> &pair : found) {
          ConstGenParticlePtr p = pair.second;
          if (!decay_products.count(p->barcode())) {
            _theParticles.push_back(p);
          }
        }

      }
    };


  };


  DECLARE_RIVET_PLUGIN(STAR_2021_PP200_JETS_PRELIM);

}
