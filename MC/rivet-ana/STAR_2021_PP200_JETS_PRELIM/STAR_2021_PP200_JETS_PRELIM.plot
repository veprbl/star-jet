BEGIN PLOT /STAR_2021_PP200_JETS_PRELIM/d01-x01-y01
Title=Inclusive jet cross section, anti-$\mathrm{k}_{\text{T}}$, $R = 0.6$, $|\eta| < 0.8$
XLabel=jet $p_{\text{T}}$ corrected for UE, GeV/c
YLabel=$\mathrm{d}^2\sigma/(\mathrm{d}p_{\text{T}}\hspace{0.1em}\mathrm{d}\eta$), pb/(GeV/c)
END PLOT
