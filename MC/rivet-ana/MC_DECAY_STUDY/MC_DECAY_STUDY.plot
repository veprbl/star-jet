# BEGIN PLOT /MC_DECAY_STUDY/jet_corr_mult
LogZ=1
XLabel=Multiplicity of jets after decay
YLabel=Multiplicity of jets before decay
# END PLOT

# BEGIN PLOT /MC_DECAY_STUDY/jet_match_corr_(eta|phi|pt)
LogZ=1
ZMin=1e-10
# END PLOT

# BEGIN PLOT /MC_DECAY_STUDY/jet_match_corr_eta
XLabel=$\eta$ of jet after decay
YLabel=$\eta$ of jet before decay
# END PLOT

# BEGIN PLOT /MC_DECAY_STUDY/jet_match_corr_phi
XLabel=$\varphi$ of jet after decay
YLabel=$\varphi$ of jet before decay
# END PLOT

# BEGIN PLOT /MC_DECAY_STUDY/jet_match_corr_pt
XLabel=$p_T$ of jet after decay
YLabel=$p_T$ of jet before decay
# END PLOT

# BEGIN PLOT /MC_DECAY_STUDY/jet_match_dr.*
XLabel=$\Delta R$
# END PLOT

# BEGIN PLOT /MC_DECAY_STUDY/pi0_corr_eta
XLabel=$\pi^0$ $\eta$
YLabel=decay product $\eta$
# END PLOT

# BEGIN PLOT /MC_DECAY_STUDY/pi0_corr_phi
XLabel=$\pi^0$ $\varphi$
YLabel=decay product $\varphi$
# END PLOT
