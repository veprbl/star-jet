// vim:et sw=2 sts=2
// -*- C++ -*-

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/FastJets.hh"

// FIXME
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/NeutralFinalState.hh"
#include "Rivet/Projections/MergedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
// FIXME

namespace Rivet {

  std::string toParticleName(PdgId pdg_id) {
    switch (pdg_id) {
      case PID::GAMMA: return "gamma";
      case PID::PROTON: return "proton";
      case PID::ANTIPROTON: return "antiproton";
      case PID::NEUTRON: return "neutron";
      case PID::ANTINEUTRON: return "antineutron";
      case PID::PI0: return "pi0";
      case PID::PIPLUS: return "pi+";
      case PID::PIMINUS: return "pi-";
      case PID::RHO0: return "rho0";
      case PID::RHOPLUS: return "rho+";
      case PID::RHOMINUS: return "rho-";
      default:
        std::stringstream ss;
        ss << pdg_id;
        return ss.str();
    }
  }

  class MyUnstableFinalState : public FinalState {
  public:

    MyUnstableFinalState(const Cut& c=Cuts::open())
      : FinalState(c)
    {
      setName("UnstableFinalState");
    }

    DEFAULT_RIVET_PROJ_CLONE(MyUnstableFinalState);

  protected:

    virtual void project(const Event& e) {
      _theParticles.clear();
      std::map<int, const GenParticle*> found;

      for (const GenParticle* p : Rivet::particles(e.genEvent())) {
        if (p->status() != 1) continue;
        bool is_decay_product = false;
        const GenVertex* pv = p->production_vertex();
        for (const GenParticle* pp : particles_in(pv)) {
          if (pp->pdg_id() == PID::PI0) {
            if (is_decay_product) {
              std::cerr << "strange" << std::endl;
              std::terminate();
            }
            found[pp->barcode()] = pp;
            is_decay_product = true;
          }
        }
        if (!is_decay_product) {
          found[p->barcode()] = p;
        }
      }

      for (const std::pair<int, const GenParticle*> &pair : found) {
        const GenParticle *p = pair.second;
        _theParticles.push_back(p);
      }
    }
  };

  /// @brief Add a short analysis description here
  class MC_DECAY_STUDY : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_DECAY_STUDY);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      FinalState fs;
      declare(fs, "FS");
      MyUnstableFinalState ufs;
      declare(ufs, "UFS");

      declare(FastJets(fs, FastJets::ANTIKT, 0.6), "Jets");
      declare(FastJets(ufs, FastJets::ANTIKT, 0.6), "UnstableJets");

      _h_jet_pt = bookHisto1D("jet_pt", 60, 0., 60.);
      _h_jet_eta = bookHisto1D("jet_eta", 20, -1., 1.);

      _h_jet_pt_unstable = bookHisto1D("jet_pt_unstable", 60, 0., 60.);
      _h_jet_eta_unstable = bookHisto1D("jet_eta_unstable", 20, -1., 1.);

      _h_jet_corr_mult = bookHisto2D("jet_corr_mult", 10, 0., 10., 10, 0., 10.);
      _h_jet_match_dr_comb = bookHisto1D("jet_match_dr_comb", 20, 0., 1.);
      _h_jet_match_dr_best = bookHisto1D("jet_match_dr_best", 20, 0., 1.);
      _h_jet_match_corr_pt = bookHisto2D("jet_match_corr_pt", 20, 0., 60., 20, 0., 60.);
      _h_jet_match_corr_eta = bookHisto2D("jet_match_corr_eta", 20, -1., 1., 20, -1., 1.);
      _h_jet_match_corr_phi = bookHisto2D("jet_match_corr_phi", 20, 0., 2 * M_PI, 20, 0., 2 * M_PI);

      _h_pi0_pt = bookHisto1D("pi0_pt", 60, 0., 60.);
      _h_pi0_decayprod_pt = bookHisto1D("pi0_decayprod_pt", 60, 0., 60.);
      _h_pi0_corr_eta = bookHisto2D("pi0_corr_eta", 20, -1., 1., 20, -1., 1.);
      _h_pi0_corr_phi = bookHisto2D("pi0_corr_phi", 20, 0., 2 * M_PI, 20, 0., 2 * M_PI);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 1.5*GeV) && (Cuts::abseta < 0.8));
      const FastJets& ufj = apply<FastJets>(event, "UnstableJets");
      const Jets& ujets = ufj.jetsByPt((Cuts::pT > 1.5*GeV) && (Cuts::abseta < 0.8));
      const double weight = event.weight();

      _h_jet_corr_mult->fill(jets.size(), ujets.size(), weight);
      for(auto jet : jets) {
        _h_jet_pt->fill(jet.pt(), weight);
        _h_jet_eta->fill(jet.eta(), weight);
      }
      for(auto ujet : ujets) {
        _h_jet_pt_unstable->fill(ujet.pt(), weight);
        _h_jet_eta_unstable->fill(ujet.eta(), weight);
      }

      typedef pair<const Jet*, const Jet*> JetPair;

      static auto sqr = [] (double x) { return x * x; };
      static auto to_mpi_pi = [] (double x) { while (x > M_PI) { x -= 2 * M_PI; }; while (x < -M_PI) { x += 2 * M_PI; }; return x; };
      static auto R2 = [] (const JetPair &p) { return sqr(p.first->eta() - p.second->eta()) + sqr(to_mpi_pi(p.first->phi() - p.second->phi())); };
      static auto R = [] (const JetPair &p) { return sqrt(R2(p)); };

      vector<JetPair> pairs;
      for(const Jet &jet : jets) {
        for(const Jet &ujet : jets) {
          JetPair p { &jet, &ujet };
          pairs.push_back(p);
          _h_jet_match_dr_comb->fill(R(p), weight);
        }
      }

      // sort in descending order by dR
      std::sort(pairs.begin(), pairs.end(), [] (const JetPair &p1, const JetPair &p2) { return R2(p1) > R2(p2); });

      while (pairs.size()) {
        const JetPair &p = pairs.back();
        if (R(p) < 0.3) {
          _h_jet_match_corr_pt->fill(p.first->pt(), p.second->pt(), weight);
          _h_jet_match_corr_eta->fill(p.first->eta(), p.second->eta(), weight);
          _h_jet_match_corr_phi->fill(p.first->phi(), p.second->phi(), weight);
        }
        _h_jet_match_dr_best->fill(R(p), weight);
        vector<JetPair> new_pairs;
        std::remove_copy_if(pairs.begin(), pairs.end(), std::back_inserter(new_pairs),
            [&] (const JetPair& o) { return (p.first == o.first) || (p.second == o.second); });
        pairs = std::move(new_pairs);
      }

      const MyUnstableFinalState& ufs = apply<MyUnstableFinalState>(event, "UFS");
      for (const Particle& p : ufs.particles()) {
        if (p.pid() == PID::PI0) {
          _h_pi0_pt->fill(p.pt(), weight);
          for (const Particle& child : p.children()) {
            _h_pi0_decayprod_pt->fill(child.pt(), weight);
            _h_pi0_corr_eta->fill(p.eta(), child.eta(), weight);
            _h_pi0_corr_phi->fill(p.phi(), child.phi(), weight);
          }
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_jet_pt, crossSection()/sumOfWeights());
      scale(_h_jet_eta, crossSection()/sumOfWeights());

      scale(_h_jet_pt_unstable, crossSection()/sumOfWeights());
      scale(_h_jet_eta_unstable, crossSection()/sumOfWeights());

      scale(_h_jet_corr_mult, crossSection()/sumOfWeights());
      scale(_h_jet_match_dr_comb, crossSection()/sumOfWeights());
      scale(_h_jet_match_dr_best, crossSection()/sumOfWeights());
      scale(_h_jet_match_corr_pt, crossSection()/sumOfWeights());
      scale(_h_jet_match_corr_eta, crossSection()/sumOfWeights());
      scale(_h_jet_match_corr_phi, crossSection()/sumOfWeights());

      scale(_h_pi0_pt, crossSection()/sumOfWeights());
      scale(_h_pi0_decayprod_pt, crossSection()/sumOfWeights());
      scale(_h_pi0_corr_eta, crossSection()/sumOfWeights());
      scale(_h_pi0_corr_phi, crossSection()/sumOfWeights());
    }

    //@}


  private:

    /// @name Histograms
    //@{
    //Histo2DPtr _h_leading_trans_vs_cone;
    Histo1DPtr _h_jet_pt;
    Histo1DPtr _h_jet_eta;

    Histo1DPtr _h_jet_pt_unstable;
    Histo1DPtr _h_jet_eta_unstable;

    Histo2DPtr _h_jet_corr_mult;
    Histo1DPtr _h_jet_match_dr_comb;
    Histo1DPtr _h_jet_match_dr_best;
    Histo2DPtr _h_jet_match_corr_pt;
    Histo2DPtr _h_jet_match_corr_eta;
    Histo2DPtr _h_jet_match_corr_phi;

    Histo1DPtr _h_pi0_pt;
    Histo1DPtr _h_pi0_decayprod_pt;
    Histo2DPtr _h_pi0_corr_eta;
    Histo2DPtr _h_pi0_corr_phi;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_DECAY_STUDY);


}
