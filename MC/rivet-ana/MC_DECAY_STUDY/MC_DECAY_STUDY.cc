// vim:et sw=2 sts=2
// -*- C++ -*-

#include <unordered_set>
#include <unordered_map>

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

// FIXME
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/NeutralFinalState.hh"
#include "Rivet/Projections/MergedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
// FIXME

namespace Rivet {

  std::string toParticleName(PdgId pdg_id) {
    switch (pdg_id) {
      case PID::GAMMA: return "gamma";
      case PID::PROTON: return "proton";
      case PID::ANTIPROTON: return "antiproton";
      case PID::NEUTRON: return "neutron";
      case PID::ANTINEUTRON: return "antineutron";
      case PID::PI0: return "pi0";
      case PID::PIPLUS: return "pi+";
      case PID::PIMINUS: return "pi-";
      case PID::RHO0: return "rho0";
      case PID::RHOPLUS: return "rho+";
      case PID::RHOMINUS: return "rho-";
      default:
        std::stringstream ss;
        ss << pdg_id;
        return ss.str();
    }
  }


  /// @brief Add a short analysis description here
  class MC_DECAY_STUDY : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_DECAY_STUDY);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      FinalState fs;
      declare(fs, "FS");
      STAR_Hadrons hfs;
      declare(hfs, "HFS");

      declare(FastJets(fs, FastJets::ANTIKT, 0.6), "Jets");
      declare(FastJets(hfs, FastJets::ANTIKT, 0.6), "HadronJets");

      book(_h_jet_pt, "jet_pt", 60, 0., 60.);
      book(_h_jet_eta, "jet_eta", 20, -1., 1.);

      book(_h_jet_pt_unstable, "jet_pt_unstable", 60, 0., 60.);
      book(_h_jet_eta_unstable, "jet_eta_unstable", 20, -1., 1.);

      book(_h_jet_corr_mult, "jet_corr_mult", 10, 0., 10., 10, 0., 10.);
      book(_h_jet_match_dr_comb, "jet_match_dr_comb", 20, 0., 1.);
      book(_h_jet_match_dr_best, "jet_match_dr_best", 20, 0., 1.);
      book(_h_jet_match_corr_pt, "jet_match_corr_pt", 20, 0., 60., 20, 0., 60.);
      book(_h_jet_match_corr_eta, "jet_match_corr_eta", 20, -1., 1., 20, -1., 1.);
      book(_h_jet_match_corr_phi, "jet_match_corr_phi", 20, 0., 2 * M_PI, 20, 0., 2 * M_PI);

      book(_h_pi0_pt, "pi0_pt", 60, 0., 60.);
      book(_h_pi0_decayprod_pt, "pi0_decayprod_pt", 60, 0., 60.);
      book(_h_pi0_corr_eta, "pi0_corr_eta", 20, -1., 1., 20, -1., 1.);
      book(_h_pi0_corr_phi, "pi0_corr_phi", 20, 0., 2 * M_PI, 20, 0., 2 * M_PI);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 1.5*GeV) && (Cuts::abseta < 0.8));
      const FastJets& hfj = apply<FastJets>(event, "HadronJets");
      const Jets& hjets = hfj.jetsByPt((Cuts::pT > 1.5*GeV) && (Cuts::abseta < 0.8));

      _h_jet_corr_mult->fill(jets.size(), hjets.size());
      for(auto jet : jets) {
        _h_jet_pt->fill(jet.pt());
        _h_jet_eta->fill(jet.eta());
      }
      for(auto hjet : hjets) {
        _h_jet_pt_unstable->fill(hjet.pt());
        _h_jet_eta_unstable->fill(hjet.eta());
      }

      typedef pair<const Jet*, const Jet*> JetPair;

      static auto sqr = [] (double x) { return x * x; };
      static auto to_mpi_pi = [] (double x) { while (x > M_PI) { x -= 2 * M_PI; }; while (x < -M_PI) { x += 2 * M_PI; }; return x; };
      static auto R2 = [] (const JetPair &p) { return sqr(p.first->eta() - p.second->eta()) + sqr(to_mpi_pi(p.first->phi() - p.second->phi())); };
      static auto R = [] (const JetPair &p) { return sqrt(R2(p)); };

      vector<JetPair> pairs;
      for(const Jet &jet : jets) {
        for(const Jet &hjet : jets) {
          JetPair p { &jet, &hjet };
          pairs.push_back(p);
          _h_jet_match_dr_comb->fill(R(p));
        }
      }

      // sort in descending order by dR
      std::sort(pairs.begin(), pairs.end(), [] (const JetPair &p1, const JetPair &p2) { return R2(p1) > R2(p2); });

      while (pairs.size()) {
        const JetPair &p = pairs.back();
        if (R(p) < 0.3) {
          _h_jet_match_corr_pt->fill(p.first->pt(), p.second->pt());
          _h_jet_match_corr_eta->fill(p.first->eta(), p.second->eta());
          _h_jet_match_corr_phi->fill(p.first->phi(), p.second->phi());
        }
        _h_jet_match_dr_best->fill(R(p));
        vector<JetPair> new_pairs;
        std::remove_copy_if(pairs.begin(), pairs.end(), std::back_inserter(new_pairs),
            [&] (const JetPair& o) { return (p.first == o.first) || (p.second == o.second); });
        pairs = std::move(new_pairs);
      }

      const STAR_Hadrons& hfs = apply<STAR_Hadrons>(event, "HFS");
      for (const Particle& p : hfs.particles()) {
        if (p.pid() == PID::PI0) {
          _h_pi0_pt->fill(p.pt());
          for (const Particle& child : p.children()) {
            _h_pi0_decayprod_pt->fill(child.pt());
            _h_pi0_corr_eta->fill(p.eta(), child.eta());
            _h_pi0_corr_phi->fill(p.phi(), child.phi());
          }
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_jet_pt, crossSection()/sumOfWeights());
      scale(_h_jet_eta, crossSection()/sumOfWeights());

      scale(_h_jet_pt_unstable, crossSection()/sumOfWeights());
      scale(_h_jet_eta_unstable, crossSection()/sumOfWeights());

      scale(_h_jet_corr_mult, crossSection()/sumOfWeights());
      scale(_h_jet_match_dr_comb, crossSection()/sumOfWeights());
      scale(_h_jet_match_dr_best, crossSection()/sumOfWeights());
      scale(_h_jet_match_corr_pt, crossSection()/sumOfWeights());
      scale(_h_jet_match_corr_eta, crossSection()/sumOfWeights());
      scale(_h_jet_match_corr_phi, crossSection()/sumOfWeights());

      scale(_h_pi0_pt, crossSection()/sumOfWeights());
      scale(_h_pi0_decayprod_pt, crossSection()/sumOfWeights());
      scale(_h_pi0_corr_eta, crossSection()/sumOfWeights());
      scale(_h_pi0_corr_phi, crossSection()/sumOfWeights());
    }

    //@}


  private:

    /// @name Histograms
    //@{
    //Histo2DPtr _h_leading_trans_vs_cone;
    Histo1DPtr _h_jet_pt;
    Histo1DPtr _h_jet_eta;

    Histo1DPtr _h_jet_pt_unstable;
    Histo1DPtr _h_jet_eta_unstable;

    Histo2DPtr _h_jet_corr_mult;
    Histo1DPtr _h_jet_match_dr_comb;
    Histo1DPtr _h_jet_match_dr_best;
    Histo2DPtr _h_jet_match_corr_pt;
    Histo2DPtr _h_jet_match_corr_eta;
    Histo2DPtr _h_jet_match_corr_phi;

    Histo1DPtr _h_pi0_pt;
    Histo1DPtr _h_pi0_decayprod_pt;
    Histo2DPtr _h_pi0_corr_eta;
    Histo2DPtr _h_pi0_corr_phi;
    //@}


    /// @brief Special hadron final state finder
    class STAR_Hadrons : public FinalState {
    private:

      std::unordered_set<PdgId> star_hadrons;

    public:

      STAR_Hadrons(const Cut& c=Cuts::open())
        : FinalState(c)
        , star_hadrons {
            PID::PI0,
            PID::PIPLUS,
            PID::ETA,
            PID::KPLUS,
            PID::K0S,
            PID::K0L,
            PID::LAMBDA,
            PID::SIGMA0,
            PID::SIGMAMINUS,
            PID::SIGMAPLUS,
            PID::XIMINUS,
            PID::XI0,
            PID::OMEGAMINUS
          }
      {
        setName("STAR_Hadrons");
      }

      DEFAULT_RIVET_PROJ_CLONE(STAR_Hadrons);

    protected:

      virtual void project(const Event& e) {

        _theParticles.clear();
        std::unordered_map<int, ConstGenParticlePtr> found;
        std::unordered_set<int> decay_products;

        // Mark final state particles for use
        for (ConstGenParticlePtr p : HepMCUtils::particles(e.genEvent())) {
          if (p->status() == 1) {
            found[p->barcode()] = p;
          }
        }

        for (ConstGenParticlePtr p : HepMCUtils::particles(e.genEvent())) {
          bool hasDecayed = p->status() == 2;
          bool decayNotAllowed = star_hadrons.count(abs(p->pdg_id()));
          if (hasDecayed && decayNotAllowed) {
            // Mark the hadron for use
            found[p->barcode()] = p;
            // Mark all of its downstream particles as decay products
            ConstGenVertexPtr pv = p->end_vertex();
            if (pv) {
              for (ConstGenParticlePtr d : HepMCUtils::particles(pv, Relatives::DESCENDANTS)) {
                decay_products.insert(d->barcode());
              }
            }
          }
        }

        // Provide all marked particles except those marked as decay products
        for (const std::pair<int, ConstGenParticlePtr> &pair : found) {
          ConstGenParticlePtr p = pair.second;
          if (!decay_products.count(p->barcode())) {
            _theParticles.push_back(p);
          }
        }

      }
    };


  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_DECAY_STUDY);


}
