// vim:et sw=2 sts=2
// -*- C++ -*-

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FinalPartons.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_JET_PARTON_MATCH : public Analysis {
  private:

    typedef std::pair<Jet*,Jet*> JetPair; /// JetPair::first - particle jet, JetPair::second - parton jet

    static double square(double x) {
      return x * x;
    }

    static double Phi_mpi_pi(double phi) {
      return std::fmod(M_PI + phi, 2 * M_PI) - M_PI;
    }

    static double dR_sqr(const JetPair &p) {
      double dphi = Phi_mpi_pi(p.first->phi() - p.second->phi());
      double deta = p.first->eta() - p.second->eta();
      return square(dphi) + square(deta);
    };

    static double JetPair_cmp(const JetPair &p1, const JetPair &p2) {
      return dR_sqr(p1) > dR_sqr(p2);
    };

    const double MAX_DR = 0.6;

  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_JET_PARTON_MATCH);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      const FinalState fs;
      declare(fs, "FS");
      const FinalPartons fp;
      declare(fp, "FP");

      declare(FastJets(fs, FastJets::ANTIKT, 0.6), "Jets");
      declare(FastJets(fp, FastJets::ANTIKT, 0.6), "PartonJets");

      _h_dr = bookHisto1D("dr", 20, 0., 2.);
      _h_pt_corr = bookHisto2D("pt_corr", 20, 0., 20., 20, 0., 20.);
      _h_particle_pt = bookHisto1D("particle_pt", 20, 0., 20.);
      _h_matched_particle_pt = bookHisto1D("matched_particle_pt", 20, 0., 20.);
      _h_parton_pt = bookHisto1D("parton_pt", 20, 0., 20.);
      _h_matched_parton_pt = bookHisto1D("matched_parton_pt", 20, 0., 20.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& particle_jets = fj.jetsByPt((Cuts::pT > 5.*GeV) && (Cuts::abseta < 0.8));
      const FastJets& pfj = apply<FastJets>(event, "PartonJets");
      const Jets& parton_jets = pfj.jetsByPt((Cuts::pT > 5.*GeV) && (Cuts::abseta < 0.8));
      const double weight = event.weight();

      std::vector<JetPair> pairs;
      for(auto particle_jet : particle_jets) {
        _h_particle_pt->fill(particle_jet.pt(), weight);
        for(auto parton_jet : parton_jets) {
          pairs.emplace_back(&particle_jet, &parton_jet);
          double dphi = Phi_mpi_pi(particle_jet.phi() - parton_jet.phi());
          double deta = particle_jet.eta() - parton_jet.eta();
          double dr = sqrt(square(dphi) + square(deta));
          _h_dr->fill(dr, weight);
        }
      }
      for(auto parton_jet : parton_jets) {
        _h_parton_pt->fill(parton_jet.pt(), weight);
      }
      std::sort(pairs.begin(), pairs.end(), JetPair_cmp);
      while(!pairs.empty())
      {
        JetPair m = pairs.back();
        std::vector<JetPair> new_pairs;
        std::remove_copy_if(pairs.begin(), pairs.end(), std::back_inserter(new_pairs),
            [&](JetPair& p) { return (p.first == m.first) || (p.second == m.second); });
        pairs = std::move(new_pairs);
        if (sqrt(dR_sqr(m)) >= MAX_DR) break;
        const Jet &particle_jet = *m.first;
        const Jet &parton_jet = *m.second;
        _h_pt_corr->fill(parton_jet.pt(), particle_jet.pt(), weight);
        _h_matched_particle_pt->fill(particle_jet.pt(), weight);
        _h_matched_parton_pt->fill(parton_jet.pt(), weight);
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_dr, crossSection()/sumOfWeights());
      scale(_h_pt_corr, crossSection()/sumOfWeights());
      scale(_h_particle_pt, crossSection()/sumOfWeights());
      scale(_h_matched_particle_pt, crossSection()/sumOfWeights());
      scale(_h_parton_pt, crossSection()/sumOfWeights());
      scale(_h_matched_parton_pt, crossSection()/sumOfWeights());

      _s_particle_eff = std::make_shared<YODA::Scatter2D>(YODA::efficiency(*_h_matched_particle_pt, *_h_particle_pt));
      _s_particle_eff->setPath(histoPath("particle_eff"));
      addAnalysisObject(_s_particle_eff);
      _s_parton_eff = std::make_shared<YODA::Scatter2D>(YODA::efficiency(*_h_matched_parton_pt, *_h_parton_pt));
      _s_parton_eff->setPath(histoPath("parton_eff"));
      addAnalysisObject(_s_parton_eff);

    }

    //@}


  private:


    /// @name Histograms
    //@{

    Histo1DPtr _h_dr;
    Histo2DPtr _h_pt_corr;
    Histo1DPtr _h_particle_pt;
    Histo1DPtr _h_matched_particle_pt;
    Histo1DPtr _h_parton_pt;
    Histo1DPtr _h_matched_parton_pt;
    Scatter2DPtr _s_particle_eff;
    Scatter2DPtr _s_parton_eff;

    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_JET_PARTON_MATCH);


}
