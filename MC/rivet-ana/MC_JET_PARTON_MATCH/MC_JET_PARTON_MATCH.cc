// vim:et sw=2 sts=2
// -*- C++ -*-

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FinalPartons.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_JET_PARTON_MATCH : public Analysis {
  private:

    typedef std::pair<const Jet*,const Jet*> JetPair; /// JetPair::first - particle jet, JetPair::second - parton jet

    static double square(double x) {
      return x * x;
    }

    static double Phi_mpi_pi(double phi) {
      return std::remainder(phi, 2 * M_PI);
    }

    static double dR_sqr(const JetPair &p) {
      double dphi = Phi_mpi_pi(p.first->phi() - p.second->phi());
      double deta = p.first->eta() - p.second->eta();
      return square(dphi) + square(deta);
    };

    static double JetPair_cmp(const JetPair &p1, const JetPair &p2) {
      return dR_sqr(p1) > dR_sqr(p2);
    };

    const double MAX_DR = 0.6;

  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_JET_PARTON_MATCH);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      const FinalState fs;
      declare(fs, "FS");
      const FinalPartons fp;
      declare(fp, "FP");

      declare(FastJets(fs, FastJets::ANTIKT, 0.6), "Jets");
      declare(FastJets(fp, FastJets::ANTIKT, 0.6), "PartonJets");

      book(_h_dr, "dr", 20, 0., 2.);
      book(_h_pt_corr, "pt_corr", 50, 0., 50., 50, 0., 50.);
      book(_h_pt_corr_diff, "pt_corr_diff", 50, 0., 50.);
      book(_h_pt_corr_diff2, "pt_corr_diff2", 50, 0., 50.);
      book(_h_pt_corr_diffT, "pt_corr_diffT", 50, 0., 50.);
      book(_h_pt_corr_diff2T, "pt_corr_diff2T", 50, 0., 50.);
      book(_h_numjet, "numjet", 10, 0., 10., 10, 0., 10.);
      book(_h_particle_pt, "particle_pt", 50, 0., 50.);
      book(_h_matched_particle_pt, "matched_particle_pt", 50, 0., 50.);
      book(_h_parton_pt, "parton_pt", 50, 0., 50.);
      book(_h_matched_parton_pt, "matched_parton_pt", 50, 0., 50.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& particle_jets = fj.jetsByPt((Cuts::pT > 1.5*GeV) && (Cuts::abseta < 0.8));
      const FastJets& pfj = apply<FastJets>(event, "PartonJets");
      const Jets& parton_jets = pfj.jetsByPt((Cuts::pT > 1.5*GeV) && (Cuts::abseta < 0.8));

      _h_numjet->fill(parton_jets.size(), particle_jets.size());

      std::vector<JetPair> pairs;
      for(const Jet &particle_jet : particle_jets) {
        _h_particle_pt->fill(particle_jet.pt());
        for(const Jet &parton_jet : parton_jets) {
          pairs.emplace_back(&particle_jet, &parton_jet);
          double dphi = Phi_mpi_pi(particle_jet.phi() - parton_jet.phi());
          double deta = particle_jet.eta() - parton_jet.eta();
          double dr = sqrt(square(dphi) + square(deta));
          _h_dr->fill(dr);
        }
      }
      for(const Jet &parton_jet : parton_jets) {
        _h_parton_pt->fill(parton_jet.pt());
      }
      std::sort(pairs.begin(), pairs.end(), JetPair_cmp);
      while(!pairs.empty())
      {
        JetPair m = pairs.back();
        if (sqrt(dR_sqr(m)) >= MAX_DR) break;
        std::vector<JetPair> new_pairs;
        std::remove_copy_if(pairs.begin(), pairs.end(), std::back_inserter(new_pairs),
            [&](JetPair& p) { return (p.first == m.first) || (p.second == m.second); });
        pairs = std::move(new_pairs);
        const Jet &particle_jet = *m.first;
        const Jet &parton_jet = *m.second;
        _h_pt_corr->fill(parton_jet.pt(), particle_jet.pt());
        double diff = particle_jet.pt() - parton_jet.pt();
        _h_pt_corr_diff->fill(parton_jet.pt(), diff);
        _h_pt_corr_diff2->fill(parton_jet.pt(), diff * diff);
        double diffT = parton_jet.pt() - particle_jet.pt();
        _h_pt_corr_diffT->fill(particle_jet.pt(), diffT);
        _h_pt_corr_diff2T->fill(particle_jet.pt(), diffT * diffT);
        _h_matched_particle_pt->fill(particle_jet.pt());
        _h_matched_parton_pt->fill(parton_jet.pt());
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_dr, crossSection()/sumOfWeights());
      scale(_h_pt_corr, crossSection()/sumOfWeights());
      scale(_h_numjet, crossSection()/sumOfWeights());
      scale(_h_particle_pt, crossSection()/sumOfWeights());
      scale(_h_matched_particle_pt, crossSection()/sumOfWeights());
      scale(_h_parton_pt, crossSection()/sumOfWeights());
      scale(_h_matched_parton_pt, crossSection()/sumOfWeights());

    }

    //@}


  private:


    /// @name Histograms
    //@{

    Histo1DPtr _h_dr;
    Histo2DPtr _h_pt_corr;
    Profile1DPtr _h_pt_corr_diff;
    Profile1DPtr _h_pt_corr_diff2;
    Profile1DPtr _h_pt_corr_diffT;
    Profile1DPtr _h_pt_corr_diff2T;
    Histo2DPtr _h_numjet;
    Histo1DPtr _h_particle_pt;
    Histo1DPtr _h_matched_particle_pt;
    Histo1DPtr _h_parton_pt;
    Histo1DPtr _h_matched_parton_pt;
    Scatter2DPtr _s_particle_eff;
    Scatter2DPtr _s_parton_eff;

    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_JET_PARTON_MATCH);


}
