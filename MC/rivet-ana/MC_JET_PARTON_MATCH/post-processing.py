#!/usr/bin/env python3

import sys

import yoda

from packaging import version
if version.parse(yoda.version().decode()) >= version.parse("1.8.0"):
    def set_path(ao, path):
        ao.setPath(path)
else:
    def set_path(ao, path):
        ao.path = path

filename = sys.argv[1]
aos = yoda.readYODA(filename)

path = '/MC_JET_PARTON_MATCH/particle_eff'
aos[path] = (
    aos['/MC_JET_PARTON_MATCH/matched_particle_pt']
    /
    aos['/MC_JET_PARTON_MATCH/particle_pt']
    )
set_path(aos[path], path)

path = '/MC_JET_PARTON_MATCH/parton_eff'
aos[path] = (
    aos['/MC_JET_PARTON_MATCH/matched_parton_pt']
    /
    aos['/MC_JET_PARTON_MATCH/parton_pt']
    )
set_path(aos[path], path)

yoda.writeYODA(aos, "output.yoda")
