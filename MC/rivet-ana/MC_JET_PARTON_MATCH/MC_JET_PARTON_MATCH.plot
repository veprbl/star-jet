# BEGIN PLOT /MC_JET_PARTON_MATCH/dr
XLabel=$\Delta R$
# END PLOT

# BEGIN PLOT /MC_JET_PARTON_MATCH/pt_corr.*
LogY=0
# END PLOT

# BEGIN PLOT /MC_JET_PARTON_MATCH/pt_corr$
LogZ=1
ZMin=1e-3
XLabel=parton jet $p_T$ [GeV]
YLabel=particle jet $p_T$ [GeV]
# END PLOT

# BEGIN PLOT /MC_JET_PARTON_MATCH/pt_corr_diff$
XLabel=parton jet $p_T$ [GeV]
YLabel=Avg. $($particle jet $p_T) - ($parton jet $p_T)$ [GeV]
# END PLOT

# BEGIN PLOT /MC_JET_PARTON_MATCH/pt_corr_diff2$
XLabel=parton jet $p_T$ [GeV]
YLabel=Avg. $(($particle jet $p_T) - ($parton jet $p_T))^2$ [GeV]
# END PLOT

# BEGIN PLOT /MC_JET_PARTON_MATCH/pt_corr_diffT$
XLabel=particle jet $p_T$ [GeV]
YLabel=Avg. $($parton jet $p_T) - ($particle jet $p_T)$ [GeV]
# END PLOT

# BEGIN PLOT /MC_JET_PARTON_MATCH/pt_corr_diff2T$
XLabel=particle jet $p_T$ [GeV]
YLabel=Avg. $(($parton jet $p_T) - ($particle jet $p_T))^2$ [GeV]
# END PLOT

# BEGIN PLOT /MC_JET_PARTON_MATCH/numjet
LogZ=1
XLabel=number of parton jets in event
YLabel=number of particle jets in event
# END PLOT

# BEGIN PLOT /MC_JET_PARTON_MATCH/.*particle.*
XLabel=particle jet $p_T$ [GeV]
# END PLOT

# BEGIN PLOT /MC_JET_PARTON_MATCH/.*parton.*
XLabel=parton jet $p_T$ [GeV]
# END PLOT

# BEGIN PLOT /MC_JET_PARTON_MATCH/.*_eff
LogY=0
YMax=1.0
# END PLOT