// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {

  /// @brief Add a short analysis description here
  class STAR_2012_JETXSection_510_Prelim : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(STAR_2012_JETXSection_510_Prelim);

    /// Book histograms and initialise projections before the run
    void init() {

      const FinalState fs(Cuts::abseta < 2.0);
      declare(fs, "FS");

      FastJets jetfs(fs, FastJets::ANTIKT, 0.5);
      jetfs.useJetArea(new fastjet::AreaDefinition(
        fastjet::AreaType::active_area,
        fastjet::GhostedAreaSpec(1.5, 1, 0.04, 1.0, 0.1, 1e-100)
      ));
      declare(jetfs, "jets");

      // Book histograms
      book(_h["Jets_eta0p9"], 1, 1, 1);
      book(_h["Jets_eta0p5"], 2, 1, 1);
      book(_h["Jets_0p5eta0p9"], 3, 1, 1);

      ue_subtraction = getOption<bool>("UE_SUBTR", true);

    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {

      const FinalState& fs = apply<FinalState>(event, "FS");
      const FastJets& jetfs = apply<FastJets>(event, "jets");
      // Retrieve clustered jets, sorted by pT, with a minimum pT cut
      Jets jets = jetfs.jetsByPt(Cuts::pT > 5.0*GeV);
      const shared_ptr<fastjet::ClusterSequenceArea> csa = ue_subtraction ? jetfs.clusterSeqArea() : nullptr;

      for (const Jet& j : jets){
        double sum_cone_pt = 0.;
        for(const Particle& p : fs.particles()) {
          const double dphi_plus = Phi_mpi_pi(p.phi() + M_PI_2 - j.phi());
          const double dphi_minus = Phi_mpi_pi(p.phi() - M_PI_2 - j.phi());
          const double deta = p.eta() - j.eta();
          if ((squared(dphi_plus) + squared(deta) < squared(UE_CONE_R))
              || (squared(dphi_minus) + squared(deta) < squared(UE_CONE_R)))
          {
            sum_cone_pt += p.pt();
          }
        }

        double jet_pt = j.pt();
        if (ue_subtraction) {
          double ue_density = sum_cone_pt / 2 / UE_CONE_AREA;
          double jet_area = csa->area(j);
          jet_pt -= ue_density * jet_area;
        }
        if(fabs(j.eta()) < 0.9)
          _h["Jets_eta0p9"]->fill(jet_pt / GeV);
        if(fabs(j.eta()) < 0.5)
          _h["Jets_eta0p5"]->fill(jet_pt / GeV);
        if(fabs(j.eta()) > 0.5 && fabs(j.eta()) < 0.9)
          _h["Jets_0p5eta0p9"]->fill(jet_pt / GeV);
      }

    }

    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h["Jets_eta0p9"], crossSection()/picobarn/sumW()/1.8);
      scale(_h["Jets_eta0p5"], crossSection()/picobarn/sumW()/1.0);
      scale(_h["Jets_0p5eta0p9"], crossSection()/picobarn/sumW()/0.8);

    }

    /// @name Histograms
    map<string, Histo1DPtr> _h;

  private:

    constexpr double squared(double x) {
      return x * x;
    }
    double Phi_mpi_pi(double phi) {
      return std::remainder(phi, 2 * M_PI);
    }
    const double UE_CONE_R = 0.5;
    const double UE_CONE_AREA = M_PI * squared(UE_CONE_R);
    bool ue_subtraction;
  };

  DECLARE_RIVET_PLUGIN(STAR_2012_JETXSection_510_Prelim);

}
