BEGIN PLOT /STAR_2012_JETXSection_510_Prelim/d01-x01-y01
Title=Jet cross-section	in 510 GeV p+p collisons $|\eta|<$0.9
XLabel=Jet $p_{T}$ (GeV/c)
YLabel=d$\sigma^{2}$/d$p_{T}$d$\eta$ (pb$\cdot c$/GeV)
END PLOT

BEGIN PLOT /STAR_2012_JETXSection_510_Prelim/d02-x01-y01
Title=Jet cross-section in 510 GeV p+p collisons $|\eta|<$0.5
XLabel=Jet $p_{T}$ (GeV/c)
YLabel=d$\sigma^{2}$/d$p_{T}$d$\eta$ (pb$\cdot c$/GeV)
END PLOT

BEGIN PLOT /STAR_2012_JETXSection_510_Prelim/d03-x01-y01
Title=Jet cross-section	in 510 GeV p+p collisons 0.5$<|\eta|<$0.9
XLabel=Jet $p_{T}$ (GeV/c)
YLabel=d$\sigma^{2}$/d$p_{T}$d$\eta$ (pb$\cdot c$/GeV)
END PLOT

# 
