# BEGIN PLOT /STAR_2017_I1493842/d.
# END PLOT

# BEGIN PLOT /STAR_2017_I1493842/d01-x01.
XLabel=$m_{jj}$, GeV
YLabel=$\mathrm{d}\sigma/\mathrm{d}m$, pb/GeV
# END PLOT

# BEGIN PLOT /STAR_2017_I1493842/d01-x01-y01
Title=Dijet mass spectrum for $|y| < 0.8$, anti-$k_T$, $R=0.6$
# END PLOT

# BEGIN PLOT /STAR_2017_I1493842/d01-x01-y02
Title=Dijet mass spectrum for $|y| < 0.8$, EE+WW, anti-$k_T$, $R=0.6$
# END PLOT

# BEGIN PLOT /STAR_2017_I1493842/d01-x01-y03
Title=Dijet mass spectrum for $|y| < 0.8$, EW+WE, anti-$k_T$, $R=0.6$
# END PLOT

# BEGIN PLOT /STAR_2017_I1493842/d02-x01.
LogX=1
XLabel=$x$
# END PLOT
