// vim:et sw=2 sts=2
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  class STAR_2017_I1493842 : public Analysis {
  public:

    /// Constructor
    STAR_2017_I1493842()
      : Analysis("STAR_2017_I1493842")
      , _mass_def(MASS_FULL)
    {    }


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      FinalState fs;
      declare(FastJets(fs, FastJets::ANTIKT, 0.6), "Jets");
      double _mbins[] = {19, 23, 28, 34, 41, 49, 58, 69, 82, 100};
      std::vector<double> mbins(_mbins, _mbins + sizeof(_mbins)/sizeof(_mbins[0]));

      book(_h_dijet_mass, "d01-x01-y01", mbins);
      book(_h_ee_ww_dijet_mass, "d01-x01-y02", mbins);
      book(_h_ew_ew_dijet_mass, "d01-x01-y03", mbins);

      book(_h_dijet_mass_fine, "d01-x02-y01", (120-16)/2, 16., 120.);
      book(_h_ee_ww_dijet_mass_fine, "d01-x02-y02", (120-16)/2, 16., 120.);
      book(_h_ew_ew_dijet_mass_fine, "d01-x02-y03", (120-16)/2, 16., 120.);

      std::string mass_def_s = getOption<std::string>("MASS", "full");
      if (mass_def_s == "full") {
        _mass_def = MASS_FULL;
      } else if (mass_def_s == "reduced") {
        _mass_def = MASS_REDUCED;
      } else {
        MSG_ERROR("Unknow mass definition type \"" << mass_def_s << "\"");
        std::terminate();
      }
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = applyProjection<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 6.*GeV) && (Cuts::absrap < 0.8));

      if (jets.size() < 2) return;

      const Jet& leading_jet = jets[0];
      const Jet& subleading_jet = jets[1];

      if ((leading_jet.pT() >= 8.*GeV)
       && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5))
      {
        const double dijet_mass_full = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;
        const double dijet_mass_reduced = sqrt(
            2 * leading_jet.momentum() * subleading_jet.momentum()
            ) / GeV;
        double dijet_mass = 0.;

        switch (_mass_def) {
        case MASS_FULL:
          dijet_mass = dijet_mass_full;
          break;

        case MASS_REDUCED:
          dijet_mass = dijet_mass_reduced;
          break;
        }

        _h_dijet_mass->fill(dijet_mass);
        _h_dijet_mass_fine->fill(dijet_mass);
        if (jets[0].eta() * jets[1].eta() >= 0)
        {
          _h_ee_ww_dijet_mass->fill(dijet_mass);
          _h_ee_ww_dijet_mass_fine->fill(dijet_mass);
        }
        else
        {
          _h_ew_ew_dijet_mass->fill(dijet_mass);
          _h_ew_ew_dijet_mass_fine->fill(dijet_mass);
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {
      scale(_h_dijet_mass, crossSection()/sumOfWeights());
      scale(_h_ee_ww_dijet_mass, crossSection()/sumOfWeights());
      scale(_h_ew_ew_dijet_mass, crossSection()/sumOfWeights());

      scale(_h_dijet_mass_fine, crossSection()/sumOfWeights());
      scale(_h_ee_ww_dijet_mass_fine, crossSection()/sumOfWeights());
      scale(_h_ew_ew_dijet_mass_fine, crossSection()/sumOfWeights());
    }

    //@}


  private:

    // Data members like post-cuts event weight counters go here


    /// @name Histograms
    //@{
    Histo1DPtr _h_dijet_mass;
    Histo1DPtr _h_ee_ww_dijet_mass;
    Histo1DPtr _h_ew_ew_dijet_mass;

    Histo1DPtr _h_dijet_mass_fine;
    Histo1DPtr _h_mass_eta_fine;
    Histo1DPtr _h_mass_y_fine;
    Histo1DPtr _h_ee_ww_dijet_mass_fine;
    Histo1DPtr _h_ew_ew_dijet_mass_fine;
    //@}


    enum {
      MASS_FULL,
      MASS_REDUCED
    } _mass_def;


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(STAR_2017_I1493842);


}
