// vim:et sw=2 sts=2
// -*- C++ -*-

#include <random>

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FinalPartons.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_JET_VPDMB : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_JET_VPDMB);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      const FinalState fs;
      declare(fs, "FS");

      declare(FastJets(fs, FastJets::ANTIKT, 0.6), "Jets");

      _h_pt = bookHisto1D("pt", 60, 0., 60.);
      _h_vpdmb100_pt = bookHisto1D("vpdmb100_pt", 60, 0., 60.);
      _h_vpdmb50_pt = bookHisto1D("vpdmb50_pt", 60, 0., 60.);
      _h_vpdmb100ew_pt = bookHisto1D("vpdmb100ew_pt", 60, 0., 60.);
      _h_vpdmb50ew_pt = bookHisto1D("vpdmb50ew_pt", 60, 0., 60.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FinalState& fs = apply<FinalState>(event, "FS");
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 5.*GeV) && (Cuts::absrap < 0.8));
      const double weight = event.weight();
      bool vpdmb100w_flag = false;
      bool vpdmb50w_flag = false;
      bool vpdmb100e_flag = false;
      bool vpdmb50e_flag = false;

      for (const Particle& p : fs.particles()) {
        if ((p.eta() < -4.24) && (p.eta() > -5.1)) {
          vpdmb100e_flag = true;
          if (dis(gen) < 0.5) { // simulate acceptance
            vpdmb50e_flag = true;
          }
        }
        if ((p.eta() > 4.24) && (p.eta() < 5.1)) {
          vpdmb100w_flag = true;
          if (dis(gen) < 0.5) { // simulate acceptance
            vpdmb50w_flag = true;
          }
        }
      }

      for(const Jet& jet : jets) {
        _h_pt->fill(jet.pt(), weight);
        if (vpdmb100w_flag || vpdmb100e_flag) {
          _h_vpdmb100_pt->fill(jet.pt(), weight);
        }
        if (vpdmb100w_flag && vpdmb100e_flag) {
          _h_vpdmb100ew_pt->fill(jet.pt(), weight);
        }
        if (vpdmb50w_flag || vpdmb50e_flag) {
          _h_vpdmb50_pt->fill(jet.pt(), weight);
        }
        if (vpdmb50w_flag && vpdmb50e_flag) {
          _h_vpdmb50ew_pt->fill(jet.pt(), weight);
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_pt, crossSection()/sumOfWeights());
      scale(_h_vpdmb100_pt, crossSection()/sumOfWeights());
      scale(_h_vpdmb50_pt, crossSection()/sumOfWeights());
      scale(_h_vpdmb100ew_pt, crossSection()/sumOfWeights());
      scale(_h_vpdmb50ew_pt, crossSection()/sumOfWeights());

    }

    //@}


  private:

    std::random_device rd;
    std::mt19937 gen = std::mt19937(rd());
    std::uniform_real_distribution<> dis = std::uniform_real_distribution<>(0.0, 1.0);

    /// @name Histograms
    //@{

    Histo1DPtr _h_pt;
    Histo1DPtr _h_vpdmb100_pt;
    Histo1DPtr _h_vpdmb50_pt;
    Histo1DPtr _h_vpdmb100ew_pt;
    Histo1DPtr _h_vpdmb50ew_pt;

    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_JET_VPDMB);


}
