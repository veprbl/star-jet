// vim:et sw=2 sts=2
// -*- C++ -*-

#include <random>

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FinalPartons.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_JET_VPDMB : public Analysis {
  private:

    constexpr double square(double x) {
      return x * x;
    }
    double Phi_mpi_pi(double phi) {
      return std::remainder(phi, 2 * M_PI);
    }
    const double UE_CONE_R = 0.5;
    const double UE_CONE_AREA = M_PI * square(UE_CONE_R);

  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_JET_VPDMB);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      const FinalState fs;
      declare(fs, "FS");

      declare(FastJets(fs, FastJets::ANTIKT, 0.6), "Jets");

      book(_h_unbiased_pt, "unbiased_pt", 60, 0., 60.);
      book(_h_unbiased_uecone_vs_pt, "unbiased_uecone_vs_pt", 60, 0., 60.);
      book(_h_unbiased_uecone_chg_vs_pt, "unbiased_uecone_chg_vs_pt", 60, 0., 60.);
      book(_h_unbiased_uecone_neut_vs_pt, "unbiased_uecone_neut_vs_pt", 60, 0., 60.);
      book(_h_vpdmb100_pt, "vpdmb100_pt", 60, 0., 60.);
      book(_h_vpdmb50_pt, "vpdmb50_pt", 60, 0., 60.);
      book(_h_vpdmb100ew_pt, "vpdmb100ew_pt", 60, 0., 60.);
      book(_h_vpdmb100ew_uecone_vs_pt, "vpdmb100ew_uecone_vs_pt", 60, 0., 60.);
      book(_h_vpdmb100ew_uecone_chg_vs_pt, "vpdmb100ew_uecone_chg_vs_pt", 60, 0., 60.);
      book(_h_vpdmb100ew_uecone_neut_vs_pt, "vpdmb100ew_uecone_neut_vs_pt", 60, 0., 60.);
      book(_h_vpdmb50ew_pt, "vpdmb50ew_pt", 60, 0., 60.);
      book(_h_vpdmb50ew_uecone_vs_pt, "vpdmb50ew_uecone_vs_pt", 60, 0., 60.);
      book(_h_vpdmb50ew_uecone_chg_vs_pt, "vpdmb50ew_uecone_chg_vs_pt", 60, 0., 60.);
      book(_h_vpdmb50ew_uecone_neut_vs_pt, "vpdmb50ew_uecone_neut_vs_pt", 60, 0., 60.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FinalState& fs = apply<FinalState>(event, "FS");
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 5.*GeV) && (Cuts::absrap < 0.8));
      bool vpdmb100w_flag = false;
      bool vpdmb50w_flag = false;
      bool vpdmb100e_flag = false;
      bool vpdmb50e_flag = false;

      for (const Particle& p : fs.particles()) {
        if ((p.eta() < -4.24) && (p.eta() > -5.1)) {
          vpdmb100e_flag = true;
          if (dis(gen) < 0.5) { // simulate acceptance
            vpdmb50e_flag = true;
          }
        }
        if ((p.eta() > 4.24) && (p.eta() < 5.1)) {
          vpdmb100w_flag = true;
          if (dis(gen) < 0.5) { // simulate acceptance
            vpdmb50w_flag = true;
          }
        }
      }

      for(const Jet& jet : jets) {
        double sum_cone_pt = 0.;
        double sum_cone_chg_pt = 0.;
        double sum_cone_neut_pt = 0.;
        for(const Particle& p : fs.particles()) {
          if (
              (std::fabs(p.eta()) < 1.0)
           && (p.pt() > 0.2)
            ) {
            const double dphi_plus = Phi_mpi_pi(p.phi() + M_PI_2 - jet.phi());
            const double dphi_minus = Phi_mpi_pi(p.phi() - M_PI_2 - jet.phi());
            const double deta = p.eta() - jet.eta();
            if (
                (square(dphi_plus) + square(deta) < square(UE_CONE_R))
             || (square(dphi_minus) + square(deta) < square(UE_CONE_R))
              ) {
              sum_cone_pt += p.pt();
              if (p.charge() != 0) {
                sum_cone_chg_pt += p.pt();
              } else {
                sum_cone_neut_pt += p.pt();
              }
            }
          }
        }

        _h_unbiased_pt->fill(jet.pt());
        _h_unbiased_uecone_vs_pt->fill(jet.pt(), sum_cone_pt / (2 * UE_CONE_AREA));
        _h_unbiased_uecone_chg_vs_pt->fill(jet.pt(), sum_cone_chg_pt / (2 * UE_CONE_AREA));
        _h_unbiased_uecone_neut_vs_pt->fill(jet.pt(), sum_cone_neut_pt / (2 * UE_CONE_AREA));

        if (vpdmb100w_flag || vpdmb100e_flag) {
          _h_vpdmb100_pt->fill(jet.pt());
        }
        if (vpdmb100w_flag && vpdmb100e_flag) {
          _h_vpdmb100ew_pt->fill(jet.pt());
          _h_vpdmb100ew_uecone_vs_pt->fill(jet.pt(), sum_cone_pt / (2 * UE_CONE_AREA));
          _h_vpdmb100ew_uecone_chg_vs_pt->fill(jet.pt(), sum_cone_chg_pt / (2 * UE_CONE_AREA));
          _h_vpdmb100ew_uecone_neut_vs_pt->fill(jet.pt(), sum_cone_neut_pt / (2 * UE_CONE_AREA));
        }
        if (vpdmb50w_flag || vpdmb50e_flag) {
          _h_vpdmb50_pt->fill(jet.pt());
        }
        if (vpdmb50w_flag && vpdmb50e_flag) {
          _h_vpdmb50ew_pt->fill(jet.pt());
          _h_vpdmb50ew_uecone_vs_pt->fill(jet.pt(), sum_cone_pt / (2 * UE_CONE_AREA));
          _h_vpdmb50ew_uecone_chg_vs_pt->fill(jet.pt(), sum_cone_chg_pt / (2 * UE_CONE_AREA));
          _h_vpdmb50ew_uecone_neut_vs_pt->fill(jet.pt(), sum_cone_neut_pt / (2 * UE_CONE_AREA));
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_unbiased_pt, crossSection()/sumOfWeights());
      _h_unbiased_uecone_vs_pt->scaleW(crossSection()/sumOfWeights());
      _h_unbiased_uecone_chg_vs_pt->scaleW(crossSection()/sumOfWeights());
      _h_unbiased_uecone_neut_vs_pt->scaleW(crossSection()/sumOfWeights());
      scale(_h_vpdmb100_pt, crossSection()/sumOfWeights());
      scale(_h_vpdmb50_pt, crossSection()/sumOfWeights());
      scale(_h_vpdmb100ew_pt, crossSection()/sumOfWeights());
      _h_vpdmb100ew_uecone_vs_pt->scaleW(crossSection()/sumOfWeights());
      _h_vpdmb100ew_uecone_chg_vs_pt->scaleW(crossSection()/sumOfWeights());
      _h_vpdmb100ew_uecone_neut_vs_pt->scaleW(crossSection()/sumOfWeights());
      scale(_h_vpdmb50ew_pt, crossSection()/sumOfWeights());
      _h_vpdmb50ew_uecone_vs_pt->scaleW(crossSection()/sumOfWeights());
      _h_vpdmb50ew_uecone_chg_vs_pt->scaleW(crossSection()/sumOfWeights());
      _h_vpdmb50ew_uecone_neut_vs_pt->scaleW(crossSection()/sumOfWeights());

    }

    //@}


  private:

    std::mt19937 gen = std::mt19937(std::mt19937::default_seed);
    std::uniform_real_distribution<> dis = std::uniform_real_distribution<>(0.0, 1.0);

    /// @name Histograms
    //@{

    Histo1DPtr _h_unbiased_pt;
    Profile1DPtr _h_unbiased_uecone_vs_pt;
    Profile1DPtr _h_unbiased_uecone_chg_vs_pt;
    Profile1DPtr _h_unbiased_uecone_neut_vs_pt;
    Histo1DPtr _h_vpdmb100_pt;
    Histo1DPtr _h_vpdmb50_pt;
    Histo1DPtr _h_vpdmb100ew_pt;
    Profile1DPtr _h_vpdmb100ew_uecone_vs_pt;
    Profile1DPtr _h_vpdmb100ew_uecone_chg_vs_pt;
    Profile1DPtr _h_vpdmb100ew_uecone_neut_vs_pt;
    Histo1DPtr _h_vpdmb50ew_pt;
    Profile1DPtr _h_vpdmb50ew_uecone_vs_pt;
    Profile1DPtr _h_vpdmb50ew_uecone_chg_vs_pt;
    Profile1DPtr _h_vpdmb50ew_uecone_neut_vs_pt;

    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_JET_VPDMB);


}
