BEGIN PLOT /STAR_2019_I1771348/d01-x01-y01
Title=Toward region $p_{T} > 0.2$, GeV
XLabel= Leading jet $p_{T}$, GeV
YLabel=$ \langle \frac{dN_{ch}}{d\eta d\phi} \rangle$
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
YMin=0
YMax=3.5
END PLOT

BEGIN PLOT /STAR_2019_I1771348/d01-x01-y02
Title=Away region $p_{T} > 0.2$, GeV
XLabel= Leading jet $p_{T}$, GeV
YLabel=$ \langle \frac{dN_{ch}}{d\eta d\phi} \rangle$
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
YMin=0
YMax=3.5
END PLOT

BEGIN PLOT /STAR_2019_I1771348/d01-x01-y03
Title=Transverse region $p_{T} > 0.2$, GeV
XLabel= Leading jet $p_{T}$, GeV
YLabel=$ \langle \frac{dN_{ch}}{d\eta d\phi} \rangle$
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
YMin=0
YMax=3.5
END PLOT

BEGIN PLOT /STAR_2019_I1771348/d02-x01-y02
Title=Transverse region $p_{T} > 0.5$, GeV
XLabel= Leading jet $p_{T}$, GeV
YLabel=$ \langle \frac{dN_{ch}}{d\eta d\phi} \rangle$
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
YMin=0
YMax=3.5
END PLOT

BEGIN PLOT /STAR_2019_I1771348/d03-x01-y01
Title=Toward region $p_{T} > 0.2$, GeV
XLabel= Leading jet $p_{T}$, GeV
YLabel=$ \langle p^{ch}_{T} \rangle$
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
YMin=0
YMax=3.2
END PLOT

BEGIN PLOT /STAR_2019_I1771348/d03-x01-y02
Title=Away region $p_{T} > 0.2$, GeV
XLabel= Leading jet $p_{T}$, GeV
YLabel=$ \langle p^{ch}_{T} \rangle$
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
YMin=0
YMax=3.2
END PLOT

BEGIN PLOT /STAR_2019_I1771348/d03-x01-y03
Title=Transverse region $p_{T} > 0.2$, GeV
XLabel= Leading jet $p_{T}$, GeV
YLabel=$ \langle p^{ch}_{T} \rangle$
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
YMin=0
YMax=3.2
END PLOT

BEGIN PLOT /STAR_2019_I1771348/d04-x01-y02
Title=Transverse region $p_{T} > 0.5$, GeV
XLabel= Leading jet $p_{T}$, GeV
YLabel=$ \langle p^{ch}_{T} \rangle$
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
YMin=0
YMax=2
END PLOT

