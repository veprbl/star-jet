with {
  inherit (import <nixpkgs> {})
    lib
    gzip
    herwig
    lhapdf
    rivet
    yoda
    ;
};
with import ../../common.nix;

{ overrides ? self: super: {} }:

let
  this = self: super: with self; {
    inherit (herwig) version;

    runHerwig7 = { num_events ? 100000, mhat_min ? "0", mhat_max ? "-1", pt_min ? "0", pt_max ? "-1", seed }@args:
      let
        # disable MPI, need to figure out how to enable it at sqrt(s)=200GeV
        steering_file = builtins.toFile "steering.in" ''
          read snippets/PPCollider.in

          cd /Herwig/Generators
          set EventGenerator:NumberOfEvents ${toString num_events}
          set EventGenerator:RandomNumberGenerator:Seed ${toString seed}
          set /Herwig/Shower/ShowerHandler:MPIHandler NULL
          set /Herwig/DipoleShower/DipoleShowerHandler:MPIHandler NULL

          set /Herwig/Shower/ShowerHandler:IntrinsicPtGaussian 2.2*GeV
          set EventGenerator:EventHandler:LuminosityFunction:Energy 200.0

          cd /Herwig/Cuts
          set Cuts:MHatMin ${mhat_min}*GeV
          ${lib.optionalString (mhat_max != "-1") "set Cuts:MHatMax ${mhat_max}*GeV"}
          set JetKtCut:MinKT ${pt_min}*GeV
          ${lib.optionalString (pt_max != "-1") "set JetKtCut:MaxKT ${pt_max}*GeV"}

          cd /Herwig/MatrixElements/
          insert SubProcess:MatrixElements[0] MEQCD2to2

          cd /Herwig/Generators
          read snippets/Rivet.in
          ${join (map (ana_name: "insert /Herwig/Analysis/Rivet:Analyses 0 ${ana_name}\n") rivet_analyses_names)}

          run RHIC EventGenerator
        '';
      in
      mkDerivation rec {
        name = "herwig7-${pt_min}-${pt_max}";

        buildInputs = [ gzip rivet herwig lhapdf.pdf_sets.MMHT2014lo68cl lhapdf.pdf_sets.MMHT2014nlo68cl ] ++ rivet_analyses;
        buildPhase = ''
          mkfifo herwig.hepmc
          ln -s ${steering_file} steering.in
          Herwig read steering.in
          mv RHIC.yoda output.yoda
          gzip output.yoda
        '';
        installPhase = ''
          install -Dm644 output.yoda.gz "$out"/output.yoda.gz
        '';
        passthru.title = builtins.replaceStrings ["_"] ["\\_"] name;
      };
    currentMC = runHerwig7;
    needmode = false;
  };
in
  import ../bits.nix { overrides = compose [ this overrides ]; }
