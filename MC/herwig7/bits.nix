{ nixpkgs ? import <nixpkgs> {} }:

with {
  inherit (nixpkgs)
    lib
    gzip
    herwig
    lhapdf
    rivet
    yoda
    ;
};
with import ../../common.nix { inherit (nixpkgs) lib; };

let
  this = final: prev: with final; {
    inherit (herwig) pname version;

    tunes = {
      default = "";
      SoftTune = ''
        set /Herwig/Hadronization/ColourReconnector:ReconnectionProbability 0.5
        set /Herwig/UnderlyingEvent/MPIHandler:pTmin0 3.502683
        set /Herwig/UnderlyingEvent/MPIHandler:InvRadius 1.402055
        set /Herwig/UnderlyingEvent/MPIHandler:Power 0.416852
        set /Herwig/Partons/RemnantDecayer:ladderPower -0.08
        set /Herwig/Partons/RemnantDecayer:ladderNorm 0.95
      '';
    };

    runHerwig7 = {
      num_events ? DEFAULT_NUM_EVENTS,
      mhat_min ? "0",
      mhat_max ? "-1",
      pt_min ? "0",
      pt_max ? "-1",
      seed,
      pdf_set ? null,
      enable_had ? true,
      enable_mpi ? true,
      mpi_tune ? "default",
      disable_decay ? false
    }@args:
      assert pdf_set == null;
      let
        # disable MPI, need to figure out how to enable it at sqrt(s)=200GeV
        steering_file = builtins.toFile "steering.in" ''
          read snippets/PPCollider.in

          cd /Herwig/EventHandlers
          ${lib.optionalString (!enable_had) ''
          set EventHandler:HadronizationHandler NULL
          set /Herwig/Analysis/Basics:CheckQuark No
          ''}
          ${if enable_mpi then tunes.${mpi_tune} else ''
            set /Herwig/Shower/ShowerHandler:MPIHandler NULL
            set /Herwig/DipoleShower/DipoleShowerHandler:MPIHandler NULL
          ''}
          ${lib.optionalString disable_decay "set EventHandler:DecayHandler NULL"}

          cd /Herwig/Generators
          set EventGenerator:NumberOfEvents ${toString num_events}
          set EventGenerator:RandomNumberGenerator:Seed ${toString seed}

          set /Herwig/Shower/ShowerHandler:IntrinsicPtGaussian 2.2*GeV
          set EventGenerator:EventHandler:LuminosityFunction:Energy 200.0

          cd /Herwig/Cuts
          set Cuts:MHatMin ${mhat_min}*GeV
          ${lib.optionalString (mhat_max != "-1") "set Cuts:MHatMax ${mhat_max}*GeV"}
          set JetKtCut:MinKT ${pt_min}*GeV
          ${lib.optionalString (pt_max != "-1") "set JetKtCut:MaxKT ${pt_max}*GeV"}

          cd /Herwig/MatrixElements/
          insert SubProcess:MatrixElements[0] MEQCD2to2

          cd /Herwig/Generators
          read snippets/Rivet.in
          ${join (map (ana_name: "insert /Herwig/Analysis/Rivet:Analyses 0 ${ana_name}\n") rivet_analyses)}

          run RHIC EventGenerator
        '';
      in
      mkDerivation rec {
        name = "herwig7-${pt_min}-${pt_max}";

        buildInputs = [ gzip rivet herwig pdf_sets.CT14lo pdf_sets.CT14nlo ] ++ rivet_built_analyses;
        buildPhase = ''
          mkfifo herwig.hepmc
          ln -s ${steering_file} steering.in
          Herwig read steering.in
          mv RHIC.yoda output.yoda
          gzip output.yoda
        '';
        installPhase = ''
          ${installOutputYodaGz}
        '';
        passthru.title = builtins.replaceStrings ["_"] ["\\_"] name;
      };
    currentMC = runHerwig7;
    needmode = false;
  };
in
  (import ../bits.nix { inherit nixpkgs; }).override this
