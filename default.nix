{ nixpkgs ? import <nixpkgs> {}, nobatch ? false }:

with nixpkgs;
with import ./common.nix { inherit (nixpkgs) lib; };

((import ./bits.nix { inherit nixpkgs; }).override (final: prev: let bits = final; in {
  LOCK_DIRECTORY = "`cat ${builtins.getEnv "HOME"}/.star_jets_lock_directory`";
  OUTPUT_DIRECTORY = "`cat ${builtins.getEnv "HOME"}/.make_jet_plots_output_path`";

  enable_lualatex = bits.override (final: prev: {
    rivet-mkhtml = prev.rivet-mkhtml.overrideAttrs (old: {
      postPatch = (old.postPatch or "") + ''
        substituteInPlace bin/make-plots \
          --replace 'False #have_cmd("dvilualatex")' 'have_cmd("dvilualatex")'
        substituteInPlace bin/make-plots \
          --replace '"dvilualatex"' '"'$latex'/bin/dvilualatex"' \
          --replace '\\usepackage[symbolgreek]{mathastext}' ""
      '';
    });
  });

  process_overrides = prev.process_overrides // {
    start_event_index = 0;
    max_track_pt = 30;
    max_vertex_z = 60; /* cm */
    blacklist_tower_3407 = 1;
    ue_shift_fraction = 1.0;
    ue_shift_fraction_particle = 1.0;
    ue_shift_fraction_detector = 1.0;
  };
  etabin_cut_0 = "0.0";
  etabin_cut_1 = "0.5";
  etabin_cut_2 = "0.9";
  inherit nobatch;
  ptjet_ue_corr_label = " corrected for UE";
  ptjet_ue_corr_title = "w/\\,UE corr";
  SQRT_S = 200.0 /* GeV */;

  # Exclude runs that don't have a correct trigger deadtime (for Jamie's numbers)
  bad_runindex = [ 57 61 62 104 146 189 535 572 ];
  runlist = builtins.filter (runid: !(builtins.elem (final.runindex_from_runid runid) final.bad_runindex)) prev.runlist;

  process_data_overrides = prev.process_data_overrides // {
    vertex_reweight_type = "none";
    trig_fire_pretend_embed = 0;
    trig_pretend_should_fire = 0;
    trig_pretend_did_fire = 0;
  };
  process_embed_overrides = prev.process_embed_overrides // {
    vertex_reweight_type = "embedding_tpc_vertex";
    vertex_reweight_sigma = 45.;
    vertex_reweight_sigma_target = 70.;
    vertex_reweight_b_target = 80.;

    # Skip first event of each file to workaround a minor bug in the
    # embedding sample. For full description, see the "Errata" section of
    # https://drupal.star.bnl.gov/STAR/starsimrequests/2021/may/17/pythia-6-run12-pp200-additional-request
    start_event_index = 1;

    tpcvz_simuvz_diff_max = 5;
  };

  jamie_lumi = final.override (final: prev: {
    run_lumi_file = misc/run_lumi.json;
    extra_custom_legend = ''\\\raisebox{-0.66\baselineskip}{$10\%$ luminosity uncertainty not shown}'';
  });

  without_soft_reweight = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      no_soft_reweight = 1;
    };
  });
  without_fudge_factors = bits.override (final: prev: {
    embed_disable_ff = true;
  });

  with_kevins_embed = bits.override (final: prev: {
    pt_bins = [
      "pt2_3"
      "pt3_4"
      "pt4_5"
      "pt5_7"
      "pt7_9"
      "pt9_11"
      "pt11_15"
      "pt15_20"
      "pt20_25"
      "pt25_35"
      "pt35_-1"
    ];
    embed_files = pt_bin: runid: ''"${final.HOME}/run12_pp200_embed/${pt_bin}/${pt_bin}_${runid}_*.jets.root:${toString (final.runindex_from_runid runid)}:${toString final.run_info.${runid}.jp0_prescaler}:${toString final.run_info.${runid}.jp1_prescaler}"'';
    embed_num_batches = pt_bin: {
      "pt2_3" = 15;
      "pt3_4" = 5;
      "pt4_5" = 5;
      "pt5_7" = 3;
      "pt7_9" = 3;
      "pt9_11" = 3;
      "pt11_15" = 2;
      "pt15_20" = 2;
      "pt20_25" = 2;
      "pt25_35" = 2;
      "pt35_-1" = 2;
    }.${pt_bin};

    process_data_overrides = prev.process_data_overrides // {
      vertex_reweight_type = "none";
    };
    process_embed_overrides = prev.process_embed_overrides // {
      vertex_reweight_type = "embedding_tpc_vertex";
      vertex_reweight_sigma = 26.5;
      vertex_reweight_sigma_target = 70.;
      vertex_reweight_b_target = 80.;
    };
  });

  reweight_none = bits.override (final: prev: {
    process_data_overrides = prev.process_data_overrides // {
      vertex_reweight_type = "none";
    };
    process_embed_overrides = prev.process_embed_overrides // {
      vertex_reweight_type = "none";
    };
  });
  reweight_by_data_tpc_vertex = bits.override (final: prev: {
    process_data_overrides = prev.process_data_overrides // {
      vertex_reweight_type = "data_tpc_vertex";
      vertex_reweight_sigma = 46.6;
      vertex_reweight_sigma_target = 26.5;
    };
    process_embed_overrides = prev.process_embed_overrides // {
      vertex_reweight_type = "none";
    };
  });
  reweight_by_embed_tpc_vertex = bits.override (final: prev: {
    process_data_overrides = prev.process_data_overrides // {
      vertex_reweight_type = "none";
    };
    process_embed_overrides = prev.process_embed_overrides // {
      vertex_reweight_type = "embedding_tpc_vertex";
      vertex_reweight_sigma = 26.5;
      vertex_reweight_sigma_target = 46.6;
    };
  });
  reweight_by_embed_gen_vertex = bits.override (final: prev: {
    process_data_overrides = prev.process_data_overrides // {
      vertex_reweight_type = "none";
    };
    process_embed_overrides = prev.process_embed_overrides // {
      vertex_reweight_type = "embedding_gen_vertex";
      vertex_reweight_sigma = 26.5;
      vertex_reweight_sigma_target = 46.6;
    };
  });

  min_ue_density_cut = min_ue_density_cut: bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      inherit min_ue_density_cut;
    };
  });

  jp_threshold_adjust_minus_one = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      jp_threshold_adjust = ((prev.process_overrides).jp_threshold_adjust or 0) - 1;
    };
  });
  jp_threshold_adjust_plus_one = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      jp_threshold_adjust = ((prev.process_overrides).jp_threshold_adjust or 0) + 1;
    };
  });

  two_vtx = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      require_two_good_vertices = 1;
    };
  });

  set_vz_range = min_vertex_z: max_vertex_z: bits: bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      inherit min_vertex_z max_vertex_z;
    };
  });
  set_max_abs_vz = max_abs_vertex_z: final.set_vz_range (-max_abs_vertex_z) max_abs_vertex_z;

  without_ue_subtraction = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      ue_shift_fraction = 0.0;
      ue_shift_fraction_particle = 0.0;
      ue_shift_fraction_detector = 0.0;
    };
    ptjet_ue_corr_label = "";
    ptjet_ue_corr_title = "w/o UE corr";
  });
  ue_subtraction = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      ue_shift_fraction = 1.0;
      ue_shift_fraction_particle = 1.0;
      ue_shift_fraction_detector = 1.0;
    };
  });
  ue_subtraction086 = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      ue_shift_fraction = 0.86;
      ue_shift_fraction_particle = 1.0;
      ue_shift_fraction_detector = 0.86;
    };
  });
  ue_subtraction118 = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      ue_shift_fraction = 1.18;
      ue_shift_fraction_particle = 1.0;
      ue_shift_fraction_detector = 1.18;
    };
  });
  no_track_cut = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      max_track_pt = -1;
    };
  });
  sector20_track_cut = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      sector20_track_cut = -1;
    };
  });
  pct4 = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      detector_jet_branch_name = "AntiKt${final.jet_r_branch}NHits12_4pct";
      detector_ue_branch_name = "AntiKt${final.jet_r_branch}NHits12_4pctOffAxisConesR050";
    };
  });
  pct1_alt = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      track_thin_out_fraction = "0.01";
    };
  });
  pct4_alt = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      track_thin_out_fraction = "0.04";
    };
  });
  pct7 = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      detector_jet_branch_name = "AntiKt${final.jet_r_branch}NHits12_7pct";
      detector_ue_branch_name = "AntiKt${final.jet_r_branch}NHits12_7pctOffAxisConesR050";
    };
  });
  charged4_shift = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      jet_pt_shift_sign = "-1";
      jet_charged_pt_shift = toString 0.04;
      jet_neutral_pt_shift = toString 0;
    };
  });
  neutral4_shift = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      jet_pt_shift_sign = "-1";
      jet_charged_pt_shift = toString 0;
      jet_neutral_pt_shift = toString 0.04;
    };
  });
  ecal_shift_plus = final.override (final: prev: {
    process_overrides = prev.process_overrides // {
      jet_pt_shift_sign = "+1";
      jet_charged_pt_shift = toString 0.011;
      jet_neutral_pt_shift = toString 0.032;
    };
  });
  ecal_shift_minus = final.override (final: prev: {
    process_overrides = prev.process_overrides // {
      jet_pt_shift_sign = "-1";
      jet_charged_pt_shift = toString 0.011;
      jet_neutral_pt_shift = toString 0.032;
    };
  });
  exclude_region = bits.override (final: prev: {
    runlist = builtins.filter
      (runid: let index = final.runindex_from_runid runid; in (index < 360) || (index >= 491))
      prev.runlist;
  });
  exclude_region340 = bits.override (final: prev: {
    runlist = builtins.filter
      (runid: let index = final.runindex_from_runid runid; in (index < 340) || (index >= 491))
      prev.runlist;
  });
  include_region = bits.override (final: prev: {
    runlist = builtins.filter
      (runid: let index = final.runindex_from_runid runid; in (index >= 360) && (index < 491))
      prev.runlist;
  });
  stack_config = bits.override (final: prev: {
    mkhtml_config = prev.mkhtml_config_stack;
  });
  vpdvz_vz_diff_lt_5 = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      vpdvz_vz_diff_max = 5;
    };
  });
  vpdvz_vz_diff_gt_5 = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      vpdvz_vz_diff_min = 5;
    };
  });
  vpd_vertex_found = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      vpd_vertex_found_cut = 1;
    };
  });
  vpd_vertex_not_found = bits.override (final: prev: {
    process_overrides = prev.process_overrides // {
      vpd_vertex_not_found_cut = 1;
    };
  });

  jet_R050 = bits.override (final: prev: {
    jet_r = "0.5";
  });
  jet_R060 = bits.override (final: prev: {
    jet_r = "0.6";
  });

  etabin_cut_09 = bits.override (final: prev: {
    etabin_cut_1 = "0.5";
    etabin_cut_2 = "0.9";
  });

  stable = bits.override (final: prev: {
    make_jet_plots_build = prev.make_jet_plots_build.overrideAttrs (old: {
      src = fetchgit {
        url = "https://gitlab.com/veprbl/star-jet.git";
        rev = "da4d1702163dd2b4f06a431d993051fc96f2593e";
        sha256 = "0jlkfsjw0h3gfr26kp450sknlrdwn0rmbmx1daggn47c7vxzrzya";
      };
    });
  });
  pdf_reweight = target_pdf: bits.override (final: prev: {
    process_do_pdf_reweight = true;
    reference_pdf = "cteq6l1";
    inherit target_pdf;
    runlist = final.remove_runs [
      "13061060" # broken skim file in pt3_4 bin of embedding
    ] prev.runlist;
  });
  rt_reweight = final.override (final: prev: {
    process_embed_overrides = prev.process_embed_overrides // {
      # prev != bits ...
      rt_reweight_yoda = final.reweight2d_ratio bits.data_yoda bits.embed_yoda + /output.yoda.gz;
      rt_reweight_histo_name = "/jet_quantities_jp1_reweight/jet_rt_vs_pt_histo";
    };
  });

  alternative_demotion = final.override (final: prev: {
    make_jet_plots_build = prev.make_jet_plots_build.overrideAttrs (_: {
      patches = [
        ./misc/alternative_demotion.patch
      ];
    });

    scale_lumi = with final; scale [
      "/.*_jp0_promoted/.* ${toString (1 / jp0_luminosity_no_ps)}x"
      "/.*_jp1_promoted/.* ${toString (1 / jp1_luminosity_no_ps)}x"
      "/.*_jp2_promoted/.* ${toString (1 / jp2_luminosity_no_ps)}x"
      "/.*_jp0_demoted/.* ${toString (1 / jp0_luminosity)}x" # removed _no_ps
      "/.*_jp1_demoted/.* ${toString (1 / jp1_luminosity)}x" # removed _no_ps
      "/.*_jp2_demoted/.* ${toString (1 / jp2_luminosity)}x" # removed _no_ps
      "/.*_jp0(_.+)?/.* ${toString (1 / jp0_luminosity)}x"
      "/.*_jp1(_.+)?/.* ${toString (1 / jp1_luminosity)}x"
      "/.*_jp2(_.+)?/.* ${toString (1 / jp2_luminosity)}x"
      "/.*_vpdmb_nobsmd(_jp0_simu)?/.* ${toString (1 / vpdmb_nobsmd_luminosity)}x"
      "/.*_zerobias/.* ${toString (1 / zerobias_luminosity * zerobias_definition_ps)}x"
    ];
  });

  require_vpdmb = final.override (final: prev: {
    process_data_overrides = prev.process_data_overrides // {
      vpdmb_did_fire_cut = 1;
    };
  });
  trig_fire_pretend_embed = final.override (final: prev: {
    process_data_overrides = prev.process_data_overrides // {
      trig_fire_pretend_embed = 1;
    };
  });
  trig_pretend_should_fire = final.override (final: prev: {
    process_data_overrides = prev.process_data_overrides // {
      trig_pretend_should_fire = 1;
    };
  });
  trig_pretend_did_fire = final.override (final: prev: {
    process_data_overrides = prev.process_data_overrides // {
      trig_pretend_did_fire = 1;
    };
  });
  force_trig_reweighting = final.override (final: prev: {
    process_data_overrides = prev.process_data_overrides // {
      force_trig_reweighting = 1;
    };
    # supply prescale factors to the data production
    data_files = runid: ''${prev.data_files runid}:${toString prev.run_info.${runid}.jp0_prescaler}:${toString prev.run_info.${runid}.jp1_prescaler}'';
  });
  without_lumi = final.override (final: prev: {
    scale_for_data = final.no_scale;
    scale_for_embed = final.unscale_lumi;
    sigma_label = "N";
    sigma_unit = "1";
  });

  without_tpcvz_cut = final.override (final: prev: {
    process_embed_overrides = builtins.removeAttrs prev.process_embed_overrides [ "tpcvz_simuvz_diff_max" ];
  });

  pp510 = final.override (final: prev: {
    SQRT_S = 510.0 /* GeV */;
    unfold_bins = [ 8.0 10.0 13.0 17.0 21.0 26.0 32.0 39.0 47.0 57.0 68.0 80.0 ];
  });

  # Binning
  unfold_bins = [ 6.9 8.2 9.7 11.5 13.6 16.1 19.0 22.5 26.6 31.4 37.2 44.0 52.0 ];
  bins_for_jp = jp:
    let
      bins = final.unfold_bins;
      nbins = builtins.length bins;
      jp_num_m = builtins.match "([0-9]+).*" jp;
      jp_num =
        if builtins.isInt jp then
          jp
        else if jp_num_m != null then
          builtins.fromJSON (builtins.elemAt jp_num_m 0)
        else
          null;
    in
      if jp_num == 0 then
        lib.sublist 0 (nbins - 1) bins
      else if jp_num == 1 then
        bins
      else if jp_num == 2 then
        lib.sublist 2 (nbins - 2) bins
      else if jp == "promoted_combined" then
        bins
      else if jp == "demoted_combined" then
        bins
      else
        throw "Invalid jp";
})).override (final: prev: with final; {
test = process {} "test" [ "${HOME}/star-jet-test/test.jets.root:1" ];
  bits = prev;
  bins_scaled_all = mkhtml' "--no-ratio" {} (lib.attrValues embed_bins);
  bins_unscaled_all = mkhtml' "--no-ratio" {} (lib.attrValues embed_bins_unscaled);
  bins = lib.mapAttrs (bin_name: bin: mkhtml' "" {} [ bin ]) embed_bins_unscaled;
  summary =
    mkhtml' "--errs" { LegendXPos = 0.2; LogZ = 1; } [
        (set_title' "Data" "data" data_yoda
         // { plot_options = { Name = "data"; }; })
        (set_title "Embedding" embed_yoda
         // { plot_options = { Name = "embed"; }; })
      ];
  data = mkhtml' "--errs" {} [
    (set_title "Data" data_yoda)
  ];
  embed = mkhtml' "--errs" {} [
    (set_title "Embedding" embed_yoda)
  ];
  data_qa = stack_config.mkhtml' "" { PlotSize = "14.4,3.6"; } [
    (set_title "Data" data_qa_yoda)
  ];
  data_qa_norm = stack_config.mkhtml' "" { PlotSize = "14.4,3.6"; } [
    (set_title "Data (run-by-run normalized)" data_qa_norm_yoda)
  ];
  embed_qa = stack_config.mkhtml' "" { PlotSize = "14.4,3.6"; } [
    (set_title "Embedding" embed_qa_yoda)
  ];
  embed_qa_norm = stack_config.mkhtml' "" { PlotSize = "14.4,3.6"; } [
    (set_title "Embedding (run-by-run normalized)" embed_qa_norm_yoda)
  ];
  summary_jp_compare =
    let
      trig_suffix = "_promoted";
      rename_jp = jp: input:
        let jp_s = toString jp; in
        rename
          (yoda_rebin
            (rename
              (rename
                input
                "_jp${jp_s}_reweight" "_jpx")
              "_jp${jp_s}" "_jpx")
            "/unfolding_jpx.*/detector_jet_pt_fine"
            unfold_bins)
        "_fine" "_rebinned"
        ;
    in
  mkhtml' "--errs -m '.*_jpx${trig_suffix}/detector_jet_pt_rebinned'" rec {
    LegendXPos = 0.4;
    RatioPlotYMin = 0.0;
    RatioPlot1YMin = 0.0;
    RatioPlot2YMin = 0.0;
    PlotSize = "10,10";
    CustomLegend = "\\raisebox{-0.33\\baselineskip}{$pp$ at $\\sqrt{s} = ${toString SQRT_S}\\text{ GeV}$}\\\\anti-$\\mathrm{k}_{\\text{T}}$, $R = ${jet_r}$, $|\\eta| < ${etabin_cut_2}$";
    RatioPlotDrawOnly = "/${rename_jp 0 embed_yoda}/output.yoda.gz/unfolding_jpx${trig_suffix}/detector_jet_pt_rebinned ${RatioPlotReference}";
    RatioPlotReference = "/${rename_jp 0 data_yoda}/output.yoda.gz/unfolding_jpx${trig_suffix}/detector_jet_pt_rebinned";
    RatioPlot1 = 1;
    RatioPlot1DrawOnly = "/${rename_jp 1 embed_yoda}/output.yoda.gz/unfolding_jpx${trig_suffix}/detector_jet_pt_rebinned ${RatioPlot1Reference}";
    RatioPlot1Reference = "/${rename_jp 1 data_yoda}/output.yoda.gz/unfolding_jpx${trig_suffix}/detector_jet_pt_rebinned";
    RatioPlot2 = 1;
    RatioPlot2DrawOnly = "/${rename_jp 2 embed_yoda}/output.yoda.gz/unfolding_jpx${trig_suffix}/detector_jet_pt_rebinned ${RatioPlot2Reference}";
    RatioPlot2Reference = "/${rename_jp 2 data_yoda}/output.yoda.gz/unfolding_jpx${trig_suffix}/detector_jet_pt_rebinned";
    RatioPlotYLabel = "Ratio to JP0 data";
    RatioPlot1YLabel = "Ratio to JP1 data";
    RatioPlot2YLabel = "Ratio to JP2 data";
  } [
    (set_title "JP0 data" (rename_jp 0 data_yoda)
     // { plot_options = { LineColor = "red"; }; })
    (set_title "JP0 embedding" (rename_jp 0 embed_yoda)
     // { plot_options = { LineColor = "red"; LineStyle = "dashed"; }; })
    (set_title "JP1 data" (rename_jp 1 data_yoda)
     // { plot_options = { LineColor ="blue"; }; })
    (set_title "JP1 embedding" (rename_jp 1 embed_yoda)
     // { plot_options = { LineColor = "blue"; LineStyle = "dashed"; }; })
    (set_title "JP2 data" (rename_jp 2 data_yoda)
     // { plot_options = { LineColor ="green"; }; })
    (set_title "JP2 embedding" (rename_jp 2 embed_yoda)
     // { plot_options = { LineColor = "green"; LineStyle = "dashed"; }; })
  ];

  ratio_jp_compare =
    let
      rename_jp = jp: input:
        let jp_s = toString jp; in
        rename
        #  (yoda_rebin
            (rename
              (rename
                input
                "_jp${jp_s}_reweight" "_jpx")
              "_jp${jp_s}" "_jpx")
        #    "/unfolding_jpx.*/detector_jet_pt_fine"
        #    unfold_bins)
        "_fine" "_rebinned"
        ;
    in
  mkhtml' "-s --errs --no-ratio -m '.*_jpx(_demoted|_promoted)?/detector_jet_pt'" rec {
    LegendXPos = 0.4;
    CustomLegend = "\\raisebox{-0.33\\baselineskip}{$pp$ at $\\sqrt{s} = ${toString SQRT_S}\\text{ GeV}$}\\\\anti-$\\mathrm{k}_{\\text{T}}$, $R = ${jet_r}$, $|\\eta| < ${etabin_cut_2}$";
    LogY = 0;
    YMin = 0.0;
    YMax = 1.0;
    YLabel = "Sim / Data";
  } [
    (set_title "JP0" (yoda_ratio (rename_jp 0 embed_yoda) (rename_jp 0 data_yoda))
     // { plot_options = { LineColor = "red"; }; })
    (set_title "JP1" (yoda_ratio (rename_jp 1 embed_yoda) (rename_jp 1 data_yoda))
     // { plot_options = { LineColor ="blue"; }; })
    (set_title "JP2" (yoda_ratio (rename_jp 2 embed_yoda) (rename_jp 2 data_yoda))
     // { plot_options = { LineColor ="green"; }; })
  ];

  data_trigger_compare = mkhtml' "--errs --no-ratio -m '.*_jpx/.*'" { LegendXPos = 0.2; } [
    (set_title "JP0 data" (rename data_yoda "_jp0_reweight" "_jpx"))
    (set_title "JP1 data" (rename data_yoda "_jp1_reweight" "_jpx"))
    (set_title "JP2 data" (rename data_yoda "_jp2_reweight" "_jpx"))
    (set_title "VPDMB NOBSMD data" (rename data_yoda "_vpdmb_nobsmd" "_jpx"))
    (set_title "Zerobias data" (rename data_yoda "_zerobias" "_jpx"))
  ];
  data_jp_compare = mkhtml' "--errs --no-ratio -m '.*_jpx/.*'" { LegendXPos = 0.2; } [
    (set_title "JP0 data" (rename data_yoda "_jp0_reweight" "_jpx"))
    (set_title "JP1 data" (rename data_yoda "_jp1_reweight" "_jpx"))
    (set_title "JP2 data" (rename data_yoda "_jp2_reweight" "_jpx"))
  ];
  data_jp_vertex_compare = mkhtml' "--errs -m '.*_jpx/(v|vertex)_.*'" { LegendXPos = 0.12; NormalizeToIntegral = 1; YTitle="$\\mathrm{d}P/\\mathrm{d}v_z$, 1/cm"; } [
    (set_title' "JP0 data" "JP0 data" (rename data_yoda "_jp0_reweight" "_jpx"))
    (set_title "JP1 data" (rename data_yoda "_jp1_reweight" "_jpx"))
    (set_title "JP2 data" (rename data_yoda "_jp2_reweight" "_jpx"))
  ];
  embed_trig_compare = mkhtml' "--errs -m '.*_jpx/.*'" { LegendXPos = 0.2; } [
    (set_title "Embedding JP0" (rename (rename embed_yoda "_jp0_reweight" "_jpx") "_jp0" "_jpx"))
    (set_title "Embedding JP1" (rename (rename embed_yoda "_jp1_reweight" "_jpx") "_jp1" "_jpx"))
    (set_title "Embedding JP2" (rename (rename embed_yoda "_jp2_reweight" "_jpx") "_jp2" "_jpx"))
    (set_title "Embedding MB" (rename embed_yoda "_vpdmb_nobsmd" "_jpx"))
  ];
  embed_etabin_compare = mkhtml' "--errs -m '.*_cmp/.*'" { LegendXPos = 0.2; } [
    (set_title "Embedding $0 < \\eta_{\\text{det.}} < ${etabin_cut_1}$, $0 < \\eta_{\\text{part.}} < ${etabin_cut_1}$" (rename embed_yoda "_00_${final.etabin_cut_1_nodot}_00_${final.etabin_cut_1_nodot}" "_cmp"))
    (set_title "Embedding $${etabin_cut_1} < \\eta_{\\text{det.}} < ${etabin_cut_2}$, $0 < \\eta_{\\text{part.}} < ${etabin_cut_1}$" (rename embed_yoda "_${final.etabin_cut_1_nodot}_${final.etabin_cut_2_nodot}_00_${final.etabin_cut_1_nodot}" "_cmp"))
    (set_title "Embedding $0 < \\eta_{\\text{det.}} < ${etabin_cut_1}$, $${etabin_cut_1} < \\eta_{\\text{part.}} < ${etabin_cut_2}$" (rename embed_yoda "_00_${final.etabin_cut_1_nodot}_${final.etabin_cut_1_nodot}_${final.etabin_cut_2_nodot}" "_cmp"))
    (set_title "Embedding $${etabin_cut_1} < \\eta_{\\text{det.}} < ${etabin_cut_2}$, $${etabin_cut_1} < \\eta_{\\text{part.}} < ${etabin_cut_2}$" (rename embed_yoda "_${final.etabin_cut_1_nodot}_${final.etabin_cut_2_nodot}_${final.etabin_cut_1_nodot}_${final.etabin_cut_2_nodot}" "_cmp"))
  ];
  pythia_fudge_factors_study_nodata = mkhtml' "--errs -m '/partonic_spectrum/'" { LegendXPos = 0.1; } [
    (set_title "Embedding (w/o soft reweighting, w/o bin weights)" without_soft_reweight.without_fudge_factors.embed_yoda
      // { plot_options = { LineColor = "lilac"; }; })
    (set_title "Embedding (w/o soft reweighting, w/ bin weights)" without_soft_reweight.embed_yoda)
    (set_title "Embedding (w/ soft reweighting, w/ bin weights)" embed_yoda)
  ];
  pythia_fudge_factors_study = mkhtml' "--errs -m '.*/jet_pt$'" { LegendXPos = 0.1; } ([
    (set_title "Data" data_yoda)
  ] ++ pythia_fudge_factors_study_nodata.yoda_files);
  data_vertex_fit = vertex_fit without_lumi.data_yoda;
  data_vertex_fit_plot = mkhtml' "-m '.*/vertex_z$'" { LegendXPos = 0.2; LegendYPos = 0.2; YLabel = "Number of events"; } [
    (set_title' "Data" "data" without_lumi.data_yoda)
    (set_title null (vertex_fit without_lumi.data_yoda))
  ];
  track_cut_study =
    mkhtml' "--errs" { LegendXPos = 0.2; LogZ = 1; } [
        (set_title "Data" data_yoda
         // { plot_options = { LineColor="red"; }; })
        (set_title "Data (no track cut)" no_track_cut.data_yoda
         // { plot_options = { LineColor="black!40!red"; LineStyle = "dashed"; LineDash = "2pt 2pt"; }; })
        (set_title "Embedding" embed_yoda
         // { plot_options = { LineColor="blue"; }; })
        (set_title "Embedding (no track cut)" no_track_cut.embed_yoda
         // { plot_options = { LineColor="black!40!blue"; LineStyle = "dashed"; LineDash = "2pt 2pt"; }; })
      ];
  dijet_monojet_study =
    mkhtml' "--single --no-ratio -m '.*_study$'" { LegendXPos = 0.2; LegendYPos = 0.3; } [
      (set_title "Di-jets" (rename data_yoda "_dijets" "_study")
       // { plot_options = { LineColor = "red"; }; })
      (set_title "Mono-jets" (rename data_yoda "_monojets" "_study")
       // { plot_options = { LineColor = "orange"; LineStyle = "dashed"; }; })
    ];
  vertex_reweighting_distributions =
    mkhtml' "--errs -m vertex_z" { LegendXPos = 0.05; LegendYPos = 0.3; } [
      (set_title "Raw data" reweight_none.data_yoda
       // { plot_options = { LineColor = "red"; }; })
      (set_title "Raw embedding" reweight_none.embed_yoda
       // { plot_options = { LineColor = "blue"; }; })
      (set_title "Data reweighted by TPC vertex" reweight_by_data_tpc_vertex.data_yoda
       // { plot_options = { LineColor = "green"; LineStyle = "dashed"; LineDash = "1pt 1pt"; }; })
      (set_title "Embedding reweighted by TPC vertex" reweight_by_embed_tpc_vertex.embed_yoda
       // { plot_options = { LineColor = "Purple"; LineStyle = "dashed"; LineDash = "1pt 1pt"; }; })
      (set_title "Embedding reweighted by gen. vertex" reweight_by_embed_gen_vertex.embed_yoda
       // { plot_options = { LineColor = "Orange"; LineStyle = "dashed"; LineDash = "1pt 1pt"; }; })
    ];
  vertex_reweighting_unfold =
    mkhtml' "--errs -m unfolding_result" { LegendXPos = 0.05; LegendYPos = 0.3; } (lib.flip lib.concatMap [0 1 2]
      (jp:
        let
          p = { inherit jp; bins = bins_for_jp jp; };
        in
          [
            (set_title "Reweight data by TPC vertex ($|v_z| < 60$~cm)"
              (with set_max_abs_vz 60 reweight_by_data_tpc_vertex; unfold' p embed_yoda data_yoda)
              // { plot_options = { LineColor = "green"; LineStyle = "dashed"; LineDash = "1pt 1pt"; }; })
            (set_title "Reweight data by TPC vertex ($|v_z| < 30$~cm)"
              (with set_max_abs_vz 30 reweight_by_data_tpc_vertex; unfold' p embed_yoda data_yoda)
              // { plot_options = { LineColor = "green"; }; })
            (set_title "Reweight embedding by TPC vertex ($|v_z| < 60$~cm)"
              (with (set_max_abs_vz 60 reweight_by_embed_tpc_vertex); unfold' p embed_yoda data_yoda)
              // { plot_options = { LineColor = "Purple"; LineStyle = "dashed"; LineDash = "1pt 1pt"; }; })
            (set_title "Reweight embedding TPC vertex ($|v_z| < 30$~cm)"
              (with (set_max_abs_vz 30 reweight_by_embed_tpc_vertex); unfold' p embed_yoda data_yoda)
              // { plot_options = { LineColor = "Purple"; }; })
            (set_title "Reweight embedding by gen. vertex ($|v_z| < 60$~cm)"
              (with (set_max_abs_vz 60 reweight_by_embed_gen_vertex); unfold' p embed_yoda data_yoda)
              // { plot_options = { LineColor = "Orange"; LineStyle = "dashed"; LineDash = "1pt 1pt"; }; })
            (set_title "Reweight embedding by gen. vertex ($|v_z| < 30$~cm)"
              (with (set_max_abs_vz 30 reweight_by_embed_gen_vertex); unfold' p embed_yoda data_yoda)
              // { plot_options = { LineColor = "Orange"; }; })
          ]
      ));
  unfold_test = unfold embed_yoda data_yoda;
  ue_effect = let
    mkPlotsForJP = jp: let
      p = { inherit jp; bins = bins_for_jp jp; };
    in
      [
        (set_title' "Unfold with nominal UE subtraction" "nominal UE subtraction"
          (with ue_subtraction; unfold' p embed_yoda data_yoda))
        (set_title "Unfold $86\\%$ data with $100\\%$ embedding"
          (unfold' p ue_subtraction.embed_yoda ue_subtraction086.data_yoda))
        (set_title "Unfold $100\\%$ data with $86\\%$ embedding"
          (unfold' p ue_subtraction086.embed_yoda ue_subtraction.data_yoda))
        (set_title "Unfold $86\\%$ data with $86\\%$ embedding"
          (with ue_subtraction086; unfold' p embed_yoda data_yoda))
        (set_title "Unfold $100\\%$ data with $118\\%$ embedding"
          (unfold' p ue_subtraction118.embed_yoda ue_subtraction.data_yoda))
        (set_title "Unfold $118\\%$ data with $100\\%$ embedding"
          (unfold' p ue_subtraction.embed_yoda ue_subtraction118.data_yoda))
        (set_title "Unfold $118\\%$ data with $118\\%$ embedding"
          (with ue_subtraction118; unfold' p embed_yoda data_yoda))
        (set_title "Unfold without UE subtraction"
          (unfold' p embed_yoda data_yoda))
      ];
  in
    mkhtml' "--errs" { MainPlot = 0; RatioPlotLegend = 1; RatioPlotYSize = 10; RatioPlotYMax = 2.25; RatioPlotLegendXPos = 0.2; } (lib.concatMap mkPlotsForJP [0 1 2]);
  jes_syst_from_data =
    mkhtml' "" { LegendXPos = 0.4; RatioPlotYMin = 0.6; RatioPlotYMax = 1.4; } (lib.flip lib.concatMap [0 1 2]
      (jp:
        let
          p = { inherit jp; bins = bins_for_jp jp; };
        in
          [
            (set_title "data reference"
              (unfold' p embed_yoda data_yoda) // { plot_options = { LineColor = "red"; ErrorBars = 0; }; })
            (set_title "data 4\\% post-jet track thin out"
              (unfold' p embed_yoda pct4_alt.data_yoda) // { plot_options = { LineColor = "black"; LineStyle = "dashed"; }; })
            (set_title "data 1\\% post-jet track thin out"
              (unfold' p embed_yoda pct1_alt.data_yoda) // { plot_options = { LineColor = "blue"; }; })
            (set_title "data ECAL syst. shift minus"
              (unfold' p embed_yoda ecal_shift_minus.data_yoda) // { plot_options = { LineColor = "green"; }; })
            (set_title "data ECAL syst. shift plus"
              (unfold' p embed_yoda ecal_shift_plus.data_yoda) // { plot_options = { LineColor = "green"; LineStyle = "dashed"; }; })
          ]));
  jes_syst_from_embed =
    mkhtml' "" { LegendXPos = 0.4; RatioPlotYMin = 0.6; RatioPlotYMax = 1.4; } (lib.flip lib.concatMap [0 1 2]
      (jp:
        let
          p = { inherit jp; bins = bins_for_jp jp; };
        in
          [
            (set_title "embed reference"
              (unfold' p embed_yoda embed_yoda) // { plot_options = { LineColor = "red"; ErrorBars = 0; }; })
            (set_title "embed 4\\% pre-jet track thin out"
              (unfold' p embed_yoda pct4.embed_yoda) // { plot_options = { LineColor = "gray"; }; })
            (set_title "embed 4\\% post-jet track thin out"
              (unfold' p embed_yoda pct4_alt.embed_yoda) // { plot_options = { LineColor = "black"; LineStyle = "dashed"; }; })
            (set_title "embed 1\\% post-jet track thin out"
              (unfold' p embed_yoda pct1_alt.embed_yoda) // { plot_options = { LineColor = "blue"; }; })
            (set_title "embed ECAL syst. shift minus"
              (unfold' p embed_yoda ecal_shift_minus.embed_yoda) // { plot_options = { LineColor = "green"; }; })
            (set_title "embed ECAL syst. shift plus"
              (unfold' p embed_yoda ecal_shift_plus.embed_yoda) // { plot_options = { LineColor = "green"; LineStyle = "dashed"; }; })
          ]));
  jes_charged_embed =
    mkhtml' "" { LegendXPos = 0.3; RatioPlotYMin = 0.6; RatioPlotYMax = 1.4; } (lib.flip lib.concatMap [0 1 2]
      (jp:
        let
          p = { inherit jp; bins = bins_for_jp jp; };
        in
          [
            (set_title' "Nominal JES" "nominal"
              (unfold' p embed_yoda embed_yoda) // { plot_options = { LineColor = "red"; ErrorBars = 0; }; })
            (set_title "JES with a $4\\%$ charged $p_{\\text{T}}$ reduction"
              (unfold' p embed_yoda charged4_shift.embed_yoda) // { plot_options = { LineColor = "black"; LineStyle = "dashed"; }; })
            (set_title "JES with a $4\\%$ neutral $p_{\\text{T}}$ reduction"
              (unfold' p embed_yoda neutral4_shift.embed_yoda) // { plot_options = { LineColor = "black!30!red"; LineStyle = "dotted"; LineWidth = "1.5pt"; }; })
            (set_title "JES with a $4\\%$ track thin out"
              (unfold' p embed_yoda pct4.embed_yoda) // { plot_options = { LineColor = "gray"; }; })
          ]));
  pt_ratio_vs_det_pt_rt = mkhtml' "-s --no-ratio -m '.*pt_ratio_vs_det_pt_rt$' -m '.*pt_ratio_vs_det_pt_rt_track_cut_..$'" {}
    (lib.mapAttrsToList (bin: title: set_title title (rename embed_yoda "pt_ratio_vs_det_pt_rt_${bin}" "pt_ratio_vs_det_pt_rt"))
     {
       "00_02" = "$0.0 < \\text{det }R_T < 0.2$";
       "02_04" = "$0.2 < \\text{det }R_T < 0.4$";
       "04_06" = "$0.4 < \\text{det }R_T < 0.6$";
       "06_08" = "$0.6 < \\text{det }R_T < 0.8$";
       "08_10" = "$0.8 < \\text{det }R_T < 1.0$";
     });
  rt_reweight_study = mkhtml' "--errs" {} [
    (set_title "Data" data_yoda)
    (set_title "Embedding" embed_yoda)
    (set_title "Embedding ($R_T$ reweighted)" rt_reweight.embed_yoda // { plot_options = { LineStyle="dashed"; }; })
  ];
  rt_reweight_study_unfold =
    mkhtml' "--errs" { LegendXPos = 0.4; RatioPlotYMin = 0.6; RatioPlotYMax = 1.4; } (lib.flip lib.concatMap [0 1 2]
      (jp:
        let
          p = { inherit jp; bins = bins_for_jp jp; };
        in
          [
            (set_title "Nominal"
              (unfold' p embed_yoda data_yoda) // { plot_options = { LineColor = "red"; ErrorBars = 0; }; })
            (set_title "$R_T$ reweighted"
              (unfold' p rt_reweight.embed_yoda data_yoda) // { plot_options = { LineColor = "gray"; }; })
          ]));
  vpd_like_trigger_comp = mkhtml' "--single --errs -m '.*/jet_pt_comp$'" { LegendXPos = 0.6; RatioPlotYMin = 0.0; RatioPlotYMax = 1.1; } [
    (set_title "no cut" (rename data_yoda "jet_pt" "jet_pt_comp"))
    (set_title "VPD E$||$W" (rename data_yoda "jet_pt_vpd_or" "jet_pt_comp")
     // { plot_options = { LineColor = "blue"; LineStyle = "dashed"; LineDash = "1.5pt 1.5pt"; LineWidth = "1.5pt"; }; })
    (set_title "VPD E$\\&\\&$W" (rename data_yoda "jet_pt_vpd_and" "jet_pt_comp")
     // { plot_options = { LineColor = "blue"; }; })
  ];
  data_vpd_bias = mkhtml' "--errs" {} [
    (set_title "Data" data_yoda)
    (set_title "Data ($|v_z^{vpd} - v_z| < 5$~cm)" vpdvz_vz_diff_lt_5.data_yoda)
    (set_title "Data ($|v_z^{vpd} - v_z| > 5$~cm)" vpdvz_vz_diff_gt_5.data_yoda)
    (set_title "Data ($v_z^{vpd} \\neq -999$)" vpd_vertex_found.data_yoda)
    (set_title "Data ($v_z^{vpd} = -999$)" vpd_vertex_not_found.data_yoda)
  ];

  sector20_qa =
    let
      mkplots = { options, extra_plot_opts ? {}, cut_description, scaleopt20 ? {} }: mkhtml' options ({ PlotSize = "14.4,6.0" /* cm */; LegendXPos = 0.2; } // extra_plot_opts) [
        (set_title "Embedding (all)"
          (rename embed_yoda "_vs_runindex" "_vs_runindex_embed")
          // { plot_options = { LineColor = "blue"; }; })
        (set_title "Embedding (${cut_description})"
          (rename embed_yoda "_vs_runindex_sector20" "_vs_runindex_embed")
          // { plot_options = { LineColor = "blue!40!purple!40"; LineStyle = "dashed"; } // scaleopt20; })
        (set_title "Data (all)"
          (rename data_yoda "_vs_runindex" "_vs_runindex_data")
          // { plot_options = { LineColor = "red"; }; })
        (set_title "Data (${cut_description})"
          (rename data_yoda "_vs_runindex_sector20" "_vs_runindex_data")
          // { plot_options = { LineColor = "red!40!orange!60"; LineStyle = "dashed"; } // scaleopt20; })
      ];
    in
      runCommandNoCC "sector20_qa" {
        track_mult = mkplots {
          options = "-m '.*jet_track_mult_vs_runindex_(embed|data)$'";
          cut_description = "all jets, only tracks in TPC sector 20, average multiplied by $\\times24$";
          scaleopt20 = { Scale = 24; };
        };
        track_dcad = mkplots {
          options = "--no-ratio -m '.*jet_track_dcad_vs_runindex_(embed|data)$'";
          cut_description = "all jets, only tracks in TPC sector 20";
        };
        track_pt = mkplots {
          options = "--no-ratio -m '.*jet_track_pt_vs_runindex_(embed|data)$'";
          cut_description = "all jets, only tracks in TPC sector 20";
        };
        tower_mult = mkplots {
          options = "--no-ratio -m '.*jet_(tower_mult|subtr_tower_mult)_vs_runindex_(embed|data)$'";
          cut_description = "all jets, only towers in TPC sector 20, average multiplied by $\\times24$";
          scaleopt20 = { Scale = 24; };
        };
        tower_pt = mkplots {
          options = "--no-ratio -m '.*jet_tower_pt_vs_runindex_(embed|data)$'";
          cut_description = "all jets, only towers in TPC sector 20";
        };
        jet = mkplots {
          options = "--no-ratio -m '.*jet_rt_vs_runindex_(embed|data)$'";
          extra_plot_opts = { YMax = 1; };
          cut_description = "jet thrust axis in TPC sector 20";
        };
      } ''
        mkdir "$out"
        ln -s "$track_mult" "$out/track_mult"
        ln -s "$track_dcad" "$out/track_dcad"
        ln -s "$track_pt" "$out/track_pt"
        ln -s "$tower_mult" "$out/tower_mult"
        ln -s "$tower_pt" "$out/tower_pt"
        ln -s "$jet" "$out/jet"
      '';
  sector20_cut =
    runCommandNoCC "sector20_region" {
      cut = mkhtml' "--errs -m '^/jet_quantities_(jp._reweight|vpdmb_nobsmd).*'" { LegendXPos = 0.2; } [
        (set_title "Data (all jets)"
          data_yoda
          // { plot_options = { LineColor = "red"; }; })
        (set_title "Embedding (all jets)"
          embed_yoda
          // { plot_options = { LineColor = "blue"; }; })
        (set_title "Data (remove jets with tracks in TPC sector 20)"
          sector20_track_cut.data_yoda
          // { plot_options = { LineColor = "red!60!magenta!40"; LineStyle = "dashed"; }; })
        (set_title "Embedding (remove jets with tracks in TPC sector 20)"
          sector20_track_cut.embed_yoda
          // { plot_options = { LineColor = "blue!60!green!40"; LineStyle = "dashed"; }; })
      ];
    } ''
      mkdir "$out"
      ln -s "$cut" "$out/sector20_cut"
    '';
  sector20_region = let
    mkhtml = mkhtml' "--errs -m '^/jet_quantities_(jp._reweight|vpdmb_nobsmd).*'" { LegendXPos = 0.2; LogZ = 1; };
  in
    runCommandNoCC "sector20_region" {
      data = mkhtml [
        (set_title "Data $[0,360) \\cup [491,600]$"
          exclude_region.data_yoda // { plot_options = { LineColor = "red"; }; })
        (set_title "Data $[360,491)$"
          include_region.data_yoda // { plot_options = { LineColor = "orange"; LineStyle = "dashed"; }; })
      ];
      embed = mkhtml [
        (set_title "Embedding $[0,360) \\cup [491,600]$"
          exclude_region.embed_yoda // { plot_options = { LineColor = "blue"; }; })
        (set_title "Embedding $[360,491)$"
          include_region.embed_yoda // { plot_options = { LineColor = "purple"; LineStyle = "dashed"; }; })
      ];
      cmp_exclude = mkhtml [
        (set_title "Data $[0,360) \\cup [491,600]$"
          exclude_region.data_yoda // { plot_options = { LineColor = "red"; }; })
        (set_title "Embedding $[0,360) \\cup [491,600]$"
          exclude_region.embed_yoda // { plot_options = { LineColor = "blue"; }; })
      ];
      cmp_include = mkhtml [
        (set_title "Data $[360,491)$"
          include_region.data_yoda // { plot_options = { LineColor = "orange"; LineStyle = "dashed"; }; })
        (set_title "Embedding $[360,491)$"
          include_region.embed_yoda // { plot_options = { LineColor = "purple"; LineStyle = "dashed"; }; })
      ];
    } ''
      mkdir "$out"
      ln -s "$data" "$out/data"
      ln -s "$embed" "$out/embed"
      ln -s "$cmp_exclude" "$out/cmp_exclude"
      ln -s "$cmp_include" "$out/cmp_include"
    '';
  region_unfold = let
    mkplots = jp: let
      unfold = unfold' { inherit jp; bins = bins_for_jp jp; };
      rm_b = input: rename input "/b" "/_b";
      rm_x = input: rename input "/x" "/_x";
    in [
      (set_title "Data $[0,360) \\cup [491,600]$ unfolded with full $[0,600]$ embed."
        (rm_b (unfold embed_yoda exclude_region.data_yoda)) // { plot_options = { LineColor = "red"; }; })
      (set_title "Data $[0,360) \\cup [491,600]$"
        (rm_x (unfold embed_yoda exclude_region.data_yoda)) // { plot_options = { LineColor = "red"; }; })

      (set_title "Data $[0,340) \\cup [491,600]$ unfolded with full $[0,600]$ embed."
        (rm_b (unfold embed_yoda exclude_region340.data_yoda)) // { plot_options = { LineColor = "black"; LineStyle = "dotted"; }; })
      (set_title "Data $[0,340) \\cup [491,600]$"
        (rm_x (unfold embed_yoda exclude_region340.data_yoda)) // { plot_options = { LineColor = "black"; LineStyle = "dotted"; }; })

      (set_title "Data $[0,360) \\cup [491,600]$ unf. with $[0,360) \\cup [491,600]$ embed."
        (rm_b (unfold exclude_region.embed_yoda exclude_region.data_yoda)) // { plot_options = { LineColor = "orange"; LineStyle = "dashed"; }; })

      (set_title "Data $[360,491)$ unfolded with full $[0,600]$ embed."
        (rm_b (unfold embed_yoda include_region.data_yoda)) // { plot_options = { LineColor = "Green"; }; })
      (set_title "Data $[360,491)$"
        (rm_x (unfold embed_yoda include_region.data_yoda)) // { plot_options = { LineColor = "Green"; }; })

      (set_title "Data $[360,491)$ unfolded with $[360,491)$ embed."
        (rm_b (unfold exclude_region.embed_yoda include_region.data_yoda)) // { plot_options = { LineColor = "green"; LineStyle = "dashed"; }; })
    ];
  in
    mkhtml' "--errs -M '/_'" { LegendXPos = 0.05; PlotSize = "12,10"; XMin = 5.0; XMax = 45.0; } (mkplots 0 ++ mkplots 1 ++ mkplots 2);

  embed_pdf_reweight = let
    mkplot = extra_opts: mkhtml' "--errs" ({ LegendXPos = 0.12; } // extra_opts) [
      (set_title "Embedding" embed_yoda)
      (set_title "Embedding (reweight to CT14lo)" (pdf_reweight "CT14lo").embed_yoda)
      (set_title "Embedding (reweight to MMHT2014lo68cl)" (pdf_reweight "MMHT2014lo68cl").embed_yoda)
      (set_title "Embedding (reweight to HERAPDF20\\_LO\\_EIG)" (pdf_reweight "HERAPDF20_LO_EIG").embed_yoda)
      (set_title "Embedding (reweight to NNPDF30\\_lo\\_as\\_0118)" (pdf_reweight "NNPDF30_lo_as_0118").embed_yoda)
    ];
  in
  {
    normal_ratio = mkplot {};
    fine_ratio = mkplot { RatioPlotYMin = 0.9; RatioPlotYMax = 1.1; };
  };
  unfold_pdf_reweight =
    mkhtml' "--errs" { LegendXPos = 0.4; RatioPlotYMin = 0.6; RatioPlotYMax = 1.4; } (lib.flip lib.concatMap [0 1 2]
      (jp:
        let
          p = { inherit jp; bins = bins_for_jp jp; };
        in
          [
            (set_title "Variance around nominal unfolding" (unfold' p embed_yoda data_yoda))
            (set_title "Unfolding using CT14lo" (unfold' p (pdf_reweight "CT14lo").embed_yoda data_yoda))
            (set_title "Unfolding using MMHT2014lo68cl" (unfold' p (pdf_reweight "MMHT2014lo68cl").embed_yoda data_yoda))
            (set_title "Unfolding using HERAPDF20\\_LO\\_EIG" (unfold' p (pdf_reweight "HERAPDF20_LO_EIG").embed_yoda data_yoda))
            (set_title "Unfolding using NNPDF30\\_lo\\_as\\_0118" (unfold' p (pdf_reweight "NNPDF30_lo_as_0118").embed_yoda data_yoda))
          ]));

  two_vtx_study = mkhtml' "--errs" {} [
    (set_title "Data" data_yoda)
    (set_title "Embedding" embed_yoda)
    (set_title "Data" two_vtx.data_yoda
     // { plot_options = { LineColor = "red"; LineStyle = "dashed"; }; })
    (set_title "Embedding" two_vtx.embed_yoda
     // { plot_options = { LineColor = "blue"; LineStyle = "dashed"; }; })
  ];
  two_vtx_study_unfold = let
    f = jp:
      let
        unfold = unfold' { inherit jp; bins = bins_for_jp jp; };
      in
        [
          (set_title "unfold single with single" (unfold embed_yoda data_yoda))
          (set_title "unfold two with two" (unfold two_vtx.embed_yoda two_vtx.data_yoda))
          (set_title "unfold two with single" (unfold embed_yoda two_vtx.data_yoda))
          (set_title "unfold single with two" (unfold two_vtx.embed_yoda data_yoda))
        ];
  in
    mkhtml' "--errs" {} (lib.concatMap f [0 1]);

  ue_vs_jet_pt = mkhtml' "-s --errs -m '.*_cmp$'" { NormalizeToIntegral = 1; Rebin = 2; } [
    (set_title "Data" (rename data_yoda "_05_10" "_cmp"))
    (set_title "Data" (rename data_yoda "_10_15" "_cmp"))
    (set_title "Data" (rename data_yoda "_15_20" "_cmp"))
    (set_title "Data" (rename data_yoda "_20_30" "_cmp"))
    (set_title "Data" (rename data_yoda "_30_40" "_cmp"))
    (set_title "Embedding" (rename embed_yoda "_05_10" "_cmp"))
    (set_title "Embedding" (rename embed_yoda "_10_15" "_cmp"))
    (set_title "Embedding" (rename embed_yoda "_15_20" "_cmp"))
    (set_title "Embedding" (rename embed_yoda "_20_30" "_cmp"))
    (set_title "Embedding" (rename embed_yoda "_30_40" "_cmp"))
  ];

  ue_density_cut = lib.recurseIntoAttrs {

    data = mkhtml' "--errs -m '^/jet_quantities.*'" { LegendXPos = 0.2; LogZ = 1; } [
      (set_title "Data (all jets)"
        data_yoda
        // { plot_options = { LineColor = "red"; }; })
      (set_title "Data (jets with UE density $>1$ MeV/[$\\varphi\\eta$])"
        (min_ue_density_cut 0.001 /* GeV */).data_yoda
        // { plot_options = { LineColor = "red!60!black"; LineStyle = "dashed"; }; })
      (set_title "Data (jets with UE density $>0.5$ GeV/[$\\varphi\\eta$])"
        (min_ue_density_cut 0.5 /* GeV */).data_yoda
        // { plot_options = { LineColor = "red!30!black"; LineStyle = "dotted"; LineWidth = "1.2pt"; }; })
      (set_title "Data (jets with UE density $>2.0$ GeV/[$\\varphi\\eta$])"
        (min_ue_density_cut 2.0 /* GeV */).data_yoda
        // { plot_options = { LineColor = "black"; LineStyle = "dashed"; LineDash = "0.3pt 1.2pt"; LineWidth = "1.5pt"; }; })
    ];

    embed = mkhtml' "--errs -m '^/jet_quantities.*'" { LegendXPos = 0.2; LogZ = 1; } [
      (set_title "Embedding (all jets)"
        embed_yoda
        // { plot_options = { LineColor = "blue"; }; })
      (set_title "Embedding (jets with UE density $>1$ MeV/[$\\varphi\\eta$])"
        (min_ue_density_cut 0.001 /* GeV */).embed_yoda
        // { plot_options = { LineColor = "blue!60!black"; LineStyle = "dashed"; }; })
      (set_title "Embedding (jets with UE density $>0.5$ GeV/[$\\varphi\\eta$])"
        (min_ue_density_cut 0.5 /* GeV */).embed_yoda
        // { plot_options = { LineColor = "blue!30!black"; LineStyle = "dotted"; LineWidth = "1.2pt"; }; })
      (set_title "Embedding (jets with UE density $>2.0$ GeV/[$\\varphi\\eta$])"
        (min_ue_density_cut 2.0 /* GeV */).embed_yoda
        // { plot_options = { LineColor = "black"; LineStyle = "dashed"; LineDash = "0.3pt 1.2pt"; LineWidth = "1.5pt"; }; })
    ];

  };

  trig_reweighting_cross_check = lib.recurseIntoAttrs {
    standard_trig =
      mkhtml' "--single --errs -m '^/jet_quantities_jp.*/jet_pt$'" { LegendXPos = 0.2; YLabel = "Number of events"; } [
        (set_title "VPDMB fired $\\land$ JP fired $\\land$ JP should fire"
          without_lumi.require_vpdmb.data_yoda)
        (set_title "VPDMB fired $\\land$ JP fired"
          without_lumi.require_vpdmb.trig_pretend_should_fire.data_yoda)
        (set_title "VPDMB fired $\\land$ JP should fire"
          without_lumi.require_vpdmb.trig_pretend_did_fire.data_yoda)
      ];

    data = lib.recurseIntoAttrs (lib.genAttrs [ "demoted" "promoted" ] (attr:
      mkhtml' "--single --errs -m '^/unfolding_jp._${attr}/detector_jet_pt'" { LegendXPos = 0.2; YLabel = "Number of events"; } [
        (set_title "VPDMB Data ${attr} JP trigger"
          without_lumi.require_vpdmb.data_yoda)
        (set_title "VPDMB Data reweighted JP trigger"
          without_lumi.require_vpdmb.trig_fire_pretend_embed.force_trig_reweighting.data_yoda)
        (set_title "VPDMB Data normal JP trigger"
          without_lumi.require_vpdmb.trig_fire_pretend_embed.data_yoda)
      ]));

    embed = lib.recurseIntoAttrs (lib.genAttrs [ "demoted" "promoted" ] (attr:
      mkhtml' "--single --errs -m '^/unfolding_jp._${attr}/detector_jet_pt'" { LegendXPos = 0.2; } [
        (set_title "Data ${attr} JP trigger" data_yoda)
        (set_title "Embedding reweighted JP trigger" embed_yoda)
      ]));
  };

  fig_4 = mkhtml' "--single --no-ratio --errs -m '.*_jpx.*_cmp$'" { /*NormalizeToIntegral = 1;*/ YMax = "5.0e4"; LegendXPos = 0.02; /*Rebin = 2;*/ } [
    (set_title "$6.9 < \\text{particle }p_{\\text{T}}\\text{${ptjet_ue_corr_label}} < 8.2$ GeV/c $\\times 1$"
      (rename (rename embed_yoda "_69_82" "_cmp") "_jp0" "_jpx"))
    (set_title "$19.0 < \\text{particle }p_{\\text{T}}\\text{${ptjet_ue_corr_label}} < 22.5$ GeV/c $\\times 10$"
      (scale "/.* 10x" (rename (rename embed_yoda "_190_225" "_cmp") "_jp1" "_jpx")))
    (set_title "$44.0 < \\text{particle }p_{\\text{T}}\\text{${ptjet_ue_corr_label}} < 52.0$ GeV/c $\\times 500$"
      (scale "/.* 500x" (rename (rename embed_yoda "_440_520" "_cmp") "_jp2" "_jpx")))
  ];

  default_num_mc_jobs = 100;

  xsec_plots = lib.recurseIntoAttrs (final.override (final: prev: with final; {
    xsec_plots = null; # avoid an infinite recursion

    final_mode = false;

    # Theory
    overlay = final: prev: {
      rivet_analyses = [
        "MC_STAR_JETS:ABS_ETA_BINS=0.0,0.4,0.8:JET_R=0.5"
        "MC_STAR_JETS:ABS_ETA_BINS=0.0,0.4,0.8:JET_R=0.6"
        "MC_STAR_JETS:ABS_ETA_BINS=0.0,0.5,0.9:JET_R=0.5"
        "MC_STAR_JETS:ABS_ETA_BINS=0.0,0.5,0.9:JET_R=0.6"
        "STAR_2017_I1493842"
      ];
      inherit nobatch SQRT_S;
    };
    nlojetpp = (import MC/nlojet++/bits.nix { inherit nixpkgs; }).override overlay;
    fastnlo = (import MC/fastnlo/default.nix { inherit nixpkgs; }).override overlay;
    nlo = { pdf_set ? "CT14nlo", contributions ? "full", scale_var ? "maxpt", ... }@args:
      let
        order = {
          "full" = "NLO";
          "born" = "LO";
        }.${contributions};
        scale = {
          "maxpt" = "p_{\\text{T}}^{\\text{max}}";
          "mjj" = "m_{jj}";
          "ht" = "H_{\\text{T}}";
        }.${scale_var};
      in
        set_title "${order} pQCD \\raisebox{0.1\\baselineskip}{$\\otimes$} ${pdf_set} ($\\mu = ${scale}$)"
          (nlojetpp.runMC (if final_mode then 1000 else 10) ({ inherit pdf_set contributions scale_var; } // args));
    scale_uncertainty =
      nlo: yoda_envelope (nlo {}) (lib.optionals final_mode [
        (nlo { xren = 2.0; xfac = 1.0; })
        (nlo { xren = 0.5; xfac = 1.0; })
        (nlo { xren = 1.0; xfac = 2.0; })
        (nlo { xren = 1.0; xfac = 0.5; })
        (nlo { xren = 2.0; xfac = 2.0; })
        (nlo { xren = 0.5; xfac = 0.5; })
      ]) // { inherit (nlo {}) title; };
    py6 = (import MC/pythia6/bits.nix { inherit nixpkgs; }).override overlay;
    py8 = (import MC/pythia8/bits.nix { inherit nixpkgs; }).override overlay;
    pythia = lib.makeOverridable (py6.runMC_splitbin default_num_mc_jobs) { seed = 1337; };
    pythia_perugia0 = py6.runMC_splitbin default_num_mc_jobs { tune = py6.PERUGIA_0; };
    pythia_no_had = py6.runMC_splitbin default_num_mc_jobs { enable_had = false; };
    pythia_no_had_no_mpi = py6.runMC_splitbin default_num_mc_jobs { enable_had = false; enable_mi = false; };
    pythia_tune."370" = pythia.override { tune = 370; };
    pythia_tune."374" = pythia.override { tune = 374; };
    pythia_tune."375" = pythia.override { tune = 375; };
    pythia_tune."376" = pythia.override { tune = 376; };
    pythia_tune."377" = pythia.override { tune = 377; };
    pythia_tune."383" = pythia.override { tune = 383; };
    pythia8 = py8.runMC_splitbin default_num_mc_jobs { tune = "Detroit"; };

    rename_mc = input: ue_corr:
      let
        histo_name =
          if ue_corr then
            "jet_pt_ue_corr_fine"
          else
            "jet_pt_fine";
        in_histo_path =
          "/MC_STAR_JETS:ABS_ETA_BINS=0.0,${final.etabin_cut_1},${final.etabin_cut_2}:JET_R=${final.jet_r}/${histo_name}";
        out_histo_path = "/result/jet_pt";
      in
        rename input in_histo_path out_histo_path;

    mc2unfold = input: ue_corr:
        yoda_rebin
          (rename_mc input ue_corr)
          "/result/jet_pt"
          unfold_bins
          ;

    nlo_curve = args:
      rename
        (rename_mc (fastnlo.convolutions args) false)
        "jet_pt_fine"
        "ignore"
        ;

    # Data
    unfold_result = { jp, response, measurement, err_type ? "b_uncorr", bins ? bins_for_jp jp, use_eta_bins ? true }:
      let
        etabin_cut_1 = builtins.fromJSON final.etabin_cut_1;
        etabin_cut_2 = builtins.fromJSON final.etabin_cut_2;
      in
      assert builtins.elem err_type [ "embed" "b_uncorr" ];
      scale [
        "/result/jet_pt$ ${toString (1.0 / etabin_cut_2 / 2)}x"
        "/result/jet_pt_00_${final.etabin_cut_1_nodot}$ ${toString (1.0 / etabin_cut_1 / 2)}x"
        "/result/jet_pt_${final.etabin_cut_1_nodot}_${final.etabin_cut_2_nodot}$ ${toString (1.0 / (etabin_cut_2 - etabin_cut_1) / 2)}x"
      ]
      (rename
        (unfold' { inherit jp bins use_eta_bins; } response measurement)
        "/unfolding_result_jp${toString jp}/x_${err_type}"
        "/result/jet_pt")
        ;

    # Non-perturbative corrections
    f_had =
      yoda_envelope
        (yoda_ratio (mc2unfold pythia true) (mc2unfold (pythia.override { enable_had = false; }) true))
        (map (tune:
          (yoda_ratio (mc2unfold pythia_tune.${tune} true) (mc2unfold (pythia_tune.${tune}.override { enable_had = false; }) true))
        ) [ "374" "375" "376" "377" "383" ])
        ;
    apply_f_had = input: set_title (input.title + " $\\times$ $f_{\\text{had.}}$") (yoda_multiply input f_had);
    f_ue_had = yoda_ratio (mc2unfold pythia false) (mc2unfold pythia_no_had_no_mpi false);
    apply_f_ue_had = input: set_title (input.title + " $\\times$ $f_{\\text{UE+had.}}$") (yoda_multiply input f_ue_had);
    f_np =
      if process_overrides.ue_shift_fraction == 1.0 then
        # data-driven UE correction already applied to the data
        f_had
      else if process_overrides.ue_shift_fraction == 0.0 then
        f_ue_had
      else
        throw "Unexpected value ue_shift_fraction = ${toString ue_shift_fraction}";
    apply_f_np =
      if process_overrides.ue_shift_fraction == 1.0 then
        # data-driven UE correction already applied to the data
        apply_f_had
      else if process_overrides.ue_shift_fraction == 0.0 then
        apply_f_ue_had
      else
        throw "Unexpected value ue_shift_fraction = ${toString ue_shift_fraction}";

    # Jet Patches
    jps = [
      0 1 2
      "0_promoted" "1_promoted" "2_promoted"
      "0_demoted" "1_demoted" "2_demoted"
      "promoted_combined" "demoted_combined"
    ];
    jps_legacy_combine = [ 0 2 ];
    jps_s = builtins.concatStringsSep "" (map toString jps_legacy_combine);

    f_had_plot = mkhtml' "--errs -s -m '/result/f_had_vs_jet_pt'" {
      RatioPlot = 0;
      Title = "Pythia ${py6.version} @ Perugia 2012, \\texttt{PARP(90)}$=0.213$";
    } [
      (set_title "$\\eta \\in [0, ${final.etabin_cut_2}]$"
        (rename f_had "/jet_pt" "/f_had_vs_jet_pt")
        // { plot_options = { ErrorBars = 0; LineColor = "red"; ErrorBands = 1; ErrorBandColor = "red"; ErrorBandOpacity = 0.2; }; })
      (set_title "$\\eta \\in [0, ${final.etabin_cut_1}]$"
        (rename f_had "/jet_pt_00_${final.etabin_cut_1_nodot}" "/f_had_vs_jet_pt_same")
        // { plot_options = { ErrorBars = 0; LineColor = "red"; ErrorBands = 1; ErrorBandColor = "red"; ErrorBandOpacity = 0.2; }; })
      (set_title "$\\eta \\in [${final.etabin_cut_1}, ${final.etabin_cut_2}]$"
        (rename f_had "/jet_pt_${final.etabin_cut_1_nodot}_${final.etabin_cut_2_nodot}" "/f_had_vs_jet_pt_same")
        // { plot_options = { ErrorBars = 0; LineColor = "blue"; ErrorBands = 1; ErrorBandColor = "blue"; ErrorBandOpacity = 0.2; }; })
    ];
    f_ue_had_plot = mkhtml' "-s -m '/result/jet_pt' -m '/result/f_ue_had_vs_jet_pt'" {} [
      (set_title "Pythia ${py6.version} @ Perugia 2012, \\texttt{PARP(90)}$=0.213$" (rename f_ue_had "/jet_pt" "/f_ue_had_vs_jet_pt"))
    ];

    stat_uncertainty = jp:
      unfold_result { inherit jp; response = embed_yoda; measurement = data_yoda; err_type = "b_uncorr"; };
    emc_uncertainty = jp:
      yoda_envelope
          (unfold_result { inherit jp; response = embed_yoda; measurement = data_yoda; })
        [
          (unfold_result { inherit jp; response = jp_threshold_adjust_minus_one.ecal_shift_minus.embed_yoda; measurement = data_yoda; })
          (unfold_result { inherit jp; response = jp_threshold_adjust_plus_one.ecal_shift_plus.embed_yoda; measurement = data_yoda; })
          (unfold_result { inherit jp; response = ecal_shift_minus.embed_yoda; measurement = data_yoda; })
          (unfold_result { inherit jp; response = ecal_shift_plus.embed_yoda; measurement = data_yoda; })
          (unfold_result { inherit jp; response = jp_threshold_adjust_minus_one.embed_yoda; measurement = data_yoda; })
          (unfold_result { inherit jp; response = jp_threshold_adjust_plus_one.embed_yoda; measurement = data_yoda; })
        ];
    trk_eff_uncertainty = jp:
      yoda_envelope
          (unfold_result { inherit jp; response = embed_yoda; measurement = data_yoda; })
        [
          (unfold_result { inherit jp; response = embed_yoda; measurement = pct1_alt.data_yoda; })
        ];
    simu_stat_uncertainty = jp:
      unfold_result { inherit jp; response = embed_yoda; measurement = data_yoda; err_type = "embed"; };
    ue_uncertainty = jp:
      yoda_envelope
        (unfold_result { inherit jp; response = embed_yoda; measurement = data_yoda; })
        [
          (unfold_result { inherit jp; response = ue_subtraction.embed_yoda; measurement = ue_subtraction.data_yoda; })
          (unfold_result { inherit jp; response = ue_subtraction.embed_yoda; measurement = ue_subtraction086.data_yoda; })
          (unfold_result { inherit jp; response = ue_subtraction086.embed_yoda; measurement = ue_subtraction.data_yoda; })
          (unfold_result { inherit jp; response = ue_subtraction086.embed_yoda; measurement = ue_subtraction086.data_yoda; })
          (unfold_result { inherit jp; response = ue_subtraction118.embed_yoda; measurement = ue_subtraction.data_yoda; })
          (unfold_result { inherit jp; response = ue_subtraction.embed_yoda; measurement = ue_subtraction118.data_yoda; })
          (unfold_result { inherit jp; response = ue_subtraction118.embed_yoda; measurement = ue_subtraction118.data_yoda; })
        ];
    syst_uncertainty = jp:
      yoda_combine_uncertainties [ (emc_uncertainty jp) (trk_eff_uncertainty jp) (simu_stat_uncertainty jp) (ue_uncertainty jp) ];

    reference_curves = [
      # Theory
      (set_short_title "NLO$\\times$NP"
        (apply_f_np
          (nlo_curve {})
          // { plot_options = { ErrorBars = 0; LineColor = "Blue"; ErrorBands = 1; ErrorBandColor = "Blue"; ErrorBandOpacity = 0.2; }; }))
      (set_title "Pythia ${py6.version} \\raisebox{0.1\\baselineskip}{\\scalebox{0.8}{@}} Perugia 2012, \\texttt{PARP(90)}$=0.213$"
        (mc2unfold pythia true)
        // { plot_options = { ErrorBars = 0; LineColor = "green"; }; })
    ];
    additional_curves = [];

    jp_title = jp: {
      "promoted_combined" = "STAR Run 2012";
    }.${toString jp} or "JP${builtins.replaceStrings [ "_" ] [ "\\_" ] (toString jp)} data";

    override_opts = { YMax = "3.0e10"; };
    curves_for_jp = jp: reference_curves ++ [
      (set_title' "${jp_title jp} ${ptjet_ue_corr_title} (stat.\\,uncertainty)" "data"
        (stat_uncertainty jp)
        // { plot_options = { ConnectBins = 0; LineColor = "black"; }; })
      (set_title "Syst. uncertainty"
        (syst_uncertainty jp)
        // { plot_options = { ConnectBins = 0; LineStyle = "none"; ErrorBars = 0; ErrorBands = 1; ErrorBandColor = "black"; ErrorBandOpacity = 0.3; }; })
    ] ++ additional_curves;
    single_trigger = lib.recurseIntoAttrs (builtins.listToAttrs (map (jp: {
      name = "jp${toString jp}";
      value = mkhtml' "-s --errs -m '/result/'" ({} // override_opts) (curves_for_jp jp);
    }) jps));

    compare_nlo_pdfs = final.override (final: prev: {
      nlo_colors = {
        "CT18NLO" = "blue";
        "MSHT20nlo_as120" = "red";
        "NNPDF40_nlo_as_01180" = "green";
        "HERAPDF20_NLO_EIG" = "orange";
      };

      reference_curves = [];
      additional_curves =
        lib.mapAttrsToList
          (pdf_set: color:
            (apply_f_np
              (nlo_curve { inherit pdf_set; })
              // { plot_options = { ErrorBars = 0; LineColor = color; LineStyle = "solid"; ErrorBands = 1; ErrorBandColor = color; ErrorBandOpacity = 0.2; }; }))
          final.nlo_colors;

      override_opts = { YMax = "3.0e12"; };
    });

    compare_mc = final.override (final: prev: {
      reference_curves = [];
      additional_curves = [
        (set_title "Pythia ${py6.version} \\raisebox{0.1\\baselineskip}{\\scalebox{0.8}{@}} Perugia 2012, \\texttt{PARP(90)}$=0.213$"
          (mc2unfold pythia true)
          // { plot_options = { ErrorBars = 0; LineColor = "green"; LineStyle = "solid"; }; })
        (set_title "Pythia ${py8.version} \\raisebox{0.1\\baselineskip}{\\scalebox{0.8}{@}} Detroit"
          (mc2unfold pythia8 true)
          // { plot_options = { ErrorBars = 0; LineColor = "Orange"; LineStyle = "solid"; }; })
      ];
    });

    compare_tunes = final.override (final: prev: {
      reference_curves = [];
      additional_curves = map (tune:
        (set_title "Pythia ${py6.version} \\raisebox{0.1\\baselineskip}{\\scalebox{0.8}{@}} Perugia 2012 (${tune}), \\texttt{PARP(90)}$=0.213$"
          (mc2unfold pythia_tune.${tune} true)
          // { plot_options = { ErrorBars = 0; LineStyle = "solid"; }; })
        ) ["370" "374" "375" "376" "377" "383"];
    });

    compare_nnlo = final.override (final: prev: {
      uncertainty = "L6";

      reference_curves = [];
      additional_curves = [
        (set_short_title "NLO"
          (add_title_suffix " ($\\mu = \\hat{H}_{\\text{T}}$)"
            (apply_f_np
              (nlo_curve { variant = "NNLOJet"; order = "NLO"; pdf_set = "CT18NLO"; inherit (final) uncertainty; })))
          // { plot_options = { ErrorBars = 0; LineColor = "orange"; LineStyle = "solid"; ErrorBands = 1; ErrorBandColor = "orange"; ErrorBandOpacity = 0.2; }; })
        (set_short_title "NNLO"
          (add_title_suffix " ($\\mu = \\hat{H}_{\\text{T}}$)"
            (apply_f_np
              (nlo_curve { variant = "NNLOJet"; order = "NNLO"; pdf_set = "CT18NNLO"; inherit (final) uncertainty; })))
          // { plot_options = { ErrorBars = 0; LineColor = "red"; LineStyle = "dashed"; ErrorBands = 1; ErrorBandColor = "red"; ErrorBandOpacity = 0.2; }; })
      ];
    });

    compare_nnlo_no_data = final.override (final: prev: {
      uncertainty_L6 = final.override (final: prev: {
        extra_custom_legend = ''\\\raisebox{-0.66\baselineskip}{Uncertainty from PDF}'';
        uncertainty = "L6";
      });
      uncertainty_6P = final.override (final: prev: {
        extra_custom_legend = ''\\\raisebox{-0.66\baselineskip}{Uncertainty from 6-point scale variations}'';
        uncertainty = "6P";
      });

      curves_for_jp = _: [
        (set_short_title "NLOJet++"
          (add_title_suffix " NLOJet++"
            (mc2unfold (nlo { pdf_set = "CT18NLO"; scale_var = "ht"; }) false)
            // { plot_options = { ErrorBars = 0; LineColor = "black"; LineStyle = "solid"; ErrorBands = 0; }; }))
        (set_short_title "NLO"
          (add_title_suffix "/FastNLO ($\\mu = p_{\\text{T};\\text{jet }i}$)"
            (nlo_curve { variant = "NLOJet++"; order = "NLO"; pdf_set = "CT18NLO"; inherit (final) uncertainty; }))
          // { plot_options = { ErrorBars = 0; LineColor = "Blue"; LineStyle = "solid"; ErrorBands = 1; ErrorBandColor = "Blue"; ErrorBandOpacity = 0.2; }; })
        (set_short_title "NLO"
          (add_title_suffix " ($\\mu = \\hat{H}_{\\text{T}}$)"
            (nlo_curve { variant = "NNLOJet"; order = "NLO"; pdf_set = "CT18NLO"; inherit (final) uncertainty; }))
          // { plot_options = { ErrorBars = 0; LineColor = "orange"; LineStyle = "solid"; ErrorBands = 1; ErrorBandColor = "orange"; ErrorBandOpacity = 0.2; }; })
        (set_short_title "NNLO"
          (add_title_suffix " ($\\mu = \\hat{H}_{\\text{T}}$)"
            (nlo_curve { variant = "NNLOJet"; order = "NNLO"; pdf_set = "CT18NLO"; inherit (final) uncertainty; }))
          // { plot_options = { ErrorBars = 0; LineColor = "green"; LineStyle = "dotted"; ErrorBands = 1; ErrorBandColor = "green"; ErrorBandOpacity = 0.2; }; })
        (set_short_title "NNLO"
          (add_title_suffix " ($\\mu = \\hat{H}_{\\text{T}}$)"
            (nlo_curve { variant = "NNLOJet"; order = "NNLO"; pdf_set = "CT18NNLO"; inherit (final) uncertainty; }))
          // { plot_options = { ErrorBars = 0; LineColor = "red"; LineStyle = "dashed"; ErrorBands = 1; ErrorBandColor = "red"; ErrorBandOpacity = 0.2; }; })
      ];
    });

    rename_bins = x: [
      (rename x "_00_${final.etabin_cut_1_nodot}" "_same" // { plot_options = x.plot_options or {}; })
      (set_title "" (scale "/.* 0.1x" (rename x "_${final.etabin_cut_1_nodot}_${final.etabin_cut_2_nodot}" "_same") // { plot_options = x.plot_options or {}; }))
    ];
    renamed_curves = jp: builtins.concatLists (map rename_bins (curves_for_jp jp));
    money_plot_trigger = "promoted_combined";
    money_plot = let
      yoda_files = renamed_curves money_plot_trigger;
      reference_short_title = (builtins.elemAt yoda_files 0).short_title
        or (throw "money_plot: ratio to ${(builtins.elemAt yoda_files 0).title} missing short_title");
    in (mkhtml' "-s --errs -m '/result/jet_pt_same'" ({
      RatioPlotYLabel = "\\shortstack[c]{Ratio to ${reference_short_title}\\\\for $\\eta \\in [0, ${etabin_cut_1}]$}";
      RatioPlot1 = 1;
      RatioPlot1Reference = "/${builtins.elemAt yoda_files 1}/output.yoda.gz/result/jet_pt_same";
      RatioPlot1YLabel = "\\shortstack[c]{Ratio to ${reference_short_title}\\\\for $\\eta \\in [${etabin_cut_1}, ${etabin_cut_2}]$}";
    } // override_opts) yoda_files).overrideAttrs (prev: {
      buildPhase = prev.buildPhase + ''
        gunzip out/result/jet_pt_same.dat.gz
        cat >>out/result/jet_pt_same.dat <<EOF
        # BEGIN SPECIAL /${builtins.elemAt yoda_files 1}/output.yoda.gz/jet_pt_same
        \psclip{\psframe[linewidth=0, linestyle=none](0,0)(1,1)}
        \uput{4pt}[180]{0}\physicscoor(36,15000){\\shortstack[l]{$\\times 1$\\\\$\\eta \\in [0, ${etabin_cut_1}]$}}
        \uput{4pt}[180]{0}\physicscoor(25,50){\\shortstack[l]{$\\times 0.1$\\\\$\\eta \\in [${etabin_cut_1}, ${etabin_cut_2}]$}}
        \endpsclip
        # END SPECIAL
        EOF
        (
        cd out/result
        make-plots --format pdf jet_pt_same.dat
        make-plots --format png jet_pt_same.dat
        )
        gzip out/result/jet_pt_same.dat
      '';
    });

    figures_for_paper = let
      pdf_sets = [
        "CT18NNLO"
        "MSHT20nlo_as120"
        "NNPDF40_nlo_as_01180"
        "HERAPDF20_NLO_EIG"
      ];
    in runCommandNoCC "figures_for_paper" ({
      inherit (without_lumi) summary_jp_compare;
      inherit fig_4;
      inherit f_had_plot;
      inherit combined_latex_table;
      money_plot_compare_mc = compare_mc.money_plot;
      money_plot_compare_nlo_pdfs = compare_nlo_pdfs.money_plot;
      PP200_f_had = f_had;
      PP510_f_had = pp510.f_had;
    } // lib.attrsets.mergeAttrsList (
      map (pdf_set: {
        "PP200_NNLO_${pdf_set}" =
          apply_f_np
            (nlo_curve { variant = "NNLOJet"; order = "NNLO"; inherit pdf_set; uncertainty = "L6"; });
        "PP510_NNLO_${pdf_set}" =
          with pp510;
          apply_f_np
            (nlo_curve { variant = "NNLOJet"; order = "NNLO"; inherit pdf_set; uncertainty = "L6"; });
      })
      pdf_sets
    )) (''
      mkdir "$out"
      for ext in pdf dat.gz; do
        ln -s "$summary_jp_compare/unfolding_jpx_promoted/detector_jet_pt_rebinned.$ext" "$out/fig_1.$ext"
        ln -s "$fig_4/unfolding_jpx_promoted/det_over_part_pt_for_part_pt_cmp.$ext" "$out/fig_4a.$ext"
        ln -s "$f_had_plot/result/f_had_vs_jet_pt_same.$ext" "$out/fig_5a.$ext"
        ln -s "$money_plot_compare_mc/result/jet_pt_same.$ext" "$out/fig_6.$ext"
        ln -s "$money_plot_compare_nlo_pdfs/result/jet_pt_same.$ext" "$out/fig_9.$ext"
      done
      ln -s "$combined_latex_table" "$out/table_1.tex"
      ln -s "$PP200_f_had/output.yoda.gz" "$out/PP200_f_had.yoda.gz"
      ln -s "$PP510_f_had/output.yoda.gz" "$out/PP510_f_had.yoda.gz"
    '' + lib.strings.concatMapStrings (pdf_set: ''
      ln -s "$PP200_NNLO_${pdf_set}/output.yoda.gz" "$out/PP200_NNLO_${pdf_set}.yoda.gz"
      ln -s "$PP510_NNLO_${pdf_set}/output.yoda.gz" "$out/PP510_NNLO_${pdf_set}.yoda.gz"
    '') pdf_sets);

    figures_final = let
      star-run12-incjet-paper = import <star-run12-incjet-paper>;
      _nixpkgs = import <nixpkgs> {
        overlays = nixpkgs.overlays ++ [
          star-run12-incjet-paper.overlays.default
        ];
      };
    in
      if builtins.any (e: e.prefix == "star-run12-incjet-paper") builtins.nixPath then
        _nixpkgs.plots
      else
        builtins.trace "Must specify -Istar-run12-incjet-paper=some_path or use an equivalent NIX_PATH!"
        [];

    projection = mkhtml' "-s --errs -m '/result/'" {
      Legend = 1;
      LegendXPos = 0.5;
      LegendYPos = 0.5;
      MainPlot = 0;
      RatioPlotYLabel = "Ratio to NLO$\\times$NP";
    }
      single_trigger.jppromoted_combined.yoda_files;

    all_triggers = let
      jps = [ 1 0 2 ];
    in mkhtml' "-s --errs -m '/result/'" {
      RatioPlotYMin = 0.5;
      RatioPlotYMax = 1.5;
      RatioPlotYLabel = "Ratio to JP${toString (builtins.elemAt jps 0)}";
    }
      (map (jp:
        let jp_title = builtins.replaceStrings [ "_" ] [ "\\_" ] (toString jp); in
        (set_title "Unfolded JP${jp_title} data (${ptjet_ue_corr_title}) (stat.\\,uncertainty)"
          (stat_uncertainty jp)
        // { plot_options = { ConnectBins = 0; LineColor = builtins.elemAt [ "green" "orange" "Purple" ] jp; LineWidth = "${toString (1.0 + 0.1 * jp)}pt"; LegendOrder = jp; }; })
      ) jps);

    demoted_promoted_triggers = mkhtml' "-s --errs -m '/result/'" {
      RatioPlotYMin = 0.5;
      RatioPlotYMax = 1.5;
      CustomLegend = "Stat. uncertainty error bars";
    }
      (map (trig:
        set_title' "Unfolded JP-${trig} data (${ptjet_ue_corr_title})" trig
          (unfold_result { jp = "${trig}_combined"; response = embed_yoda; measurement = data_yoda; err_type = "b_uncorr"; use_eta_bins = false; })
        // { plot_options = { ConnectBins = 0; LineWidth = "1.2pt"; LineStyle = "dashed"; }; }
      ) [ "demoted" "promoted" ]);

    combined = mkhtml' "-s --errs -m '/result/jet_pt(_.._..)?$'" { RatioPlotYLabel = "Ratio to data"; } [
      (set_title "Unfolded data (stat.\\,uncertainty)"
        (combine_results
          (map (jp: {
            input = stat_uncertainty jp;
          }) jps_legacy_combine))
        // { plot_options = { ConnectBins = 0; LineColor = "black"; LineStyle = "solid"; }; })
      (set_title "Jet Energy Scale syst.\\,uncertainty for EMC"
        (combine_results
          (map (jp: {
            input = emc_uncertainty jp;
            reference_uncertainty = stat_uncertainty jp;
          }) jps_legacy_combine))
        // { plot_options = { ConnectBins = 0; LineColor = "black"; LineStyle = "none"; ErrorBars = 0; ErrorBands = 1; ErrorBandColor = "black"; ErrorBandOpacity = 0.3; }; })
      (set_title "Unfolding syst.\\,uncertainty (simulation statistics)"
        (combine_results
          (map (jp: {
            input = simu_stat_uncertainty jp;
            reference_uncertainty = stat_uncertainty jp;
          }) jps_legacy_combine))
        // { plot_options = { ConnectBins = 0; LineStyle = "none"; ErrorBars = 0; ErrorTubes = 1; ErrorTubeLineStyle = "dashed"; ErrorTubeLineColor = "black"; ErrorTubeLineWidth = "0.8pt"; ErrorTubeLineOpacity = 0.95; }; })

      (apply_f_np
        (nlo_curve {})
        // { plot_options = { ErrorBars = 0; LineColor = "Blue"; ErrorBands = if final_mode then 1 else 0; ErrorBandColor = "Blue"; ErrorBandOpacity = 0.2; }; })
      (set_title "Pythia ${py6.version} \\raisebox{0.1\\baselineskip}{\\scalebox{0.8}{@}} Perugia 2012, \\texttt{PARP(90)}$=0.213$"
        (mc2unfold pythia true)
        // { plot_options = { ErrorBars = 0; LineColor = "green"; }; })
    ];
    combined_yoda =
      (uncert_breakdown {
        unc_stat = stat_uncertainty "promoted_combined";
        unc_emc = emc_uncertainty "promoted_combined";
        unc_emb = simu_stat_uncertainty "promoted_combined";
      });

    combined_latex_table = to_latex {
      unc_stat = stat_uncertainty "promoted_combined";
      unc_syst = syst_uncertainty "promoted_combined";
    };

    combine_weights = mkhtml' "-s -m '/result/jet_pt_relw'" {
      YLabel = "Relative weights";
      LogY = 0;
      YMin = -0.1;
      YMax = 1.1;
    } [
      (set_title "combined JP[${jps_s}] data (${ptjet_ue_corr_title})"
        (combine_results
          (map (jp: {
            input = unfold_result { inherit jp; response = embed_yoda; measurement = data_yoda; };
          }) jps_legacy_combine)))
    ];

    combined_same = let
      unc_emc = builtins.elemAt combined.yoda_files 1;
      nlo = builtins.elemAt combined.yoda_files 3;
      pythia = builtins.elemAt combined.yoda_files 4;
      f = result: [
          (rename result "_00_${etabin_cut_1_nodot}" "_same" // {
            title = (result.title or "") + ", $|\\eta| < ${etabin_cut_1}$";
            plot_options = result.plot_options // {
              LineStyle = "solid";
            };
          })
          (rename result "_${etabin_cut_1_nodot}_${etabin_cut_2_nodot}" "_same" // {
            title = "$${etabin_cut_1} < |\\eta| < ${etabin_cut_2}$";
            plot_options = result.plot_options // {
              LineStyle = "dashed";
              LineOpacity = (result.plot_options.LineOpacity or 1.0) - 0.5;
              ErrorBandOpacity = (result.plot_options.ErrorBandOpacity or 1.0) * 0.5;
              ErrorTubeOpacity = (result.plot_options.ErrorTubeOpacity or 1.0) - 0.5;
            };
          })
      ];
    in
      mkhtml' "-s --errs -m '/result/jet_pt_same$'" {}
        (builtins.concatMap f [ unc_emc nlo pythia]);

    run12pp200_prelim = with override (final: prev: {
      unfold_bins = [ 6.7 8.9 11.9 15.8 21.1 28.1 37.5 50.0 ];
    }); mkhtml' "-s --errs -m '/result/jet_pt'" {} [
      (set_title "The run12pp200 preliminary result"
        (rename (to_output ./MC/rivet-ana/STAR_2021_PP200_JETS_PRELIM/STAR_2021_PP200_JETS_PRELIM.yoda) "/REF/STAR_2021_PP200_JETS_PRELIM/d01-x01-y01" "/result/jet_pt")
        // { plot_options = { ConnectBins = 0; LineColor = "red"; LineWidth = "1.6pt"; ErrorBars = 0; ErrorBands = 1; ErrorBandColor = "red"; ErrorBandOpacity = 0.3; }; })
      (set_title "Current result (stat.\\,uncertainty)"
        (stat_uncertainty "promoted_combined")
        // { plot_options = { ConnectBins = 0; LineColor = "black"; }; })

      (set_title "Pythia ${py6.version} \\raisebox{0.1\\baselineskip}{\\scalebox{0.8}{@}} Perugia 2012, \\texttt{PARP(90)}$=0.213$"
        (mc2unfold pythia true)
        // { plot_options = { ErrorBars = 0; LineColor = "green"; }; })
    ];

    run9 = with override (final: prev: with final; {
      unfold_bins = [ 9.7 11.5 13.6 16.1 19.0 22.5 26.6 31.4 37.2 44.0 52.0 ];
      bins_for_jp = jp:
        let
          bins = final.unfold_bins;
          nbins = builtins.length bins;
          jp_num_m = builtins.match "([0-9]+).*" jp;
          jp_num =
            if builtins.isInt jp then
              jp
            else if jp_num_m != null then
              builtins.fromJSON (builtins.elemAt jp_num_m 0)
            else
              null;
        in
          if jp_num == 0 then
            lib.sublist 0 (nbins - 5) bins
          else if jp_num == 1 then
            bins
          else if jp_num == 2 then
            lib.sublist 3 (nbins - 3) bins
          else
            prev.bins_for_jp jp;
    }); mkhtml' "-s --errs -m '/result/jet_pt'" { XLabel = "jet $p_{\\text{T}}$, GeV/c"; CustomLegend = ""; RatioPlotYLabel = "Ratio to JP0"; } (
      map (jp:
        (with without_ue_subtraction; set_title "JP${toString jp} data (${ptjet_ue_corr_title}) (stat.\\,uncertainty)"
          (unfold_result { inherit jp; response = embed_yoda; measurement = data_yoda; err_type = "b_uncorr"; bins = bins_for_jp jp; })
          // { plot_options = { ConnectBins = 0; LineColor = { "0" = "red"; "1" = "pink"; "2" = "purple"; }.${toString jp}; LineOpacity = 0.6 + 0.2 * jp; LineStyle = { "0" = "dashed"; "1" = "solid"; "2" = "dotted"; }.${toString jp}; }; })
      ) [ 1 0 2 ]
      ++ [
        (apply_f_ue_had
          (scale_uncertainty (args: mc2unfold (nlo args) false))
          // { plot_options = { ErrorBars = 0; LineColor = "Blue"; ErrorBands = 1; ErrorBandColor = "Blue"; ErrorBandOpacity = 0.2; }; })
        (set_title "Pythia ${py6.version} (${ptjet_ue_corr_title}) @ Perugia 2012, \\texttt{PARP(90)}$=0.213$"
          (mc2unfold pythia false)
          // { plot_options = { ErrorBars = 0; LineColor = "green"; }; })
        (set_title "Pythia ${py6.version} (${ptjet_ue_corr_title}) @ Perugia 0"
          (mc2unfold pythia_perugia0 false)
          // { plot_options = { ErrorBars = 0; LineColor = "green!80"; LineStyle = "dashed"; }; })

        # Data run9pp200
        (set_title "Run 9 preliminary release (no UE corr, $|\\eta| < 1.0$)"
          (to_output ./misc/run9.yoda)
          // { plot_options = { ConnectBins = 0; LineColor = "black"; }; })
      ]
    );

    run9_dijets = mkhtml' "-s --errs -m '/STAR_2017_I1493842/d01-x01-y01'" {} [
      # Theory
      (scale_uncertainty (args: nlo ({ scale_var = "mjj"; } // args))
        // { plot_options = { ErrorBars = 0; LineColor = "Blue"; ErrorBands = 1; ErrorBandColor = "Blue"; ErrorBandOpacity = 0.2; }; })
      (set_title "Pythia ${py6.version} (no Had., no MPI)"
        pythia_no_had_no_mpi
        // { plot_options = { ErrorBars = 0; LineColor = "green"; }; })

      # Data run9pp200
      (set_title "Data $/$ $f_{had.+UE}$ (from Brian)"
        (rename (to_output ./MC/rivet-ana/STAR_2017_I1493842/STAR_2017_I1493842.yoda) "/REF" "")
        // { plot_options = { ConnectBins = 0; LineColor = "black"; }; })
    ];
  }));
})
