with import <nixpkgs> {};
with import ./common.nix;

let
  bits = overrides: import ./bits.nix { inherit overrides; };
  no_overrides = self: super: { };
  default_overrides = self: super: {
    process_overrides = super.process_overrides // {
      max_track_pt = 30;
      blacklist_tower_3407 = 1;
    };
  };
  no_track_cut = compose [ default_overrides (self: super: {
    process_overrides = super.process_overrides // {
      max_track_pt = -1;
    };
  }) ];
  pct4 = self: super: {
    process_overrides = super.process_overrides // {
      detector_jet_branch_name = "AntiKtR060NHits12_4pct";
      detector_ue_branch_name = "AntiKtR060NHits12_4pctOffAxisConesR050";
    };
  };
  pct7 = self: super: {
    process_overrides = super.process_overrides // {
      detector_jet_branch_name = "AntiKtR060NHits12_7pct";
      detector_ue_branch_name = "AntiKtR060NHits12_7pctOffAxisConesR050";
    };
  };
  stable = compose [ default_overrides (self: super: {
    make_jet_plots_build = super.make_jet_plots_build.overrideAttrs (old: {
      src = fetchgit {
        url = "https://github.com/veprbl/star-jet.git";
        rev = "1e49ea46da2568ae8a87b2432aa35627feaa3745";
        sha256 = "05s3b3b6xnr8nrqwcbj9i5a5ld95vilmx2zjglgl5ink68jsyp88";
      };
    });
  }) ];
in
with bits default_overrides;

{
  build = make_jet_plots_build;
  stable_build = (bits stable).make_jet_plots_build;
  bins_scaled_all = mkhtml (stdenv.lib.attrValues embed_bins) "--no-ratio";
  bins_unscaled_all = mkhtml (stdenv.lib.attrValues embed_bins_unscaled) "--no-ratio";
  bins = stdenv.lib.mapAttrs (bin_name: bin: mkhtml [ bin ] "") embed_bins_unscaled;
  summary =
    mkhtml [
        (set_title "Data" (rescale_ref data_yoda embed_yoda))
        (set_title "Embedding" embed_yoda)
      ] "--mc-errs PLOT:LegendXPos=0.2:LogZ=1";
  data = mkhtml [
    (set_title "Data" data_yoda)
  ] "--mc-errs";
  embed = mkhtml [
    (set_title "Embedding" embed_yoda)
  ] "--mc-errs";
  data_trigger_comp = mkhtml [
    (set_title "Data JP0" (rename data_yoda "_jp0" "_jpx"))
    (set_title "Data JP0 VPDMB CUT" (rename data_yoda "_jp0_vpdmb_cut" "_jpx"))
    (set_title "Data JP1" (rename data_yoda "_jp1" "_jpx"))
    (set_title "Data JP2" (rename data_yoda "_jp2" "_jpx"))
    (set_title "Data VPDMB NOBSMD" (rename data_yoda "_vpdmb_nobsmd" "_jpx"))
    (set_title "Data VPDMB NOBSMD + JP0 simu" (rename data_yoda "_vpdmb_nobsmd_jp0_simu" "_jpx"))
  ] "--mc-errs --no-ratio -m '.*_jpx.*'";
  embed_trig_compare = mkhtml [
    (set_title "Embedding JP0" (rename embed_yoda "_jp0" "_jpx"))
    (set_title "Embedding JP1" (rename embed_yoda "_jp1" "_jpx"))
    (set_title "Embedding JP2" (rename embed_yoda "_jp2" "_jpx"))
    (set_title "Embedding MB" (rename embed_yoda "_vpdmb_nobsmd" "_jpx"))
  ] "--mc-errs -m '.*_jpx.*'";
  track_cut_study =
    mkhtml [
        (set_title "Data (no track cut)" (rescale_ref (bits no_track_cut).data_yoda embed_yoda) // { plot_options = ":LineColor=blue:LineStyle=dashed:LineDash=1pt\\ 1pt"; })
        (set_title "Data" (rescale_ref data_yoda embed_yoda) // { plot_options = ":LineColor=blue"; })
        (set_title "Embedding (no track cut)" (bits no_track_cut).embed_yoda // { plot_options = ":LineColor=orange:LineStyle=dashed:LineDash=1pt\\ 1pt"; })
        (set_title "Embedding" embed_yoda // { plot_options = ":LineColor=orange"; })
      ] "--mc-errs PLOT:LegendXPos=0.2:LogZ=1";
  unfold = unfold embed_yoda data_yoda;
  unfold_tracking_eff_embed = mkhtml [
    (set_title "Embedding" (unfold embed_yoda embed_yoda)) # FIXME
    (set_title "Unfold nominal embedding with $4\\%$ embedding" (unfold (bits pct4).embed_yoda embed_yoda) // { plot_options = ":LineColor=blue"; })
    (set_title "Unfold $4\\%$ embedding with nominal embedding" (unfold embed_yoda (bits pct4).embed_yoda) // { plot_options = ":LineColor=green"; })
    (set_title "Unfold nominal embedding with $7\\%$ embedding" (unfold (bits pct7).embed_yoda embed_yoda) // { plot_options = ":LineColor=blue:LineStyle=dashed"; })
    (set_title "Unfold $7\\%$ embedding with nominal embedding" (unfold embed_yoda (bits pct7).embed_yoda) // { plot_options = ":LineColor=green:LineStyle=dashed"; })
    ] "PLOT:LegendXPos=0.1:RatioPlotYMin=0.7:RatioPlotYMax=1.3";
  unfold_tracking_eff_data = mkhtml [
    (set_title "Unfold data with nominal embedding" (unfold embed_yoda data_yoda))
    (set_title "Unfold data with $4\\%$ embedding" (unfold (bits pct4).embed_yoda data_yoda) // { plot_options = ":LineColor=blue"; })
    (set_title "Unfold data with $7\\%$ embedding" (unfold (bits pct7).embed_yoda data_yoda) // { plot_options = ":LineColor=blue:LineStyle=dashed"; })
    ] "PLOT:LegendXPos=0.3:RatioPlotYMin=0.7:RatioPlotYMax=1.3";
  do_unfold = let
    _unfold = to_yoda (unfold embed_yoda embed_yoda);
    unfold_jp = _jp: let jp = toString _jp; in [
        (set_title "JP${jp} Truth" (rename _unfold "particle_jet_pt_jp${jp}" "unfold/particle_jet_pt_jp${jp}"))
        (set_title "JP${jp} TSVDUnfold" (rename _unfold "particle_jet_pt_jp${jp}_tsvdunfold" "unfold/particle_jet_pt_jp${jp}") // { plot_options = ":ErrorBandOpacity=0.5"; })
        (set_title "JP${jp} RooUnfold SVD" (rename _unfold "particle_jet_pt_jp${jp}_roounfold_svd" "unfold/particle_jet_pt_jp${jp}") // { plot_options = ":ErrorBandOpacity=0.5"; })
        (set_title "JP${jp} RooUnfold Bayes" (rename _unfold "particle_jet_pt_jp${jp}_roounfold_bayes" "unfold/particle_jet_pt_jp${jp}"))
      ];
  in
    mkhtml (builtins.concatLists (map unfold_jp [0 1 2])) "--mc-errs -m '/unfold/.*_jp[012](|_d|_sv)$' -m '/unfold/sv'";
  pt_ratio_vs_det_pt_rt = mkhtml
    (lib.mapAttrsToList (bin: title: set_title title (rename embed_yoda "pt_ratio_vs_det_pt_rt_${bin}" "pt_ratio_vs_det_pt_rt"))
     {
       "00_02" = "$0.0 < \\text{det }R_T < 0.2$";
       "02_04" = "$0.2 < \\text{det }R_T < 0.4$";
       "04_06" = "$0.4 < \\text{det }R_T < 0.6$";
       "06_08" = "$0.6 < \\text{det }R_T < 0.8$";
       "08_10" = "$0.8 < \\text{det }R_T < 1.0$";
     })
    "-s --no-ratio -m '.*pt_ratio_vs_det_pt_rt$' -m '.*pt_ratio_vs_det_pt_rt_track_cut_..$'";
  inherit data_yoda embed_yoda;
}
