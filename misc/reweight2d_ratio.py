#!/usr/bin/env python3

import yoda
import numpy as np


def to_bins2(h2):
  return np.reshape(h2.bins(), (h2.numBinsX(), h2.numBinsY()))


def to_points2(s2, h2ref):
  return np.reshape(s2.points(), (h2ref.numBinsX(), h2ref.numBinsY()))


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Rebin a YODA histogram.")
    parser.add_argument('--numerator', required=True)
    parser.add_argument('--denominator', required=True)
    parser.add_argument('--output', required=True)
    args = parser.parse_args()

    aos_numerator = yoda.readYODA(args.numerator)
    aos_denominator = yoda.readYODA(args.denominator)

    out_aos = []
    for h_numerator in aos_numerator.values():
        if h_numerator.path() not in aos_denominator: continue
        if not isinstance(h_numerator, yoda.core.Histo2D): continue

        h_denominator = aos_denominator[h_numerator.path()]
        assert isinstance(h_denominator, yoda.core.Histo2D)

        # Take the ratio of bin's sums of weights
        r = h_numerator / h_denominator

        # Normalize the ratio to become a ratio of probabilities
        for row_numerator, row_denominator, row_ratio in zip(to_bins2(h_numerator), to_bins2(h_denominator), to_points2(r, h_numerator)):
            norm_numerator = sum([
              # Bins with zero denominator will be reweighted to zero, so exclude them from the numerator as well
              (bin_numerator.sumW() if bin_denominator.sumW() != 0 else 0)
              for bin_numerator, bin_denominator in zip(row_numerator, row_denominator)
            ])
            norm_denominator = sum([
              bin_denominator.sumW()
              for bin_denominator in row_denominator
            ])
            for p in row_ratio:
                p.scaleZ((norm_denominator / norm_numerator) if norm_numerator != 0 else 0)
                # If it's NaN, it must be from a zero denominator
                if np.isnan(p.z()):
                    p.setZ(0)
                    p.setZErrs(0)

        r.setPath(h_numerator.path())
        out_aos.append(r)

    yoda.writeYODA(out_aos, args.output)
