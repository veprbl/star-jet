#!/usr/bin/env python2

import sys

import yoda

filename = sys.argv[1]
tag = sys.argv[2]
aos = yoda.readYODA(filename)
aos_new = []

for ao in aos.values():
    if not ao.path.startswith("/unfolding_jp"):
        continue
    if not ao.path.endswith("response_fine"):
        continue
    ao.path = "/" + tag + ao.path
    aos_new.append(ao)

for ao in aos_new:
    print(filename, ao.path)
yoda.writeYODA(aos_new, "output.yoda")
