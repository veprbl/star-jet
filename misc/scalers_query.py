import pymysql

conn = pymysql.connect(
        host="mstardb02.nersc.gov",
        port=3316,
        user="harmless",
        password="",
        db="RunLog_onl",
        )

IDX_JP0 = 8
IDX_JP1 = 9

with open("./2012_200GeV_FinalResultRunlist_fromKevin.txt", "r") as fp:
    runlist = list(map(int, filter(lambda s: s != "", fp.readlines())))

try:
    with conn.cursor() as cursor:
        runcond = " OR ".join(["`runNumber` = {}".format(run) for run in runlist])
        sql = "SELECT `runNumber`, `idxTrigger`, `ps` FROM `trigPrescales` WHERE idxLevel = 3 AND (`idxTrigger` = %s OR `idxTrigger` = %s) AND ({});".format(runcond)
        cursor.execute(sql, (IDX_JP0, IDX_JP1))
        ps_jp0 = {}
        ps_jp1 = {}
        for run, idxTrigger, ps in cursor.fetchall():
            if idxTrigger == IDX_JP0:
                assert run not in ps_jp0
                ps_jp0[run] = ps
            elif idxTrigger == IDX_JP1:
                assert run not in ps_jp1
                ps_jp1[run] = ps
            else:
                raise ValueError("Unknown idxTrigger")
        for run in runlist:
            assert run in ps_jp0
            assert run in ps_jp1
            print(run, ps_jp0[run], ps_jp1[run])
        import numpy as np
        import sys
        print("mean jp0", np.mean(list(ps_jp0.values())), file=sys.stderr)
        print("mean jp1", np.mean(list(ps_jp1.values())), file=sys.stderr)
finally:
    conn.close()
