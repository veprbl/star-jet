#!/usr/bin/env python3

import sys

import yoda


def find_point(scatter, x):
    for point in scatter:
        if point.x() == x:
            return point
    raise IndexError("Matching point not found")


def combine_aos(path, aos, weights):
    bins = sum([[(p.x(), tuple(p.xErrs())) for p in ao] for ao in aos], [])
    bins_w = sum([[(p.x(), tuple(p.xErrs())) for p in ao] for ao in aos], [])
    assert bins == bins_w
    s = yoda.Scatter2D(path)
    sws = [yoda.Scatter2D(path + f"_relw{i}") for i in range(len(aos))]
    for (x, (xerrm, xerrp)) in set(bins):
        ps = []
        ps_m = []
        ps_p = []
        ps_norm = []
        maxw = -1.
        for wao in weights:
            try:
                pw = find_point(wao, x)
                w = 1.0 / max(pw.yErrs()) ** 2
                maxw = max(w, maxw)
            except IndexError:
                pass
        for ao, wao in zip(aos, weights):
            try:
                p = find_point(ao, x)
                pw = find_point(wao, x)
                w = 1.0 / max(pw.yErrs()) ** 2
                w = 1. if w == maxw else 0.;
                y = p.y()
                yerrs = p.yErrs()
            except IndexError:
                w = 0
                y = 0
                yerrs = (0, 0)
            ps_norm.append(w)
            ps.append(y * w)
            ps_m.append(yerrs[0] * w)
            ps_p.append(yerrs[1] * w)
        for sw, w in zip(sws, ps_norm):
            sw.addPoint(x, w / sum(ps_norm), xerrs=(xerrm, xerrp))
        s.addPoint(x, sum(ps) / sum(ps_norm), xerrs=(xerrm, xerrp), yerrs=(sum(ps_m) / sum(ps_norm), sum(ps_p) / sum(ps_norm)))
    return s, sws


def combine(inputs):
    paths = set.intersection(*[set(_input[0].keys()) & set(_input[1].keys()) for _input in inputs])
    aos_out = []
    for path in paths:
        aos = [aos[path] for (aos, _) in inputs]
        weights = [ref[path] for (_, ref) in inputs]
        ao, relw_aos = combine_aos(path, aos, weights)
        aos_out.append(ao)
        aos_out += relw_aos
    return aos_out


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Rebin a YODA histogram.")
    parser.add_argument('--input', nargs=2, metavar=('input', 'reference_uncertainty'), action='append', required=True)
    parser.add_argument('output_filename')
    args = parser.parse_args()

    print([ref for (_, ref) in args.input])

    inputs = [
      (yoda.readYODA(_input), yoda.readYODA(ref))
      for (_input, ref) in args.input
    ]

    aos = combine(inputs)

    yoda.writeYODA(aos, args.output_filename)
