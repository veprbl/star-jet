#!/usr/bin/env python3

import yoda
import yaml

def as_err(p):
    if type(p) is tuple:
        return {
            "dn": p[0],
            "up": p[1],
        }
    return {
        "dn": p.yErrs().minus,
        "up": p.yErrs().plus,
    }

def combine_with_uncert_breakdown(s_stat, s_emc, s_emb):
    breakdown = {}
    ps = []
    for ix, (p_stat, p_emc, p_emb) in enumerate(zip(s_stat, s_emc, s_emb)):
        breakdown[ix] = {
            "stat": as_err(p_stat),
            "emc": as_err(p_emc),
            "emb": as_err(p_emb),
            "lumi": as_err((p_stat.y() * 0.1,) * 2), # 10% lumi uncertainty
        }
        yerrs = tuple(
            sum(e[_dir] ** 2 for e in breakdown[ix].values()) ** 0.5
            for _dir in ["dn", "up"]
            )
        p_total = yoda.Point2D(x=p_stat.x(), y=p_stat.y(), xerrs=p_stat.xErrs(), yerrs=yerrs)
        #YODA 1.8.5 doesn't seem to be able to write those out
        #p_total.setYErrs((p_stat.yErrs().plus, p_stat.yErrs().minus), "stat")
        #p_total.setYErrs((p_emc.yErrs().plus, p_emc.yErrs().minus), "emc")
        #p_total.setYErrs((p_emb.yErrs().plus, p_emb.yErrs().minus), "emb")
        ps.append(p_total)

    s = yoda.Scatter2D(ps)
    s.setAnnotation("ErrorBreakdown", yaml.dump(breakdown, default_flow_style=True, width=1e10))
    return s

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Add several uncertainties into a single yoda histogram.")
    parser.add_argument('--unc_stat', required=True)
    parser.add_argument('--unc_emb', required=True)
    parser.add_argument('--unc_emc', required=True)
    parser.add_argument('-o', dest="output_filename", required=True)
    args = parser.parse_args()

    aos = []
    aos_stat = yoda.readYODA(args.unc_stat)
    aos_emb = yoda.readYODA(args.unc_emb)
    aos_emc = yoda.readYODA(args.unc_stat)
    for path in aos_stat.keys():
        if not path.startswith('/result/jet_pt'): continue
        s_stat = aos_stat[path]
        s_emb = aos_emb[path]
        s_emc = aos_emc[path]

        cmb = combine_with_uncert_breakdown(s_stat, s_emc, s_emb)
        cmb.setPath(path)
        aos.append(cmb)

    yoda.writeYODA(aos, args.output_filename)
