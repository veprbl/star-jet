#!/usr/bin/env python3

from collections import namedtuple

LumiInfo = namedtuple("LumiInfo", ["lumi", "ps_fac", "live_frac"])

def read_lumi_info(filename):
    with open(filename, "r") as fp:
        lines = fp.readlines()

    rows = [l.split() for l in lines]

    def row2lumi_info(row):
        return LumiInfo(
            lumi=float(row[4]), # lumi already includes all corrections
            ps_fac=float(row[5]),
            live_frac=float(row[6]),
        )

    run_lumi = dict((row[0], row2lumi_info(row)) for row in rows)

    return run_lumi

with open("2012_200GeV_FinalResultRunlist_fromKevin.txt", "r") as fp:
    lines = fp.readlines()

runlist = [l.rstrip() for l in lines]

lumi_info_jp0 = read_lumi_info("lum_perrun_JP0.txt")
lumi_info_jp1 = read_lumi_info("lum_perrun_JP1.txt")
lumi_info_jp2 = read_lumi_info("lum_perrun_JP2.txt")
lumi_info_vpdmb_nobsmd = read_lumi_info("lum_perrun_VPDMB-nobsmd.txt")
lumi_info_zerobias = read_lumi_info("lum_perrun_ZEROBIAS.txt")

run_lumi = dict()

for run in runlist:
    run_lumi[run] = {
        "jp0_luminosity": lumi_info_jp0[run].lumi,
        "jp1_luminosity": lumi_info_jp1[run].lumi,
        "jp2_luminosity": lumi_info_jp2[run].lumi,
        "jp0_ps": lumi_info_jp0[run].ps_fac,
        "jp1_ps": lumi_info_jp1[run].ps_fac,
        "jp2_ps": lumi_info_jp2[run].ps_fac,
        "jp0_live_frac": lumi_info_jp0[run].live_frac,
        "jp1_live_frac": lumi_info_jp1[run].live_frac,
        "jp2_live_frac": lumi_info_jp2[run].live_frac,
        "vpdmb_nobsmd_luminosity": lumi_info_vpdmb_nobsmd[run].lumi,
        "vpdmb_nobsmd_ps": lumi_info_vpdmb_nobsmd[run].ps_fac,
        "zerobias_luminosity": lumi_info_zerobias[run].lumi,
        "zerobias_ps": lumi_info_zerobias[run].ps_fac,
    }

import json
with open("run_lumi.json", "w") as fp:
    json.dump(run_lumi, fp, indent=2)

with open("run12pp200.v2.list_fromZilong", "r") as fp:
    lines = fp.readlines()

runlist = [l.rstrip() for l in lines]

run_lumi_fromZilong = {}

for run in runlist:
    for jp in [0, 1, 2]:
        filename = f"lum_fromZilong/{run}.zdc.JP{jp}.lum.txt"

        with open(filename) as fp:
            lines = fp.readlines()

        assert len(lines) == 1
        live_frac, ps_fac, daq_rec_frac, ZDC_coin, lumi, accidentals_multiples_corr = [float(val) for val in lines[0].strip().split()]
        # lumi already includes all corrections

        run_lumi_fromZilong.setdefault(run, {})[f"jp{jp}_luminosity"] = lumi
        run_lumi_fromZilong.setdefault(run, {})[f"jp{jp}_ps"] = ps_fac
        run_lumi_fromZilong.setdefault(run, {})[f"jp{jp}_live_frac"] = live_frac

overlapping_runs = set.intersection(set(run_lumi.keys()), set(run_lumi_fromZilong.keys()))
sum_ratios = {}
num_ratios = {}
for run in overlapping_runs:
    for jp in [0, 1, 2]:
        sum_ratios.setdefault(jp, 0.)
        sum_ratios[jp] += run_lumi_fromZilong[run][f"jp{jp}_luminosity"] / run_lumi[run][f"jp{jp}_luminosity"]
        num_ratios.setdefault(jp, 0)
        num_ratios[jp] += 1

avg_ratio = {}
for jp in [0, 1, 2]:
    avg_ratio[jp] = sum_ratios[jp] / num_ratios[jp]
    print(f"Average Zilong / Jamie lumi ratio for JP{jp}: {avg_ratio[jp]}")

missing_runs = set(run_lumi.keys()) - set(run_lumi_fromZilong.keys())
print(f"The following runs are missing: {missing_runs}")
for run in missing_runs:
    assert run not in run_lumi_fromZilong
    for jp in [0, 1, 2]:
        run_lumi_fromZilong.setdefault(run, {})[f"jp{jp}_luminosity"] = run_lumi[run][f"jp{jp}_luminosity"] * avg_ratio[jp]
        run_lumi_fromZilong.setdefault(run, {})[f"jp{jp}_luminosity_comment"] = "extrapolated"
        run_lumi_fromZilong.setdefault(run, {})[f"jp{jp}_ps"] = run_lumi[run][f"jp{jp}_ps"]

for run in run_lumi.keys():
    run_lumi_fromZilong[run][f"vpdmb_nobsmd_luminosity"] = run_lumi[run][f"vpdmb_nobsmd_luminosity"] * avg_ratio[2]
    run_lumi_fromZilong[run][f"vpdmb_nobsmd_luminosity_comment"] = "estimated"
    run_lumi_fromZilong[run][f"vpdmb_nobsmd_ps"] = run_lumi[run][f"vpdmb_nobsmd_ps"]
    run_lumi_fromZilong[run][f"zerobias_luminosity"] = run_lumi[run][f"zerobias_luminosity"] * avg_ratio[2]
    run_lumi_fromZilong[run][f"zerobias_luminosity_comment"] = "estimated"
    run_lumi_fromZilong[run][f"zerobias_ps"] = run_lumi[run][f"zerobias_ps"]

with open("run_lumi_fromZilong.json", "w") as fp:
    json.dump(run_lumi_fromZilong, fp, indent=2)
