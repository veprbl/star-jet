#!/usr/bin/env python3

import argparse
import sys
import re
from math import isnan

from tqdm import tqdm
import yoda

parser = argparse.ArgumentParser(
    description="Stack given histograms along a specific axis"
)
parser.add_argument("inputs", nargs="+", metavar="filename:xmin[:xmax]", help="")
parser.add_argument("-o", dest="output_path", default="output.yoda")
parser.add_argument("-m", action="append", dest="patterns", default=None)
parser.add_argument("-M", action="append", dest="unpatterns", default=[])
args = parser.parse_args()

# This is the default format plus newline character
BAR_FORMAT = "{l_bar}{bar}{r_bar}\r\n"

objs = {}

for input_s in tqdm(args.inputs, bar_format=BAR_FORMAT):
    _input = input_s.split(":")
    if len(_input) == 3:
        filename, xmin_s, xmax_s = _input
        xmin = float(xmin_s)
        xmax = float(xmax_s)
    elif len(_input) == 2:
        filename, xmin_s = _input
        xmin = float(xmin_s)
        xmax = xmin + 1.0
    else:
        parser.print_help()
        sys.stderr.write(f"Error: Wrong input format: `{input_s}' is not `filename:xmin[:xmax]'\n")
        sys.exit(1)

    aos = yoda.readYODA(filename, patterns=args.patterns, unpatterns=args.unpatterns)

    for path, ao in aos.items():
        if isinstance(ao, yoda.Counter):
            if path not in objs:
                objs[path] = dict(
                    ao=yoda.Histo1D(path=ao.path(), title=ao.title()),
                    bins=[],
                    sumws=[],
                )

            obj = objs[path]
            assert (
                type(obj["ao"]) is yoda.Histo1D
            ), f"type of objs[\"{path}\"] is {type(obj['ao'])} and not yoda.Histo1D"

            obj["bins"].append((xmin, xmax))
            obj["sumws"].append(ao.sumW())
        elif isinstance(ao, yoda.Histo1D):
            if path not in objs:
                objs[path] = dict(
                    ao=yoda.Histo2D(path=ao.path(), title=ao.title()),
                    bins=[],
                    sumws=[],
                )

            obj = objs[path]
            assert (
                type(obj["ao"]) is yoda.Histo2D
            ), f"type of objs[\"{path}\"] is {type(obj['ao'])} and not yoda.Histo2D"

            bins = ao.bins()
            obj["bins"].extend((xmin, xmax, b.xMin(), b.xMax()) for b in bins)
            obj["sumws"].extend(b.sumW() for b in bins)
        elif isinstance(ao, yoda.Scatter2D):
            if path not in objs:
                objs[path] = dict(
                    ao=yoda.Scatter3D(path=ao.path(), title=ao.title()),
                    points=[],
                )

            obj = objs[path]
            assert (
                type(obj["ao"]) is yoda.Scatter3D
            ), f"type of objs[\"{path}\"] is {type(obj['ao'])} and not yoda.Scatter3D"

            points = ao.points()
            obj["points"].extend(((xmin + xmax) / 2, p.x(), p.y() if not isnan(p.y()) else 0., ((xmax - xmin) / 2,) * 2, p.xErrs(), p.yErrs()) for p in points)

    del aos

aos_new = {}

for path, obj in tqdm(objs.items(), bar_format=BAR_FORMAT):
    tqdm.write(f'Processing Analysis Object "{path}"')
    ao_new = obj["ao"]
    if isinstance(ao_new, yoda.Histo2D):
        ao_new.addBins(obj["bins"])
        for bin_ix, sumw in enumerate(obj["sumws"]):
            # TODO: handle sumw2
            ao_new.fillBin(bin_ix, weight=sumw)
        aos_new[path] = ao_new
    elif isinstance(ao_new, yoda.Scatter3D):
        ao_new.addPoints(obj["points"])
        aos_new[path] = ao_new

yoda.writeYODA(aos_new, args.output_path)
