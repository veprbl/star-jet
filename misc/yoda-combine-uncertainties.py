#!/usr/bin/env python3

import sys
import math

import yoda


def find_point(scatter, point):
    for p in scatter:
        if math.isclose(p.x(), point.x()):
            return p
    raise IndexError("Matching point not found")


def mul2d(s1, s2):
    s = yoda.Scatter2D()
    for p1 in s1:
        p2 = find_point(s2, p1)
        assert p1.y() == p2.y()
        # Sum errors in quadratures
        yerrs = (
            math.sqrt((p1.yErrs()[i])**2 + (p2.yErrs()[i])**2)
            for i in [0, 1]
        )
        s.addPoint(p1.x(), p1.y(), xerrs=p1.xErrs(), yerrs=yerrs)
    return s


def add_aos(aos1, aos2):
    out_aos = {}

    for s1 in aos1.values():
        if isinstance(s1, yoda.core.Histo1D):
            s1 = s1.mkScatter()
        if not isinstance(s1, yoda.core.Scatter2D):
            continue
        if s1.path() not in aos2:
            continue

        s2 = aos2[s1.path()]
        assert isinstance(s2, yoda.core.Scatter2D)
        out_ao = mul2d(s1, s2)

        out_ao.setPath(s1.path())
        out_aos[s1.path()] = out_ao

    return out_aos


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Rebin a YODA histogram.")
    parser.add_argument('input_filename', nargs='+')
    parser.add_argument('-o', '--output')
    args = parser.parse_args()

    out_aos = None
    for input_filename in args.input_filename:
        aos = yoda.readYODA(input_filename)
        print(f"Adding {input_filename}")
        if out_aos is not None:
            out_aos = add_aos(out_aos, aos)
        else:
            out_aos = aos

    yoda.writeYODA(out_aos, args.output)
