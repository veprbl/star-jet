#!/usr/bin/env python2

import sys

import yoda

filename = sys.argv[1]
aos = yoda.readYODA(filename)
aos_new = []

for ao in aos.values():
    ao.path = ao.path.replace(sys.argv[2], sys.argv[3])
    aos_new.append(ao)

yoda.writeYODA(aos_new, "output.yoda")
