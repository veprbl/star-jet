#!/usr/bin/env python3

import sys
import re

import yoda


def parse_bins(bin_spec):
    return [float(x) for x in bin_spec.split()]


def rebin_aos(aos, specs):
    for path, h in aos.items():
        if not isinstance(h, yoda.core.Histo1D):
            continue

        matches = [bin_edges for (regex, bin_edges) in specs if regex.search(path)]

        if len(matches) == 0:
            continue
        elif len(matches) > 1:
            print(f"Error: ambiguous matches for {path}: {matches}")

        bin_edges, = matches

        h.rebinTo(bin_edges)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Rebin a YODA histogram.")
    parser.add_argument('input_filename')
    parser.add_argument('output_filename')
    parser.add_argument('-s', '--spec', nargs=2, metavar=('pattern', 'bins'), action='append')
    args = parser.parse_args()

    aos = yoda.readYODA(args.input_filename)

    specs = [(re.compile(regex), parse_bins(bin_spec)) for (regex, bin_spec) in args.spec]

    rebin_aos(aos, specs)

    yoda.writeYODA(aos, args.output_filename)
