from cycler import cycler
import matplotlib as mpl
import matplotlib.pyplot as plt

def setup_paper_style():
    mpl.rcParams.update(mpl.rcParamsDefault)

    # colors from ggplot
    ggplot_prop_cycle = cycler(color=[
        '#E24A33', # red
        '#348ABD', # blue
        '#988ED5', # purple
        '#777777', # gray
        '#FBC15E', # yellow
        '#8EBA42', # green
        '#EFA5A8', # pink
    ])

    plt.style.use('seaborn-paper')
    figsize_scale = 0.6
    plt.rcParams.update({
        'axes.grid': True,
        'axes.labelsize': 8,
        'axes.prop_cycle': ggplot_prop_cycle,
        'figure.figsize': (5 * figsize_scale, 5 * figsize_scale),
        'font.family': ['serif'],
        'grid.linestyle': ':',
        'grid.linewidth': 0.4,
        'legend.fontsize': 7,
        'legend.frameon': False,
        'xtick.bottom': True,
        'xtick.direction': 'in',
        'xtick.labelsize': 8,
        'xtick.major.pad': 2.5,
        'xtick.minor.pad': 2.4,
        'xtick.top': True,
        'ytick.direction': 'in',
        'ytick.labelsize': 8,
        'ytick.left': True,
        'ytick.major.pad': 2.5,
        'ytick.minor.pad': 2.4,
        'ytick.right': True,
        'pgf.rcfonts': False,
    })


def setup_presentation_style():
    mpl.rcParams.update(mpl.rcParamsDefault)
    plt.style.use('ggplot')
    plt.rcParams.update({
        'axes.labelsize': 8,
        'axes.titlesize': 9,
        'figure.titlesize': 9,
        'figure.figsize': (3, 3),
        'legend.fontsize': 7,
        'xtick.labelsize': 8,
        'ytick.labelsize': 8,
        'pgf.rcfonts': False,
    })


def set_label(x=None, y=None, offset=1.0):
    if x is not None:
        plt.xlabel(x, horizontalalignment='right', x=offset, fontweight='semibold')
    if y is not None:
        plt.ylabel(y, horizontalalignment='right', y=offset, fontweight='semibold')
