#!/usr/bin/env python

# fix rootpy in nix environment
import os
os.environ['XDG_CONFIG_HOME'] = os.environ['TMP']
os.environ['XDG_CACHE_HOME'] = os.environ['TMP']

import ROOT
ROOT.gROOT.SetBatch(True)
import rootpy.io
from rootpy.io import root_open

def process(rf, collection):
    for path, dirs, obj_names in rf.walk():
        for obj_name in obj_names:
            obj_path = "{}/{}".format(path, obj_name)
            try:
                obj = rf.get(obj_path)
            except RuntimeError:
                # this is weird, but it works to try again...
                obj = rf.get(obj_path)
            if obj_path not in collection:
                collection[obj_path] = []
            collection[obj_path].append({
                    "rf_name" : rf.name,
                    "path" : path,
                    "obj" : obj
                    })

def _root_open(*args, **kwargs):
    """
    Prevent warnings
    """
    from rootpy.context import preserve_current_directory
    with preserve_current_directory():
        return root_open(*args, **kwargs)

def merge_files(input_files, weight, output_file):
    collection = {}
    inputs = map(_root_open, input_files)
    for rf in inputs:
        process(rf, collection)
    del inputs
    with root_open(output_file, 'w') as orf:
        for _, objs in collection.items():
            path = objs[0]["path"]
            obj = objs[0]["obj"]
            obj_type = type(obj)
            assert all(map(lambda p: type(p["obj"]) == obj_type, objs))
            if hasattr(obj, 'Add'):
                assert hasattr(obj, 'Scale')
                obj.Scale(weight[objs[0]["rf_name"]])
                for other in objs[1:]:
                    wo = weight[other["rf_name"]]
                    obj.Add(other["obj"], wo)
            try:
                obj_dir = orf.Get(path)
            except rootpy.io.file.DoesNotExist:
                obj_dir = orf.mkdir(path, recurse=True)
            obj_dir.WriteTObject(obj)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--add', action='store_true') # yodamerge interface mock
    parser.add_argument('inputs', nargs='+')
    parser.add_argument('-o', '--output', type=str, required=True)
    args = parser.parse_args()
    inputs = map(lambda s: s.split(":"), args.inputs)
    weight = {}
    for p in inputs:
        assert len(p) <= 2
        weight[p[0]] = float(p[1]) if len(p) == 2 else 1.0
    inputs = map(lambda p: p[0], inputs)
    merge_files(inputs, weight, args.output)
