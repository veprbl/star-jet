#!/usr/bin/env python3

from math import log10, floor

import yoda

def valueToLatex(x, dx, val=True, cdot=False):
    # TODO round unc up
    exponent = floor(log10(x))
    mantissa = x / (10 ** exponent)
    if type(dx) is float:
        dmantissa = dx / (10 ** exponent)
        return (f"{mantissa:.2f}" if val else "") + f" \pm {dmantissa:.2f}" + (rf"\:\times\: 10^{{{exponent}}}" if cdot else "")
    else:
        dmantissa = [v / (10 ** exponent) for v in dx]
        return (f"{mantissa:.2f}" if val else "") + f"{{}}^{{+{dmantissa[0]:.2f}}}_{{-{dmantissa[1]:.2f}}}" + (rf"\:\times\:10^{{{exponent}}}" if cdot else "")


def xsecToLatex(aos_stat, aos_syst):
    p = "/result/jet_pt_00_05"
    hs_00_05 = aos_stat[p], aos_syst[p]
    p = "/result/jet_pt_05_09"
    hs_05_09 = aos_stat[p], aos_syst[p]
    eta_bins = [
      ("$|\eta| < 0.5$", hs_00_05),
      ("$0.5 < |\eta| < 0.9$", hs_05_09),
    ]

    result = ""

    for etabin, (eta_range, eta_hists) in enumerate(eta_bins):
        if etabin != 0:
            result += r"\addlinespace" + "\n"
        result += r"\hline" + "\n"
        result += r"\multicolumn{2}{|c|}{" + eta_range + r"} \\" + "\n"
        result += r"\hline" + "\n"
        for ptbin, (p_stat, p_syst) in enumerate(zip(*eta_hists)):
            assert p_stat.y() == p_syst.y()
            columns = [
              f"${p_stat.x() - p_stat.xErrs()[1]:.1f}$ -- ${p_stat.x() + p_stat.xErrs()[0]:.1f}$",
              f"${valueToLatex(p_stat.y(), p_stat.yErrs()[0])}{valueToLatex(p_syst.y(), p_syst.yErrs()[0], val=False, cdot=True)}$",
            ]
            result += "\t& ".join(columns)
            if (etabin < len(eta_bins) - 1) or (ptbin < len(eta_hists[0]) - 1):
                result += r" \\"
            result += "\n"

    return result


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Add several uncertainties into a single yoda histogram.")
    parser.add_argument('--unc_stat', required=True)
    parser.add_argument('--unc_syst', required=True)
    args = parser.parse_args()

    aos = []
    aos_stat = yoda.readYODA(args.unc_stat)
    aos_syst = yoda.readYODA(args.unc_syst)

    print(xsecToLatex(aos_stat, aos_syst), end="")
