#!/usr/bin/env python2

import sys

import yoda

input_file = sys.argv[1]

stages = [
"events_raw",
"events_after_rank_cut",
"events_after_jet_cut",
"events_after_vertex_cut",
]
stages = [
"jets_raw",
"jets_after_rank_cut",
"jets_after_jet_cut",
"jets_after_vertex_cut",
]
stages = [
    "events_raw",
    "events_after_rank_cut",
    "events_after_det_eta_cut",
    "events_after_phys_eta_cut",
    "events_after_jp_match_cut",
    "events_after_pt_cut",
    "events_after_tower_3407_cut",
    "events_after_max_track_pt_cut",
    "events_after_vertex_cut",
    "events_after_rt_cut",
    ]
#stages = [stage.replace("events_", "jets_") for stage in stages]

aos = yoda.readYODA(input_file)

prev_sumw = {}
for s in stages:
    print "{:30s}".format(s),
    for trg in [ "jp0", "jp1", "jp2", "vpdmb_nobsmd" ]:
        sumw = aos["/jet_quantities_{}/{}".format(trg, s)].sumW()
        print "\t{:4.0f}M".format(sumw / 1e6),
        if trg in prev_sumw and prev_sumw[trg] > 0:
            print "{:5.1f}%".format((sumw - prev_sumw[trg]) / prev_sumw[trg] * 100),
        else:
            print "       ",
        prev_sumw[trg] = sumw
    print ""
