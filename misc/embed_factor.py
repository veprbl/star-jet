#!/usr/bin/env python3

import argparse
import sys
import yoda

pt_xsec = { # in mb
    "pp200_pt2_3"   :      9.0012,
    "pp200_pt3_4"   :     1.46253,
    "pp200_pt4_5"   :    0.354566,
    "pp200_pt5_7"   :    0.151622,
    "pp200_pt7_9"   :   0.0249062,
    "pp200_pt9_11"  :  0.00584527,
    "pp200_pt11_15" :  0.00230158,
    "pp200_pt15_20" : 0.000342755,
    "pp200_pt20_25" : 4.57002e-05,
    "pp200_pt25_35" : 9.72535e-06,
    "pp200_pt35_-1" : 4.95791e-07,
    "pp200_pt35_45" : 4.69889e-07,
    "pp200_pt45_55" : 2.69202e-08,
    "pp200_pt55_-1" : 1.43453e-09,

    "pp510_pt2_3"   : 29,
    "pp510_pt3_4"   : 6.1,
    "pp510_pt4_5"   : 1.8,
    "pp510_pt5_7"   : 0.9,
    "pp510_pt7_9"   : 0.17,
    "pp510_pt9_11"  : 0.052,
    "pp510_pt11_15" : 0.027,
    "pp510_pt15_20" : 0.0052,
    "pp510_pt20_25" : 0.0011,
    "pp510_pt25_35" : 0.00038,
    "pp510_pt35_45" : 4.3e-5,
    "pp510_pt45_55" : 9.1e-6,
    "pp510_pt55_-1" : 2.5e-6,
};
MB_TO_PB = 1e9

# corrections to cross sections reported incorrectly by Pythia
fudge_factors = {
    "pp200_pt2_3" : 1/1.228,
    "pp200_pt3_4" : 1/1.051,
    "pp200_pt4_5" : 1/1.014,
}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('pt_bin', type=str)
    parser.add_argument('input_file', type=str)
    parser.add_argument('--disable-ff', action='store_true')
    args = parser.parse_args(sys.argv[1:])
    if args.disable_ff:
        fudge_factor = 1.
    else:
        fudge_factor = fudge_factors.get(args.pt_bin, 1.)
    xsec = pt_xsec[args.pt_bin] * MB_TO_PB * fudge_factor
    aos = yoda.readYODA(args.input_file)
    num_entries = aos["/partonic_spectrum/partonic_pt"].totalDbn().numEntries()
    print("File {} from p_T bin {} has {} entries with cross-section {} pb".format(args.input_file, args.pt_bin, num_entries, xsec), file=sys.stderr)
    print("%e" % (xsec / num_entries))
