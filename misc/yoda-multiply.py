#!/usr/bin/env python3

import sys
import math

import yoda


def find_point(scatter, point):
    for p in scatter:
        if math.isclose(p.x(), point.x()):
            return p
    raise IndexError("Matching point not found")

def mul2d(s1, s2):
    s = yoda.Scatter2D()
    for p1 in s1:
        p2 = find_point(s2, p1)
        yerrs = (
            math.sqrt((p1.yErrs()[i] * p2.y())**2 + (p2.yErrs()[i] * p1.y())**2)
            for i in [0, 1]
        )
        s.addPoint(p1.x(), p1.y() * p2.y(), xerrs=p1.xErrs(), yerrs=yerrs)
    return s

if __name__ == '__main__':
    if (len(sys.argv) != 4):
        print(f"Usage: {sys.argv[0]} mult1_file mult2_file output_file")
        sys.exit()

    mult1_filename = sys.argv[1]
    mult2_filename = sys.argv[2]
    output_filename = sys.argv[3]

    aos1 = yoda.readYODA(mult1_filename)
    aos2 = yoda.readYODA(mult2_filename)

    out_aos = []
    for s1 in aos1.values():
        if isinstance(s1, yoda.core.Histo1D):
            s1 = s1.mkScatter()
        if not isinstance(s1, yoda.core.Scatter2D):
            continue
        if s1.path() not in aos2:
            continue

        s2 = aos2[s1.path()]
        assert isinstance(s2, yoda.core.Scatter2D)
        out_ao = mul2d(s1, s2)

        out_ao.setPath(s1.path())
        out_aos.append(out_ao)
    yoda.writeYODA(out_aos, output_filename)
