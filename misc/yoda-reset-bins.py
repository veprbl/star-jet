#!/usr/bin/env python3

import sys
import re

import yoda


def fill_from_bin(h, _bin):
    if _bin.sumW() == 0:
        return

    point = (_bin.xMean(),)
    if isinstance(_bin, yoda.HistoBin2D):
        point = point + (_bin.yMean(),) 
    point = (_bin.xMid(),)
    if isinstance(_bin, yoda.HistoBin2D):
        point = point + (_bin.yMid(),) 

    if _bin.numEntries() == 1:
        h.fill(*point, weight=_bin.sumW() / _bin.numEntries(), fraction=_bin.numEntries())
    elif _bin.numEntries() > 1:
        # See derivation https://gitlab.com/hepcedar/yoda/-/issues/38#note_646937796
        w2 = _bin.sumW() / _bin.numEntries() - 1. # arbitrary choice
        w1 = (
            (_bin.sumW2() - _bin.sumW() * w2)
            /
            (_bin.sumW() - _bin.numEntries() * w2)
        )
        f1 = (
            -(_bin.sumW() - _bin.numEntries() * w2) ** 2
            /
            (-_bin.sumW2() + 2 * _bin.sumW() * w2 - _bin.numEntries() * (w2 ** 2))
        )
        f2 = (
            (_bin.sumW() ** 2 - _bin.numEntries() * _bin.sumW2())
            /
            (-_bin.sumW2() + 2 * _bin.sumW() * w2 - _bin.numEntries() * (w2 ** 2))
        )
        h.fill(*point, weight=w1, fraction=f1)
        h.fill(*point, weight=w2, fraction=f2)
        bin_new = h.binAt(*point)
        import math
        print(_bin.sumW(), bin_new.sumW(), _bin.sumW2(), bin_new.sumW2(), _bin.numEntries(), bin_new.numEntries())
        assert math.isclose(_bin.sumW(), bin_new.sumW())
        assert math.isclose(_bin.sumW2(), bin_new.sumW2())
        ## FIXME: seems like rounding can make entries to be off by 1
        assert math.isclose(_bin.numEntries(), bin_new.numEntries(), rel_tol=0., abs_tol=1.)


def zero_out_bins(ao, cond):
    if not (isinstance(ao, yoda.Histo1D) or isinstance(ao, yoda.Histo2D)):
        print(f"Warning: {ao} is not a Histo1D or a Histo2D")
        return ao

    bins = ao.bins()
    def bin_to_dict(_bin):
        if isinstance(_bin, yoda.HistoBin1D):
            return {
                "x": _bin.xMid(),
            }
        elif isinstance(_bin, yoda.HistoBin2D):
            return {
                "x": _bin.xMid(),
                "y": _bin.yMid(),
            }
        else:
            raise ValueError(f"zeroed_bin: Argument of unexpected type {repr(type(_bin))}")
    conds = [eval(cond, dict(), bin_to_dict(_bin)) for _bin in ao.bins()]
    if any(conds):
        h = type(ao)(path=ao.path(), title=ao.title())
        # A hacky way to copy bins over
        # This likely doesn't preserve some global bin statistics
        if isinstance(ao, yoda.Histo1D):
             h.addBins([_bin.xEdges() for _bin in ao.bins()])
        elif isinstance(h, yoda.Histo2D):
             h.addBins([(tuple(_bin.xEdges()) + tuple(_bin.yEdges())) for _bin in ao.bins()])
        for cond, _bin in zip(conds, ao.bins()):
            if cond:
                fill_from_bin(h, _bin)
        return h
    else:
        return ao


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Zero out bins with specific condition. This can be used to apply a cut for an existing histogram.")
    parser.add_argument('input_filename')
    parser.add_argument('output_filename')
    parser.add_argument('-s', '--spec', nargs=2, metavar=('pattern', 'cond'), action='append')
    args = parser.parse_args()

    aos = yoda.readYODA(args.input_filename)

    specs = [(re.compile(regex), cond) for (regex, cond) in args.spec]

    for regex, cond in specs:
        for path, ao in aos.items():
            if not regex.search(path):
                continue

            aos[path] = zero_out_bins(ao, cond)

    yoda.writeYODA(aos, args.output_filename)
