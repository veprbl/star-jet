#!/usr/bin/env python3

import sys
import math

import yoda
import numpy as np
import pandas as pd
import scipy.optimize
import matplotlib as mpl
import matplotlib.pyplot as plt

import plot

filename = sys.argv[1]
ao = yoda.readYODA(filename)

plot.setup_presentation_style()
mpl.rcParams['axes.formatter.useoffset'] = False

def fit_vertex_z(fname, hist):
    xs = np.array([_bin.xMid() for _bin in hist])
    ys = np.array([_bin.height() for _bin in hist]) # todo numEntries?
    dys = np.array([max(1, _bin.heightErr()) for _bin in hist])
    rms = np.sqrt(np.sum(ys * (xs ** 2)) / np.sum(ys))

    var_names = []
    do_hg = fname.find("vpdmb") == -1

    def f(xs, mu, N, sigma):
        N = np.max(N, 0)
        return N * np.exp(-(xs - mu)**2 / 2 / sigma**2) / np.sqrt(2 * np.pi) / sigma

    def f_hg(xs, mu_hg, N_hg, sigma_hg, b_hg):
        return f(xs, mu_hg, N_hg, sigma_hg) / (1 + (xs / b_hg)**2)

    def f2(xs, mu2, N1, sigma1, r2, sigma2):
        N2 = r2 * (N1 / sigma1) * sigma2
        return f(xs, mu2, N1, sigma1) + f(xs, mu2, N2, sigma2)

    mu_min = -10. # cm
    mu_max = 10. # cm
    N_min = 0.
    N_max = np.inf
    sigma1_min = 10. # cm
    sigma1_max = rms
    sigma2_min = rms
    sigma2_max = 100 # cm

    f2_bounds = ([mu_min, N_min, sigma1_min, 0.0, sigma2_min], [mu_max, N_max, sigma1_max, 10.0, sigma2_max])

    f_hg_bounds = ([mu_min, N_min, rms, rms], [mu_max, N_max, sigma2_max, sigma2_max])

    vzmax = 60 # cm
    vzmax_fit = 200 # cm

    cond = np.abs(xs) < vzmax_fit
    par2, _ = scipy.optimize.curve_fit(
      f2, xs[cond], ys[cond],
      sigma=dys[cond],
      p0=(xs.mean(), ys.max() * rms, rms, 0.1, sigma2_max),
      bounds=f2_bounds,
      maxfev=100000,
    )
    mu2, N1, sigma1, r2, sigma2 = par2
    var_names += ["r2", "mu2", "sigma1", "sigma2"]
    N2 = r2 * (N1 / sigma1) * sigma2
    print(par2)
    e2 = (N1 * math.erf(vzmax / np.sqrt(2) / sigma1) + N2 * math.erf(vzmax / np.sqrt(2) / sigma2)) / (N1 + N2)

    cond = np.abs(xs) < 60
    par, _ = scipy.optimize.curve_fit(
      f, xs[cond], ys[cond],
      sigma=dys[cond],
      p0=(xs.mean(), ys.max(), rms),
      maxfev=100000,
    )
    mu, N, sigma = par
    var_names += ["mu", "sigma"]
    e = math.erf(vzmax / np.sqrt(2) / sigma)

    if do_hg:
        cond = np.abs(xs) < vzmax_fit
        par_hg, _ = scipy.optimize.curve_fit(
          f_hg, xs[cond], ys[cond],
          sigma=dys[cond],
          p0=(xs.mean(), ys.max(), rms, rms),
          bounds=f_hg_bounds,
          maxfev=100000,
        )
        mu_hg, N_hg, sigma_hg, b_hg = par_hg
        var_names += ["mu_hg", "sigma_hg", "b_hg"]
        e = math.erf(vzmax / np.sqrt(2) / sigma)

    print(f"e2/e = {e2/e}")

    _, ax = plt.subplots(3, sharex=True, gridspec_kw = { 'height_ratios': [2, 3, 1] }, figsize=(4, 6))

    plt.sca(ax[1])
    ref_line, = plt.plot(xs, ys, ls='none', marker='.')

    plot2, = plt.plot(
      xs, f2(xs, *par2),
      label=r"Double Gaussian"
        + "\n"
        + rf"$N_2/N_1 = {N2/N1:.2f}, \sigma_1 = {sigma1:.1f}$ cm, $\sigma_2 = {sigma2:.1f}$ cm, $\mu = {mu:.1f}$ cm"
        + "\n"
        + rf"Efficiency of the $60$ cm $v_z$ cut: $\epsilon_2 = {e2:.3f}$"
        ,
        lw=2,
    )

    if do_hg:
        plot_hg, = plt.plot(
          xs, f_hg(xs, *par_hg),
          label=r"Hourglass Gaussian"
            + "\n"
            + rf"$\sigma = {sigma_hg:.1f}$ cm, $\mu = {mu_hg:.1f}$ cm, $b = {b_hg:.1f}$ cm"
            ,
          color="C6",
          ls="--",
        )

    plot1, = plt.plot(
      xs, f(xs, *par),
      label=r"Single Gaussian"
        + "\n"
        + rf"$\sigma = {sigma:.1f}$ cm, $\mu = {mu:.1f}$ cm"
        + "\n"
        + rf"Efficiency of the $60$ cm $v_z$ cut: $\epsilon_1 = {e:.3f}$"
        ,
      color="C3",
    )

    plt.plot([], [], label=f"Summary: $\epsilon_2/\epsilon_1 = {e2/e:.3f}$", ls='none')

    plot.set_label(y=r"Yield")

    formatter = mpl.ticker.LogFormatterSciNotation(minor_thresholds=(np.inf, np.inf))
    plt.gca().yaxis.set_major_formatter(formatter)

    handles, labels = plt.gca().get_legend_handles_labels()
    plt.sca(ax[0])
    plt.gca().set_axis_off()
    plt.title(fname)
    plt.legend(handles, labels, loc="center", labelspacing=1)

    plt.sca(ax[2])
    plt.plot(xs, f2(xs, *par2) / f2(xs, *par2), color=plot2.get_color(), lw=plot2.get_linewidth())
    if do_hg:
        plt.plot(xs, f_hg(xs, *par_hg) / f2(xs, *par2), color=plot_hg.get_color(), ls=plot_hg.get_linestyle())
    plt.plot(xs, f(xs, *par) / f2(xs, *par2), color=plot1.get_color())
    plt.plot(xs, ys / f2(xs, *par2), ls='none', marker='.', color=ref_line.get_color())
    plt.ylim(0., 2.)
    plot.set_label(r"$v_z$, cm", r"Ratio")

    plt.sca(ax[1])
    plt.text(-vzmax_fit, plt.ylim()[1] / 2, "fit range", size=8, color="grey", rotation=90, ha="right", va="bottom")
    for _ax in ax[1:]:
        plt.sca(_ax)
        plt.axvline(-vzmax_fit, ls="--", color="grey", lw=1.)
        plt.axvline( vzmax_fit, ls="--", color="grey", lw=1.)

    plt.subplots_adjust(left=0.2, right=0.99, top=0.95, bottom=0.08)
    plt.subplots_adjust(hspace=0.05)
    plt.savefig(f"{fname}.pdf")
    plt.savefig(f"{fname}.png")
    plt.clf()

    s = yoda.Scatter2D()
    s.setPath(hist.path())
    if do_hg:
        ys = f_hg(xs, *par_hg)
        s.setAnnotation("Title", rf"$\sigma = {sigma_hg:.1f}$ cm, $\mu = {mu_hg:.1f}$ cm, $b = {b_hg:.1f}$ cm")
    else:
        ys = f(xs, *par)
        s.setAnnotation("Title", rf"$\sigma = {sigma:.1f}$~cm, $\mu = {mu:.1f}$~cm")
    for x, y, _bin in zip(xs, ys, hist):
        s.addPoint(x, y, xerrs=(_bin.xWidth()/2,)*2)

    _locals = locals()
    par_dict = dict(
        (var_name, _locals[var_name]) for var_name in var_names
    )
    return par_dict, s

histo_names = [
    ("vz_raw", "raw"),
    ("vertex_z", "jet"),
    ("vpd_vz", "vpd_jet"),
    ("vpd_vz_raw", "vpd_raw"),
]

var_names = ["mu", "sigma", "mu_hg", "sigma_hg", "b_hg", "r2", "mu2", "sigma1", "sigma2"]
df = pd.DataFrame(
    columns=pd.Index(var_names),
    index=pd.MultiIndex.from_product(
        [
            ["jp0", "jp1", "jp2", "zerobias", "vpdmb"],
            ["raw", "vpd_raw", "jet", "vpd_jet"],
        ],
        names=["Trigger", "Distribution"],
    )
)

aos = []
for histo_name, column_name in histo_names:
    print(ret := fit_vertex_z(f"{histo_name}_vpdmb", ao[f"/jet_quantities_vpdmb_nobsmd/{histo_name}"]))
    aos.append(ret[1])
    df.loc[("vpdmb", column_name)].update(ret[0])
    print(ret := fit_vertex_z(f"{histo_name}_jp0", ao[f"/jet_quantities_jp0_reweight/{histo_name}"]))
    aos.append(ret[1])
    df.loc[("jp0", column_name)].update(ret[0])
    print(ret := fit_vertex_z(f"{histo_name}_jp1", ao[f"/jet_quantities_jp1_reweight/{histo_name}"]))
    aos.append(ret[1])
    df.loc[("jp1", column_name)].update(ret[0])
    print(ret := fit_vertex_z(f"{histo_name}_jp2", ao[f"/jet_quantities_jp2_reweight/{histo_name}"]))
    aos.append(ret[1])
    df.loc[("jp2", column_name)].update(ret[0])
    #ao[f"/jet_quantities_zerobias/{histo_name}"].rebinBy(2)
    print(ret := fit_vertex_z(f"{histo_name}_zerobias", ao[f"/jet_quantities_zerobias/{histo_name}"]))
    aos.append(ret[1])
    df.loc[("zerobias", column_name)].update(ret[0])

with pd.option_context("display.precision", 3):
    print(df)

with open("par.tex", "wt") as fp:
    fp.write(df.to_latex())

yoda.writeYODA(aos, "output.yoda")
