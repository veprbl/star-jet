#!/usr/bin/env python2

import sys

import yoda

filename = sys.argv[1]
ao = yoda.readYODA(filename)

for jp in [0, 1, 2]:
    path_prefix = "/unfolding_jp{}".format(jp)
    fake_ratio = ao[path_prefix + "/fake"] / ao[path_prefix + "/detector_jet_pt"]
    fake_ratio.path = path_prefix + "/fake_ratio"
    ao[fake_ratio.path] = fake_ratio
    miss_ratio = ao[path_prefix + "/miss"] / ao[path_prefix + "/particle_jet_pt"]
    miss_ratio.path = path_prefix + "/miss_ratio"
    ao[miss_ratio.path] = miss_ratio
    detector_matching_ratio = ao[path_prefix + "/matched_detector_jet_pt"] / ao[path_prefix + "/detector_jet_pt"]
    detector_matching_ratio.path = path_prefix + "/detector_matching_ratio"
    ao[detector_matching_ratio.path] = detector_matching_ratio
    particle_matching_ratio = ao[path_prefix + "/matched_particle_jet_pt"] / ao[path_prefix + "/particle_jet_pt"]
    particle_matching_ratio.path = path_prefix + "/particle_matching_ratio"
    ao[particle_matching_ratio.path] = particle_matching_ratio

yoda.writeYODA(ao, "output.yoda")
