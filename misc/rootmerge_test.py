import unittest
from tempfile import NamedTemporaryFile
import ROOT

import rootmerge

class TestROOTMerge(unittest.TestCase):

    def setUp(self):
        self.f1s = NamedTemporaryFile()
        self.f1 = ROOT.TFile(self.f1s.name, 'RECREATE')
        self.f1.cd()
        h = ROOT.TH1D("h", "", 3, 0., 3.)
        h.Fill(0.)
        self.f1.WriteTObject(h)
        self.f1.Close()
        del self.f1
        del h
        self.f2s = NamedTemporaryFile()
        self.f2 = ROOT.TFile(self.f2s.name, 'RECREATE')
        self.f2.cd()
        h = ROOT.TH1D("h", "", 3, 0., 3.)
        h.Fill(2.)
        h2 = ROOT.TH1D("h2", "", 3, 0., 3.)
        h2.Fill(1.)
        self.f2.WriteTObject(h)
        self.f2.WriteTObject(h2)
        self.f2.Close()
        del self.f2
        del h
        del h2

    def test_merge1(self):
        rootmerge.merge_files([self.f1s.name], { self.f1s.name : 100 }, "output.root")
        fo = ROOT.TFile("output.root", 'READ')
        h = fo.Get("h")
        assert h
        self.assertEqual(h.GetBinContent(h.FindBin(0.)), 100.0)
        del fo

    def test_merge2(self):
        rootmerge.merge_files([self.f1s.name, self.f2s.name], { self.f1s.name : 1.0, self.f2s.name : 1.0 }, "output.root")
        fo = ROOT.TFile("output.root", 'READ')
        h = fo.Get("h")
        assert h
        self.assertEqual(h.GetBinContent(h.FindBin(0.)), 1.0)
        self.assertEqual(h.GetBinContent(h.FindBin(1.)), 0.0)
        self.assertEqual(h.GetBinContent(h.FindBin(2.)), 1.0)
        h2 = fo.Get("h2")
        self.assertEqual(h2.GetBinContent(h2.FindBin(0.)), 0.0)
        self.assertEqual(h2.GetBinContent(h2.FindBin(1.)), 1.0)
        self.assertEqual(h2.GetBinContent(h2.FindBin(2.)), 0.0)
        del fo

    def test_merge_same(self):
        rootmerge.merge_files([self.f1s.name, self.f1s.name], { self.f1s.name : 1.0, self.f1s.name : 1.0 }, "output.root")
        fo = ROOT.TFile("output.root", 'READ')
        h = fo.Get("h")
        assert h
        self.assertEqual(h.GetBinContent(h.FindBin(0.)), 2.0)
        self.assertEqual(h.GetBinContent(h.FindBin(1.)), 0.0)
        self.assertEqual(h.GetBinContent(h.FindBin(2.)), 0.0)
        del fo

if __name__ == '__main__':
    unittest.main()
