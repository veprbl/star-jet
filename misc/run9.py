import yoda

bins = [9.7, 11.5, 13.6, 16.1, 19.0, 22.5, 26.6, 31.4, 37.2, 44.0, 52.0]
values = """\
10.559032540771218, 1.8309381592531186
12.52251705151653, 0.5463171794551612
14.857241178936132, 0.19630406500402664
17.376281245237, 0.06135907273413163
20.45087686709342, 0.018095715412433183
24.15544981710104, 0.005154287336670212
28.637974394147218, 0.0012472674708860429
33.52721098155767, 0.00027827112971828626
39.67727147157471, 0.00005336407926175475
46.64162522862398, 0.000007565046215441458"""
values = [(float(v) for v in l.split(", ")) for l in values.splitlines()]

MB_TO_PB = 1e6

s = yoda.Scatter2D("/result/jet_pt")

for bin_low, bin_high, (x, y) in zip(bins, bins[1:], values):
    assert bin_low <= x <= bin_high
    bin_x = (bin_high + bin_low) / 2
    bin_width = bin_high - bin_low
    s.addPoint(bin_x, y * MB_TO_PB, xerrs=[bin_width/2, bin_width/2])

yoda.writeYODA([s], "run9.yoda")
