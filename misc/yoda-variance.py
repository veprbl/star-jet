#!/usr/bin/env python3

import argparse
import math

import yoda

def find_point(scatter, p):
    for point in scatter:
        if point.x() == p.x():
            return point
    raise IndexError("Matching point not found")

def variance(aos, central_ao=None):
    if central_ao is not None:
        any_ao = central_ao
    else:
        any_ao = aos[0]
    s = yoda.Scatter2D(any_ao.path())
    if isinstance(any_ao, yoda.core.Histo1D):
        for hbin in any_ao:
            x = hbin.xMid()
            ys = [ao.binAt(x).height() for ao in aos]
            if central_ao is not None:
                y_mean = central_ao.binAt(x).height()
            else:
                y_mean = sum(ys) / len(ys)
            y2 = [(y - y_mean)**2 for y in ys]
            dy = math.sqrt(sum(y2) / len(y2))
            s.addPoint(x, y_mean, xerrs=[hbin.xWidth()/2, hbin.xWidth()/2], yerrs=[dy, dy])
    elif isinstance(any_ao, yoda.core.Scatter2D):
        for point in any_ao:
            x = point.x()
            ys = [find_point(ao, point).y() for ao in aos]
            if central_ao is not None:
                y_mean = find_point(central_ao, point).y()
            else:
                y_mean = sum(ys) / len(ys)
            y2 = [(y - y_mean)**2 for y in ys]
            dy = math.sqrt(sum(y2) / len(y2))
            s.addPoint(x, y_mean, xerrs=point.xErrs(), yerrs=[dy, dy])
    else:
        return None
    return s

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", required=True, help="Path to the output file")
    parser.add_argument("--central", default=None, help="Central values input file")
    parser.add_argument("inputs", nargs='*', help="Input files")
    args = parser.parse_args()

    inputs = list(map(yoda.readYODA, args.inputs))

    if args.central is not None:
        central_aos = yoda.readYODA(args.central)
        paths = set(central_aos.keys())
    else:
        paths = set(sum([list(_input.keys()) for _input in inputs], []))

    output_aos = []
    for path in paths:
        aos = [_input[path] for _input in inputs if path in _input]
        if args.central is not None:
            central_ao = central_aos[path]
        else:
            central_ao = None
        variance_ao = variance(aos, central_ao)
        if variance_ao is not None:
            output_aos.append(variance_ao)
    yoda.write(output_aos, args.output)
