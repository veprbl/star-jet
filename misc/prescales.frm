Symbol JP0F, JP1F, JP2F, JP0SF, JP1SF, JP2SF, ps0, ps1, ps2;

Local Promotion0 = (1-(1-JP0F))*(1-JP1SF)*JP0SF;
Local Promotion1 = (1-(1-JP0F)*(1-JP1F))*(1-JP2SF)*JP1SF;
Local Promotion2 = (1-(1-JP0F)*(1-JP1F)*(1-JP2F))*JP2SF;

Local Demotion0 = JP0F*JP0SF;
Local Demotion1 = (1-JP0F)*JP1F*JP1SF;
Local Demotion2 = (1-JP0F)*(1-JP1F)*JP2F*JP2SF;

* "fire" implies "should fire"
id JP2F = JP2F*JP2SF;
id JP2F = JP1SF*JP2F;
id JP1F = JP1SF*JP1F;
id JP0F = JP0SF*JP0F;

* "should fire" implies "fire" with a prescale
id JP0F*JP0SF = ps0*JP0SF;
id JP0F*JP1SF = ps0*JP1SF;
id JP0F*JP2SF = ps0*JP2SF;
id JP1F*JP1SF = ps1*JP1SF;
id JP1F*JP2SF = ps1*JP2SF;
id JP2F*JP2SF = JP2SF;

* Reduce extra "should fire"'s
repeat;
id JP0SF*JP2SF = JP2SF;
id JP1SF*JP2SF = JP2SF;
id JP2SF*JP2SF = JP2SF;
id JP0SF*JP1SF = JP1SF;
id JP1SF*JP1SF = JP1SF;
id JP0SF*JP0SF = JP0SF;
endrepeat;

Print;
.end