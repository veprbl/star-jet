{ nixpkgs ? import <nixpkgs> {} }:

with nixpkgs;

let
  overlay = final: prev: {
    rivet_mkhtml_font = "cm";
    #mkhtml = yoda_files: extra_args: final.submit_as_job ((prev.mkhtml yoda_files extra_args).overrideAttrs (prev: {
    #  buildPhase = builtins.replaceStrings [ "flock" ] [ "#" ] prev.buildPhase;
    #}));
  };
  # TODO override everything
  override_if_supported = attrset: overlay:
    if attrset ? override then
      attrset.override overlay
    else
      attrset;

  star-jet' = path: rev: override_if_supported (import (builtins.fetchGit {
    #url = "https://gitlab.com/veprbl/star-jet.git";
    url = "https://git.sr.ht/~veprbl/star-jet";
    inherit rev;
  } + path) { inherit nixpkgs; }) overlay;
  star-jet = rev: star-jet' /. rev;

  qa_size = final: prev: {
    embed_factor = pt_bin: input: final.submit_as_job (prev.embed_factor pt_bin input);
    embed_merge = inputs: final.submit_as_job ((prev.embed_merge inputs).overrideAttrs (prev: {
      __htcondor_request_memory = "4G";
      buildPhase = builtins.replaceStrings [ "flock" ] [ "#" ] prev.buildPhase;
    }));
  };
in
  rec {
    inherit
      ((star-jet "b051f858ec24ffd56973bf5c2c2fd97ca4b0c04c").enable_lualatex.override qa_size)
      data_qa
      embed_qa
      ;
    inherit
      (star-jet "b051f858ec24ffd56973bf5c2c2fd97ca4b0c04c")
      summary
      sector20_qa
      sector20_region
      ue_density_cut
      data_jp_compare
      data_jp_vertex_compare
      data_trigger_compare
      dijet_monojet_study
      pythia_fudge_factors_study
      pythia_fudge_factors_study_nodata
      jes_charged_embed
      ue_effect
      xsec_plots
      trig_reweighting_cross_check
      unfold_pdf_reweight
      rt_reweight_study
      rt_reweight_study_unfold
      two_vtx_study
      track_cut_study
      summary_jp_compare
      embed_trig_compare
      data_vertex_fit_plot
      ;
    inherit
      (star-jet' /MC/fudge_factor_study "b051f858ec24ffd56973bf5c2c2fd97ca4b0c04c")
      fudge_factors
      soft_reweight_fit
      ckin_cuts_effect
      ckin_cuts_effect2_with_mi
      ckin_cuts_effect2_without_mi
      partonic_bin_outliers
      ;
    inherit
      (star-jet' /MC/tune_study "b051f858ec24ffd56973bf5c2c2fd97ca4b0c04c")
      parp90_var
      parp90_var_ref_starugia
      parp91_var
      ;
    inherit
      (star-jet' /MC/pdf_reweighting_study "b051f858ec24ffd56973bf5c2c2fd97ca4b0c04c")
      change_model
      ;
    inherit
      (star-jet' /MC/hadronization_study "b051f858ec24ffd56973bf5c2c2fd97ca4b0c04c")
      f_had_model_change
      f_had_parp90
      ;
    inherit
      (star-jet' /study/qcd-analysis "b051f858ec24ffd56973bf5c2c2fd97ca4b0c04c")
      xsec_change
      fit
      pseudo
      ;
    inherit
      (import ./. {})
      ;

    all = runCommandNoCC "all" {
      inherit
        summary
        summary_jp_compare
        embed_trig_compare
        data_jp_compare
        data_jp_vertex_compare
        data_trigger_compare
        data_vertex_fit_plot
        dijet_monojet_study
        pythia_fudge_factors_study
        pythia_fudge_factors_study_nodata
        jes_charged_embed
        ue_effect
        unfold_pdf_reweight
        rt_reweight_study
        rt_reweight_study_unfold
        two_vtx_study
        sector20_qa
        sector20_region
        track_cut_study
        embed_qa
        data_qa
        #ue_density_cut
        fudge_factors
        soft_reweight_fit
        ckin_cuts_effect
        ckin_cuts_effect2_with_mi
        ckin_cuts_effect2_without_mi
        partonic_bin_outliers
        change_model
        parp90_var
        parp90_var_ref_starugia
        parp91_var
        f_had_model_change
        f_had_parp90
        xsec_change
        fit
        pseudo
        ;
      inherit (xsec_plots.single_trigger) jppromoted_combined;
      jppromoted_combined_mc = xsec_plots.compare_mc.single_trigger.jppromoted_combined;
      jppromoted_combined_nlo_pdfs = xsec_plots.compare_nlo_pdfs.single_trigger.jppromoted_combined;
      inherit (xsec_plots) all_triggers demoted_promoted_triggers f_had_plot combined_latex_table;
      trig_reweighting_cross_check_data_demoted = trig_reweighting_cross_check.data.demoted;
      trig_reweighting_cross_check_data_promoted = trig_reweighting_cross_check.data.promoted;
      trig_reweighting_cross_check_embed_demoted = trig_reweighting_cross_check.embed.demoted;
      trig_reweighting_cross_check_embed_promoted = trig_reweighting_cross_check.embed.promoted;
      preferLocalBuild = true;
    } ''
      mkdir "$out"
      ln -s "$summary" "$out/summary"
      ln -s "$summary_jp_compare" "$out/summary_jp_compare"
      ln -s "$embed_trig_compare" "$out/embed_trig_compare"
      ln -s "$data_jp_compare" "$out/data_jp_compare"
      ln -s "$data_jp_vertex_compare" "$out/data_jp_vertex_compare"
      ln -s "$data_trigger_compare" "$out/data_trigger_compare"
      ln -s "$data_vertex_fit_plot" "$out/data_vertex_fit_plot"
      ln -s "$dijet_monojet_study" "$out/dijet_monojet_study"
      ln -s "$pythia_fudge_factors_study" "$out/pythia_fudge_factors_study"
      ln -s "$pythia_fudge_factors_study_nodata" "$out/pythia_fudge_factors_study_nodata"
      ln -s "$jes_charged_embed" "$out/jes_charged_embed"
      ln -s "$ue_effect" "$out/ue_effect"
      ln -s "$unfold_pdf_reweight" "$out/unfold_pdf_reweight"
      ln -s "$rt_reweight_study" "$out/rt_reweight_study"
      ln -s "$rt_reweight_study_unfold" "$out/rt_reweight_study_unfold"
      ln -s "$two_vtx_study" "$out/two_vtx_study"
      ln -s "$sector20_qa" "$out/sector20_qa"
      ln -s "$sector20_region" "$out/sector20_region"
      ln -s "$track_cut_study" "$out/track_cut_study"
      ln -s "$embed_qa" "$out/embed_qa"
      ln -s "$data_qa" "$out/data_qa"
      #ln -s "$ue_density_cut" "$out/ue_density_cut"
      ln -s "$jppromoted_combined" "$out/jppromoted_combined"
      ln -s "$jppromoted_combined_mc" "$out/jppromoted_combined_mc"
      ln -s "$jppromoted_combined_nlo_pdfs" "$out/jppromoted_combined_nlo_pdfs"
      ln -s "$all_triggers" "$out/all_triggers"
      ln -s "$demoted_promoted_triggers" "$out/demoted_promoted_triggers"
      ln -s "$f_had_plot" "$out/f_had_plot"
      ln -s "$combined_latex_table" "$out/combined_latex_table.tex"
      ln -s "$trig_reweighting_cross_check_data_demoted" "$out/trig_reweighting_cross_check_data_demoted"
      ln -s "$trig_reweighting_cross_check_data_promoted" "$out/trig_reweighting_cross_check_data_promoted"
      ln -s "$trig_reweighting_cross_check_embed_demoted" "$out/trig_reweighting_cross_check_embed_demoted"
      ln -s "$trig_reweighting_cross_check_embed_promoted" "$out/trig_reweighting_cross_check_embed_promoted"

      mkdir -p "$out/MC/fudge_factor_study"
      ln -s "$fudge_factors" "$out/MC/fudge_factor_study/fudge_factors"
      ln -s "$soft_reweight_fit" "$out/MC/fudge_factor_study/soft_reweight_fit"
      ln -s "$ckin_cuts_effect" "$out/MC/fudge_factor_study/ckin_cuts_effect"
      ln -s "$ckin_cuts_effect2_with_mi" "$out/MC/fudge_factor_study/ckin_cuts_effect2_with_mi"
      ln -s "$ckin_cuts_effect2_without_mi" "$out/MC/fudge_factor_study/ckin_cuts_effect2_without_mi"
      ln -s "$partonic_bin_outliers" "$out/MC/fudge_factor_study/partonic_bin_outliers"

      mkdir -p "$out/MC/tune_study"
      ln -s "$change_model" "$out/MC/tune_study/change_model"
      ln -s "$parp90_var" "$out/MC/tune_study/parp90_var"
      ln -s "$parp90_var_ref_starugia" "$out/MC/tune_study/parp90_var_ref_starugia"
      ln -s "$parp91_var" "$out/MC/tune_study/parp91_var"

      mkdir -p "$out/MC/hadronization_study"
      ln -s "$f_had_model_change" "$out/MC/hadronization_study/f_had_model_change"
      ln -s "$f_had_parp90" "$out/MC/hadronization_study/f_had_parp90"

      mkdir -p "$out/study/qcd-analysis"
      ln -s "$xsec_change" "$out/study/qcd-analysis/xsec_change"
      ln -s "$fit" "$out/study/qcd-analysis/fit"
      ln -s "$pseudo" "$out/study/qcd-analysis/pseudo"
    '';
  }
