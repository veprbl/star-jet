{ pkgs ? <nixpkgs> }:

with import pkgs {};
let
  blddir = "nixbld";
in
stdenv.mkDerivation {
  name = "jet-env";
  shellHook = ''
    export _PROMPT="PROMPT=\"\$PROMPT jet-shell > \""
  '';
  buildInputs = [
    root
    yoda
  ];
}
