#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class StJetCandidate+;
#pragma link C++ class StJetElement+;
#pragma link C++ class StJetEvent+;
#pragma link C++ class StJetEventCommon+;
#pragma link C++ class StJetParticle+;
#pragma link C++ class StJetTower+;
#pragma link C++ class StJetTrack+;
#pragma link C++ class StJetVertex+;
#pragma link C++ class StUeEvent+;

#pragma link C++ class std::vector<StJetTower*>+;
#pragma link C++ class std::vector<StJetTrack*>+;

#endif
