#!/usr/bin/env python2

from __future__ import print_function

import argparse
import sys
import yoda

pt_xsec = { # in mb
    "pp200_pt2_3"   : 8.998960e+00,
    "pp200_pt3_4"   : 1.463983e+00,
    "pp200_pt4_5"   : 3.543083e-01,
    "pp200_pt5_7"   : 1.529195e-01,
    "pp200_pt7_9"   : 2.494312e-02,
    "pp200_pt9_11"  : 5.851208e-03,
    "pp200_pt11_15" : 2.305567e-03,
    "pp200_pt15_20" : 3.465733e-04,
    "pp200_pt20_25" : 4.623700e-05,
    "pp200_pt25_35" : 9.962600e-06,
    "pp200_pt35_-1" : 4.957917e-07,
};
MB_TO_PB = 1e9

# corrections to cross sections reported incorrectly by Pythia
fudge_factors = {
    "pp200_pt2_3" : 1/1.228,
    "pp200_pt3_4" : 1/1.051,
    "pp200_pt4_5" : 1/1.014,
}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('pt_bin', type=str)
    parser.add_argument('input_file', type=str)
    parser.add_argument('--disable-ff', action='store_true')
    args = parser.parse_args(sys.argv[1:])
    if args.disable_ff:
        fudge_factor = 1.
    else:
        fudge_factor = fudge_factors.get(args.pt_bin, 1.)
    xsec = pt_xsec[args.pt_bin] * MB_TO_PB * fudge_factor
    aos = yoda.readYODA(args.input_file)
    num_entries = aos["/partonic_spectrum/partonic_pt"].totalDbn.numEntries
    print("File {} from p_T bin {} has {} entries with cross-section {} mb".format(args.input_file, args.pt_bin, num_entries, xsec), file=sys.stderr)
    print("%e" % (xsec / num_entries))
