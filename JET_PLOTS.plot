# BEGIN PLOT /partonic_spectrum/partonic_pt
Title=Raw partonic $p_T$ spectrum
XLabel=$\hat{p}_T$, GeV
YLabel=$d\sigma/dp_T$ [pb/GeV]
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp0.*
Title=JP0
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp1.*
Title=JP1
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp2.*
Title=JP2
# END PLOT
# BEGIN PLOT /jet_quantities_vpdmb.*
Title=VPDMB
# END PLOT
# BEGIN PLOT /jet_quantities_vpdmb_nobsmd.*
Title=VPDMB NOBSMD
# END PLOT
# BEGIN PLOT /jet_quantities_vpdmb_nobsmd_jp0_simu.*
Title=VPDMB NOBSMD \&\& JP0 simu
# END PLOT
# BEGIN PLOT .*/jet_eta
XLabel=jet $\eta$
# END PLOT
# BEGIN PLOT .*/jet_phi
XLabel=jet $\phi$
# END PLOT
# BEGIN PLOT .*/jet_pt
XLabel=jet $p_T$
# END PLOT
# BEGIN PLOT .*/jet_tower_mult
XLabel=number of towers in the jet
# END PLOT
# BEGIN PLOT .*/jet_track_mult
XLabel=number of tracks in the jet
# END PLOT
# BEGIN PLOT .*/jet_(track|tower|subtr_tower)_(pt|mult)_vs_jet_pt
LogY=0
XLabel=jet $p_T$
# END PLOT
# BEGIN PLOT .*/jet_track_pt_vs_jet_pt
YLabel=average track $p_T$
# END PLOT
# BEGIN PLOT .*/jet_tower_pt_vs_jet_pt
YLabel=average tower $p_T$
# END PLOT
# BEGIN PLOT .*/jet_track_mult_vs_jet_pt
YLabel=average track multiplicity
# END PLOT
# BEGIN PLOT .*/jet_tower_mult_vs_jet_pt
YLabel=average tower multiplicity
# END PLOT
# BEGIN PLOT .*/jet_subtr_tower_mult_vs_jet_pt
YLabel=average fully subtracted tower multiplicity
# END PLOT
# BEGIN PLOT .*/jet_ue_.*_vs.*_jet_pt.*
LogY=0
XLabel=jet $p_T$
# END PLOT
# BEGIN PLOT .*/jet_ue_density_vs_jet_pt
YLabel=average cone UE density
# END PLOT
# BEGIN PLOT .*/jet_ue_nchg_vs_jet_pt
YLabel=average $N_{\text{chg}}$
# END PLOT
# BEGIN PLOT .*/jet_ue_density_vs_jet_pt(|_corrected)
YMax=2.
# END PLOT
# BEGIN PLOT .*/jet_ue_(chg|neut)_mult_vs_jet_pt
YMin=0.2
YMax=2.6
# END PLOT
# BEGIN PLOT .*/jet_ue_(chg|neut)_sumpt_vs_jet_pt
YMax=1.2
# END PLOT
# BEGIN PLOT .*/jet_ue_neut_mult_vs_jet_pt
YLabel=average tower multiplicy (after subtraction)
# END PLOT
# BEGIN PLOT .*/jet_ue_neut_sumpt_vs_jet_pt
YLabel=average tower $\sum p_T$
# END PLOT
# BEGIN PLOT .*/jet_ue_chg_mult_vs_jet_pt
YLabel=average track multiplicy
# END PLOT
# BEGIN PLOT .*/jet_ue_chg_sumpt_vs_jet_pt
YLabel=average track $\sum p_T$
# END PLOT
# BEGIN PLOT .*/jet_pt_vs_vz
YLabel=average $p_T$ of a jet in the primary vertex
# END PLOT
# BEGIN PLOT .*/jet_rt_vs_vz
YLabel=average $R_T$ of a jet in the primary vertex
# END PLOT
# BEGIN PLOT .*/jet_rt_vs_pt
LogY=0
YLabel=average $R_T$
XLabel=jet $p_T$
YMin=0.0
YMax=1.0
# END PLOT
# BEGIN PLOT .*/jet_rt_vs.*_eta
LogY=0
YLabel=average $R_T$
XLabel=jet $\eta$
# END PLOT
# BEGIN PLOT .*/jet_rt_vs.*_eta
XLabel=jet detector $\eta$
# END PLOT
# BEGIN PLOT .*/num_vertex_vs_vz
YLabel=number of vertices in the event
# END PLOT
# BEGIN PLOT .*/num_jets_vs_vz
YLabel=number of jets in the event
# END PLOT
# BEGIN PLOT .*/vertex_x
XLabel=primary vertex x
# END PLOT
# BEGIN PLOT .*/vertex_y
XLabel=primary vertex y
# END PLOT
# BEGIN PLOT .*/vertex_z
XLabel=primary vertex z
# END PLOT
# BEGIN PLOT .*/.*vertex_[xyz]
Title=Cut: At least one jet in vertex has $\eta < 0.8$, $R_T < 0.95$, JPx-matched
# END PLOT
# BEGIN PLOT .*/.*_vs_vz
XLabel=primary vertex z
LogY=0
ConnectGaps=0
ErrorBars=1
# END PLOT
# BEGIN PLOT .*/gvz_vs_vz
XLabel=primary vertex z
YLabel=generator vertex z
Legend=0 # 2d histogram, filled for embedding only
LogY=0
# END PLOT
# BEGIN PLOT .*/vpd_vz_vs_vz
LogZ=1
# END PLOT
# BEGIN PLOT /triggers/trig_coincedence
LogZ=1
# END PLOT
# BEGIN PLOT /unfolding.*/.*_ratio$
LogY=0
YMin=0.
YMax=1.
# END PLOT
# BEGIN PLOT /unfolding.*/detector_matching_ratio
YLabel=matched detector jets $/$ all detector jets
# END PLOT
# BEGIN PLOT /unfolding.*/particle_matching_ratio
YLabel=matched particle jets $/$ all particle jets
# END PLOT
# BEGIN PLOT /unfolding.*/pt_ratio_vs_det_pt.*
LegendYPos=0.4
LogY=0
YMin=0.
YMax=2.
XLabel=detector jet $p_T$
YLabel=detector jet $p_T$ / particle jet $p_T$
# END PLOT
# BEGIN PLOT /unfolding.*/response
LogZ=1
# END PLOT
# BEGIN PLOT /unfolding.*/.*detector.*
XLabel=detector jet $p_T$
# END PLOT
# BEGIN PLOT /unfolding.*/.*particle.*
XLabel=particle jet $p_T$
# END PLOT
# BEGIN PLOT /unfolding.*/particle_jet_pt
YMin=1e-2
# END PLOT
# BEGIN PLOT /unfolding.*/fake.*
XLabel=detector jet $p_T$
# END PLOT
# BEGIN PLOT /unfolding.*/miss.*
XLabel=particle jet $p_T$
# END PLOT
# BEGIN PLOT /unfolding.*/response
XLabel=detector jet $p_T$
YLabel=particle jet $p_T$
# END PLOT
# BEGIN PLOT /unfolding.*/(ue|jet)_.*_corr.*
LogZ=1
# END PLOT
# BEGIN PLOT /unfolding.*/jet_chg_mult_corr
XLabel=jet track mutlitplicy
YLabel=jet charged particle multiplicy
# END PLOT
# BEGIN PLOT /unfolding.*/jet_neut_mult_corr
XLabel=jet tower mutlitplicy
YLabel=jet neutral particle multiplicy
# END PLOT
# BEGIN PLOT /unfolding.*/jet_chg_sumpt_corr
XLabel=jet track $\sum p_T$
YLabel=jet charged particle $\sum p_T$
# END PLOT
# BEGIN PLOT /unfolding.*/jet_neut_sumpt_corr
XLabel=jet tower $\sum p_T$
YLabel=jet neutral particle $\sum p_T$
# END PLOT
# BEGIN PLOT /unfolding.*/ue_sumpt_corr
XLabel=detector cone UE $\sum p_T$
YLabel=particle cone UE $\sum p_T$
# END PLOT
# BEGIN PLOT /unfolding.*/ue_chg_mult_corr
XLabel=cone UE track mutlitplicy
YLabel=cone UE charged particle multiplicy
# END PLOT
# BEGIN PLOT /unfolding.*/ue_neut_mult_corr
XLabel=cone UE tower mutlitplicy
YLabel=cone UE neutral particle multiplicy
# END PLOT
# BEGIN PLOT /unfolding.*/ue_chg_sumpt_corr
XLabel=cone UE track $\sum p_T$
YLabel=cone UE charged particle $\sum p_T$
# END PLOT
# BEGIN PLOT /unfolding.*/ue_neut_sumpt_corr
XLabel=cone UE tower $\sum p_T$
YLabel=cone UE neutral particle $\sum p_T$
# END PLOT
# BEGIN PLOT /unfolding_result.*/b
XLabel=detector jet $p_T$
# END PLOT
# BEGIN PLOT /unfolding_result.*/x_(b_uncorr|b_corr|embed)
XLabel=particle jet $p_T$
# END PLOT
