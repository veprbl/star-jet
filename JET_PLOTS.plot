# BEGIN PLOT .*
LeftMargin=1.5
RightMargin=0.3
# END PLOT
# BEGIN PLOT /partonic_spectrum/partonic_pt
Title=Raw partonic $p_{\text{T}}$ spectrum
XLabel=$\hat{p}_{\text{T}}$, GeV
YLabel=$d\sigma/dp_{\text{T}}$, pb/GeV
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp0.*
Title=JP0
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp1.*
Title=JP1
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp2.*
Title=JP2
# END PLOT
# BEGIN PLOT /jet_quantities_vpdmb.*
Title=VPDMB
# END PLOT
# BEGIN PLOT /jet_quantities_vpdmb_nobsmd.*
Title=VPDMB NOBSMD
# END PLOT
# BEGIN PLOT /jet_quantities_vpdmb_nobsmd_jp0_simu.*
Title=VPDMB NOBSMD \&\& JP0 simu
# END PLOT
# BEGIN PLOT /jet_quantities_zerobias.*
Title=Zerobias
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp0_promoted.*
Title=JP0 promoted
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp1_promoted.*
Title=JP1 promoted
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp2_promoted.*
Title=JP2 promoted
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp0_demoted.*
Title=JP0 demoted
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp1_demoted.*
Title=JP1 demoted
# END PLOT
# BEGIN PLOT /(jet_quantities|unfolding|unfolding_result)_jp2_demoted.*
Title=JP2 demoted
# END PLOT
# BEGIN PLOT .*/jet_eta
XLabel=jet $\eta$
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}\eta$, @sigma_unit@
# END PLOT
# BEGIN PLOT .*/jet_phi
XLabel=jet $\varphi$
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}\varphi$, @sigma_unit@
# END PLOT
# BEGIN PLOT /jet_.*/jet_pt
XLabel=jet $p_{\text{T}}$, GeV
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT .*/jet_charged_pt
XLabel=jet $p_{\text{T}} * (1 - \text{jet }R_{\text{T}})$, GeV
# END PLOT
# BEGIN PLOT .*/jet_neutral_pt
XLabel=jet $p_{\text{T}} * \text{jet }R_{\text{T}}$, GeV
# END PLOT
# BEGIN PLOT .*/jet_rt
XLabel=jet $R_{\text{T}}$
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}R_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT .*/jet_tower_eta$
XLabel=jet tower $\eta$
# END PLOT
# BEGIN PLOT .*/jet_tower_phi$
XLabel=jet tower $\varphi$
# END PLOT
# BEGIN PLOT .*/jet_tower_z$
XLabel=jet tower $z$
# END PLOT
# BEGIN PLOT .*/jet_track_eta$
XLabel=jet track $\eta$
# END PLOT
# BEGIN PLOT .*/jet_track_phi$
XLabel=jet track $\varphi$
# END PLOT
# BEGIN PLOT .*/jet_track_z$
XLabel=jet track $z$
# END PLOT
# BEGIN PLOT .*/jet_tower_mult$
XLabel=number of towers in the jet
# END PLOT
# BEGIN PLOT .*/.*_vs_runindex.*
XLabel=run index
LogY=0
# END PLOT
# BEGIN PLOT .*/jet_tower_mult_vs_runindex.*
YLabel=avg. \# of towers in a jet
YMax=16.0
YMin=0
# END PLOT
# BEGIN PLOT .*/jet_subtr_tower_mult_vs_runindex.*
YLabel=avg. \# fully subtr. towers in a jet
YMax=3.0
YMin=0
# END PLOT
# BEGIN PLOT .*/jet_tower_pt_vs_runindex.*
YLabel=avg. $p_{\text{T}}$ of jet towers, GeV
YMax=2.0
YMin=0
# END PLOT
# BEGIN PLOT .*/jet_track_mult$
XLabel=number of tracks in the jet
# END PLOT
# BEGIN PLOT .*/jet_track_dcad_vs_runindex.*
YLabel=avg. signed DCA for tracks in a jet, cm
YMax=0.5
YMin=-0.5
# END PLOT
# BEGIN PLOT .*/jet_track_mult_vs_runindex.*
YLabel=avg. \# of tracks in a jet
YMax=7.0
YMin=0
# END PLOT
# BEGIN PLOT .*/jet_track_pt_vs_runindex.*
YLabel=avg. $p_{\text{T}}$ of jet tracks, GeV
YMax=2.0
YMin=0
# END PLOT
# BEGIN PLOT .*/jet_rt_vs_.*
YLabel=average jet $R_{\text{T}}$
# END PLOT
# BEGIN PLOT .*/jet_(bemc|eemc)_tower_id
XLabel=Tower id
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT
# BEGIN PLOT .*/jet_track_eta
LogY=0
XLabel=track $\eta$
YLabel=yield of tracks from jets
# END PLOT
# BEGIN PLOT .*/jet_area_vs_jet_pt
LogY=0
XLabel=jet $p_{\text{T}}$, GeV
YLabel=jet area
# END PLOT
# BEGIN PLOT .*/jet_(track|tower|subtr_tower)_(pt|mult)_vs_jet_pt
LogY=0
XLabel=jet $p_{\text{T}}$, GeV
# END PLOT
# BEGIN PLOT .*/jet_track_pt_vs_jet_pt
YLabel=average track $p_{\text{T}}$, GeV
# END PLOT
# BEGIN PLOT .*/jet_tower_pt_vs_jet_pt
YLabel=average tower $p_{\text{T}}$, GeV
# END PLOT
# BEGIN PLOT .*/jet_track_mult_vs_jet_pt
YLabel=average track multiplicity
# END PLOT
# BEGIN PLOT .*/jet_tower_mult_vs_jet_pt
YLabel=average tower multiplicity
# END PLOT
# BEGIN PLOT .*/jet_subtr_tower_mult_vs_jet_pt
YLabel=average fully subtracted tower multiplicity
# END PLOT
# BEGIN PLOT .*/jet_ue_.*_vs.*_jet_pt.*
LogY=0
XLabel=jet $p_{\text{T}}$, GeV
# END PLOT
# BEGIN PLOT .*/jet_ue_density_vs_jet_pt
YLabel=average cone UE density
# END PLOT
# BEGIN PLOT .*/jet_ue_density_for_jet_pt_
XLabel=average cone UE density
# END PLOT
# BEGIN PLOT .*/jet_ue_nchg_vs_jet_pt
YLabel=average $N_{\text{chg}}$
# END PLOT
# BEGIN PLOT .*/jet_ue_density_vs_jet_pt(|_corrected)
YMax=2.
# END PLOT
# BEGIN PLOT .*/jet_ue_(chg|neut)_mult_vs_jet_pt
YMin=0.2
YMax=2.6
# END PLOT
# BEGIN PLOT .*/jet_ue_(chg|neut)_sumpt_vs_jet_pt
YMax=1.2
# END PLOT
# BEGIN PLOT .*/jet_ue_neut_mult_vs_jet_pt
YLabel=average tower multiplicy (after subtraction)
# END PLOT
# BEGIN PLOT .*/jet_ue_neut_mult_for_jet_pt_
XLabel=average tower multiplicy (after subtraction)
# END PLOT
# BEGIN PLOT .*/jet_ue_neut_sumpt_vs_jet_pt
YLabel=average tower $\sum p_{\text{T}}$, GeV
# END PLOT
# BEGIN PLOT .*/jet_ue_neut_sumpt_for_jet_pt_
XLabel=average tower $\sum p_{\text{T}}$, GeV
# END PLOT
# BEGIN PLOT .*/jet_ue_chg_mult_vs_jet_pt
YLabel=average track multiplicy
# END PLOT
# BEGIN PLOT .*/jet_ue_chg_mult_for_jet_pt_
XLabel=average track multiplicy
# END PLOT
# BEGIN PLOT .*/jet_ue_chg_sumpt_vs_jet_pt
YLabel=average track $\sum p_{\text{T}}$, GeV
# END PLOT
# BEGIN PLOT .*/jet_ue_chg_sumpt_for_jet_pt_
XLabel=average track $\sum p_{\text{T}}$, GeV
# END PLOT
# BEGIN PLOT .*/.*_for_jet_pt_05_10
YLabel=Yield for jets with $5\text{ GeV} < p_{\text{T}} < 10\text{ GeV}$
# END PLOT
# BEGIN PLOT .*/.*_for_jet_pt_10_15
YLabel=Yield for jets with $10\text{ GeV} < p_{\text{T}} < 15\text{ GeV}$
# END PLOT
# BEGIN PLOT .*/.*_for_jet_pt_15_20
YLabel=Yield for jets with $15\text{ GeV} < p_{\text{T}} < 20\text{ GeV}$
# END PLOT
# BEGIN PLOT .*/.*_for_jet_pt_20_30
YLabel=Yield for jets with $20\text{ GeV} < p_{\text{T}} < 30\text{ GeV}$
# END PLOT
# BEGIN PLOT .*/.*_for_jet_pt_30_40
YLabel=Yield for jets with $30\text{ GeV} < p_{\text{T}} < 40\text{ GeV}$
# END PLOT
# BEGIN PLOT .*/dijet_asym$
NormalizeToIntegral=1
Rebin=4
LogY=0
XLabel=$\sqrt{2} (p_{\text{T};1} - p_{\text{T};2}) / (p_{\text{T};1} + p_{\text{T};2})$
YLabel=Yield
# END PLOT
# BEGIN PLOT .*/dijet_asym_vs_mean_pt$
LogZ=1
XLabel=$(p_{\text{T};1} + p_{\text{T};2}) / 2$, GeV
YLabel=$\sqrt{2} (p_{\text{T};1} - p_{\text{T};2}) / (p_{\text{T};1} + p_{\text{T};2})$
# END PLOT
# BEGIN PLOT .*/dijet_asym2_vs_mean_pt$
LogY=0
XLabel=$(p_{\text{T};1} + p_{\text{T};2}) / 2$, GeV
YLabel=avg. $(\sqrt{2} (p_{\text{T};1} - p_{\text{T};2}) / (p_{\text{T};1} + p_{\text{T};2}))^2$
# END PLOT
# BEGIN PLOT .*/jet_pt_vs_vz
YLabel=avg. $p_{\text{T}}$ of a jet in the primary vertex, GeV
# END PLOT
# BEGIN PLOT .*/jet_rt_vs_vz
YLabel=avg. $R_{\text{T}}$ of a jet in the primary vertex
# END PLOT
# BEGIN PLOT .*/jet_rt_vs_pt
LogY=0
YLabel=average $R_{\text{T}}$
XLabel=jet $p_{\text{T}}$, GeV
YMin=0.0
YMax=1.0
# END PLOT
# BEGIN PLOT .*/jet_rt_vs.*_eta
LogY=0
YLabel=average $R_{\text{T}}$
XLabel=jet $\eta$
# END PLOT
# BEGIN PLOT .*/jet_rt_vs.*_eta
XLabel=jet detector $\eta$
# END PLOT
# BEGIN PLOT .*/num_vertex_vs_vz
YLabel=number of vertices in the event
# END PLOT
# BEGIN PLOT .*/num_jets_vs_vz
YLabel=number of jets in the event
# END PLOT
# BEGIN PLOT .*/vertex_x
XLabel=primary vertex x, cm
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}v_x$, @sigma_unit@/cm
# END PLOT
# BEGIN PLOT .*/vertex_y
XLabel=primary vertex y, cm
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}v_y$, @sigma_unit@/cm
# END PLOT
# BEGIN PLOT .*/(vertex_z|vz_raw)
XLabel=primary vertex z, cm
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}v_z$, @sigma_unit@/cm
LogY=0
# END PLOT
# BEGIN PLOT .*/.*vertex_[xyz]
Title=Cut: At least one jet in vertex has $\eta < @etabin_cut_2@$, $R_{\text{T}} < 0.95$, JPx-match
# END PLOT
# BEGIN PLOT .*/.*_vs_vz
XLabel=highest ranking primary vertex z
LogY=0
ConnectGaps=0
ErrorBars=1
# END PLOT
# BEGIN PLOT .*/gvz_vs_vz
XLabel=primary vertex z
YLabel=generator vertex z
Legend=0 # 2d histogram, filled for embedding only
LogY=0
# END PLOT
# BEGIN PLOT .*/vpd_vz_vs_vz
LogZ=1
# END PLOT
# BEGIN PLOT /triggers/trig_coincedence
LogZ=1
# END PLOT
# BEGIN PLOT /triggers/bemc_patch_id_above_jp._threshold
XLabel=Jet Patch ID
YLabel=$@sigma_label@$, @sigma_unit@
# END PLOT
# BEGIN PLOT /triggers/bemc_patch_id_above_jp0_threshold
Title=JP0
# END PLOT
# BEGIN PLOT /triggers/bemc_patch_id_above_jp1_threshold
Title=JP1
# END PLOT
# BEGIN PLOT /triggers/bemc_patch_id_above_jp2_threshold
Title=JP2
# END PLOT
# BEGIN PLOT /unfolding.*/jet_dr
XLabel=$\Delta R$
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}\Delta R$, @sigma_unit@
# END PLOT
# BEGIN PLOT /unfolding.*/.*_ratio$
LogY=0
YMin=0.
YMax=1.
# END PLOT
# BEGIN PLOT /unfolding.*/detector_matching_ratio
YLabel=matched detector jets $/$ all detector jets
# END PLOT
# BEGIN PLOT /unfolding.*/particle_matching_ratio
YLabel=matched particle jets $/$ all particle jets
# END PLOT
# BEGIN PLOT /unfolding.*/pt_ratio_vs_det_pt.*
LegendYPos=0.4
LogY=0
YMin=0.
YMax=2.
XLabel=detector jet $p_{\text{T}}$@ptjet_ue_corr_label@, GeV
YLabel=$\frac{\text{detector jet }p_{\text{T}}\text{@ptjet_ue_corr_label@}}{\text{particle jet }p_{\text{T}}\text{@ptjet_ue_corr_label@}}$
# END PLOT
# BEGIN PLOT /unfolding.*/det_over_part_pt_for_part_pt.*
LogY=0
XLabel=$\frac{\text{detector jet }p_{\text{T}}\text{@ptjet_ue_corr_label@}}{\text{particle jet }p_{\text{T}}\text{@ptjet_ue_corr_label@}}$
YLabel=
# END PLOT
# BEGIN PLOT /unfolding.*/response
LogZ=1
# END PLOT
# BEGIN PLOT /unfolding.*/.*detector.*
XLabel=detector jet $p_{\text{T}}$@ptjet_ue_corr_label@, GeV
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/.*particle.*
XLabel=particle jet $p_{\text{T}}$@ptjet_ue_corr_label@, GeV
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/fake.*
XLabel=detector jet $p_{\text{T}}$@ptjet_ue_corr_label@, GeV
# END PLOT
# BEGIN PLOT /unfolding.*/miss.*
XLabel=particle jet $p_{\text{T}}$@ptjet_ue_corr_label@, GeV
# END PLOT
# BEGIN PLOT /unfolding.*/response
XLabel=detector jet $p_{\text{T}}$@ptjet_ue_corr_label@, GeV
YLabel=particle jet $p_{\text{T}}$@ptjet_ue_corr_label@, GeV
ZLabel=$\mathrm{d}^2@sigma_label@/\mathrm{d}p_{\text{T}}\hspace{0.1em}\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/(ue|jet)_.*_corr.*
LogZ=1
# END PLOT
# BEGIN PLOT /unfolding.*/jet_chg_mult_corr
XLabel=jet track mutlitplicy
YLabel=jet charged particle multiplicy
ZLabel=$@sigma_label@$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/jet_neut_mult_corr
XLabel=jet tower mutlitplicy
YLabel=jet neutral particle multiplicy
ZLabel=$@sigma_label@$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/jet_chg_sumpt_corr
XLabel=jet track $\sum p_{\text{T}}$, GeV
YLabel=jet charged particle $\sum p_{\text{T}}$, GeV
ZLabel=$\mathrm{d}^2@sigma_label@/\mathrm{d}p_{\text{T}}\hspace{0.1em}\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/jet_neut_sumpt_corr
XLabel=jet tower $\sum p_{\text{T}}$, GeV
YLabel=jet neutral particle $\sum p_{\text{T}}$, GeV
ZLabel=$\mathrm{d}^2@sigma_label@/\mathrm{d}p_{\text{T}}\hspace{0.1em}\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/ue_sumpt_corr
XLabel=detector cone UE $\sum p_{\text{T}}$, GeV
YLabel=particle cone UE $\sum p_{\text{T}}$, GeV
ZLabel=$\mathrm{d}^2@sigma_label@/\mathrm{d}p_{\text{T}}\hspace{0.1em}\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/ue_chg_mult_corr
XLabel=cone UE track mutlitplicy
YLabel=cone UE charged particle multiplicy
ZLabel=$@sigma_label@$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/ue_neut_mult_corr
XLabel=cone UE tower mutlitplicy
YLabel=cone UE neutral particle multiplicy
ZLabel=$@sigma_label@$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/ue_chg_sumpt_corr
XLabel=cone UE track $\sum p_{\text{T}}$, GeV
YLabel=cone UE charged particle $\sum p_{\text{T}}$, GeV
ZLabel=$\mathrm{d}^2@sigma_label@/\mathrm{d}p_{\text{T}}\hspace{0.1em}\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding.*/ue_neut_sumpt_corr
XLabel=cone UE tower $\sum p_{\text{T}}$, GeV
YLabel=cone UE neutral particle $\sum p_{\text{T}}$, GeV
ZLabel=$\mathrm{d}^2@sigma_label@/\mathrm{d}p_{\text{T}}\hspace{0.1em}\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding_result.*/b
XLabel=detector jet $p_{\text{T}}$@ptjet_ue_corr_label@, GeV
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /unfolding_result.*/x_(b_uncorr|b_corr|embed)
XLabel=particle jet $p_{\text{T}}$@ptjet_ue_corr_label@, GeV
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}p_{\text{T}}$, @sigma_unit@/GeV
# END PLOT
# BEGIN PLOT /result/jet_pt(_.._..|_same)?$
Title=Inclusive jet cross section
XLabel=jet $p_{\text{T}}$, GeV
YLabel=$\mathrm{d}^2@sigma_label@/(\mathrm{d}p_{\text{T}}\hspace{0.1em}\mathrm{d}\eta)$, @sigma_unit@/GeV
PlotSize=10,9
LegendXPos=0.05
LegendYPos=0.95
YMin=3.0e-1
YMax=3.0e10
RatioPlotYMin=0.5
RatioPlotYMax=1.5
# END PLOT
# BEGIN PLOT /result/jet_pt$
CustomLegend=\raisebox{-0.33\baselineskip}{$pp$ at $\sqrt{s} = 200\text{ GeV}$, $|\eta| < @etabin_cut_2@$, anti-$\mathrm{k}_{\text{T}}$, $R = @jet_r@$}@extra_custom_legend@
# END PLOT
# BEGIN PLOT /result/jet_pt_00_@etabin_cut_1_nodot@$
CustomLegend=\raisebox{-0.33\baselineskip}{$pp$ at $\sqrt{s} = 200\text{ GeV}$, $|\eta| < @etabin_cut_1@$, anti-$\mathrm{k}_{\text{T}}$, $R = @jet_r@$}@extra_custom_legend@
# END PLOT
# BEGIN PLOT /result/jet_pt_@etabin_cut_1_nodot@_@etabin_cut_2_nodot@$
CustomLegend=\raisebox{-0.33\baselineskip}{$pp$ at $\sqrt{s} = 200\text{ GeV}$, $@etabin_cut_1@ < |\eta| < @etabin_cut_2@$, anti-$\mathrm{k}_{\text{T}}$, $R = @jet_r@$}@extra_custom_legend@
# END PLOT
# BEGIN PLOT /result/jet_pt_same$
YMin=3.0e-2
CustomLegend=\raisebox{-0.33\baselineskip}{$pp$ at $\sqrt{s} = 200\text{ GeV}$, anti-$\mathrm{k}_{\text{T}}$, $R = @jet_r@$}@extra_custom_legend@
# END PLOT
# BEGIN PLOT /result/f(_ue)?_had_vs_jet_pt.*
LegendXPos=0.05
LogY=0
YMin=0.5
YMax=1.0
# END PLOT
# BEGIN PLOT /result/f(_ue)?_had_vs_jet_pt$
CustomLegend=\raisebox{-0.33\baselineskip}{$pp$ at $\sqrt{s} = 200\text{ GeV}$, $|\eta| < @etabin_cut_2@$, anti-$\mathrm{k}_{\text{T}}$, $R = @jet_r@$}
# END PLOT
# BEGIN PLOT /result/f(_ue)?_had_vs_jet_pt_00_@etabin_cut_1_nodot@$
CustomLegend=\raisebox{-0.33\baselineskip}{$pp$ at $\sqrt{s} = 200\text{ GeV}$, $|\eta| < @etabin_cut_1@$, anti-$\mathrm{k}_{\text{T}}$, $R = @jet_r@$}
# END PLOT
# BEGIN PLOT /result/f(_ue)?_had_vs_jet_pt_@etabin_cut_1_nodot@_@etabin_cut_2_nodot@$
CustomLegend=\raisebox{-0.33\baselineskip}{$pp$ at $\sqrt{s} = 200\text{ GeV}$, $@etabin_cut_1@ < |\eta| < @etabin_cut_2@$, anti-$\mathrm{k}_{\text{T}}$, $R = @jet_r@$}
# END PLOT
# BEGIN PLOT /result/f(_ue)?_had_vs_jet_pt_same$
CustomLegend=\raisebox{-0.33\baselineskip}{$pp$ at $\sqrt{s} = 200\text{ GeV}$, anti-$\mathrm{k}_{\text{T}}$, $R = @jet_r@$}
# END PLOT
# BEGIN PLOT /result/f_had_vs_jet_pt.*
XLabel=jet $p_{\text{T}}$ corrected for UE, GeV
YLabel=$f_{\text{had.}} = @sigma_label@_{\text{hadron}} / @sigma_label@_{\text{parton}}$
# END PLOT
# BEGIN PLOT /result/f_ue_had_vs_jet_pt.*
XLabel=jet $p_{\text{T}}$, GeV
YLabel=$f_{\text{UE+had.}} = @sigma_label@_{\text{hadron}} / @sigma_label@_{\text{parton}}$
# END PLOT
# BEGIN PLOT /STAR_2017_I1493842/d01-x01-y01
Title=Run 9 $pp$ $200$~\text{GeV} Di-Jets
XLabel=$m_{jj}$, GeV
YLabel=$\mathrm{d}@sigma_label@/\mathrm{d}m_{jj}$, @sigma_unit@/GeV
PlotSize=9.5,9
LegendXPos=0.05
LegendYPos=0.95
YMin=2.0e0
YMax=2.0e7
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT
