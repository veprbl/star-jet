#!/usr/bin/env python3

import fastnlo
import numpy as np

def main(args):
    table = fastnlo.fastNLOLHAPDF(args.fastnlo_grid)
    table.SetLHAPDFFilename(args.pdf_set)
    table.SetLHAPDFMember(0)

    eta_bins = np.array(table.GetObsBinsBounds(0))
    pt_bins = np.array(table.GetObsBinsBounds(1))
    xsec = np.array(table.GetCrossSection())

    with open(args.input_datafile) as fp:
        lines = fp.readlines()

    data_start_ix = [ix for ix, l in enumerate(lines) if l.find("&End") != -1][-1] + 1

    column_name, = [
      [item.strip().strip("'") for item in line.split("=")[1].split(",")]
      for line in lines if line.find("ColumnName") != -1
    ]
    try:
        etabin_ix = column_name.index("EtaBinNumber")
    except ValueError:
        etabin_ix = None
        etalow_ix = column_name.index("etalow")
    try:
        ptlow_ix = column_name.index("pt1")
    except ValueError:
        ptlow_ix = column_name.index("ptlow")
    try:
        pthigh_ix = column_name.index("pt2")
    except ValueError:
        pthigh_ix = column_name.index("pthigh")
    sigma_ix = [n.startswith("Sigma") for n in column_name].index(True)
    npcorr_ix = column_name.index("NPCorr")
    print(column_name)
    rows = [list(map(float, line.split())) for line in lines[data_start_ix:]]

    possible_etalow_values = np.unique(eta_bins[:,0])
    print(possible_etalow_values)

    for row in rows:
        if etabin_ix is not None:
            EtaBinNumber = int(row[etabin_ix])
            etalow = possible_etalow_values[EtaBinNumber - 1]
        else:
            etalow = row[etalow_ix]
        ptlow = row[ptlow_ix]
        pthigh = row[pthigh_ix]
        y, = xsec[
            np.isclose(eta_bins[:,0], etalow)
            &
            np.all(np.isclose(pt_bins, (ptlow, pthigh)), axis=1)
        ]
        row[sigma_ix] = y
        row[npcorr_ix] = 1
        # TODO scale uncertainties if they are not defined as relative

    with open(args.output_datafile, "w") as fp:
        fp.write("".join(lines[:data_start_ix]))
        fp.write("\n".join("\t".join(map(str, row)) for row in rows))

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="Replace real data by pseudo-data.")
    parser.add_argument('input_datafile')
    parser.add_argument('--fastnlo-grid', required=True)
    parser.add_argument("--pdf-set", default="HERAPDF20_NLO_EIG")
    parser.add_argument('-o', dest="output_datafile", required=True)
    args = parser.parse_args()

    main(args)
