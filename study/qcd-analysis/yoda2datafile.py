#!/usr/bin/env python3

import yoda

def yoda2datafile(aos, aos_f_had, fastnlo_grid, etabin_cut_1=0.4, etabin_cut_2=0.8):
    s = aos["/result/jet_pt"]
    err_col_names = [key.decode() for key in list(s)[0].errMap().keys() if key != b""]
    err_col_types = ['Error'] * len(err_col_names)

    table = []
    table_f_had = []

    for etalow, etahigh in [(0.0, etabin_cut_1), (etabin_cut_1, etabin_cut_2)]:
        etalow_nodot = str(etalow).replace(".", "")
        etahigh_nodot = str(etahigh).replace(".", "")
        s = aos[f"/result/jet_pt_{etalow_nodot}_{etahigh_nodot}"]
        s_f_had = aos_f_had[f"/result/jet_pt_{etalow_nodot}_{etahigh_nodot}"]
        for p, p_f_had in zip(s, s_f_had):
            ptlow = p.xMin()
            pthigh = p.xMax()
            y = p.y()
            dy_breakdown = [max(p.errMap()[name.encode()]) for name in err_col_names if name not in [b""]]
            row = [etalow, etahigh, ptlow, pthigh, y] + dy_breakdown + [0.05 * y, p_f_had.y()]
            table.append("\t".join(map(lambda v: f"{v:.5}", row)))

    table_s = "\n".join(table)

    # Rename emb column to uncorr
    err_col_names = ["uncorr" if col_name == "emb" else col_name for col_name in err_col_names]

    datafile = f"""\
&Data
   Name = 'STAR pp200 jets'
   Reaction = 'pp jets fastNLO'

   NDATA = {len(s) * 2}
   NColumn = {7 + len(err_col_types)}
   ColumnType = 'Bin', 'Bin', 'Bin', 'Bin', 'Sigma', {len(err_col_types)}*'Error', 'Error', 'Bin'
   ColumnName = 'etalow', 'etahigh', 'ptlow', 'pthigh', 'Sigma Inclusive', {", ".join(f"'{t}'" for t in err_col_names)}, 'lumi', 'NPCorr'
   NInfo   = 4
   CInfo    = 'sqrt(S)', 'PublicationUnits', 'MurDef' , 'MufDef'
   DataInfo = 200., 1, -1, -1

   IndexDataset = 1234

   TermName   = 'R','H'
   TermSource = 'fastNLO','KFactor'
   TermInfo   = 'Filename={fastnlo_grid}:Units=publication',
                'DataColumn=NPCorr'
   TheorExpr  = 'R*H'

   Percent   = {len(err_col_types)}*False, False
&End
&PlotDesc
   PlotN = 2
   PlotDefColumn = 'etalow'
   PlotDefValue = -0.1, 0.3, 0.7
   PlotVarColumn = 'ptlow'
   PlotOptions(1) = 'Experiment:STAR@ExtraLabel:pp #rightarrow jets; #sqrt{{s}} = 200 GeV; R=0.6@Title: |y| < 0.4  @XTitle:P_{{T}} (GeV)@YTitle:#sigma_{{jet}}@Xmin:6.7@Xmax:50.@Ylog'
   PlotOptions(2) = 'Experiment:STAR@ExtraLabel:pp #rightarrow jets; #sqrt{{s}} = 200 GeV; R=0.6@Title: 0.4 < |y| < 0.8  @XTitle:P_{{T}} (GeV)@YTitle:#sigma_{{jet}}@Xmin:6.7@Xmax:50.@Ylog'
&End
{table_s}"""
    return datafile


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="Convert yoda input to xFitter datafile.")
    parser.add_argument("input_yoda")
    parser.add_argument("--fhad", required=True)
    parser.add_argument("--fastnlo-grid", required=True)
    parser.add_argument("--etabin-cut-1", required=True, type=float)
    parser.add_argument("--etabin-cut-2", required=True, type=float)
    parser.add_argument("-o", dest="output_datafile", required=True)
    args = parser.parse_args()

    aos = yoda.readYODA(args.input_yoda)
    aos_f_had = yoda.readYODA(args.fhad)

    datafile = yoda2datafile(aos, aos_f_had, args.fastnlo_grid, etabin_cut_1=args.etabin_cut_1, etabin_cut_2=args.etabin_cut_2)

    with open(args.output_datafile, "w") as fp:
        fp.write(datafile)
