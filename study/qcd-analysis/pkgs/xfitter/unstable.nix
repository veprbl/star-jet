{ lib, fetchFromGitLab, xfitter_2_2_0 }:

xfitter_2_2_0.overrideAttrs (prev: {
  version = "unstable-2022-08-25";

  src = fetchFromGitLab {
    domain = "gitlab.cern.ch";
    owner = "fitters";
    repo = "xfitter";
    rev = "84c362f1d29c0554afce9b2d7b10e5273d4c9f85";
    sha256 = "0hf4a76j27qcfddqbpmgkc1ajzm0iyd3gp098q8wm7cr5nldm5rz";
  };
})
