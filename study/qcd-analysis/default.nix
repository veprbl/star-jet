{ nixpkgs ? import <nixpkgs> { }, nobatch ? false }:

with import ../../common.nix { inherit (nixpkgs) lib; };

(import ../../default.nix { inherit nixpkgs nobatch; }).override (final: prev: add_eval_scope (with final.pkgs; with final; {
  pkgs = nixpkgs.pkgs;

  root = pkgs.root.overrideAttrs (prev: lib.optionalAttrs (lib.versionOlder prev.version "6.26.0") {
    patches = (prev.patches or [ ]) ++ [
      # fix asimage/libpng warnings
      (fetchpatch {
        url = "https://github.com/root-project/root/commit/32d8dd0062ad597de3645a8239f837ba3cd98363.diff";
        sha256 = "13cknxmrcq4a25yn0idnzbpfgm4zrriax3y8n18p7bwlhbiby4ld";
      })
      (fetchpatch {
        url = "https://github.com/root-project/root/commit/67977646ad23a037124998384cf5dd32a5114bc4.diff";
        sha256 = "18g9r62j7ksxipmf6rkw5nd3bmzc3flfi3r4wdjgdjnmv2680p8c";
      })
    ];
  });
  xfitter_2_2_0 = callPackage pkgs/xfitter/2.2.nix { inherit root; };
  xfitter_unstable = callPackage pkgs/xfitter/unstable.nix { inherit xfitter_2_2_0; };
  xfitter = xfitter_2_2_0;

  use_xfitter_unstable = final.override (final: prev: {
    xfitter = xfitter_unstable;
  });

  fastnlo_tookit = callPackage pkgs/fastnlo_toolkit/2.5.0-2826.nix { python = python3; withPython = true; };

  datafiles_hera = fetchzip {
    # The "#.tar.bz2" suffix is needed to workaround https://github.com/NixOS/nixpkgs/issues/60157
    url = "https://gitlab.cern.ch/fitters/xfitter-datafiles/-/archive/2b3add7e529fe2f1eecbe9b15200fc30a03f9b2a/xfitter-datafiles-2b3add7e529fe2f1eecbe9b15200fc30a03f9b2a.tar.bz2?path=hera/h1zeusCombined/inclusiveDis/1506.06042#.tar.bz2";
    sha256 = "1rl9jbpwcipdr4kirixlgwrx5ikl0vngbcfh8aak7w7sf25c9fzc";
    passthru.InputFileNames = [
      "datafiles/hera/h1zeusCombined/inclusiveDis/1506.06042/HERA1+2_NCep_920-thexp.dat"
      "datafiles/hera/h1zeusCombined/inclusiveDis/1506.06042/HERA1+2_NCep_820-thexp.dat"
      "datafiles/hera/h1zeusCombined/inclusiveDis/1506.06042/HERA1+2_NCep_575-thexp.dat"
      "datafiles/hera/h1zeusCombined/inclusiveDis/1506.06042/HERA1+2_NCep_460-thexp.dat"
      "datafiles/hera/h1zeusCombined/inclusiveDis/1506.06042/HERA1+2_NCem-thexp.dat"
      "datafiles/hera/h1zeusCombined/inclusiveDis/1506.06042/HERA1+2_CCep-thexp.dat"
      "datafiles/hera/h1zeusCombined/inclusiveDis/1506.06042/HERA1+2_CCem-thexp.dat"
    ];
  };

  datafiles_cdf = fetchzip {
    # The "#.tar.bz2" suffix is needed to workaround https://github.com/NixOS/nixpkgs/issues/60157
    url = "https://gitlab.cern.ch/fitters/xfitter-datafiles/-/archive/2b3add7e529fe2f1eecbe9b15200fc30a03f9b2a/xfitter-datafiles-2b3add7e529fe2f1eecbe9b15200fc30a03f9b2a.tar.bz2?path=tevatron/cdf/jets/0807.2204#.tar.bz2";
    sha256 = "0ipy46s0b0bndgzz877s93rj5alkgqzqpyrz7xvqz7xvmnn2wyrp";
    passthru.InputFileNames = [
      "datafiles/tevatron/cdf/jets/0807.2204/CDF_JETS2008-thexp.dat"
    ];
  };

  datafiles_d0 = fetchzip {
    # The "#.tar.bz2" suffix is needed to workaround https://github.com/NixOS/nixpkgs/issues/60157
    url = "https://gitlab.cern.ch/fitters/xfitter-datafiles/-/archive/2b3add7e529fe2f1eecbe9b15200fc30a03f9b2a/xfitter-datafiles-2b3add7e529fe2f1eecbe9b15200fc30a03f9b2a.tar.bz2?path=tevatron/d0/jets/0802.2400#.tar.bz2";
    sha256 = "0a4r8nh163n1fpjm9zmx68xnfnxfbg15clm9b3xfa6n3ick7wzil";
    extraPostFetch = ''
      substituteInPlace "$out/tevatron/d0/jets/0802.2400/D0_JETS-thexp.dat" \
        --replace "Units=absolute" "Units=publication"
    '';
    passthru.InputFileNames = [
      "datafiles/tevatron/d0/jets/0802.2400/D0_JETS-thexp.dat"
    ];
  };

  yoda2datafile = input: fhad: fastnlo_grid: runCommandNoCC "${input.name or "unnamed"}-datafile"
    rec {
      yoda2datafile = ./yoda2datafile.py;
      inherit input fhad fastnlo_grid;
      nativeBuildInputs = [
        python3
        python3Packages.numpy
        python3Packages.yoda
      ];
      passthru.InputFileNames = [
        "datafiles/unnamed.dat" # FIXME clashes with other instances
      ];
      preferLocalBuild = true;
    } ''
    mkdir "$out"

    cp "$yoda2datafile" ./yoda2datafile.py
    ln -s "$fastnlo_grid" "$out/unnamed.tab"
    python ./yoda2datafile.py \
      "$input/output.yoda.gz" --fhad "$fhad/output.yoda.gz" --fastnlo-grid "datafiles/unnamed.tab" -o "$out/unnamed.dat" \
      --etabin-cut-1 ${etabin_cut_1} --etabin-cut-2 ${etabin_cut_2}
  '';

  inherit (import ../../MC/fastnlo/default.nix { inherit nixpkgs; }) get_grid_file;
  grid_file = get_grid_file {
    jet_r = "0.6";
    inherit etabin_cut_1_nodot etabin_cut_2_nodot;
    variant = "NLOJet++";
  };

  inherit (xsec_plots) combined_yoda f_np;
  datafiles_pp200 = yoda2datafile combined_yoda f_np grid_file;

  to_pseudo = input: dat_path: tab_path: runCommandNoCC "${input.name}_pseudo" rec {
    to_pseudodata = ./to_pseudodata.py;
    inherit input dat_path tab_path;
    dat_path_new = builtins.replaceStrings [ ".dat" ] [ "_PSEUDO.dat" ] dat_path;
    passthru.InputFileNames = [
      "datafiles/${dat_path_new}"
    ];
    nativeBuildInputs = [
      python3
      python3Packages.numpy
      fastnlo_tookit
      lhapdf.pdf_sets.HERAPDF20_NLO_EIG
    ];
  } ''
    mkdir -vp `dirname "$out/$dat_path_new"`
    ln -sv {"$input","$out"}/$tab_path

    cp "$to_pseudodata" ./to_pseudodata.py
    python ./to_pseudodata.py "$input/$dat_path" --fastnlo-grid "$out/$tab_path" -o "$out/$dat_path_new"
  '';

  datafiles_cdf_pseudo =
    to_pseudo
      datafiles_cdf
      "tevatron/cdf/jets/0807.2204/CDF_JETS2008-thexp.dat"
      "tevatron/cdf/jets/0807.2204/fnt2007midp_cv21_I790693.tab";

  datafiles_d0_pseudo =
    to_pseudo
      datafiles_d0
      "tevatron/d0/jets/0802.2400/D0_JETS-thexp.dat"
      "tevatron/d0/jets/0802.2400/fnt2009midp.tab";

  datafiles_pp200_pseudo =
    to_pseudo
      datafiles_pp200
      "unnamed.dat"
      "unnamed.tab"
  ;

  datafiles_hera_hepforge = fetchzip {
    url = "http://www.hepforge.org/archive/xfitter/1506.06042.tar.gz";
    sha256 = "13rjzdcl86m8hnqvj6rfal208iasy50a9vzn6waw9wh3z3fgbd77";
    stripRoot = false;
    passthru.InputFileNames = [
      "datafiles/1506.06042/HERA1+2_NCep_920-thexp.dat"
      "datafiles/1506.06042/HERA1+2_NCep_820-thexp.dat"
      "datafiles/1506.06042/HERA1+2_NCep_575-thexp.dat"
      "datafiles/1506.06042/HERA1+2_NCep_460-thexp.dat"
      "datafiles/1506.06042/HERA1+2_NCem-thexp.dat"
      "datafiles/1506.06042/HERA1+2_CCep-thexp.dat"
      "datafiles/1506.06042/HERA1+2_CCem-thexp.dat"
    ];
  };

  parameters = {
    Adbar = [ 0.10537 0.01 ];
    Adv = "DEPENDENT";
    Ag = "DEPENDENT";
    Agp = [ 0.240458 0.01 ];
    Aubar = [ 0 0 ];
    Auv = "DEPENDENT";
    Bdbar = [ (-0.172358) 0.004 ];
    Bdv = [ 0.805766 0.06 ];
    Bg = [ (-0.014834) 0.27 ];
    Bgp = [ (-0.1664) 0.01 ];
    Bubar = [ 0 0 ];
    Buv = [ 0.713453 0.016 ];
    Cdbar = [ 4.88548 1.2345 ];
    Cdv = [ 4.0796 0.3 ];
    Cg = [ 9.10684 0.32 ];
    Cgp = [ 25 0 ];
    Cubar = [ 8.05525 0.8 ];
    Cuv = [ 4.84072 0.06 ];
    DbarToS = "=fs/(1-fs)";
    Dubar = [ 11.9003 1 ];
    Duv = [ 0 0 ];
    Euv = [ 13.4053 0.8 ];
    ZERO = [ 0 0 ];
    fs = [ 0.4 0 ];
  };

  run_fit =
    { do_fit ? true
    , doErrors ? "Pumplin"
    , commands ? ''
        call fcn 1
        migrad
        call fcn 3
        set str 2
      ''
    , pdf_name ? "proton"
    , xfitter ? final.xfitter
    , benchmark ? true
    , preferLocalBuild ? false
    , constants_file ? ./steering/2_2_0/constants.yaml
    , parameters_file_template ? ./steering/2_2_0/parameters.yaml
    , steering_file_template ? ./steering/2_2_0/steering.txt
    }: datafiles:
    let
      InputFileNames = lib.concatMap (d: d.InputFileNames) datafiles;
    in
    runCommandNoCC "xfitter-fit" {
      inherit datafiles;
      nativeBuildInputs = [ xfitter yq-go ] ++ lib.optional benchmark time;
      inherit preferLocalBuild;
      inherit constants_file;
      parameters_file = substituteAll {
        src = parameters_file_template;
        doerrors = doErrors;
        commands = builtins.replaceStrings [ "\n" ] [ "\\n\n" ] commands;
        parameters = builtins.toJSON parameters;
        pdfname = pdf_name;
      };
      steering_file = substituteAll {
        src = steering_file_template;
        ninputfiles = builtins.length InputFileNames;
        inputfilenames = lib.concatMapStrings (i: "     '${i}'\n") InputFileNames;
      };
    }
    ''
      mkdir "$out"
      cd "$out"

      mkdir ./datafiles
      for d in $datafiles; do
        ln -sv -t ./datafiles/ $d/*
      done
      cp "$constants_file" ./constants.yaml
      cp "$parameters_file" ./parameters.yaml
      cp "$steering_file" ./steering.txt
      ${lib.optionalString benchmark "command time -v "}xfitter
      test -e output/Results.txt
    '';

  xfitter_draw = options: inputs: runCommandNoCC "xfitter-draw" {
    nativeBuildInputs = [
      xfitter
      glibc
      fontconfig
      ghostscript
      (texlive.combine { inherit (texlive) scheme-basic booktabs extsizes geometry palatino pxfonts xcolor; })
    ];
    preferLocalBuild = true;
    FONTCONFIG_FILE = makeFontsConf { fontDirectories = [ ]; };
  } ''
    export HOME=`mktemp -d`
    mkdir "$out"
    xfitter-draw \
      --outdir "$out" \
      ${join options} \
      ${join (map (i: "${i}/output:'${i.title or ""}'") inputs)}
  '';

  herapdf_pars = {
    commands = ''
      set str 2
      call fcn 3
    '';
    parameters_file_template = ./steering/2_2_0/parameters_herapdf.yaml;
  };

  xsec_change =
    let
      commands = ''
        call fcn 1
        set str 0
        migrad
        call fcn 3
      '';
    in
    xfitter_draw [ "--splitplots-pdf" "--nothshifts" "--highres" "--bands" ] [
      (run_fit ({ inherit commands; pdf_name = "HERAPDFonly"; } // herapdf_pars) [ datafiles_hera datafiles_pp200 ] // { title = "HERA only"; })
      (run_fit ({ inherit commands; pdf_name = "STARjets200"; }) [ datafiles_hera datafiles_pp200 ] // { title = "HERA+pp200"; })
    ];

  fit =
    let
      commands = ''
        call fcn 1
        set str 0
        migrad
        call fcn 3
      '';
    in
    xfitter_draw [ "--splitplots-pdf" "--nothshifts" "--highres" "--bands" "--relative-errors" ] [
      (run_fit { inherit commands; pdf_name = "HERAPDFonly"; } [ datafiles_hera ] // { title = "HERA only"; })
      (run_fit { inherit commands; pdf_name = "STARjets200"; } [ datafiles_hera datafiles_pp200 ] // { title = "HERA+pp200"; })
    ];

  fit_unstable =
    let
      commands = ''
        call fcn 1
        set str 0
        migrad
        call fcn 3
      '';
    in
    xfitter_draw [ "--splitplots-pdf" "--nothshifts" "--highres" "--bands" "--relative-errors" ] [
      (run_fit { inherit commands; pdf_name = "HERAPDFonly"; } [ datafiles_hera ] // { title = "HERA only"; })
      (use_xfitter_unstable.run_fit { inherit commands; pdf_name = "HERAPDFonly unstable"; } [ datafiles_hera ] // { title = "HERA only unstable"; })
    ];

  pseudo =
    let
      commands = ''
        call fcn 1
        set str 0
        migrad
        call fcn 3
      '';
    in
    xfitter_draw [ "--splitplots-pdf" "--nothshifts" "--highres" "--bands" "--relative-errors" ] [
      (run_fit { inherit commands; } [ datafiles_hera ] // { title = "HERA only"; })
      (run_fit { inherit commands; } [ datafiles_hera datafiles_pp200_pseudo ] // { title = "HERA+pp200 pseudo"; })
      (run_fit { inherit commands; } [ datafiles_hera datafiles_cdf_pseudo ] // { title = "HERA+CDF pseudo"; })
      (run_fit { inherit commands; } [ datafiles_hera datafiles_d0_pseudo ] // { title = "HERA+D0 pseudo"; })
    ];
}))
