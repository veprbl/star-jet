{ lib
, stdenv
, fetchFromGitHub
, fetchpatch
, autoreconfHook
, pkg-config
, apfel
, applgrid
, gfortran
, gsl
, lhapdf
, nnpdf
, root5
, sqlite
}:

stdenv.mkDerivation rec {
  pname = "apfelcomb";
  version = "unstable-2022-11-07";

  src = fetchFromGitHub {
    owner = "NNPDF";
    repo = pname;
    rev = "bbe76328e4dea237a2a4d8b1615c96298b286560";
    sha256 = "sha256-bak/xufhkCU3eGsXqTxO9/Qcg4OJE36EoBh7Cx5+KhE=";
  };

  postPatch = ''
    substituteInPlace Makefile.inc \
      --replace "CXX = g++" "#CXX := g++" \
      --replace "SHELL=/bin/bash" "SHELL := /bin/bash" \
      --replace "../applgrids/" "./applgrids/"
  '';

  nativeBuildInputs = [
    pkg-config
  ];

  buildInputs = [
    apfel
    (applgrid.overrideAttrs (prev: {
      src = fetchFromGitHub {
        owner = "scarrazza";
        repo = "applgridphoton";
        rev = "503ab5329f58c15782974abbbb321efc67f0fdda";
        sha256 = "sha256-o9B1YNZ6NpxGCVcdB6ALAeq+F4ImghtvHkEYcqTtCTc=";
      };
      patches = [];
      preConfigure = ""; # don't patch gfortran.a stuff
      nativeBuildInputs = prev.nativeBuildInputs ++ [ autoreconfHook ];
      buildInputs = prev.buildInputs ++ [ gfortran.cc.lib ];
    }))
    gsl
    lhapdf
    nnpdf
    root5
    sqlite
  ];

  dontConfigure = true;

  installPhase = ''
    runHook preInstall

    install -Dm500 apfel_comb "$out"/bin/apfel_comb
    install -Dm500 src/cfac_scale "$out"/bin/cfac_scale
    install -Dm500 src/fKmerge2 "$out"/bin/fkmerge2
    install -Dm400 src/libac_core.a "$out"/lib/libac_core.a

    runHook postInstall
  '';

  meta = with lib; {
    description = "Generates FK tables for NNPDF fits";
    homepage = "https://docs.nnpdf.science/external-code/grids.html";
    license = licenses.unfree; # no license
    maintainers = [ maintainers.veprbl ];
    platforms = platforms.unix;
  };
}
