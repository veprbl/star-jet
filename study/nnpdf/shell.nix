{ pkgs ? import <nixpkgs> {} }:

let

  apfelcomb = pkgs.callPackage pkgs/apfelcomb {};

in

pkgs.mkShell {
  buildInputs = [
    apfelcomb
    pkgs.apfel
    pkgs.nnpdf
    pkgs.python3
    pkgs.python3.pkgs.n3fit
    pkgs.python3.pkgs.validobj
    pkgs.lhapdf.pdf_sets.NNPDF40_nnlo_as_01180
  ];
}
