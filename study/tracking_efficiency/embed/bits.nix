{ nixpkgs /*? import <nixpkgs> {}*/, nixpkgs_src /*? <nixpkgs> */, overlays /*? []*/ }:

let
  inherit (nixpkgs) lib;
  inherit (import ../../../common.nix { inherit lib; }) wrap_condor;
  star2parquet = import ../. { inherit nixpkgs nixpkgs_src overlays; };

  this = final: with final; {
    nixpkgs_local = nixpkgs;
    stdenv = nixpkgs_local.stdenv;
    stdenvNoCC = nixpkgs_local.stdenvNoCC;
    pkgs = nixpkgs_local.pkgs;
    submit_as_job = drv: wrap_condor (
      if drv ? "INTERACTIVE" then
        drv.overrideAttrs (_: { INTERACTIVE=0; })
      else
        drv
    );
    inherit (star2parquet) track2parquet geant2parquet;

    SEED = 137;

    NODEBUG = "yes";

    PATCH_MGR = false;

    RESET_TPCRAWDATA = false;

    HOOK_PREFIX = builtins.getEnv "HOME";
    mkStarPhaseHook = hook_name: let
      hook_path = ''"${HOOK_PREFIX}/.embed/${hook_name}"'';
    in
      ''[ -e ${hook_path} ] && source ${hook_path} || echo "No hook found at ${hook_path}"'';
    mkRunHooks = key: value: let
      phase_name = builtins.match "(.+)Phase" key;
      phase_name_unwrap = builtins.elemAt phase_name 0;
      phase_name_cap =
        (lib.toUpper (builtins.substring 0 1 phase_name_unwrap))
        + (builtins.substring 1 (builtins.stringLength phase_name_unwrap) phase_name_unwrap);
    in
      if isNull phase_name then
        value
      else
        assert builtins.isString value;
        ''
        runHook pre${phase_name_cap}
        ${value}
        runHook post${phase_name_cap}
        '';
    #
    # Wrapper for derivation that will run in STAR environment
    # This is very much impure
    #
    mkStarDerivation = args: stdenvNoCC.mkDerivation ((lib.mapAttrs mkRunHooks args) // {
      preConfigure = ''
      ${mkStarPhaseHook "preConfigure"}
      source <(
        /usr/bin/csh -c "setenv SILENT 1; setenv ECHO 0; setenv GROUP_DIR /afs/rhic.bnl.gov/rhstar/group; source /afs/rhic.bnl.gov/rhstar/group/star_cshrc.csh; source \''${GROUP_DIR}/.starver ${STAR_VERSION}; env" \
          | egrep '(^STAR|^OPTSTAR|^ROOT|^SITE|^OPTSTAR|^XOPTSTAR|^USE_64BITS|^HOSTNAME|^PATH|^LD_LIBRARY_PATH)' \
          ${lib.optionalString USE_SL5 "| sed 's/sl73_/sl53_/g' | sed 's/_gcc485/_gcc432/g'"} \
          | while read VAR; do echo "export $VAR"; done
        )
      ''
      # "$STAR/.$STAR_HOST_SYS/{bin,BIN}" may not appear in $PATH in some cases
      + ''
      if ! type -p starsim; then
        export PATH=$STAR/.$STAR_HOST_SYS/bin:$PATH
      fi
      ''
      # We also need to undo the substitution in OPTSTAR
      + (lib.optionalString USE_SL5 "export LD_LIBRARY_PATH=$STAR_LIB:$LD_LIBRARY_PATH; export OPTSTAR=/opt/star/sl73_gcc485")
      # Old mgr doesn't work with modern Perl
      + (lib.optionalString PATCH_MGR ''
        mkdir mgr
        cp $STAR/mgr/Construct mgr/Construct
        sed -i '/foreach my $dir qw/,+1 { s/qw(/(qw(/; s/) {/)) {/ }' mgr/Construct
      '')

      + (args.preConfigure or "");
      preferLocalBuild = true;
      allowSubstitutes = false;

      preUnpack = mkStarPhaseHook "preUnpack";
      postUnpack = mkStarPhaseHook "postUnpack";
      postConfigure = mkStarPhaseHook "postConfigure";
      preBuild = mkStarPhaseHook "preBuild";
      postBuild = mkStarPhaseHook "postBuild";
      preCheck = mkStarPhaseHook "preCheck";
      postCheck = mkStarPhaseHook "postCheck";
      preInstall = mkStarPhaseHook "preInstall";
      postInstall = mkStarPhaseHook "postInstall";
    });

    #
    # Produces vertices located at the origin (x=y=z=0)
    #
    dummy_vertex = num_events: assert (num_events > 0); stdenv.mkDerivation {
      name = "dummy_vertex.txt";
      buildCommand = ''
        (yes "0 0 0" || true) | head -n ${toString num_events} > "$out"
      '';
      preferLocalBuild = true;
      allowSubstitutes = false;
    };

    #
    # Produces vertices distributed by the normal distribution
    #
    GAUSS_MU_X = 0; # cm
    GAUSS_SIGMA_X = 0; # cm
    GAUSS_MU_Y = 0; # cm
    GAUSS_SIGMA_Y = 0; # cm
    GAUSS_MU_Z = -2; # cm
    GAUSS_SIGMA_Z = 45.2; # cm
    gauss_vertex = num_events: assert (num_events > 0); stdenv.mkDerivation {
      name = "gauss_vertex.txt";
      src = lib.sourceByRegex ./tools [
        "^gauss_vertex\.py$"
      ];
      nativeBuildInputs = [ pkgs.python3 ];
      buildPhase = ''
        ./gauss_vertex.py \
          --seed=${toString SEED} \
          --num-events=${toString num_events} \
          --mu-x=${toString GAUSS_MU_X} \
          --sigma-x=${toString GAUSS_SIGMA_X} \
          --mu-y=${toString GAUSS_MU_Y} \
          --sigma-y=${toString GAUSS_SIGMA_Y} \
          --mu-z=${toString GAUSS_MU_Z} \
          --sigma-z=${toString GAUSS_SIGMA_Z} \
          > "$out"
      '';
      installPhase = ''
        true
      '';
      preferLocalBuild = true;
      allowSubstitutes = false;
    };

    #
    # Produces vertices distributed by the normal distribution with
    # beamline constraint determined from the database
    #
    generate_vertex_macro = ./macros/GenerateVertex.C;
    generate_vertex = num_events: input_daq: submit_as_job (mkStarDerivation rec {
      name = "generated_vertex.txt";
      unpackPhase = "true";
      buildPhase = ''
        touch --no-create `readlink -e ${input_daq}`
        cp ${input_daq} input.daq
        macro_name="$(stripHash "${generate_vertex_macro}")"
        cp ${generate_vertex_macro} "$macro_name"
        root4star -l -b "$macro_name"'(${toString NUM_EVENTS}, ${toString SEED}, "input.daq", "vertex.txt", "Jet", ${toString GAUSS_SIGMA_X}, ${toString GAUSS_SIGMA_Y}, ${toString GAUSS_SIGMA_Z}, ${toString GAUSS_MU_Z})'
      '';
      installPhase = ''
        install -m444 vertex.txt "$out"
      '';
    });

    PYTHIA_PT_MIN = 0;
    PYTHIA_PT_MAX = -1;

    #
    # libfaketime is used to set some of the RNG seeds that keep being
    # seeded from the current time
    #
    # Needs to be compiled with impure environment to use the same
    # libc as STAR
    #
    faketime = mkStarDerivation rec {
      name = "libfaketime-${version}";
      version = "0.9.7";

      src = pkgs.fetchurl {
        url = "https://github.com/wolfcw/libfaketime/archive/v${version}.tar.gz";
        sha256 = "07l189881q0hybzmlpjyp7r5fwz23iafkm957bwy4gnmn9lg6rad";
      };

      PREFIX = placeholder "out";
      LIBDIRNAME = "/lib";
      CFLAGS = "-m32";
      LDFLAGS = "-m32";
    };

    USE_FAKETIME = true;
    FAKETIME_CMD = lib.optionalString USE_FAKETIME ''${faketime}/bin/faketime "1970-01-01 00:00:00" '';

    #
    # Run starsim with pythia simulation
    #
    starsim_pythia_kumac = ./kumac/pythia.kumac;
    starsim_pythia = vertex_file: submit_as_job (mkStarDerivation {
      #inherit NODEBUG; # enabling possibly breaks starsim
      name = "starsim-pythia.fzd";
      nativeBuildInputs = [ pkgs.lhapdf.pdf_sets.cteq6l1 ];
      unpackPhase = "true";
      buildPhase = ''
        ${FAKETIME_CMD}starsim -w 0 -b ${starsim_pythia_kumac} RUN=${toString RUN_NUMBER} VERTEX_FILE=${vertex_file} SEED=${toString SEED} PTMIN=${toString PYTHIA_PT_MIN} PTMAX=${toString PYTHIA_PT_MAX} ROOTS=${toString PYTHIA_ROOTS} FILE=output.fzd
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        install -m444 output.fzd "$out_data"
        ln -s "$out_data" "$out"
      '';
    });

    #
    # Run starsim on events from a file in txgen format
    #
    starsim_txgen = txgen_file: submit_as_job (mkStarDerivation {
      #inherit NODEBUG; # enabling possibly breaks starsim
      name = "starsim-txgen.fzd";
      unpackPhase = "true";
      buildPhase = ''
        ${FAKETIME_CMD}starsim -w 0 -b ${./kumac/txgen.kumac} RUN=${toString RUN_NUMBER} TXGEN_FILE=${txgen_file} SEED=${toString SEED} FILE=output.fzd NUM_EVENTS=${toString NUM_EVENTS}
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        install -m444 output.fzd "$out_data"
        ln -s "$out_data" "$out"
      '';
    });

    CONS_CMD = "cons" + lib.optionalString USE_GCC44 " CC=/usr/bin/gcc44 CXX=/usr/bin/g++44 LD=/usr/bin/g++44";

    DBLIB_FIX = false;

    #
    # Provides a patched version of StDbLib capable of handling recent database
    # changes
    #
    StDbLib = mkStarDerivation {
      inherit NODEBUG;
      name = "StDbLib";
      unpackPhase = "true";
      buildPhase = ''
        mkdir StRoot
        cp -rv $STAR/StRoot/StDbLib StRoot/StDbLib
        patch -p1 <${./StDbLib.patch}
        ${CONS_CMD}
      '';
      installPhase = ''
        mkdir $out
        cp -rv ".$STAR_HOST_SYS" "$out"/
      '';
    };

    #
    # Provides StTpcRawDataSink
    # A patched version of StEvent is needed
    #
    StTpcRawDataSink = mkStarDerivation {
      inherit NODEBUG;
      name = "StTpcRawDataSink";
      src = lib.sourceByRegex ./. [
        "^StRoot$"
        "^StRoot/StTpcRawDataSink$"
        "^StRoot/StTpcRawDataSink/.+\.(cxx|h)$"
      ];
      buildPhase = ''
        cp -rv $STAR/StRoot/StEvent StRoot/StEvent
        patch -p1 <${./StTpcRawData_streamer.patch}
        ${CONS_CMD}
      '';
      installPhase = ''
        mkdir $out
        cp -rv ".$STAR_HOST_SYS" "$out"/
      '';
    };

    #
    # Provides StTpcRawDataSourceMixer
    # A patched version of StEvent is needed
    #
    StTpcRawDataSourceMixer = mkStarDerivation {
      inherit NODEBUG;
      name = "StTpcRawDataSourceMixer";
      src = lib.sourceByRegex ./. [
        "^StRoot$"
        "^StRoot/StTpcRawDataSourceMixer$"
        "^StRoot/StTpcRawDataSourceMixer/.+\.(cxx|h)$"
      ];
      buildPhase = ''
        cp -rv $STAR/StRoot/StEvent StRoot/StEvent
        patch -p1 <${./StTpcRawData_streamer.patch}
        ${CONS_CMD}
      '';
      installPhase = ''
        mkdir $out
        cp -rv ".$STAR_HOST_SYS" "$out"/
      '';
    };

    EMBED_EMC_FIX = false;

    #
    # Provides patched version of St_geant_Maker to fix
    # https://www.star.bnl.gov/rt3/Ticket/Display.html?id=3368
    #
    St_geant_Maker = mkStarDerivation {
      inherit NODEBUG;
      name = "St_geant_Maker";
      src = lib.sourceByRegex ./. [
      ];
      buildPhase = ''
        mkdir StRoot
        cp -rv $STAR/StRoot/St_geant_Maker StRoot/St_geant_Maker
        patch -p1 <${./St_geant_Maker_emc_fix.patch}
        ${CONS_CMD}
      '';
      installPhase = ''
        mkdir $out
        cp -rv ".$STAR_HOST_SYS" "$out"/
      '';
    };

    #
    # Provides StSetTruthIdMaker
    #
    StSetTruthIdMaker = mkStarDerivation {
      inherit NODEBUG;
      name = "StSetTruthIdMaker";
      src = lib.sourceByRegex ./. [
        "^StRoot$"
        "^StRoot/StSetTruthIdMaker$"
        "^StRoot/StSetTruthIdMaker/.+\.(cxx|h)$"
      ];
      buildPhase = ''
        ${CONS_CMD}
      '';
      installPhase = ''
        mkdir $out
        cp -rv ".$STAR_HOST_SYS" "$out"/
      '';
    };

    #
    # Provides StTpcRawDataWipeMaker
    #
    StTpcRawDataWipeMaker = mkStarDerivation {
      inherit NODEBUG;
      name = "StTpcRawDataWipeMaker";
      src = lib.sourceByRegex ./. [
        "^StRoot$"
        "^StRoot/StTpcRawDataWipeMaker$"
        "^StRoot/StTpcRawDataWipeMaker/.+\.(cxx|h)$"
      ];
      buildPhase = ''
        ${CONS_CMD}
      '';
      installPhase = ''
        mkdir $out
        cp -rv ".$STAR_HOST_SYS" "$out"/
      '';
    };

    # from https://www.star.bnl.gov/devcgi/dbProdOptionRetrv.pl
    RECO_BFC_OPTIONS = " DbV20130212 pp2012b AgML mtdDat btof fmsDat VFPPVnoCTB useBTOF4Vtx beamline BEmcChkStat Corr4 OSpaceZ2 OGridLeak3D -hitfilt -evout ";

    #
    # Run production-like reconstruction
    #
    bfc_reco = input_daq: submit_as_job (mkStarDerivation rec {
      inherit NODEBUG;
      name = "reco";
      unpackPhase = "true";
      buildPhase = ''
        touch --no-create `readlink -e ${input_daq}`
        cp ${input_daq} input.daq
        root4star -l -b bfc.C'(${toString NUM_EVENTS}, "${RECO_BFC_OPTIONS}", "input.daq", "output.root")'
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        install -Dm444 ./output.MuDst.root "$out_data"/output.MuDst.root
        install -Dm444 ./output.event.root "$out_data"/output.event.root
        ln -s "$out_data" "$out"
      '';
      INTERACTIVE=1;
      USER="veprbl";
      LOGNAME="veprbl";
      DB_SERVER_LOCAL_CONFIG="/direct/star+u/veprbl/dbLoadBalancerLocalConfig.xml";
    });

    #
    # Implements two modes of mixer operation. If *sink_mode* is false
    # then the mixer operates like a normal embedding mixer.  It can
    # take additional optional TPC response *input_rs* to mix into the
    # event.  If *sink_mode* is true then the mixer will write
    # simulated TPC response to a root file. The other products are
    # then discarded.
    #
    mixer_src = lib.sourceByRegex ./. [
      "^macros$"
      "^macros/mixer\.C$"
    ];
    mixer_impl = sink_mode: input_daq: input_fzd: input_rs:
    let
      # if set to true, engages StTpcRawDataWipeMaker that will
      # remove TPC data coming from DAQ file. It is not enabled in the
      # sink mode as it would have no effect there.
      do_wipe_tpcrawdata = (!sink_mode) && RESET_TPCRAWDATA;
      input_rs_C = if isNull input_rs then "NULL" else ''"${input_rs}"'';
    in submit_as_job (mkStarDerivation rec {
      inherit NODEBUG;
      name = if sink_mode then "tpcrs" else "mixer";
      src = mixer_src;
      soft = pkgs.symlinkJoin {
        name = "${name}-StRoot";
        paths = [
        ] ++ (lib.optional sink_mode StTpcRawDataSink)
          ++ (lib.optional (input_rs != null) StTpcRawDataSourceMixer)
          ++ (lib.optional do_wipe_tpcrawdata StTpcRawDataWipeMaker)
          ++ (lib.optional DBLIB_FIX StDbLib)
          ++ (lib.optional EMBED_EMC_FIX St_geant_Maker);
      };  
      buildPhase = ''
        touch --no-create `readlink -e ${input_daq}`
        touch --no-create `readlink -e ${input_fzd}`
        ln -s "$soft"/".$STAR_HOST_SYS" ".$STAR_HOST_SYS"
        cp ${input_daq} input.daq
        cp ${input_fzd} file.fzd
        root4star -l -b ./macros/mixer.C'(${toString NUM_EVENTS}, "input.daq", "file.fzd", ${if sink_mode then "true" else "false"}, ${input_rs_C}, "${MIXER_PROD_NAME}", "Jet", 0, ${if do_wipe_tpcrawdata then "true" else "false"})'
      '';
      doCheck = true;
      check_mudst = tools/check_mudst.C;
      checkPhase = ''
        cp $check_mudst ./check_mudst.C
        root -l -b -q ./check_mudst.C'("./file.MuDst.root", ${toString NUM_EVENTS})' | grep "MuDst is good"
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
      '' + (if sink_mode then ''
        install -Dm444 ./file.tpcraw.root "$out_data"/output.tpcraw.root
      '' else ''
        install -Dm444 ./file.MuDst.root "$out_data"/output.MuDst.root
      '') + ''
        ln -s "$out_data" "$out"
      '';
      INTERACTIVE=1;
      USER="veprbl";
      LOGNAME="veprbl";
      DB_SERVER_LOCAL_CONFIG="/direct/star+u/veprbl/dbLoadBalancerLocalConfig.xml";
      passthru = {
        daq = input_daq;
        fzd = input_fzd;
        rs = input_rs;
        rs_fzd = if input_rs != null then (input_rs.fzd or null) else null;
      };
    });
    #
    # Run standard jet embedding
    #
    mixer = mixer_impl false;

    #
    # A simple simulation setup to produce write StTpcRawData to root
    # files via StTpcRawDataSink. This code doesn't pick up correct
    # timestamps because it doesn't read any daq files.
    #
    tpcrs_naive = input_fzd: submit_as_job (mkStarDerivation rec {
      inherit NODEBUG;
      name = "${input_fzd.name}-tpcrs_naive.root";
      src = lib.sourceByRegex ./. [
        "^macros$"
        "^macros/run_tpcrs\.C$"
      ];
      soft = StTpcRawDataSink;
      buildPhase = ''
        touch --no-create `readlink -e ${input_fzd}`
        cp ${input_fzd} input.fzd
        ln -s "$soft"/".$STAR_HOST_SYS" ".$STAR_HOST_SYS"
        root4star -l -b ./macros/run_tpcrs.C'(${toString NUM_EVENTS}, "input.fzd")'
      '';
      installPhase = ''
        install -m444 output.root "$out"
      '';
    });

    take_response = out: {
      inherit (out) daq fzd rs rs_fzd;
      __toString = _: "${out}/output.tpcraw.root";
    };
    tpcrs = daq_file: fzd: take_response (mixer_impl true daq_file fzd null);

    # Not including DbV20160506_EMC_Calibrations since emc is not embedded
    MIXER_BFC_OPTIONS = RECO_BFC_OPTIONS + " TpxRaw TpxClu ";

    #
    # Implements basic production chain with additional TPC mixer.
    # Used to mix single tracks into the data.
    #
    data_mixer_src = lib.sourceByRegex ./. [
      "^macros$"
      "^macros/data_mixer\.C$"
    ];
    data_mixer = input_daq: input_rs: submit_as_job (mkStarDerivation rec {
      inherit NODEBUG;
      name = "data_mixer";
      src = data_mixer_src;
      soft = pkgs.symlinkJoin {
        name = "${name}-StRoot";
        paths = [
          StTpcRawDataSourceMixer
          StSetTruthIdMaker
        ]
          ++ (lib.optional DBLIB_FIX StDbLib);
      };
      # For use as C string: pass NULL if no response passed
      input_rs_C = if isNull input_rs then "NULL" else ''"${input_rs}"'';
      buildPhase = ''
        touch --no-create `readlink -e ${input_daq}`
        ln -s "$soft"/".$STAR_HOST_SYS" ".$STAR_HOST_SYS"
        cp ${input_daq} input.daq
        root4star -l -b ./macros/data_mixer.C'(${toString NUM_EVENTS}, "${MIXER_BFC_OPTIONS}", "input.daq", ${input_rs_C})'
      '';
      doCheck = true;
      check_mudst = tools/check_mudst.C;
      checkPhase = ''
        cp $check_mudst ./check_mudst.C
        root -l -b -q ./check_mudst.C'("./output.MuDst.root", ${toString NUM_EVENTS})' | grep "MuDst is good"
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        install -Dm444 ./output.MuDst.root "$out_data"/output.MuDst.root
        ln -s "$out_data" "$out"
      '';
      INTERACTIVE=1;
      USER="veprbl";
      LOGNAME="veprbl";
      DB_SERVER_LOCAL_CONFIG="/direct/star+u/veprbl/dbLoadBalancerLocalConfig.xml";
      passthru = {
        daq = input_daq;
        rs = input_rs;
        rs_fzd = if input_rs != null then (input_rs.fzd or null) else null;
      };
    });

# P12id
# rawData
# SL12d
# ry2012a	DbV20130212 pp2012b AgML mtdDat btof fmsDat VFPPVnoCTB useBTOF4Vtx beamline BEmcChkStat Corr4 OSpaceZ2 OGridLeak3D -hitfilt
# pp 200GeV run 2012 data production

# embedding missing btofMixer option

    fzd2geant = input_fzd: submit_as_job (mkStarDerivation rec {
      inherit NODEBUG;
      name = builtins.replaceStrings [".fzd"] [".geant.root"] input_fzd.name;
      src = lib.sourceByRegex ./. [
      ];
      soft = lib.optional EMBED_EMC_FIX St_geant_Maker;
      buildPhase = ''
        touch --no-create `readlink -e ${input_fzd}`
        ln -s "$soft"/".$STAR_HOST_SYS" ".$STAR_HOST_SYS"
        cp ${input_fzd} file.fzd
        root4star -l -b $STAR/StRoot/macros/fz2root.C'("file.fzd", ${toString NUM_EVENTS})'
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        install -Dm444 ./file.geant.root "$out_data"
        ln -s "$out_data" "$out"
      '';
      INTERACTIVE=1;
      USER="veprbl";
      LOGNAME="veprbl";
      DB_SERVER_LOCAL_CONFIG="/direct/star+u/veprbl/dbLoadBalancerLocalConfig.xml";
    });

    fzd2parquet = tag: input_fzd:
      geant2parquet tag (fzd2geant input_fzd);

    to_parquet = input_prod: stdenv.mkDerivation {
      name = "${input_prod.name or ""}-pq";
      phases = [ "installPhase" ];
      track_out = submit_as_job (track2parquet (input_prod.name or "") "${input_prod}/output.MuDst.root");
      # geant file for the original *input_fzd* (used for mixing pythia event):
      geant_out = if (input_prod.fzd or null) != null then
                  submit_as_job (fzd2parquet (input_prod.name or "") input_prod.fzd)
        else null;
      # geant file for the original *input_rs* (used for mixing single track response):
      rs_geant_out = if (input_prod.rs_fzd or null) != null then
                     submit_as_job (fzd2parquet (input_prod.name or "") input_prod.rs_fzd)
        else null;
      installPhase = ''
        mkdir "$out"
        ln -s "$track_out" "$out"/track.parquet
        if [ ! -z "$geant_out" ]; then
          ln -s "$geant_out" "$out"/geant.parquet
        fi
        if [ ! -z "$rs_geant_out" ]; then
          ln -s "$rs_geant_out" "$out"/rs.geant.parquet
        fi
      '';
      preferLocalBuild = true;
      allowSubstitutes = false;
      passthru = {
        inherit input_prod;
      };
    };

    pq2vertex = input_pq: stdenv.mkDerivation {
      name = "${builtins.replaceStrings ["-pq"] [""] (input_pq.name)}-vertex.txt";
      src = lib.sourceByRegex ./tools [
        "^pq2vertex\.py$"
      ];
      nativeBuildInputs = [
        pkgs.python3
        pkgs.python3Packages.numpy
        pkgs.python3Packages.pyarrow
        pkgs.python3Packages.pandas
        ];
      buildPhase = ''
        ./pq2vertex.py ${input_pq} > output.txt
      '';
      installPhase = ''
        install -m444 output.txt "$out"
      '';
      preferLocalBuild = true;
      allowSubstitutes = false;
    };

    EMBEDDED_PARTICLE_ID = 8; # \pi^+
    EMBEDDED_TRACK_PT_MIN = 0.2;
    EMBEDDED_TRACK_PT_MAX = 2.0;
    ETA_MIN = -1.5;
    ETA_MAX = 1.5;

    gen_txgen = input_vertex: stdenv.mkDerivation {
      name = "${builtins.replaceStrings ["-vertex.txt"] [""] input_vertex.name}.txgen";
      src = lib.sourceByRegex ./tools [
        "^gen_txgen\.py$"
      ];
      nativeBuildInputs = [ pkgs.python3 ];
      buildPhase = ''
        ./gen_txgen.py ${input_vertex} \
          --num-events ${toString NUM_EVENTS} \
          --seed ${toString SEED} \
          --particle-id ${toString EMBEDDED_PARTICLE_ID} \
          --track-pt-min ${toString EMBEDDED_TRACK_PT_MIN} \
          --track-pt-max ${toString EMBEDDED_TRACK_PT_MAX} \
          --eta-min ${toString ETA_MIN} \
          --eta-max ${toString ETA_MAX} \
          > output.txgen
      '';
      installPhase = ''
        install -m444 output.txgen "$out"
      '';
      preferLocalBuild = true;
      allowSubstitutes = false;
    };

    software = [
      pkgs.python3
      pkgs.python3Packages.numpy
      pkgs.python3Packages.pyarrow
      pkgs.python3Packages.pandas
      pkgs.lhapdf.pdf_sets.cteq6l1
      faketime
      StTpcRawDataSink
      StTpcRawDataSourceMixer
      StSetTruthIdMaker
      star2parquet.software
      star2parquet.eff_pkgs.openssl.out
    ]
    # A hacky way to get dependencies
    ++ (map (f: (f null null).nativeBuildInputs or []) [track2parquet geant2parquet])
    ;

    reco = to_parquet (bfc_reco DAQ_FILE);
    data = to_parquet (data_mixer DAQ_FILE null);
    data_vertices = pq2vertex data;
    data_simulated_tracks = tpcrs DAQ_FILE (starsim_txgen (gen_txgen data_vertices));
    data_single_track = to_parquet (data_mixer DAQ_FILE data_simulated_tracks);

    embed_vertices = generate_vertex NUM_EVENTS ZB_DAQ_FILE;
    embed_pythia = starsim_pythia embed_vertices;
    embed = to_parquet (mixer
      ZB_DAQ_FILE
      embed_pythia
      null
      );

    # A naive single track embedding embeds into the same vertex as
    # was given to pythia.
    embed_simulated_tracks_naive = tpcrs ZB_DAQ_FILE (starsim_txgen (gen_txgen embed_vertices));
    embed_single_track_naive = to_parquet (mixer
      ZB_DAQ_FILE
      embed_pythia
      embed_simulated_tracks_naive
      );

    # This is a more sophisticated single track embedding that matchess
    # the procedure used in the data. Here, the vertices are taken
    # from reconstructed embedding.
    embed_vertices_single_track = pq2vertex embed;
    embed_simulated_tracks = tpcrs ZB_DAQ_FILE (starsim_txgen (gen_txgen embed_vertices_single_track));
    embed_single_track = to_parquet (mixer
      ZB_DAQ_FILE
      embed_pythia
      embed_simulated_tracks
      );

  };
in
  lib.makeExtensibleWithCustomName "override" this
