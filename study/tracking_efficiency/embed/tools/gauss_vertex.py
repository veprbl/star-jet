#!/usr/bin/env python3

import argparse
import random

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--seed", type=int, required=True,
                        help="random number generator seed")
    parser.add_argument("--num-events", type=int, required=True,
                        help="number of events to generate")
    for axis in ["x", "y", "z"]:
        parser.add_argument(f"--mu-{axis}", type=float, required=True,
                            help=f"mean of the v_{axis} normal distribution")
        parser.add_argument(f"--sigma-{axis}", type=float, required=True,
                            help="standard deviation of the v_{axis} normal distribution")
    args = parser.parse_args()

    assert args.sigma_x >= 0
    assert args.sigma_y >= 0
    assert args.sigma_z >= 0

    # fixed offset to prevent correlations with other tools
    random.seed(a=args.seed + 67890, version=2)

    for _ in range(args.num_events):
        vx = random.gauss(args.mu_x, args.sigma_x)
        vy = random.gauss(args.mu_y, args.sigma_y)
        vz = random.gauss(args.mu_z, args.sigma_z)
        print(f"{vx}\t{vy}\t{vz}")
