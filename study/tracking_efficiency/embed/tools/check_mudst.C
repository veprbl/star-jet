void check_mudst(const char *filename, unsigned int num_events) {
  TFile f(filename);
  if (!f.IsOpen()) {
    std::cerr << "Can't open `" << filename << "'." << std::endl;
    return;
  }
  TTree *tree = dynamic_cast<TTree*>(f.Get("MuDst"));
  if (!tree) {
    std::cerr
      << "MuDst tree is not found in `" << filename << "'." << std::endl;
    return;
  }
  if (tree->GetEntries() != num_events) {
    std::cerr
      << "Found " << tree->GetEntries() << " entries in `" << filename << "'"
      << " when " << num_events << " was needed."
      << std::endl;
    return;
  }
  std::cout << "MuDst is good" << std::endl;
}
