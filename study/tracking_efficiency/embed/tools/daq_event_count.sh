#!/bin/sh

set -e

for daq_file in $HOME/daq/st_*13104003*.daq; do
  printf "`basename $daq_file`\t"
  root4star -q -b -l 'bfc.C(1000000, "in", "'$daq_file'")' 2>&1 | grep "QAInfo:Run is finished"
done
