#!/usr/bin/env python3

import argparse
import math
import random

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", type=argparse.FileType('r'),
                        help="input file containing vertices")
    parser.add_argument("--num-events", type=int,
                        help="number of events to generate")
    parser.add_argument("--seed", type=int, required=True,
                        help="random seed")
    parser.add_argument("--particle-id", type=int, required=True,
                        help="geant id code of the particle to embed")
    parser.add_argument("--track-pt-min", type=float, required=True,
                        help="minimal track p_T in GeV")
    parser.add_argument("--track-pt-max", type=float, required=True,
                        help="maximal track p_T in GeV")
    parser.add_argument("--eta-min", type=float, required=True,
                        help="minimal track eta")
    parser.add_argument("--eta-max", type=float, required=True,
                        help="maximal track eta")
    args = parser.parse_args()

    # fixed offset to prevent correlations with other tools
    random.seed(a=args.seed + 234578, version=2)

    input_vertices = [tuple(float(x) for x in line.split())
                      for line in args.input_file.readlines()]

    if len(input_vertices) > args.num_events:
        sys.stderr.write("Error: Number events requested exceeds number of provided vertices")

    for event_id, vertex in zip(range(args.num_events), input_vertices):
        p_T = random.uniform(args.track_pt_min, args.track_pt_max) # GeV
        eta = random.uniform(args.eta_min, args.eta_max)
        phi = random.uniform(0., 2 * math.pi)
        p_x = p_T * math.cos(phi)
        p_y = p_T * math.sin(phi)
        p_z = p_T * math.sinh(eta)
        print(f"""\
EVENT {event_id + 1} 1 0
VERTEX {vertex[0]} {vertex[1]} {vertex[2]} 0 1 0 0 1
TRACK {args.particle_id} {p_x} {p_y} {p_z} 1 1 0 0
""")
