#!/usr/bin/env python3

import argparse
import json
import os.path

import numpy as np
import pyarrow.parquet as pq

def _dir(path):
    """
    Validator for directory argument
    """
    import os.path
    if not os.path.exists(path):
        raise argparse.ArgumentTypeError(f"{path} doesn't exist")
    elif not os.path.isdir(path):
        raise argparse.ArgumentTypeError(f"{path} is not a directory")
    else:
        return path

def get_best_vertex(row):
    """
    Returns position of the highest ranking vertex
    """
    vertices = zip(
        row['primaryVertexX'],
        row['primaryVertexY'],
        row['primaryVertexZ'],
    )
    vs = list(zip(
        row['primaryVertexRank'],
        vertices,
    ))
    vs.sort(key=lambda v: -v[0]) # sort by rank from high to low
    if not vs:
        return (-999., -999., -999.)
    else:
        rank, vertex = vs[0]
        return vertex # it is dangerous to return list-like object to DataFrame.apply() ...

def is_vertex(v):
    return (type(v) is tuple) and (len(v) == 3)
    
if __name__ == '__main__':
    import sys

    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir', type=_dir)
    args = parser.parse_args()
    tracks = pq.read_table(os.path.join(args.input_dir, "track.parquet"), use_threads=False).to_pandas()

    tracks['eventBestVertexZ'] = tracks.apply(get_best_vertex, axis=1, result_type="reduce")

    vertices = tracks['eventBestVertexZ'].values
    for vx, vy, vz in vertices:
        print(f"{vx}\t{vy}\t{vz}")
