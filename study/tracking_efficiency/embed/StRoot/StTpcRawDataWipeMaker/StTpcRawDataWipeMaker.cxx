#include <StEvent/StTpcRawData.h>
#include <St_base/StMessMgr.h>

#include "StTpcRawDataWipeMaker.h"

Int_t StTpcRawDataWipeMaker::Make() {
  LOG_INFO << "Make()" << endm;

  TObjectSet *event = dynamic_cast<TObjectSet*>(GetDataSet("Target"));
  if (!event) {
    LOG_FATAL << "Can't find output event" << endm;
    return kStErr;
  }

  StTpcRawData *tpcrawdata = dynamic_cast<StTpcRawData*>(event->GetObject());
  if (!tpcrawdata) {
    LOG_FATAL << "Missing StTpcRawData object" << endm;
    return kStErr;
  }

  tpcrawdata->Clear();

  return kStOK;
}
