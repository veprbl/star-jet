#pragma once

#include <StChain/StMaker.h>

/**
 * Clears StTpcRawData in the event
 */
class StTpcRawDataWipeMaker : public StMaker {
public:

  StTpcRawDataWipeMaker(const char *name = "tpc_raw_data_wipe")
    : StMaker(name)
  {};
  virtual ~StTpcRawDataWipeMaker() {};

  Int_t Make();

  ClassDef(StTpcRawDataWipeMaker, 0);
};
