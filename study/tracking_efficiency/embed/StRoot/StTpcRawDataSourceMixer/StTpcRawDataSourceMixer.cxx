#include <type_traits>
#include <TObject.h>
#include <StEvent/StTpcRawData.h>
static_assert(
  std::is_base_of<TObject,StDigitalPair>::value,
  "This version of StEvent doesn't support serialization of StTpcRawData"
);

#include "StTpcRawDataSourceMixer.h"

Int_t StTpcRawDataSourceMixer::Init() {
  _infile = new TFile(_filename.c_str());
  _tree = dynamic_cast<TTree*>(_infile->Get("tpc_raw"));
  if (!_tree) {
    LOG_ERROR << "StTpcRawDataSourceMixer: can't get input tree" << endm;
    return kStErr;
  }
  _entry = 0;
  _tpc_raw_data = nullptr;
  _first = true;
  return kStOK;
}

void StTpcRawDataSourceMixer::ApplyTpcRawDataTruthIdOffset(
  StTpcRawData *trd, UShort_t _id_offset
) {
  UInt_t num_sectors = trd->getNoSectors();
  for (UInt_t sector_id = 1; sector_id <= num_sectors; sector_id++) {
    StTpcDigitalSector *sector = trd->getSector(sector_id);
    if (!sector) continue;
    StDigitalSector &sector_rows = *sector->rows();

    for (StDigitalSector::iterator row_it = sector_rows.begin();
         row_it != sector_rows.end(); ++row_it) {
      StDigitalPadRow &row = *row_it;

      for (StDigitalPadRow::iterator pad_it = row.begin(); pad_it != row.end();
           ++pad_it) {
        StDigitalTimeBins &pad = *pad_it;

        for (StDigitalTimeBins::iterator time_bin_it = pad.begin();
             time_bin_it != pad.end(); ++time_bin_it) {
          StDigitalPair &time_bin = *time_bin_it;

          if (!time_bin.isIdt()) continue;
          UShort_t *idt = time_bin.idt();
          assert(idt);
          Int_t size = time_bin.size();
          for (Int_t ix = 0; ix < size; ix++) {
            idt[ix] += _id_offset;
          }
        }
      }
    }
  }
}

Int_t StTpcRawDataSourceMixer::Make() {
  LOG_INFO << "StTpcRawDataSourceMixer::Make()" << endm;

  // Workround for SL12d_embed:
  // We can't set up the branch before event timestamp is ready. It implicitly
  // calls the constructor of StTpcRawData which tries to access databse to
  // learn about TPC parameters. That leads to a crash.
  if (_first) {
    _tree->SetBranchAddress("tpc_raw", &_tpc_raw_data);
    _first = false;
  }

  TObjectSet *event = dynamic_cast<TObjectSet*>(GetDataSet("Output"));
  if (!event) {
    LOG_ERROR << "StTpcRawDataSourceMixer: can't find output event" << endm;
    return kStErr;
  }
  StTpcRawData *daq_raw_data = dynamic_cast<StTpcRawData*>(event->GetObject());
  if (!daq_raw_data) {
    LOG_ERROR
      << "StTpcRawDataSourceMixer: can't get StTpcRawData instance"
      << endm;
    return kStErr;
  }

  Int_t read = _tree->GetEntry(_entry);
  if (read <= 0) {
    LOG_ERROR << "StTpcRawDataSourceMixer: can't read input tree" << endm;
    return kStErr;
  }

  ApplyTpcRawDataTruthIdOffset(_tpc_raw_data, id_offset);
  *daq_raw_data += *_tpc_raw_data;

  _entry++;

  return kStOK;
}

Int_t StTpcRawDataSourceMixer::Finish() {
  delete _tpc_raw_data; _tpc_raw_data = nullptr;
  delete _tree; _tree = nullptr;
  delete _infile; _infile = nullptr;
  return kStOK;
}
