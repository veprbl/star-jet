#pragma once

#include <string>

#include <TFile.h>
#include <TTree.h>

#include <StChain/StMaker.h>
#include <StEvent/StTpcRawData.h>

/**
 * Read StTpcRawData from a ROOT, tree then mix it into TpxRaw event
 */
class StTpcRawDataSourceMixer : public StMaker {
public:

  StTpcRawDataSourceMixer(std::string &filename)
    : id_offset(0)
    , _filename(filename)
  {}
  virtual ~StTpcRawDataSourceMixer() {};

  Int_t Init();
  Int_t Make();
  Int_t Finish();

  static void ApplyTpcRawDataTruthIdOffset(StTpcRawData *trd, UShort_t id_offset);
  UShort_t id_offset;

  ClassDef(StTpcRawDataSourceMixer, 0);

private:

  std::string _filename;
  TFile *_infile;
  TTree *_tree;
  Long64_t _entry;
  StTpcRawData *_tpc_raw_data;
  bool _first;
};
