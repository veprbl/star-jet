#pragma once

#include <StChain/StMaker.h>

/**
 * In standard embedding StEvent::setIdTruth() is called by
 * StMuDstMaker::fillMC(), but latter requires geant datasets to be
 * present. This simple maker will do the job instead.
 */
class StSetTruthIdMaker : public StMaker {
public:

  virtual ~StSetTruthIdMaker() {};

  Int_t Make();

  ClassDef(StSetTruthIdMaker, 0);
};
