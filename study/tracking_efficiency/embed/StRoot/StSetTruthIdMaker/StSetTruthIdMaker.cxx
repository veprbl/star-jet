#include <St_base/StMessMgr.h>
#include <StEvent/StEvent.h>

#include "StSetTruthIdMaker.h"

Int_t StSetTruthIdMaker::Make() {
  LOG_INFO << "Calling StEvent::setIdTruth()" << endm;
  StEvent *st_event = dynamic_cast<StEvent*>(GetInputDS("StEvent"));
  st_event->setIdTruth();
  return kStOk;
}
