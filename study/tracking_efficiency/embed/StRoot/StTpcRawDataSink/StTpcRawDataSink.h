#pragma once

#include <string>

#include <TFile.h>
#include <TTree.h>

#include <StChain/StMaker.h>
#include <StEvent/StTpcRawData.h>

/**
 * Dumps StTpcRawData into a ROOT file
 */
class StTpcRawDataSink : public StMaker {
public:

  StTpcRawDataSink(const char *name = "tpc_raw_data_sink") : StMaker(name) {};
  virtual ~StTpcRawDataSink() {};

  Int_t Init();
  Int_t Make();
  Int_t Finish();

  void set_skip(bool flag);

  ClassDef(StTpcRawDataSink, 0);

private:

  TFile *_outfile;
  TTree *_tree;
  StTpcRawData *_tpc_raw_data;
  bool _enable_skip;
  bool _first;
};
