#include <type_traits>
#include <TObject.h>
#include <StEvent/StTpcRawData.h>
static_assert(
  std::is_base_of<TObject,StDigitalPair>::value,
  "This version of StEvent doesn't support serialization of StTpcRawData"
);
#include <TFile.h>
#include <StChain/StChainOpt.h>
#include <St_base/StMessMgr.h>

#include "StTpcRawDataSink.h"

Int_t StTpcRawDataSink::Init() {
  TString output_filename = GetChainOpt()->GetFileOut();
  output_filename.ReplaceAll(".root", ".tpcraw.root");
  _outfile = new TFile(output_filename.Data(), "RECREATE");
  _tree = new TTree("tpc_raw", "");
  _tpc_raw_data = nullptr;
  _first = true;
  return kStOK;
}

Int_t StTpcRawDataSink::Make() {
  LOG_INFO << "Make()" << endm;

  // Workround for SL12d_embed:
  // We can't set up the branch before event timestamp is ready. It implicitly
  // calls the constructor of StTpcRawData which tries to access databse to
  // learn about TPC parameters. That leads to a crash.
  if (_first) {
    _tree->Branch("tpc_raw", &_tpc_raw_data);
    _first = false;
  }

  Int_t retcode = kStOk;
  static StTpcRawData dummy;

  TObjectSet *event = dynamic_cast<TObjectSet*>(GetDataSet("Input"));
  if (!event) {
    LOG_ERROR << "StTpcRawDataSink: can't find input event" << endm;
    _tpc_raw_data = &dummy;
    retcode = kStWarn;
  }
  else
  {
    _tpc_raw_data = dynamic_cast<StTpcRawData*>(event->GetObject());
    if (!_tpc_raw_data) {
      LOG_ERROR << "StTpcRawDataSink: can't get StTpcRawData instance" << endm;
      _tpc_raw_data = &dummy;
      retcode = kStWarn;
    }
  }

  _tree->Fill();

  if (_enable_skip) {
    retcode = kStSkip;
  }

  return retcode;
}

Int_t StTpcRawDataSink::Finish() {
  auto _dir = gDirectory;
  _outfile->cd();
  _tree->Write();
  gDirectory = _dir;
  delete _tree; _tree = nullptr;
  delete _outfile; _outfile = nullptr;
  return kStOK;
}

/**
 * If StTpcRawDataSink is inserted in the midle of a big chain (like
 * embedding), then, oftentimes, we are not interested in further processing
 * the current event. This option makes StTpcRawDataSink automatically skip to
 * the next event after dumping StTpcRawData.
 */
void StTpcRawDataSink::set_skip(bool flag) {
  _enable_skip = flag;
  SetAttr(".Privilege", _enable_skip ? 1 : 0);
}
