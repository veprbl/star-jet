{
bits,
attr_name,
inputs,
num_repeats,
seed_offset ? 0,
lib,
}:

let
  # like range() in python
  range = builtins.genList (i: i);
  repeat_list = xs: n: builtins.concatLists (builtins.genList (_: xs) n);

  num_inputs = builtins.length inputs;
  inputs_and_seeds = lib.zipLists
    (repeat_list inputs num_repeats)
    (range (num_repeats * num_inputs))
    ;
in
  map (args:
    let
      input = args.fst;
      seed = args.snd + seed_offset;
      _bits = bits.override (final: prev:
        { SEED = seed; } // input
      );
      splitPath = path: with builtins; filter isString (split "\\." path);
    in
      lib.getAttrFromPath (splitPath attr_name) _bits
  ) inputs_and_seeds
