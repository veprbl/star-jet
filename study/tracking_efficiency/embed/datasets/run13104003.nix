{
  st_physics_daq_files = [
    #{ DAQ_FILENAME = "st_physics_adc_13104003_raw_0490001.daq"; NUM_EVENTS = 2; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_1500001.daq"; NUM_EVENTS = 466; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_1500002.daq"; NUM_EVENTS = 469; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_1500003.daq"; NUM_EVENTS = 476; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_1500004.daq"; NUM_EVENTS = 482; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_1500005.daq"; NUM_EVENTS = 488; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_1500006.daq"; NUM_EVENTS = 492; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_1500007.daq"; NUM_EVENTS = 435; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_1520001.daq"; NUM_EVENTS = 467; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_1520002.daq"; NUM_EVENTS = 483; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_1520003.daq"; NUM_EVENTS = 304; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_2520001.daq"; NUM_EVENTS = 466; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_2520002.daq"; NUM_EVENTS = 467; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_2520003.daq"; NUM_EVENTS = 475; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_2520004.daq"; NUM_EVENTS = 477; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_2520005.daq"; NUM_EVENTS = 482; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_2520006.daq"; NUM_EVENTS = 485; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_2520007.daq"; NUM_EVENTS = 492; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_2520008.daq"; NUM_EVENTS = 498; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_2520009.daq"; NUM_EVENTS = 499; }
    #{ DAQ_FILENAME = "st_physics_adc_13104003_raw_2520010.daq"; NUM_EVENTS = 52; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_3520001.daq"; NUM_EVENTS = 465; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_3520002.daq"; NUM_EVENTS = 468; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_3520003.daq"; NUM_EVENTS = 473; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_3520004.daq"; NUM_EVENTS = 479; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_3520005.daq"; NUM_EVENTS = 483; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_3520006.daq"; NUM_EVENTS = 486; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_3520007.daq"; NUM_EVENTS = 489; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_3520008.daq"; NUM_EVENTS = 492; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_3520009.daq"; NUM_EVENTS = 497; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_3520010.daq"; NUM_EVENTS = 245; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_4520001.daq"; NUM_EVENTS = 464; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_4520002.daq"; NUM_EVENTS = 469; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_4520003.daq"; NUM_EVENTS = 473; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_4520004.daq"; NUM_EVENTS = 477; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_4520005.daq"; NUM_EVENTS = 482; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_4520006.daq"; NUM_EVENTS = 486; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_4520007.daq"; NUM_EVENTS = 488; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_4520008.daq"; NUM_EVENTS = 490; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_4520009.daq"; NUM_EVENTS = 495; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_4520010.daq"; NUM_EVENTS = 306; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_5490001.daq"; NUM_EVENTS = 466; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_5490002.daq"; NUM_EVENTS = 468; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_5490003.daq"; NUM_EVENTS = 475; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_5490004.daq"; NUM_EVENTS = 477; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_5490005.daq"; NUM_EVENTS = 482; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_5490006.daq"; NUM_EVENTS = 487; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_5490007.daq"; NUM_EVENTS = 491; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_5490008.daq"; NUM_EVENTS = 495; }
    { DAQ_FILENAME = "st_physics_adc_13104003_raw_5490009.daq"; NUM_EVENTS = 496; }
  ];
  st_zerobias_daq_files = [
    #{ ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_0580001.daq"; NUM_EVENTS = 7; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_1580001.daq"; NUM_EVENTS = 473; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_1580002.daq"; NUM_EVENTS = 491; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_2590001.daq"; NUM_EVENTS = 474; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_2590002.daq"; NUM_EVENTS = 466; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_3580001.daq"; NUM_EVENTS = 474; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_3580002.daq"; NUM_EVENTS = 470; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_4570001.daq"; NUM_EVENTS = 476; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_4570002.daq"; NUM_EVENTS = 467; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_5580001.daq"; NUM_EVENTS = 473; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13104003_raw_5580002.daq"; NUM_EVENTS = 470; }
  ];
}
