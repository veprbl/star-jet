{
  st_physics_daq_files = [
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_1490001.daq"; NUM_EVENTS = 1377; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_1490002.daq"; NUM_EVENTS = 1385; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_1490003.daq"; NUM_EVENTS = 1389; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_1490004.daq"; NUM_EVENTS = 1402; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_1490005.daq"; NUM_EVENTS = 1402; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_1490006.daq"; NUM_EVENTS = 489; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_2490001.daq"; NUM_EVENTS = 1374; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_2490002.daq"; NUM_EVENTS = 1384; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_2490003.daq"; NUM_EVENTS = 1394; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_2490004.daq"; NUM_EVENTS = 1405; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_2490005.daq"; NUM_EVENTS = 562; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_3520001.daq"; NUM_EVENTS = 1372; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_3520002.daq"; NUM_EVENTS = 1379; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_3520003.daq"; NUM_EVENTS = 1381; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_3520004.daq"; NUM_EVENTS = 1394; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_3520005.daq"; NUM_EVENTS = 1413; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_3520006.daq"; NUM_EVENTS = 561; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_4490001.daq"; NUM_EVENTS = 1374; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_4490002.daq"; NUM_EVENTS = 1390; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_4490003.daq"; NUM_EVENTS = 1391; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_4490004.daq"; NUM_EVENTS = 1401; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_4490005.daq"; NUM_EVENTS = 1026; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_5520001.daq"; NUM_EVENTS = 1371; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_5520002.daq"; NUM_EVENTS = 1383; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_5520003.daq"; NUM_EVENTS = 1395; }
    { DAQ_FILENAME = "st_physics_adc_13059007_raw_5520004.daq"; NUM_EVENTS = 1164; }
  ];
  st_zerobias_daq_files = [
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13059007_raw_1580001.daq"; NUM_EVENTS = 418; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13059007_raw_2580001.daq"; NUM_EVENTS = 355; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13059007_raw_3570001.daq"; NUM_EVENTS = 446; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13059007_raw_4580001.daq"; NUM_EVENTS = 471; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13059007_raw_5580001.daq"; NUM_EVENTS = 363; }
  ];
}
