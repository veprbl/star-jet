#!/bin/sh

set -e

runnumber="$1"

echo "{"
echo "  st_physics_daq_files = ["
get_file_list.pl \
      -keys filename,events \
      -cond runnumber=$runnumber,filetype=online_daq,storage=HPSS,filename~st_physics_adc \
      -limit 0 | \
      awk -F"::" '{ print "    { DAQ_FILENAME = \""$1"\"; NUM_EVENTS = "$2"; }"; }'
echo "  ];"
echo "  st_zerobias_daq_files = ["
get_file_list.pl \
      -keys filename,events \
      -cond runnumber=$runnumber,filetype=online_daq,storage=HPSS,filename~st_zerobias_adc \
      -limit 0 | \
      awk -F"::" '{ print "    { ZB_DAQ_FILENAME = \""$1"\"; NUM_EVENTS = "$2"; }"; }'
echo "  ];"
echo "}"
