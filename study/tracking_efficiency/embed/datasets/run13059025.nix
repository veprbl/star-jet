{
  st_physics_daq_files = [
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_0520001.daq"; NUM_EVENTS = 34; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_1500001.daq"; NUM_EVENTS = 1829; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_1500002.daq"; NUM_EVENTS = 1834; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_1500003.daq"; NUM_EVENTS = 1828; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_1500004.daq"; NUM_EVENTS = 1837; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_1500005.daq"; NUM_EVENTS = 1245; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_2520001.daq"; NUM_EVENTS = 1814; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_2520002.daq"; NUM_EVENTS = 1824; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_2520003.daq"; NUM_EVENTS = 1836; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_2520004.daq"; NUM_EVENTS = 1842; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_3520001.daq"; NUM_EVENTS = 1822; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_3520002.daq"; NUM_EVENTS = 1834; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_3520003.daq"; NUM_EVENTS = 1828; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_3520004.daq"; NUM_EVENTS = 1846; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_3520005.daq"; NUM_EVENTS = 1184; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_4500001.daq"; NUM_EVENTS = 1830; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_4500002.daq"; NUM_EVENTS = 1838; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_4500003.daq"; NUM_EVENTS = 1832; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_4500004.daq"; NUM_EVENTS = 1834; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_4500005.daq"; NUM_EVENTS = 197; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_5520001.daq"; NUM_EVENTS = 1819; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_5520002.daq"; NUM_EVENTS = 1827; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_5520003.daq"; NUM_EVENTS = 1830; }
    { DAQ_FILENAME = "st_physics_adc_13059025_raw_5520004.daq"; NUM_EVENTS = 1095; }
  ];
  st_zerobias_daq_files = [
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13059025_raw_1580001.daq"; NUM_EVENTS = 509; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13059025_raw_2580001.daq"; NUM_EVENTS = 428; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13059025_raw_3570001.daq"; NUM_EVENTS = 499; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13059025_raw_4580001.daq"; NUM_EVENTS = 530; }
    { ZB_DAQ_FILENAME = "st_zerobias_adc_13059025_raw_5600001.daq"; NUM_EVENTS = 404; }
  ];
}
