final: prev:

{
  arrow-cpp =
    # we already build arrow-cpp with python3
    (prev.arrow-cpp.override { python = prev.python3; })
  ;

  boost = prev.boost.override { enablePython = false; };

  # nghttp2 doesn't build with gcc48 out of the box
  curl = prev.curl.override { http2Support = false; };

  # can't build modern glibc with gcc48
  glibcLocales = null;

  # graphviz doesn't build with gcc48
  graphviz = null;
  uriparser = prev.uriparser.overrideAttrs (old: {
    configureFlags = "--disable-doc";
  });
}
