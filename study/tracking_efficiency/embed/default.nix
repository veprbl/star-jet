let
  nixpkgs_src = fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/79875644e0892f4bd76080f48faf577805ec3174.tar.gz";
    sha256 = "0n6varg3f2gwj0lq8pnfj0bfk1a06wmzf1aik556liv8ymrs2mhz";
  };
  my_nixpkgs_overlay = import (fetchTarball {
    url = "https://gitlab.com/veprbl/my-nixpkgs-overlay/-/archive/b4077a8066727eeaa199b42b7fb151b12866b909/my-nixpkgs-overlay-b4077a8066727eeaa199b42b7fb151b12866b909.tar.gz";
    sha256 = "005l9jdyhkrdi67qi6cswbnz0f8cx4lk2kjbvias05lj7gjxismc";
  });
  nixpkgs_2009_hacks = import ./nixpkgs_20.09_hacks;
  overlays = [
    my_nixpkgs_overlay
    nixpkgs_2009_hacks
  ];
  nixpkgs = (import nixpkgs_src {
    inherit overlays;
  });
  inherit (nixpkgs) lib;

  give_name = path:
    assert builtins.typeOf path == "path";
    {
      inherit path;
      name = builtins.baseNameOf path;
      __toString = final: toString final.path;
    };

  OUTPUT_DIRECTORY = "`cat ${builtins.getEnv "HOME"}/.embed_output_path`";
  DAQ_ROOT = "${builtins.getEnv "HOME"}/daq";

  bits = (import ./bits.nix { inherit nixpkgs nixpkgs_src overlays; }).override (final: prev: {
    inherit OUTPUT_DIRECTORY;
    NUM_EVENTS = 1000;
    PYTHIA_PT_MIN = 0 /* GeV */;
    DAQ_FILE = "${DAQ_ROOT}/${final.DAQ_FILENAME}";
    ZB_DAQ_FILE = "${DAQ_ROOT}/${final.ZB_DAQ_FILENAME}";
    GAUSS_SIGMA_Z = 26.20; # cm. This value is used for comparisons to VPDMB
  });
  override_sl12d_embed = final: prev: {
    STAR_VERSION = "SL12d_embed"; USE_SL5 = false; USE_GCC44 = true; EMBED_EMC_FIX = false; DBLIB_FIX = true; PATCH_MGR = true;
  };
  override_sl13b_embed = final: prev: {
    STAR_VERSION = "SL13b_embed"; USE_SL5 = false; USE_GCC44 = true; EMBED_EMC_FIX = false;
  };
  override_reset_tpcrawdata = final: prev: {
    RESET_TPCRAWDATA = true;
  };
  override_sl18c = final: prev: {
    STAR_VERSION = "SL18c"; USE_SL5 = false; USE_GCC44 = false; EMBED_EMC_FIX = true;
  };
  override_eplus = final: prev: {
    EMBEDDED_PARTICLE_ID = 2;
  };
  override_eminus = final: prev: {
    EMBEDDED_PARTICLE_ID = 3;
  };
  override_piplus = final: prev: {
    EMBEDDED_PARTICLE_ID = 8;
  };
  override_piminus = final: prev: {
    EMBEDDED_PARTICLE_ID = 9;
  };
  override_kplus = final: prev: {
    EMBEDDED_PARTICLE_ID = 11;
  };
  override_kminus = final: prev: {
    EMBEDDED_PARTICLE_ID = 12;
  };
  override_pp200 = final: prev: {
    RUN_NUMBER = 13059025;
    MIXER_PROD_NAME = "P12idpp200";
    PYTHIA_ROOTS = "200.0";
    ZB_DAQ_FILENAME = "st_zerobias_adc_13059025_raw_1580001.daq";
    DAQ_FILENAME = "st_physics_adc_13059025_raw_1500001.daq";
    DATA_SINGLE_TRACK_NUM_REPEATS = 20;
    EMBED_NUM_REPEATS = 160;
    EMBED_SINGLE_TRACK_NUM_REPEATS = 160;
  };
  override_pp510 = final: prev: {
    RUN_NUMBER = 13104003;
    MIXER_PROD_NAME = "P13ibpp500RFF";
    PYTHIA_ROOTS = "510.0";
    ZB_DAQ_FILENAME = throw "embed/default.nix: No single daq file chosen for pp510";
    DAQ_FILENAME = throw "embed/default.nix: No single daq file chosen for pp510";
    DATA_SINGLE_TRACK_NUM_REPEATS = 20;
    EMBED_NUM_REPEATS = 40;
    EMBED_SINGLE_TRACK_NUM_REPEATS = 40;
    RECO_BFC_OPTIONS = " DbV20130502 pp2012b AgML mtdDat btof fmsDat VFPPVnoCTB beamline BEmcChkStat Corr4 OSpaceZ2 OGridLeak3D -hitfilt -evout ";
    tpcrs_naive = throw "embed/default.nix: tpcrs_naive is not implemented for pp510";
  };
  override_pt2_3 = final: prev: {
    PYTHIA_PT_MIN = 2;
    PYTHIA_PT_MAX = 3;
  };
  override_pt11_15 = final: prev: {
    PYTHIA_PT_MIN = 11;
    PYTHIA_PT_MAX = 15;
  };
  override_no_repeats = final: prev: {
    DATA_SINGLE_TRACK_NUM_REPEATS = 1;
    EMBED_NUM_REPEATS = 1;
    EMBED_SINGLE_TRACK_NUM_REPEATS = 1;
  };
  override_ten_events = final: prev: {
    NUM_EVENTS = 10;
  };
  override_no_submit = final: prev: {
    submit_as_job = x: x;
  };
  override_debug = final: prev: {
    NODEBUG = "no";
  };

  #
  # The overrides are engaged in various combinations via a tree level
  # structure. The first is to choice is between two reactions:
  #
  # * pp200 - for pp collisions at \sqrt{s} = 200 GeV
  # * pp510 - same at 510 GeV
  #
  # At next level the user must chose a version of the library to be
  # used for event simulation and reconstruction. The sl13b_embed is
  # the safe choice for the current setup.
  #
  # All of the following overrides are not required for the
  # reconstruction and embedding to work. The final part of the "path"
  # is an attribute name for the task to be executed. A typical
  # attributes of interest would be:
  #
  # * data
  # * data_single_track
  # * embed
  # * embed_single_track
  #
  # See the bits.nix for the full list:
  #
  # A few examples:
  #
  # 1) A following example generates a single data reconstruction run:
  #
  #    nix-build . -A pp200.sl13b_embed.data
  #
  # 2) An example of running over all DAQ files in run 13104003 of
  # embedding with single positive kaon embedded:
  #
  #    nix-build . -A pp510.sl13b_embed.kplus.run13104003.embed_single_track
  #
  # 3) To run the "Pure MC" embedding:
  #
  #    nix-build . -A pp200.sl13b_embed.reset_tpcrawdata.embed
  #

  datasets_dir = ./datasets;

  /* Import *.nix files from the dataset dir as an attrset which maps:

       (filename without extension) -> (import of the file)

     Type:
       datasets :: AttrSet
  */
  datasets =
    builtins.listToAttrs
      (map
        (m: rec {
          name = builtins.elemAt m 1;
          value = import (datasets_dir + ("/" + builtins.elemAt m 0));
        })
        (builtins.filter (m: m != null)
          (map (builtins.match "^((.+)\.nix)$")
            (builtins.attrNames (builtins.readDir datasets_dir)))));

  /* Defines a level to evaluate attributes "data",
     "data_single_track", "embed" and "embed_single_track" over all
     relevant DAQ files of the selected dataset.

     Type:
       make_multi :: AttrSet -> AttrSet
  */
  make_multi = bits:
      lib.mapAttrs (name: dataset:
        {
          data = import ./multi.nix {
            attr_name = "data";
            inputs = dataset.st_physics_daq_files;
            # there is no point in repeating data if we don't embed
            num_repeats = 1;
            seed_offset = 0;
            inherit bits lib;
          };
          data_single_track = import ./multi.nix {
            attr_name = "data_single_track";
            inputs = dataset.st_physics_daq_files;
            num_repeats = bits.DATA_SINGLE_TRACK_NUM_REPEATS;
            seed_offset = 0;
            inherit bits lib;
          };
          embed = import ./multi.nix {
            attr_name = "embed";
            inputs = dataset.st_zerobias_daq_files;
            num_repeats = bits.EMBED_NUM_REPEATS;
            seed_offset = 0;
            inherit bits lib;
          };
          embed_single_track = import ./multi.nix {
            attr_name = "embed_single_track";
            inputs = dataset.st_zerobias_daq_files;
            num_repeats = bits.EMBED_SINGLE_TRACK_NUM_REPEATS;
            seed_offset = 0;
            inherit bits lib;
          };

          embed_mudst = import ./multi.nix {
            attr_name = "embed.input_prod";
            inputs = dataset.st_zerobias_daq_files;
            num_repeats = bits.EMBED_NUM_REPEATS;
            seed_offset = 0;
            inherit bits lib;
          };
        }) datasets;

  node = override: children: children // { override = override; };
  traverse = bits: tree:
    lib.mapAttrs (name: subtree:
      let
        new_bits = bits.override subtree.override;
      in
        new_bits // (traverse new_bits subtree) // (make_multi new_bits)
    ) tree;

  reaction_level = {
    pp200 = node override_pp200 library_level;
    pp510 = node override_pp510 library_level;
  };
  library_level = {
    sl12d_embed = node override_sl12d_embed species_level;
    sl13b_embed = node override_sl13b_embed species_level;
    sl18c = node override_sl18c species_level;
  };
  species_level = option_level // { # allow to skip this level
    eplus = node override_eplus option_level;
    eminus = node override_eminus option_level;
    piplus = node override_piplus option_level;
    piminus = node override_piminus option_level;
    kplus = node override_kplus option_level;
    kminus = node override_kminus option_level;
  };
  option_level = {
    pt2_3 = node override_pt2_3 option_level;
    pt11_15 = node override_pt11_15 option_level;

    no_repeats = node override_no_repeats option_level;
    ten_events = node override_ten_events option_level;
    no_submit = node override_no_submit option_level;
    debug = node override_debug option_level;

    # used for pure MC
    reset_tpcrawdata = node override_reset_tpcrawdata option_level;
  };
in
  traverse bits reaction_level
