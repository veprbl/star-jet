final: prev:

{
  arrow-cpp =
    if final.stdenv.hostPlatform.system == "i686-linux" then
      prev.arrow-cpp.overrideAttrs (old: {
        # undefined reference to `__warn_memset_zero_len'
        hardeningDisable = [ "fortify" ];
        # fatal error: stdatomic.h: No such file or directory 
        cmakeFlags = (old.cmakeFlags or []) ++ [ "-DARROW_MIMALLOC=OFF" ];
        # tests fail because of floating point arithmetics
        doInstallCheck = false;
      })
    else
      prev.arrow-cpp;

  # icu doesn't build, we don't use boost-locale
  boost = (prev.boost.override { enablePython = false; icu = null; }).overrideAttrs (old: {
    configureFlags = final.lib.sublist 0 2 (old.configureFlags or []);
  });

  # nghttp2 doesn't build with gcc48 out of the box
  curl = prev.curl.override { http2Support = false; };

  # doesn't build, actually belongs to Cython's checkInputs, not buildInputs
  gdb = null;

  # can't build modern glibc with gcc48
  glibcLocales = null;

  # graphviz doesn't build with gcc48
  graphviz = null;

  # drop a perl dependency
  help2man = null;
  flex = prev.flex.overrideAttrs (_: {
    HELP2MAN = "${final.coreutils}/bin/echo";
  });
  libtool = prev.libtool.overrideAttrs (_: { HELP2MAN = "echo"; });

  # drop unneeded dependencies
  thrift = prev.thrift.override { python = null; twisted = null; };
}
