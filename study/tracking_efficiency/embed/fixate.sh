#!/bin/sh

set -e

drvs="$(nix-instantiate ./. -I starnix=$HOME/star-nix "$@" --show-trace)"
( echo "$drvs" ; echo "$@" ) | git commit --allow-empty -a -F - 1>&2 || true
nix-build $drvs --option cores 200 --option keep-going true
