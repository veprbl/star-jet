#include <cmath>
#include <iostream>
#include <memory>

#include <arrow/api.h>
#include <arrow/io/api.h>
#include <arrow/util/logging.h> // for ARROW_CHECK

#include <parquet/arrow/writer.h>

#include <TFile.h>
#include <TKey.h>

#include <St_base/StTree.h> // for StIOEvent
#include <StChain/StEvtHddr.h>
#include <tables/St_g2t_pythia_Table.h>
#include <tables/St_g2t_track_Table.h>
#include <tables/St_g2t_tpc_hit_Table.h>

void print_assert(const char* exp, const char* file, int line)
{
  std::cerr << "Assertion '" << exp << "' failed in '" << file << "' at line '" << line << "'" << std::endl;
  std::terminate();
}

#define myassert(exp) ((exp) ? (void)0 : print_assert(#exp, __FILE__, __LINE__))

#include "column_helper.ipp"

DEFINE_COLUMN(eventRunNumber, arrow::Int64Type);
DEFINE_COLUMN(eventNumber, arrow::Int64Type);
DEFINE_COLUMN(g2t_pythia_hard_p, arrow::FloatType);

DEFINE_LIST_COLUMN(g2t_track_eg_pid, arrow::Int32Type);
DEFINE_LIST_COLUMN(g2t_track_ge_pid, arrow::Int32Type);
DEFINE_LIST_COLUMN(g2t_track_n_emc_hit, arrow::Int32Type);
DEFINE_LIST_COLUMN(g2t_track_n_tpc_hit, arrow::Int32Type);
DEFINE_LIST_COLUMN(g2t_track_id, arrow::Int32Type);
DEFINE_LIST_COLUMN(g2t_track_eta, arrow::FloatType);
DEFINE_LIST_COLUMN(g2t_track_phi, arrow::FloatType);
DEFINE_LIST_COLUMN(g2t_track_pt, arrow::FloatType);
DEFINE_LIST_COLUMN(g2t_track_ptot, arrow::FloatType);

DEFINE_LIST_COLUMN(g2t_tpc_hit_id, arrow::Int32Type);
DEFINE_LIST_COLUMN(g2t_tpc_hit_track_p, arrow::Int32Type);
DEFINE_LIST_COLUMN(g2t_tpc_hit_de, arrow::FloatType);
DEFINE_LIST_COLUMN(g2t_tpc_hit_ds, arrow::FloatType);
DEFINE_LIST_COLUMN(g2t_tpc_hit_x, arrow::FloatType);
DEFINE_LIST_COLUMN(g2t_tpc_hit_y, arrow::FloatType);
DEFINE_LIST_COLUMN(g2t_tpc_hit_z, arrow::FloatType);
DEFINE_LIST_COLUMN(g2t_tpc_hit_np, arrow::Int32Type);

std::shared_ptr<arrow::Schema> schema;
std::unique_ptr<parquet::arrow::FileWriter> writer;

void write_table() {
  std::vector<std::shared_ptr<arrow::Array>> arrays;

  for(BasePqColumn *column : columns) {
    column->finish();
    arrays.push_back(column->get_array());
  }

  std::shared_ptr<arrow::Table> table = arrow::Table::Make(schema, arrays);

  int64_t chunk_size = 2*1024*1024;
  PARQUET_THROW_NOT_OK(writer->WriteTable(*table, chunk_size));
}

int main(int argc, char *argv[]) {
  if ((argc < 2) || (argc > 3)) {
    std::cerr << "Usage: " << argv[0] << " inputfile.geant.root [ outputfile.geant.parquet ]" << std::endl;
    return EXIT_FAILURE;
  }
  std::string in_filename = argv[1];
  std::string out_filename;
  if (argc >= 3) {
    out_filename = argv[2];
  } else {
    size_t pos = in_filename.rfind(".");
    if (pos == std::string::npos) {
      std::cerr << "Invalid filename" << std::endl;
      return EXIT_FAILURE;
    }
    out_filename = in_filename.substr(0, pos) + ".parquet";
  }

  TFile in(in_filename.c_str());
  if (in.GetListOfKeys()->GetEntries() == 0) {
    std::cerr << "No objects found in \"" << in_filename << "\"!" << std::endl;
    return EXIT_FAILURE;
  }

  std::vector<std::shared_ptr<arrow::Field>> fields;
  for(BasePqColumn *column : columns) {
    fields.push_back(column->get_field());
  }
  schema = arrow::schema(fields);

  std::shared_ptr<arrow::io::FileOutputStream> outfile;
  outfile = std::move(arrow::io::FileOutputStream::Open(out_filename).ValueOrDie());
  {
    parquet::WriterProperties::Builder builder;
    builder.compression(parquet::Compression::GZIP);
    std::shared_ptr<parquet::WriterProperties> writer_properties = builder.build();
    PARQUET_THROW_NOT_OK(parquet::arrow::FileWriter::Open(
                           *schema,
                           arrow::default_memory_pool(),
                           outfile,
                           writer_properties,
                           &writer
                           ));
  }

  unsigned long long num_events = 0;
  TIter key_iter(in.GetListOfKeys());
  TKey *key;
  while ((key = (TKey*)key_iter())) {
    std::string key_name = key->GetName();
    size_t pos = key_name.find(".");
    std::string branch_name = key_name.substr(0, pos);
    if (branch_name != "geantBranch") {
      continue;
    }

    // List columns start with an empty list
    for(BasePqColumn *column : columns) {
      if (column->is_list()) {
        ARROW_CHECK_OK(column->append_list());
      }
    }

    std::unique_ptr<StIOEvent> ioevent { dynamic_cast<StIOEvent*>(in.Get(key_name.c_str())) };
    myassert(ioevent);
    std::unique_ptr<TList> obj_list { dynamic_cast<TList*>(ioevent->fObj) };
    myassert(obj_list);
    obj_list->SetOwner(true);
    TListIter obj_list_iter(obj_list.get());
    TObject *o;
    while ((o = obj_list_iter())) {
      std::cerr << o->GetName() << ":" << o->ClassName() << ", ";
    }
    std::cerr << std::endl;
    StEvtHddr *evt_hdr = dynamic_cast<StEvtHddr*>(obj_list->FindObject("RunEvent"));
    if (!evt_hdr) {
      std::cerr << "Can't find RunEvent in branch" << std::endl;
      return EXIT_FAILURE;
    }

    ARROW_CHECK_OK(eventRunNumber.builder.Append(evt_hdr->GetRunNumber()));
    ARROW_CHECK_OK(eventNumber.builder.Append(evt_hdr->GetEventNumber()));

    St_g2t_pythia *g2t_pythia = dynamic_cast<St_g2t_pythia*>(obj_list->FindObject("g2t_pythia"));
    if (g2t_pythia) {
      if (g2t_pythia->GetNRows() != 1) {
        std::cerr << "Expected a single row in g2t_pythia, found " << g2t_pythia->GetNRows() << std::endl;
        return EXIT_FAILURE;
      }
      auto *g2t_pythia_row = static_cast<const g2t_pythia_st*>(g2t_pythia->At(0));
      ARROW_CHECK_OK(g2t_pythia_hard_p.builder.Append(g2t_pythia_row->hard_p));
    } else {
      ARROW_CHECK_OK(g2t_pythia_hard_p.builder.AppendNull());
    }

    St_g2t_track *g2t_track = dynamic_cast<St_g2t_track*>(obj_list->FindObject("g2t_track"));
    if (!g2t_track) {
      std::cerr << "Can't find g2t_track in branch" << std::endl;
      return EXIT_FAILURE;
    }
    for (Long_t i = 0; i < g2t_track->GetNRows(); i++) {
      auto *g2t_track_row = static_cast<const g2t_track_st*>(g2t_track->At(i));

      ARROW_CHECK_OK(g2t_track_eg_pid.value_builder.Append(g2t_track_row->eg_pid));
      ARROW_CHECK_OK(g2t_track_ge_pid.value_builder.Append(g2t_track_row->ge_pid));
      ARROW_CHECK_OK(g2t_track_n_emc_hit.value_builder.Append(g2t_track_row->n_emc_hit));
      ARROW_CHECK_OK(g2t_track_n_tpc_hit.value_builder.Append(g2t_track_row->n_tpc_hit));
      ARROW_CHECK_OK(g2t_track_id.value_builder.Append(g2t_track_row->id));
      ARROW_CHECK_OK(g2t_track_eta.value_builder.Append(g2t_track_row->eta));
      double phi = std::atan2(g2t_track_row->p[1], g2t_track_row->p[0]);
      ARROW_CHECK_OK(g2t_track_phi.value_builder.Append(phi));
      ARROW_CHECK_OK(g2t_track_pt.value_builder.Append(g2t_track_row->pt));
      ARROW_CHECK_OK(g2t_track_ptot.value_builder.Append(g2t_track_row->ptot));
    }

    St_g2t_tpc_hit *g2t_tpc_hit = dynamic_cast<St_g2t_tpc_hit*>(obj_list->FindObject("g2t_tpc_hit"));
    // g2t_tpc_hit is sometimes absent in events. Probably because it's empty.
    if (g2t_tpc_hit) {
      for (Long_t i = 0; i < g2t_tpc_hit->GetNRows(); i++) {
        auto *g2t_tpc_hit_row = static_cast<const g2t_tpc_hit_st*>(g2t_tpc_hit->At(i));

        ARROW_CHECK_OK(g2t_tpc_hit_id.value_builder.Append(g2t_tpc_hit_row->id));
        ARROW_CHECK_OK(g2t_tpc_hit_track_p.value_builder.Append(g2t_tpc_hit_row->track_p));
        ARROW_CHECK_OK(g2t_tpc_hit_de.value_builder.Append(g2t_tpc_hit_row->de));
        ARROW_CHECK_OK(g2t_tpc_hit_ds.value_builder.Append(g2t_tpc_hit_row->ds));
        ARROW_CHECK_OK(g2t_tpc_hit_x.value_builder.Append(g2t_tpc_hit_row->x[0]));
        ARROW_CHECK_OK(g2t_tpc_hit_y.value_builder.Append(g2t_tpc_hit_row->x[1]));
        ARROW_CHECK_OK(g2t_tpc_hit_z.value_builder.Append(g2t_tpc_hit_row->x[2]));
        ARROW_CHECK_OK(g2t_tpc_hit_np.value_builder.Append(g2t_tpc_hit_row->np));
      }
    }

    int64_t megabytes_allocated = arrow::default_memory_pool()->bytes_allocated() / 1024. / 1024.;
    if (megabytes_allocated > 256 /* mb */) {
      write_table();
      std::cerr << megabytes_allocated << " mb\t" << num_events << std::endl;
    }
    num_events++;
  }

  write_table();
  PARQUET_THROW_NOT_OK(writer->Close());
  outfile.reset();

  return EXIT_SUCCESS;
}
