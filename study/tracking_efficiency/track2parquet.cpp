#include <arrow/api.h>
#include <arrow/io/api.h>
#include <arrow/util/logging.h> // for ARROW_CHECK

#include <parquet/arrow/writer.h>

#include "StMuDSTMaker/COMMON/StMuDst.h"
#include "StMuDSTMaker/COMMON/StMuDstMaker.h"
#include "StMuDSTMaker/COMMON/StMuEvent.h"

#include "StMuDSTMaker/COMMON/StMuTypes.hh"
#include "StBTofHeader.h"

#include "StBichsel/Bichsel.h"

void print_assert(const char* exp, const char* file, int line)
{
  std::cerr << "Assertion '" << exp << "' failed in '" << file << "' at line '" << line << "'" << std::endl;
  std::terminate();
}

#define myassert(exp) ((exp) ? (void)0 : print_assert(#exp, __FILE__, __LINE__))

#include "column_helper.ipp"

DEFINE_COLUMN(eventRunNumber, arrow::Int64Type);
DEFINE_COLUMN(eventNumber, arrow::Int64Type);
DEFINE_COLUMN_NULLABLE(eventPtBin, arrow::StringType);

DEFINE_LIST_COLUMN(eventTriggers, arrow::UInt32Type);

DEFINE_LIST_COLUMN(primaryVertexRank, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryVertexX, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryVertexY, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryVertexZ, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryVertexNumMatchesBTOF, arrow::UInt16Type);
DEFINE_LIST_COLUMN(primaryVertexNumMatchesBEMC, arrow::UInt16Type);
DEFINE_LIST_COLUMN(primaryVertexNumMatchesEEMC, arrow::UInt16Type);
DEFINE_LIST_COLUMN(primaryTrackFlag, arrow::Int16Type);
DEFINE_LIST_COLUMN(primaryTrackPt, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackPhi, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackEta, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackCharge, arrow::Int8Type);
DEFINE_LIST_COLUMN(primaryTrackHelixPX, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackHelixPY, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackHelixPZ, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackHelixOriginX, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackHelixOriginY, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackHelixOriginZ, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackHelixQ, arrow::Int8Type);
DEFINE_LIST_COLUMN(primaryTrackHelixB, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackOuterHelixPX, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackOuterHelixPY, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackOuterHelixPZ, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackOuterHelixOriginX, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackOuterHelixOriginY, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackOuterHelixOriginZ, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackOuterHelixQ, arrow::Int8Type);
DEFINE_LIST_COLUMN(primaryTrackNHits, arrow::UInt8Type);
DEFINE_LIST_COLUMN(primaryTrackNHitsFit, arrow::UInt8Type);
DEFINE_LIST_COLUMN(primaryTrackNHitsPoss, arrow::UInt8Type);
DEFINE_LIST_COLUMN(primaryTrackTopologyMap, arrow::UInt64Type);
DEFINE_LIST_COLUMN(primaryTrackTdca, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdcaX, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdcaY, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdcaZ, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdcaD, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdx, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackNHitsdEdx, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTracknSigmaElectron, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTracknSigmaPion, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTracknSigmaKaon, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTracknSigmaProton, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxTrackLength, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxTruncated, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxTruncatedError, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxTruncatedBichselElectron, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxTruncatedBichselPion, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxTruncatedBichselKaon, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxTruncatedBichselProton, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxFit, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxFitError, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxFitBichselElectron, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxFitBichselPion, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxFitBichselKaon, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackdEdxFitBichselProton, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackChi2, arrow::FloatType);
DEFINE_LIST_COLUMN(primaryTrackVertexId, arrow::Int32Type);
DEFINE_LIST_COLUMN(primaryTrackIdTruth, arrow::Int32Type);
DEFINE_LIST_COLUMN(primaryTrackQaTruth, arrow::Int32Type);

std::shared_ptr<arrow::Schema> schema;
std::unique_ptr<parquet::arrow::FileWriter> writer;

void write_table() {
   std::vector<std::shared_ptr<arrow::Array>> arrays;

   for(BasePqColumn *column : columns) {
      column->finish();
      arrays.push_back(column->get_array());
   }

   std::shared_ptr<arrow::Table> table = arrow::Table::Make(schema, arrays);

   int64_t chunk_size = 2*1024*1024;
   PARQUET_THROW_NOT_OK(writer->WriteTable(*table, chunk_size));
}

std::vector<const char*> pt_bins = {
   "pt2_3",
   "pt3_4",
   "pt4_5",
   "pt5_7",
   "pt7_9",
   "pt9_11",
   "pt11_15",
   "pt15_20",
   "pt20_25",
   "pt25_35",
   "pt35_-1",
};

std::string detect_bin(const StMuDstMaker &muDstMaker) {
   const char* filepath = muDstMaker.GetFile();
   unsigned int num_matches = 0;
   std::string last_match;
   for(const string &pt_bin : pt_bins) {
      if (strstr(filepath, pt_bin.c_str())) {
         last_match = pt_bin.c_str();
         num_matches++;
      }
   }
   if (num_matches == 0) {
      return "";
   } else if (num_matches == 1) {
      return last_match;
   } else {
      std::cerr << "Ambigous pt bin match in '" << filepath << "'. Matching bins:";
      for(const string &pt_bin : pt_bins) {
         if (strstr(filepath, pt_bin.c_str())) {
            std::cerr << " " << pt_bin << std::endl;
         }
      }
      throw std::exception();
   }
}

int main(int argc, char *argv[]) {
   if (argc != 3) {
      std::cerr << "Usage: " << argv[0] << " (input.lis|input.MuDst.root) output.parquet" << std::endl;
      return EXIT_FAILURE;
   }
   std::string in_filename = argv[1];
   std::string out_filename = argv[2];

   StMuDstMaker muDstMaker(0, 0, "", in_filename.c_str(), ".", 1e9);

   muDstMaker.SetStatus("*", 0);

   std::vector<std::string> activeBranchNames = {
      "MuEvent",
      "PrimaryVertices",
      "PrimaryTracks",
      "GlobalTracks",
   };

   // Enable selected branches
   for (const auto& branchName : activeBranchNames) {
      muDstMaker.SetStatus(branchName.c_str(), 1);
   }

   std::vector<std::shared_ptr<arrow::Field>> fields;
   for(BasePqColumn *column : columns) {
      fields.push_back(column->get_field());
   }
   schema = arrow::schema(fields);

   std::shared_ptr<arrow::io::FileOutputStream> outfile;
   outfile = std::move(arrow::io::FileOutputStream::Open(out_filename).ValueOrDie());
   {
      parquet::WriterProperties::Builder builder;
      builder.compression(parquet::Compression::GZIP);
      std::shared_ptr<parquet::WriterProperties> writer_properties = builder.build();
      PARQUET_THROW_NOT_OK(parquet::arrow::FileWriter::Open(
                             *schema,
                             arrow::default_memory_pool(),
                             outfile,
                             writer_properties,
                             &writer
                             ));
   }

   unsigned long long num_events = 0;
   int return_code = EXIT_SUCCESS;
   do
   {
      try {
         if ( muDstMaker.Make() ) break;
      } catch(...) {
         return_code = EXIT_FAILURE;
         break;
      }

      // Access all event data
      StMuDst *muDst = muDstMaker.muDst();

      // Access event header-wise information
      StMuEvent *muEvent = muDst->event();
      
      std::string bin = detect_bin(muDstMaker);

      // List columns start with an empty list
      for(BasePqColumn *column : columns) {
         if (column->is_list()) {
            ARROW_CHECK_OK(column->append_list());
         }
      }

      ARROW_CHECK_OK(eventRunNumber.builder.Append(muEvent->runNumber()));
      ARROW_CHECK_OK(eventNumber.builder.Append(muEvent->eventNumber()));
      if (bin.size() != 0) {
         ARROW_CHECK_OK(eventPtBin.builder.Append(bin));
      } else {
         ARROW_CHECK_OK(eventPtBin.builder.AppendNull());
      }

      const StTriggerId &nominal = muEvent->triggerIdCollection().nominal();
      for (unsigned int trig_id : nominal.triggerIds()) {
         ARROW_CHECK_OK(eventTriggers.value_builder.Append(trig_id));
      }

      unsigned int num_vertices = StMuDst::numberOfPrimaryVertices();
      for (unsigned int j = 0; j < num_vertices; j++)
      {
         StMuDst::setVertexIndex(j);
         ARROW_CHECK_OK(primaryVertexRank.value_builder.Append(StMuDst::primaryVertex()->ranking()));
         ARROW_CHECK_OK(primaryVertexX.value_builder.Append(StMuDst::primaryVertex()->position().x()));
         ARROW_CHECK_OK(primaryVertexY.value_builder.Append(StMuDst::primaryVertex()->position().y()));
         ARROW_CHECK_OK(primaryVertexZ.value_builder.Append(StMuDst::primaryVertex()->position().z()));
         ARROW_CHECK_OK(primaryVertexNumMatchesBTOF.value_builder.Append(StMuDst::primaryVertex()->nBTOFMatch()));
         ARROW_CHECK_OK(primaryVertexNumMatchesBEMC.value_builder.Append(StMuDst::primaryVertex()->nBEMCMatch()));
         ARROW_CHECK_OK(primaryVertexNumMatchesEEMC.value_builder.Append(StMuDst::primaryVertex()->nEEMCMatch()));

         unsigned int num_tracks = StMuDst::numberOfPrimaryTracks();
         for (unsigned int i = 0; i < num_tracks; i++) {
            const StMuTrack* track = StMuDst::primaryTracks(i);

            ARROW_CHECK_OK(primaryTrackFlag.value_builder.Append(track->flag()));
            ARROW_CHECK_OK(primaryTrackPt.value_builder.Append(track->pt()));
            ARROW_CHECK_OK(primaryTrackPhi.value_builder.Append(track->phi()));
            ARROW_CHECK_OK(primaryTrackEta.value_builder.Append(track->eta()));
            ARROW_CHECK_OK(primaryTrackCharge.value_builder.Append(track->charge()));
            ARROW_CHECK_OK(primaryTrackHelixPX.value_builder.Append(track->muHelix().p().x()));
            ARROW_CHECK_OK(primaryTrackHelixPY.value_builder.Append(track->muHelix().p().y()));
            ARROW_CHECK_OK(primaryTrackHelixPZ.value_builder.Append(track->muHelix().p().z()));
            ARROW_CHECK_OK(primaryTrackHelixOriginX.value_builder.Append(track->muHelix().origin().x()));
            ARROW_CHECK_OK(primaryTrackHelixOriginY.value_builder.Append(track->muHelix().origin().y()));
            ARROW_CHECK_OK(primaryTrackHelixOriginZ.value_builder.Append(track->muHelix().origin().z()));
            ARROW_CHECK_OK(primaryTrackHelixQ.value_builder.Append(track->muHelix().q()));
            ARROW_CHECK_OK(primaryTrackHelixB.value_builder.Append(track->muHelix().b()));
            ARROW_CHECK_OK(primaryTrackOuterHelixPX.value_builder.Append(track->muOuterHelix().p().x()));
            ARROW_CHECK_OK(primaryTrackOuterHelixPY.value_builder.Append(track->muOuterHelix().p().y()));
            ARROW_CHECK_OK(primaryTrackOuterHelixPZ.value_builder.Append(track->muOuterHelix().p().z()));
            ARROW_CHECK_OK(primaryTrackOuterHelixOriginX.value_builder.Append(track->muOuterHelix().origin().x()));
            ARROW_CHECK_OK(primaryTrackOuterHelixOriginY.value_builder.Append(track->muOuterHelix().origin().y()));
            ARROW_CHECK_OK(primaryTrackOuterHelixOriginZ.value_builder.Append(track->muOuterHelix().origin().z()));
            ARROW_CHECK_OK(primaryTrackOuterHelixQ.value_builder.Append(track->muOuterHelix().q()));
            myassert(track->muHelix().b() == track->muOuterHelix().b());
            ARROW_CHECK_OK(primaryTrackNHits.value_builder.Append(track->nHits()));
            ARROW_CHECK_OK(primaryTrackNHitsFit.value_builder.Append(track->nHitsFit()));
            ARROW_CHECK_OK(primaryTrackNHitsPoss.value_builder.Append(track->nHitsPoss()));
            StTrackTopologyMap topologyMap = track->topologyMap();
            uint64_t topologyMapI = static_cast<uint64_t>(topologyMap.data(0)) | static_cast<uint64_t>(topologyMap.data(1)) << 32;
            ARROW_CHECK_OK(primaryTrackTopologyMap.value_builder.Append(topologyMapI));

            // The DCA definitons match the convention used in StJetMaker
            ARROW_CHECK_OK(primaryTrackTdca.value_builder.Append(track->dcaGlobal().mag()));
            ARROW_CHECK_OK(primaryTrackdcaX.value_builder.Append(track->dcaGlobal().x()));
            ARROW_CHECK_OK(primaryTrackdcaY.value_builder.Append(track->dcaGlobal().y()));
            ARROW_CHECK_OK(primaryTrackdcaZ.value_builder.Append(track->dcaZ()));
            ARROW_CHECK_OK(primaryTrackdcaD.value_builder.Append(track->dcaD()));

            // A newer STAR library uses the dEdxFit variant instead of dEdxTruncated
            ARROW_CHECK_OK(primaryTrackdEdx.value_builder.Append(track->dEdx()));
            ARROW_CHECK_OK(primaryTrackNHitsdEdx.value_builder.Append(track->nHitsDedx()));
            ARROW_CHECK_OK(primaryTracknSigmaElectron.value_builder.Append(track->nSigmaElectron()));
            ARROW_CHECK_OK(primaryTracknSigmaPion.value_builder.Append(track->nSigmaPion()));
            ARROW_CHECK_OK(primaryTracknSigmaKaon.value_builder.Append(track->nSigmaKaon()));
            ARROW_CHECK_OK(primaryTracknSigmaProton.value_builder.Append(track->nSigmaProton()));

            ARROW_CHECK_OK(primaryTrackdEdxTrackLength.value_builder.Append(track->probPidTraits().dEdxTrackLength()));
            ARROW_CHECK_OK(primaryTrackdEdxTruncated.value_builder.Append(track->probPidTraits().dEdxTruncated()));
            ARROW_CHECK_OK(primaryTrackdEdxTruncatedError.value_builder.Append(track->probPidTraits().dEdxErrorTruncated()));
            ARROW_CHECK_OK(primaryTrackdEdxFit.value_builder.Append(track->probPidTraits().dEdxFit()));
            ARROW_CHECK_OK(primaryTrackdEdxFitError.value_builder.Append(track->probPidTraits().dEdxErrorFit()));

            std::vector<std::tuple<double, PqListColumn<arrow::FloatType>*, PqListColumn<arrow::FloatType>*> > bischel_columns {{
               std::make_tuple<>(0.51099907e-3, &primaryTrackdEdxTruncatedBichselElectron, &primaryTrackdEdxFitBichselElectron),
               std::make_tuple<>(0.13956995,    &primaryTrackdEdxTruncatedBichselPion,     &primaryTrackdEdxFitBichselPion),
               std::make_tuple<>(0.493677,      &primaryTrackdEdxTruncatedBichselKaon,     &primaryTrackdEdxFitBichselKaon),
               std::make_tuple<>(0.93827231,    &primaryTrackdEdxTruncatedBichselProton,   &primaryTrackdEdxFitBichselProton),
            }};
            for(auto tup : bischel_columns)
            {
               PqListColumn<arrow::FloatType> *pq_column_trunc = std::get<1>(tup);
               PqListColumn<arrow::FloatType> *pq_column_fit = std::get<2>(tup);

               double charge = 1;
               double mass = std::get<0>(tup);
               double momentum;
               if (track->globalTrack()) {
                  // StEvent and MuDst only use the global track
                  momentum = track->globalTrack()->muHelix().p().mag();
               } else {
                  momentum = track->muHelix().p().mag();
               }

               Double_t log2dX = track->probPidTraits().log2dX();
               if (log2dX <= 0) log2dX = 1;
               // StMuTrack::dEdxPull doesn't use log2dX
               // StTpcDedxPidAlgorithm::numberOfSigma uses log2dX
               // It is said that the dependence is weak
               double dEdx_expected_trunc =
                  1.e-6*charge*charge*Bichsel::Instance()->GetI70M(TMath::Log10(momentum*TMath::Abs(charge)/mass), log2dX);

               ARROW_CHECK_OK(pq_column_trunc->value_builder.Append(dEdx_expected_trunc));

               double dEdx_expected_fit =
                  1.e-6*charge*charge*TMath::Exp(Bichsel::Instance()->GetMostProbableZ(TMath::Log10(momentum*TMath::Abs(charge)/mass)));

               ARROW_CHECK_OK(pq_column_fit->value_builder.Append(dEdx_expected_fit));
            }

            ARROW_CHECK_OK(primaryTrackChi2.value_builder.Append(track->chi2()));
            ARROW_CHECK_OK(primaryTrackVertexId.value_builder.Append(j));
            ARROW_CHECK_OK(primaryTrackIdTruth.value_builder.Append(track->idTruth()));
            ARROW_CHECK_OK(primaryTrackQaTruth.value_builder.Append(track->qaTruth()));
         }
      }

      int64_t megabytes_allocated = arrow::default_memory_pool()->bytes_allocated() / 1024. / 1024.;
      if (megabytes_allocated > 256 /* mb */) {
        write_table();
        std::cerr << megabytes_allocated << " mb\t" << num_events << std::endl;
      }
      num_events++;
   } while (true);

   write_table();
   PARQUET_THROW_NOT_OK(writer->Close());
   outfile.reset();

   return return_code;
}
