{ nixpkgs /*? import <nixpkgs>*/, nixpkgs_src /*? null*/, overlays /*? null*/ }:

let
  inherit (nixpkgs) lib;
  starnix = import ./star.nix { inherit nixpkgs nixpkgs_src overlays; };
  stdenv = starnix.stdenv;
  mkStarDerivation = args: stdenv.mkDerivation (args // {
      nativeBuildInputs = (args.nativeBuildInputs or []) ++ [ starnix.root ];
      preConfigure = ''
      export NODEBUG=yes
      source <(
        /usr/bin/csh -c "setenv SILENT 1; setenv ECHO 0; setenv GROUP_DIR /afs/rhic.bnl.gov/rhstar/group; source /afs/rhic.bnl.gov/rhstar/group/star_cshrc.csh; source \''${GROUP_DIR}/.starver SL18c; env" \
          | egrep '(^STAR)' | while read VAR; do echo "export $VAR"; done
        )
#export MANPATH=: # workaround for thisroot
#pushd $ROOTSYS; . $ROOTSYS/bin/thisroot.sh; popd
      '' + (args.preConfigure or "");
      preferLocalBuild = true;
      allowSubstitutes = false;
  });
  boost = starnix.eff_pkgs.boost.override { inherit stdenv; };
  arrow-cpp = (starnix.eff_pkgs.arrow-cpp.override { inherit stdenv boost; }).overrideAttrs (old: {
    cmakeFlags = old.cmakeFlags ++ [ "-DARROW_USE_SIMD=OFF" ];
  });
  track2parquet = mkStarDerivation {
    name = "track2parquet";
    src = lib.sourceByRegex ./. [
      "^column_helper\.ipp$"
      "^track2parquet\.cpp$"
      ];
    buildInputs = [ arrow-cpp ];
    NIX_ENFORCE_PURITY = 0;
    buildPhase = ''
      g++ -m32 -fPIC -pipe -Wall -Woverloaded-virtual -std=c++0x -Wno-long-long -pthread -g -Dsl73_gcc485 -D__ROOT__ -DNEW_DAQ_READER -I"$STAR" -I"$STAR"/StRoot -I"$STAR"/.sl73_gcc485/include -L"$STAR"/.sl73_gcc485/lib -Wl,-rpath="$STAR"/.sl73_gcc485/lib \
          -lStarClassLibrary \
          -lStarRoot \
          -lPhysics \
          -lSt_base \
          -lStChain \
          -lSt_Tables \
          -lStUtilities \
          -lStTreeMaker \
          -lStIOMaker \
          -lStBichsel \
          -lStEvent \
          -lStEventUtilities \
          -lStTriggerDataMaker \
          -lStDbLib \
          -lStEmcUtil \
          -lStTofUtil \
          -lStPmdUtil \
          -lStMuDSTMaker \
          -lStStrangeMuDstMaker \
          -Wl,--warn-unresolved-symbols \
          $(root-config --cflags --libs) -lTable -lEG -lMinuit -lVMC \
          -larrow -lparquet \
          track2parquet.cpp -o track2parquet
    '';
    installPhase = ''
      install -Dm755 ./track2parquet $out/bin/track2parquet
    '';
  };
  event2parquet = mkStarDerivation {
    name = "event2parquet";
    src = lib.sourceByRegex ./. [
      "^event2parquet\.cpp$"
      ];
    buildInputs = [ arrow-cpp ];
    NIX_ENFORCE_PURITY = 0;
    buildPhase = ''
      g++ -m32 -fPIC -pipe -Wall -Woverloaded-virtual -std=c++0x -Wno-long-long -pthread -g -Dsl73_gcc485 -D__ROOT__ -DNEW_DAQ_READER -I"$STAR" -I"$STAR"/StRoot -I"$STAR"/.sl73_gcc485/include -L"$STAR"/.sl73_gcc485/lib -Wl,-rpath="$STAR"/.sl73_gcc485/lib \
          -lStarClassLibrary \
          -lStarRoot \
          -lPhysics \
          -lSt_base \
          -lStChain \
          -lSt_Tables \
          -lStUtilities \
          -lStTreeMaker \
          -lStIOMaker \
          -lStBichsel \
          -lStEvent \
          -lStEventUtilities \
          -lStTriggerDataMaker \
          -lStDbLib \
          -lStEmcUtil \
          -lStTofUtil \
          -lStPmdUtil \
          -Wl,--warn-unresolved-symbols \
          $(root-config --cflags --libs) -lTable -lEG -lMinuit -lVMC \
          -larrow -lparquet \
          event2parquet.cpp -o event2parquet
    '';
    installPhase = ''
      install -Dm755 ./event2parquet $out/bin/event2parquet
    '';
  };
  geant2parquet = mkStarDerivation {
    name = "geant2parquet";
    src = lib.sourceByRegex ./. [
      "^geant2parquet\.cpp$"
      "^column_helper\.ipp$"
      ];
    buildInputs = [ arrow-cpp ];
    NIX_ENFORCE_PURITY = 0;
    buildPhase = ''
      g++ -m32 -fPIC -pipe -Wall -Woverloaded-virtual -std=c++0x -Wno-long-long -pthread -g -Dsl73_gcc485 -D__ROOT__ -DNEW_DAQ_READER -I"$STAR" -I"$STAR"/StRoot -I"$STAR"/.sl73_gcc485/include -L"$STAR"/.sl73_gcc485/lib -Wl,-rpath="$STAR"/.sl73_gcc485/lib \
          -lStarClassLibrary \
          -lStarRoot \
          -lPhysics \
          -lSt_base \
          -lStChain \
          -lSt_Tables \
          -lStUtilities \
          -lStTreeMaker \
          -lStIOMaker \
          -lsim_Tables \
          -Wl,--warn-unresolved-symbols \
          $(root-config --cflags --libs) -lTable -lEG -lMinuit -lVMC \
          -larrow -lparquet \
          geant2parquet.cpp -o geant2parquet
    '';
    installPhase = ''
      install -Dm755 ./geant2parquet $out/bin/geant2parquet
    '';
  };
  software = [
    starnix.log4cxx
    starnix.libxml2.out
    starnix.eff_pkgs.zlib
    starnix.eff_pkgs.lzma.out
    starnix.eff_pkgs.pcre.out
    starnix.eff_pkgs.freetype
  ];
  star_libs_path = lib.makeLibraryPath software;
  process = tag: inputfile: mkStarDerivation {
    name = "${tag}.tracks.parquet";
    unpackPhase = "true";
    nativeBuildInputs = [ track2parquet ];
    buildPhase = ''
      export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${star_libs_path}:"$STAR"/.sl73_gcc485/lib:.
      for i in libgfortran.so.3 libquadmath.so.0; do
        ln -s /usr/lib/$i $i
      done
      ln -s ${starnix.eff_pkgs.openssl.out}/lib/libcrypto.so.1.0.0 libcrypto.so.10
      ln -s ${starnix.eff_pkgs.openssl.out}/lib/libssl.so.1.0.0 libssl.so.10
      cp ${inputfile} input.MuDst.root
      /usr/bin/time -f "%C\n%Uuser %Ssystem %eelapsed %PCPU (%Xtext+%Ddata %Mmax)\n%Iinputs+%Ooutputs (%Fmajor+%Rminor)pagefaults %Wswaps\ncontext switches: %cinvoluntarily %wwaits" -o time \
        track2parquet input.MuDst.root output.parquet 2>&1 | grep -v "read too few bytes"
    '';
    installPhase = ''
      out_data="/gpfs/mnt/gpfs01/star/pwg/veprbl/parquet/$(basename $out)"
      install -m444 ./output.parquet "$out_data"
      ln -s "$out_data" "$out"
    '';
  };
  process_event = tag: inputfile: mkStarDerivation {
    name = "${tag}.event.parquet";
    unpackPhase = "true";
    nativeBuildInputs = [ event2parquet ];
    buildPhase = ''
      export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${star_libs_path}:"$STAR"/.sl73_gcc485/lib:.
      for i in libgfortran.so.3 libquadmath.so.0; do
        ln -s /usr/lib/$i $i
      done
      ln -s ${starnix.eff_pkgs.openssl.out}/lib/libcrypto.so.1.0.0 libcrypto.so.10
      ln -s ${starnix.eff_pkgs.openssl.out}/lib/libssl.so.1.0.0 libssl.so.10
      cp ${inputfile} input.event.root
      /usr/bin/time -f "%C\n%Uuser %Ssystem %eelapsed %PCPU (%Xtext+%Ddata %Mmax)\n%Iinputs+%Ooutputs (%Fmajor+%Rminor)pagefaults %Wswaps\ncontext switches: %cinvoluntarily %wwaits" -o time \
        event2parquet input.event.root output.parquet
    '';
    installPhase = ''
      out_data="/gpfs/mnt/gpfs01/star/pwg/veprbl/parquet/$(basename $out)"
      install -m444 ./output.parquet "$out_data"
      ln -s "$out_data" "$out"
    '';
  };
  process_geant = tag: inputfile: mkStarDerivation {
    name = "${tag}.geant.parquet";
    unpackPhase = "true";
    nativeBuildInputs = [ geant2parquet ];
    buildPhase = ''
      export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${star_libs_path}:"$STAR"/.sl73_gcc485/lib:.
      for i in libgfortran.so.3 libquadmath.so.0; do
        ln -s /usr/lib/$i $i
      done
      ln -s ${starnix.eff_pkgs.openssl.out}/lib/libcrypto.so.1.0.0 libcrypto.so.10
      ln -s ${starnix.eff_pkgs.openssl.out}/lib/libssl.so.1.0.0 libssl.so.10
      cp ${inputfile} input.geant.root
      /usr/bin/time -f "%C\n%Uuser %Ssystem %eelapsed %PCPU (%Xtext+%Ddata %Mmax)\n%Iinputs+%Ooutputs (%Fmajor+%Rminor)pagefaults %Wswaps\ncontext switches: %cinvoluntarily %wwaits" -o time \
        geant2parquet input.geant.root output.parquet
    '';
    installPhase = ''
      out_data="/gpfs/mnt/gpfs01/star/pwg/veprbl/parquet/$(basename $out)"
      install -m444 ./output.parquet "$out_data"
      ln -s "$out_data" "$out"
    '';
  };
in
  {
    inherit software;
    track2parquet = process;
    event2parquet = process_event;
    geant2parquet = process_geant;
    run13059025 = process "13059025" "/gpfs/mnt/gpfs01/star/scratch/veprbl/13059025/*.MuDst.root";
    run13059025_emb = process "13059025_emb" "/star/u/veprbl/pwg/embed_13059025/*.MuDst.root";
    inherit (starnix.eff_pkgs) glibc;
    inherit (starnix) eff_pkgs;
  }
