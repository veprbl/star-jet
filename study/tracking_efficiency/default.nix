with import <nixpkgs> {};

let
  starnix = import <starnix> {};
  stdenv = starnix.stdenv;
  mkStarDerivation = args: stdenv.mkDerivation (args // {
      propagatedBuildInputs = (args.propagatedBuildInputs or []) ++ [ starnix.root ];
      preConfigure = ''
      export NODEBUG=yes
      source <(
        /usr/bin/csh -c "setenv SILENT 1; setenv ECHO 0; setenv GROUP_DIR /afs/rhic.bnl.gov/rhstar/group; source /afs/rhic.bnl.gov/rhstar/group/star_cshrc.csh; source \''${GROUP_DIR}/.starver SL17d; env" \
          | egrep '(^STAR)' | while read VAR; do echo "export $VAR"; done
        )
#export MANPATH=: # workaround for thisroot
#pushd $ROOTSYS; . $ROOTSYS/bin/thisroot.sh; popd
      '' + (args.preConfigure or "");
      preferLocalBuild = true;
      allowSubstitutes = false;
  });
  boost = starnix.eff_pkgs.boost.override { inherit stdenv; };
  arrow-cpp = starnix.eff_pkgs.arrow-cpp.override { inherit stdenv boost; };
  parquet-cpp = starnix.eff_pkgs.parquet-cpp.override { inherit stdenv boost arrow-cpp; };
  track_to_parquet = mkStarDerivation {
    name = "track_to_parquet";
    src = lib.sourceByRegex ./. [
      "^build\.sh$"
      "^track_to_parquet\.cpp$"
      ];
    buildInputs = [ starnix.fastjet arrow-cpp parquet-cpp ];
    NIX_ENFORCE_PURITY = 0;
    buildPhase = ''
      ./build.sh
    '';
    installPhase = ''
      install -Dm755 ./track_to_parquet $out/bin/track_to_parquet
    '';
  };
  process = runnumber: glob: mkStarDerivation {
    name = "process-${runnumber}";
    unpackPhase = "true";
    buildInputs = [ track_to_parquet ];
    buildPhase = ''
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${starnix.eff_pkgs.gfortran.cc.lib}/lib:${starnix.log4cxx}/lib:${starnix.libxml2.out}/lib:${starnix.eff_pkgs.zlib}/lib:${starnix.eff_pkgs.lzma.out}/lib:${starnix.eff_pkgs.pcre.out}/lib:${starnix.eff_pkgs.freetype}/lib:.
      export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${starnix.log4cxx}/lib:${starnix.libxml2.out}/lib:${starnix.eff_pkgs.zlib}/lib:${starnix.eff_pkgs.lzma.out}/lib:${starnix.eff_pkgs.pcre.out}/lib:${starnix.eff_pkgs.freetype}/lib:.
      for i in libgfortran.so.3 libquadmath.so.0; do
        ln -s /usr/lib/$i $i
      done
      ln -s ${starnix.eff_pkgs.openssl.out}/lib/libcrypto.so.1.0.0 libcrypto.so.10
      ln -s ${starnix.eff_pkgs.openssl.out}/lib/libssl.so.1.0.0 libssl.so.10
      ls -1 ${glob} > run${runnumber}.lis
      # check that runlist is not empty
      [ -n "$(cat run${runnumber}.lis)" ]
      cat run${runnumber}.lis
#/usr/bin/strace -o /star/u/veprbl/parquet/log -r \
      /usr/bin/time -f "%C\n%Uuser %Ssystem %eelapsed %PCPU (%Xtext+%Ddata %Mmax)\n%Iinputs+%Ooutputs (%Fmajor+%Rminor)pagefaults %Wswaps\ncontext switches: %cinvoluntarily %wwaits" -o time \
        sh -c "track_to_parquet run${runnumber}.lis 2>&1 || true" | grep -v "read too few bytes" | tee log
      grep "Finish" log
    '';
    installPhase = ''
      install -Dm644 ./run${runnumber}.lis $out/filelist
      out_data="/gpfs/mnt/gpfs01/star/pwg/veprbl/parquet/$(basename $out)"
      mkdir "$out_data"
      install -Dm644 ./run${runnumber}.parquet "$out_data"/output.parquet
      ln -s "$out_data"/output.parquet "$out"/output.parquet
    '';
  };
in
  {
    inherit track_to_parquet;
    run13059025 = process "13059025" "/gpfs/mnt/gpfs01/star/scratch/veprbl/13059025/*.MuDst.root";
    run13059025_emb = process "13059025_emb" "/star/u/veprbl/pwg/embed_13059025/*.MuDst.root";
    inherit (starnix.eff_pkgs) glibc;
    inherit (starnix) eff_pkgs;
  }
