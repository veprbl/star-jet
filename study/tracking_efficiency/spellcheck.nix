{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  spellcheck = notebook:
    runCommand "spellcheck-${builtins.baseNameOf notebook}"
    {
      nativeBuildInputs = [ aspell aspellDicts.en jq ];
      inherit notebook;
    }
    ''
      jq --raw-output '.cells[] | select(.cell_type=="markdown") | .source | add' $notebook \
        | aspell --dict-dir="${aspellDicts.en}/lib/aspell" list \
        | tee "$out"
    '';
in
  [
    (spellcheck ./data-driven.ipynb)
    (spellcheck ./single_track_embedding.ipynb)
  ]
