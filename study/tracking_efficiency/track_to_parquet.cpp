#include <arrow/api.h>
#include <arrow/io/api.h>
#include <arrow/util/logging.h> // for ARROW_CHECK

#include <parquet/arrow/writer.h>

#include "StMuDSTMaker/COMMON/StMuDst.h"
#include "StMuDSTMaker/COMMON/StMuDstMaker.h"
#include "StMuDSTMaker/COMMON/StMuEvent.h"

#include "StMuDSTMaker/COMMON/StMuTypes.hh"
#include "StBTofHeader.h"

arrow::MemoryPool* pool = arrow::default_memory_pool();
arrow::Int64Builder eventRunNumber(pool);
std::shared_ptr<arrow::Array> eventRunNumberA;
arrow::Int64Builder eventNumber(pool);
std::shared_ptr<arrow::Array> eventNumberA;
arrow::StringBuilder eventPtBin(pool);
std::shared_ptr<arrow::Array> eventPtBinA;

arrow::ListBuilder primaryTrackFlag(pool, std::unique_ptr<arrow::Int16Builder>(new arrow::Int16Builder(pool)));
std::shared_ptr<arrow::Array> primaryTrackFlagA;

arrow::ListBuilder primaryTrackPt(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> primaryTrackPtA;

arrow::ListBuilder primaryTrackPhi(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> primaryTrackPhiA;

arrow::ListBuilder primaryTrackEta(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> primaryTrackEtaA;

arrow::ListBuilder primaryTrackNHits(pool, std::unique_ptr<arrow::UInt8Builder>(new arrow::UInt8Builder(pool)));
std::shared_ptr<arrow::Array> primaryTrackNHitsA;

arrow::ListBuilder primaryTrackNHitsFit(pool, std::unique_ptr<arrow::UInt8Builder>(new arrow::UInt8Builder(pool)));
std::shared_ptr<arrow::Array> primaryTrackNHitsFitA;

arrow::ListBuilder primaryTrackNHitsPoss(pool, std::unique_ptr<arrow::UInt8Builder>(new arrow::UInt8Builder(pool)));
std::shared_ptr<arrow::Array> primaryTrackNHitsPossA;

arrow::ListBuilder primaryTrackTopologyMap(pool, std::unique_ptr<arrow::UInt64Builder>(new arrow::UInt64Builder(pool)));
std::shared_ptr<arrow::Array> primaryTrackTopologyMapA;

arrow::ListBuilder primaryTrackdEdx(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> primaryTrackdEdxA;

arrow::ListBuilder primaryTrackChi2(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> primaryTrackChi2A;

arrow::ListBuilder primaryTrackVertexId(pool, std::unique_ptr<arrow::Int32Builder>(new arrow::Int32Builder(pool)));
std::shared_ptr<arrow::Array> primaryTrackVertexIdA;

arrow::ListBuilder primaryTrackVertexRank(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> primaryTrackVertexRankA;

arrow::ListBuilder primaryTrackVertexZ(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> primaryTrackVertexZA;

arrow::ListBuilder primaryTrackIdTruth(pool, std::unique_ptr<arrow::Int32Builder>(new arrow::Int32Builder(pool)));
std::shared_ptr<arrow::Array> primaryTrackIdTruthA;

arrow::ListBuilder primaryTrackQaTruth(pool, std::unique_ptr<arrow::Int32Builder>(new arrow::Int32Builder(pool)));
std::shared_ptr<arrow::Array> primaryTrackQaTruthA;

std::shared_ptr<arrow::Schema> schema = arrow::schema({
      arrow::field("eventRunNumber", arrow::int64()),
      arrow::field("eventNumber", arrow::int64()),
      arrow::field("eventPtBin", arrow::utf8()),
      arrow::field("primaryTrackFlag", arrow::list(arrow::int16())),
      arrow::field("primaryTrackPt", arrow::list(arrow::float32())),
      arrow::field("primaryTrackPhi", arrow::list(arrow::float32())),
      arrow::field("primaryTrackEta", arrow::list(arrow::float32())),
      arrow::field("primaryTrackNHits", arrow::list(arrow::uint8())),
      arrow::field("primaryTrackNHitsFit", arrow::list(arrow::uint8())),
      arrow::field("primaryTrackNHitsPoss", arrow::list(arrow::uint8())),
      arrow::field("primaryTrackTopologyMap", arrow::list(arrow::uint64())),
      arrow::field("primaryTrackdEdx", arrow::list(arrow::float32())),
      arrow::field("primaryTrackChi2", arrow::list(arrow::float32())),
      arrow::field("primaryTrackVertexId", arrow::list(arrow::int32())),
      arrow::field("primaryTrackVertexRank", arrow::list(arrow::float32())),
      arrow::field("primaryTrackVertexZ", arrow::list(arrow::float32())),
      arrow::field("primaryTrackIdTruth", arrow::list(arrow::int32())),
      arrow::field("primaryTrackQaTruth", arrow::list(arrow::int32())),
      });
std::shared_ptr<parquet::WriterProperties> writer_properties;
std::shared_ptr<arrow::io::FileOutputStream> outfile;

void write_table() {
   ARROW_CHECK_OK(eventRunNumber.Finish(&eventRunNumberA));
   ARROW_CHECK_OK(eventNumber.Finish(&eventNumberA));
   ARROW_CHECK_OK(eventPtBin.Finish(&eventPtBinA));
   ARROW_CHECK_OK(primaryTrackFlag.Finish(&primaryTrackFlagA));
   ARROW_CHECK_OK(primaryTrackPt.Finish(&primaryTrackPtA));
   ARROW_CHECK_OK(primaryTrackPhi.Finish(&primaryTrackPhiA));
   ARROW_CHECK_OK(primaryTrackEta.Finish(&primaryTrackEtaA));
   ARROW_CHECK_OK(primaryTrackNHits.Finish(&primaryTrackNHitsA));
   ARROW_CHECK_OK(primaryTrackNHitsFit.Finish(&primaryTrackNHitsFitA));
   ARROW_CHECK_OK(primaryTrackNHitsPoss.Finish(&primaryTrackNHitsPossA));
   ARROW_CHECK_OK(primaryTrackTopologyMap.Finish(&primaryTrackTopologyMapA));
   ARROW_CHECK_OK(primaryTrackdEdx.Finish(&primaryTrackdEdxA));
   ARROW_CHECK_OK(primaryTrackChi2.Finish(&primaryTrackChi2A));
   ARROW_CHECK_OK(primaryTrackVertexId.Finish(&primaryTrackVertexIdA));
   ARROW_CHECK_OK(primaryTrackVertexRank.Finish(&primaryTrackVertexRankA));
   ARROW_CHECK_OK(primaryTrackVertexZ.Finish(&primaryTrackVertexZA));
   ARROW_CHECK_OK(primaryTrackIdTruth.Finish(&primaryTrackIdTruthA));
   ARROW_CHECK_OK(primaryTrackQaTruth.Finish(&primaryTrackQaTruthA));

   std::shared_ptr<arrow::Table> table = arrow::Table::Make(schema, {
         eventRunNumberA,
         eventNumberA,
         eventPtBinA,
         primaryTrackFlagA,
         primaryTrackPtA,
         primaryTrackPhiA,
         primaryTrackEtaA,
         primaryTrackNHitsA,
         primaryTrackNHitsFitA,
         primaryTrackNHitsPossA,
         primaryTrackTopologyMapA,
         primaryTrackdEdxA,
         primaryTrackChi2A,
         primaryTrackVertexIdA,
         primaryTrackVertexRankA,
         primaryTrackVertexZA,
         primaryTrackIdTruthA,
         primaryTrackQaTruthA,
         });

   PARQUET_THROW_NOT_OK(parquet::arrow::WriteTable(*table, arrow::default_memory_pool(), outfile, 2*1024*1024, writer_properties));
}

std::vector<const char*> pt_bins = {
   "pt2_3",
   "pt3_4",
   "pt4_5",
   "pt5_7",
   "pt7_9",
   "pt9_11",
   "pt11_15",
   "pt15_20",
   "pt20_25",
   "pt25_35",
   "pt35_-1",
};

std::string detect_bin(const StMuDstMaker &muDstMaker) {
   const char* filepath = muDstMaker.GetFile();
   unsigned int num_matches = 0;
   std::string last_match;
   for(const string &pt_bin : pt_bins) {
      if (strstr(filepath, pt_bin.c_str())) {
         last_match = pt_bin.c_str();
         num_matches++;
      }
   }
   if (num_matches == 0) {
      return "";
   } else if (num_matches == 1) {
      return last_match;
   } else {
      std::cerr << "Ambigous pt bin match in '" << filepath << "'. Matching bins:";
      for(const string &pt_bin : pt_bins) {
         if (strstr(filepath, pt_bin.c_str())) {
            std::cerr << " " << pt_bin << std::endl;
         }
      }
      throw std::exception();
   }
}

int main(int argc, char *argv[]) {
   if (argc != 2) {
      std::cerr << "Usage: " << argv[0] << " filename.lis" << std::endl;
      return EXIT_FAILURE;
   }
   std::string in_filename = argv[1];
   size_t pos = in_filename.rfind(".");
   if (pos == std::string::npos) {
      std::cerr << "Invalid filename" << std::endl;
      return EXIT_FAILURE;
   }
   std::string out_filename = in_filename.substr(0, pos) + ".parquet";
   std::cerr << "Output to " << out_filename << std::endl;

   StMuDstMaker muDstMaker(0, 0, "", in_filename.c_str(), ".", 1e9);

   muDstMaker.SetStatus("*", 0);

   std::vector<std::string> activeBranchNames = {
      "MuEvent",
      "PrimaryVertices",
      "PrimaryTracks",
      "GlobalTracks",
   };

   // Enable selected branches
   for (const auto& branchName : activeBranchNames) {
      muDstMaker.SetStatus(branchName.c_str(), 1);
   }

   {
      parquet::WriterProperties::Builder builder;
      builder.compression(parquet::Compression::GZIP);
      writer_properties = builder.build();
   }
   PARQUET_THROW_NOT_OK(arrow::io::FileOutputStream::Open(out_filename, &outfile));

   unsigned long long num_events = 0;
   do
   {
      try {
         if ( muDstMaker.Make() ) break;
      } catch(...) {
         break;
      }

      // Access all event data
      StMuDst *muDst = muDstMaker.muDst();

      // Access event header-wise information
      StMuEvent *muEvent = muDst->event();
      
      std::string bin = detect_bin(muDstMaker);

      ARROW_CHECK_OK(eventRunNumber.Append(muEvent->runNumber()));
      ARROW_CHECK_OK(eventNumber.Append(muEvent->eventNumber()));
      if (bin.size() != 0) {
         ARROW_CHECK_OK(eventPtBin.Append(bin));
      } else {
         ARROW_CHECK_OK(eventPtBin.AppendNull());
      }
      ARROW_CHECK_OK(primaryTrackFlag.Append());
      ARROW_CHECK_OK(primaryTrackPt.Append());
      ARROW_CHECK_OK(primaryTrackPhi.Append());
      ARROW_CHECK_OK(primaryTrackEta.Append());
      ARROW_CHECK_OK(primaryTrackNHits.Append());
      ARROW_CHECK_OK(primaryTrackNHitsFit.Append());
      ARROW_CHECK_OK(primaryTrackNHitsPoss.Append());
      ARROW_CHECK_OK(primaryTrackTopologyMap.Append());
      ARROW_CHECK_OK(primaryTrackdEdx.Append());
      ARROW_CHECK_OK(primaryTrackChi2.Append());
      ARROW_CHECK_OK(primaryTrackVertexId.Append());
      ARROW_CHECK_OK(primaryTrackVertexRank.Append());
      ARROW_CHECK_OK(primaryTrackVertexZ.Append());
      ARROW_CHECK_OK(primaryTrackIdTruth.Append());
      ARROW_CHECK_OK(primaryTrackQaTruth.Append());

      unsigned int num_vertices = StMuDst::numberOfPrimaryVertices();
      for (unsigned int j = 0; j < num_vertices; j++)
      {
         StMuDst::setVertexIndex(j);

         unsigned int num_tracks = StMuDst::numberOfPrimaryTracks();
         for (unsigned int i = 0; i < num_tracks; i++) {
            const StMuTrack* track = StMuDst::primaryTracks(i);

            ARROW_CHECK_OK(dynamic_cast<arrow::Int16Builder*>(primaryTrackFlag.value_builder())->Append(track->flag()));
            ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(primaryTrackPt.value_builder())->Append(track->pt() * track->charge()));
            ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(primaryTrackPhi.value_builder())->Append(track->phi()));
            ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(primaryTrackEta.value_builder())->Append(track->eta()));
            ARROW_CHECK_OK(dynamic_cast<arrow::UInt8Builder*>(primaryTrackNHits.value_builder())->Append(track->nHits()));
            ARROW_CHECK_OK(dynamic_cast<arrow::UInt8Builder*>(primaryTrackNHitsFit.value_builder())->Append(track->nHitsFit()));
            ARROW_CHECK_OK(dynamic_cast<arrow::UInt8Builder*>(primaryTrackNHitsPoss.value_builder())->Append(track->nHitsPoss()));
            StTrackTopologyMap topologyMap = track->topologyMap();
            uint64_t topologyMapI = static_cast<uint64_t>(topologyMap.data(0)) | static_cast<uint64_t>(topologyMap.data(1)) << 32;
            ARROW_CHECK_OK(dynamic_cast<arrow::UInt64Builder*>(primaryTrackTopologyMap.value_builder())->Append(topologyMapI));
            ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(primaryTrackdEdx.value_builder())->Append(track->dEdx()));
            ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(primaryTrackChi2.value_builder())->Append(track->chi2()));
            ARROW_CHECK_OK(dynamic_cast<arrow::Int32Builder*>(primaryTrackVertexId.value_builder())->Append(j));
            ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(primaryTrackVertexRank.value_builder())->Append(StMuDst::primaryVertex()->ranking()));
            ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(primaryTrackVertexZ.value_builder())->Append(StMuDst::primaryVertex()->position().z()));
            ARROW_CHECK_OK(dynamic_cast<arrow::Int32Builder*>(primaryTrackIdTruth.value_builder())->Append(track->idTruth()));
            ARROW_CHECK_OK(dynamic_cast<arrow::Int32Builder*>(primaryTrackQaTruth.value_builder())->Append(track->qaTruth()));
         }
      }
      {
         unsigned int num_tracks = StMuDst::numberOfGlobalTracks();
         for(unsigned int i = 0; i < num_tracks; i++) {
            const StMuTrack* track = StMuDst::globalTracks(i);

         }
      }

      if (num_events % 100000 == 0) {
        write_table();
        std::cerr << arrow::default_memory_pool()->bytes_allocated() / 1024. / 1024. << " mb\t" << num_events << std::endl;
      }
      num_events++;
   } while (true);

   write_table();
   outfile.reset();

   std::cerr << "Finish" << std::endl;

   return EXIT_SUCCESS;
}
