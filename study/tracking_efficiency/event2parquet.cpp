#include <memory>

#include <arrow/api.h>
#include <arrow/io/api.h>
#include <arrow/util/logging.h> // for ARROW_CHECK

#include <parquet/arrow/writer.h>

#include <TFile.h>
#include <TKey.h>

#include <St_base/StTree.h> // for StIOEvent
#include <StEvent/StEvent.h>
#include <StEvent/StTpcHitCollection.h>
#include <StEvent/StTpcHit.h>

void print_assert(const char* exp, const char* file, int line)
{
  std::cerr << "Assertion '" << exp << "' failed in '" << file << "' at line '" << line << "'" << std::endl;
  std::terminate();
}

#define myassert(exp) ((exp) ? (void)0 : print_assert(#exp, __FILE__, __LINE__))

arrow::MemoryPool* pool = arrow::default_memory_pool();

arrow::Int64Builder eventRunNumber(pool);
std::shared_ptr<arrow::Array> eventRunNumberA;

arrow::Int64Builder eventNumber(pool);
std::shared_ptr<arrow::Array> eventNumberA;

arrow::ListBuilder tpcHitAdc(pool, std::unique_ptr<arrow::UInt16Builder>(new arrow::UInt16Builder(pool)));
std::shared_ptr<arrow::Array> tpcHitAdcA;

arrow::ListBuilder tpcHitCharge(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> tpcHitChargeA;

arrow::ListBuilder tpcHitPadsInHit(pool, std::unique_ptr<arrow::UInt8Builder>(new arrow::UInt8Builder(pool)));
std::shared_ptr<arrow::Array> tpcHitPadsInHitA;

arrow::ListBuilder tpcHitTimeBucketsInHit(pool, std::unique_ptr<arrow::UInt8Builder>(new arrow::UInt8Builder(pool)));
std::shared_ptr<arrow::Array> tpcHitTimeBucketsInHitA;

arrow::ListBuilder tpcHitPositionX(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> tpcHitPositionXA;

arrow::ListBuilder tpcHitPositionY(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> tpcHitPositionYA;

arrow::ListBuilder tpcHitPositionZ(pool, std::unique_ptr<arrow::FloatBuilder>(new arrow::FloatBuilder(pool)));
std::shared_ptr<arrow::Array> tpcHitPositionZA;

arrow::ListBuilder tpcHitIdTruth(pool, std::unique_ptr<arrow::Int32Builder>(new arrow::Int32Builder(pool)));
std::shared_ptr<arrow::Array> tpcHitIdTruthA;

arrow::ListBuilder tpcHitQaTruth(pool, std::unique_ptr<arrow::Int32Builder>(new arrow::Int32Builder(pool)));
std::shared_ptr<arrow::Array> tpcHitQaTruthA;

std::shared_ptr<arrow::Schema> schema = arrow::schema({
      arrow::field("eventRunNumber", arrow::int64()),
      arrow::field("eventNumber", arrow::int64()),
      arrow::field("tpcHitAdc", arrow::list(arrow::uint16())),
      arrow::field("tpcHitCharge", arrow::list(arrow::float32())),
      arrow::field("tpcHitPadsInHit", arrow::list(arrow::uint8())),
      arrow::field("tpcHitTimeBucketsInHit", arrow::list(arrow::uint8())),
      arrow::field("tpcHitPositionX", arrow::list(arrow::float32())),
      arrow::field("tpcHitPositionY", arrow::list(arrow::float32())),
      arrow::field("tpcHitPositionZ", arrow::list(arrow::float32())),
      arrow::field("tpcHitIdTruth", arrow::list(arrow::int32())),
      arrow::field("tpcHitQaTruth", arrow::list(arrow::int32())),
      });
std::shared_ptr<arrow::io::FileOutputStream> outfile;
std::unique_ptr<parquet::arrow::FileWriter> writer;

void write_table() {
   ARROW_CHECK_OK(eventRunNumber.Finish(&eventRunNumberA));
   ARROW_CHECK_OK(eventNumber.Finish(&eventNumberA));
   ARROW_CHECK_OK(tpcHitAdc.Finish(&tpcHitAdcA));
   ARROW_CHECK_OK(tpcHitCharge.Finish(&tpcHitChargeA));
   ARROW_CHECK_OK(tpcHitPadsInHit.Finish(&tpcHitPadsInHitA));
   ARROW_CHECK_OK(tpcHitTimeBucketsInHit.Finish(&tpcHitTimeBucketsInHitA));
   ARROW_CHECK_OK(tpcHitPositionX.Finish(&tpcHitPositionXA));
   ARROW_CHECK_OK(tpcHitPositionY.Finish(&tpcHitPositionYA));
   ARROW_CHECK_OK(tpcHitPositionZ.Finish(&tpcHitPositionZA));
   ARROW_CHECK_OK(tpcHitIdTruth.Finish(&tpcHitIdTruthA));
   ARROW_CHECK_OK(tpcHitQaTruth.Finish(&tpcHitQaTruthA));

   std::shared_ptr<arrow::Table> table = arrow::Table::Make(schema, {
         eventRunNumberA,
         eventNumberA,
         tpcHitAdcA,
         tpcHitChargeA,
         tpcHitPadsInHitA,
         tpcHitTimeBucketsInHitA,
         tpcHitPositionXA,
         tpcHitPositionYA,
         tpcHitPositionZA,
         tpcHitIdTruthA,
         tpcHitQaTruthA,
         });

   int64_t chunk_size = 2*1024*1024;
   PARQUET_THROW_NOT_OK(writer->WriteTable(*table, chunk_size));
}

int main(int argc, char *argv[]) {
  if ((argc < 2) || (argc > 3)) {
    std::cerr << "Usage: " << argv[0] << " inputfile.event.root [ outputfile.event.parquet ]" << std::endl;
    return EXIT_FAILURE;
  }
  std::string in_filename = argv[1];
  std::string out_filename;
  if (argc >= 3) {
    out_filename = argv[2];
  } else {
    size_t pos = in_filename.rfind(".");
    if (pos == std::string::npos) {
      std::cerr << "Invalid filename" << std::endl;
      return EXIT_FAILURE;
    }
    out_filename = in_filename.substr(0, pos) + ".parquet";
  }

  TFile in(in_filename.c_str());

  outfile = std::move(arrow::io::FileOutputStream::Open(out_filename).ValueOrDie());
  {
    parquet::WriterProperties::Builder builder;
    builder.compression(parquet::Compression::GZIP);
    std::shared_ptr<parquet::WriterProperties> writer_properties = builder.build();
    PARQUET_THROW_NOT_OK(parquet::arrow::FileWriter::Open(
                           *schema,
                           arrow::default_memory_pool(),
                           outfile,
                           writer_properties,
                           &writer
                           ));
  }

  unsigned long long num_events = 0;
  TIter key_iter(in.GetListOfKeys());
  TKey *key;
  while ((key = (TKey*)key_iter())) {
    string key_name = key->GetName();
    size_t pos = key_name.find(".");
    string branch_name = key_name.substr(0, pos);
    if (branch_name != "eventBranch") {
      continue;
    }

    std::unique_ptr<StIOEvent> ioevent { dynamic_cast<StIOEvent*>(in.Get(key_name.c_str())) };
    myassert(ioevent);
    std::unique_ptr<TList> obj_list { dynamic_cast<TList*>(ioevent->fObj) };
    myassert(obj_list);
    obj_list->SetOwner(true);
    TListIter obj_list_iter(obj_list.get());
    TObject *o;
    StEvent *event = nullptr;
    while ((o = obj_list_iter())) {
      if ((event = dynamic_cast<StEvent*>(o))) {
        break;
      }
    }
    if (!event) {
      std::cerr << "Can't find StEvent in branch" << std::endl;
      std::terminate();
    }

    ARROW_CHECK_OK(eventRunNumber.Append(event->runId()));
    ARROW_CHECK_OK(eventNumber.Append(event->id()));
    ARROW_CHECK_OK(tpcHitAdc.Append());
    ARROW_CHECK_OK(tpcHitCharge.Append());
    ARROW_CHECK_OK(tpcHitPadsInHit.Append());
    ARROW_CHECK_OK(tpcHitTimeBucketsInHit.Append());
    ARROW_CHECK_OK(tpcHitPositionX.Append());
    ARROW_CHECK_OK(tpcHitPositionY.Append());
    ARROW_CHECK_OK(tpcHitPositionZ.Append());
    ARROW_CHECK_OK(tpcHitIdTruth.Append());
    ARROW_CHECK_OK(tpcHitQaTruth.Append());
    StTpcHitCollection *hit_collection = event->tpcHitCollection();
    if (!hit_collection) {
      std::cerr << "Event " << event->runId() << ":" << event->id() << ": hit_collection is a nullptr" << std::endl;
      // no need to fill hits, produce an empty event
      continue;
    }
    unsigned int num_sectors = hit_collection->numberOfSectors();
    for (unsigned int sector = 0; sector < num_sectors; sector++) {
      StTpcSectorHitCollection *sector_hit_collection = hit_collection->sector(sector);
      myassert(sector_hit_collection);
      unsigned int num_padrows = sector_hit_collection->numberOfPadrows();

      for (unsigned int padrow = 0; padrow < num_padrows; padrow++) {
        StTpcPadrowHitCollection *padrow_hit_collection = sector_hit_collection->padrow(padrow);
        myassert(padrow_hit_collection);
        StSPtrVecTpcHit &hits = padrow_hit_collection->hits();

        for (const StTpcHit *hit : hits) {
          ARROW_CHECK_OK(dynamic_cast<arrow::UInt16Builder*>(tpcHitAdc.value_builder())->Append(hit->adc()));
          ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(tpcHitCharge.value_builder())->Append(hit->charge()));
          ARROW_CHECK_OK(dynamic_cast<arrow::UInt8Builder*>(tpcHitPadsInHit.value_builder())->Append(hit->padsInHit()));
          ARROW_CHECK_OK(dynamic_cast<arrow::UInt8Builder*>(tpcHitTimeBucketsInHit.value_builder())->Append(hit->timeBucketsInHit()));
          ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(tpcHitPositionX.value_builder())->Append(hit->position().x()));
          ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(tpcHitPositionY.value_builder())->Append(hit->position().y()));
          ARROW_CHECK_OK(dynamic_cast<arrow::FloatBuilder*>(tpcHitPositionZ.value_builder())->Append(hit->position().z()));
          ARROW_CHECK_OK(dynamic_cast<arrow::Int32Builder*>(tpcHitIdTruth.value_builder())->Append(hit->idTruth()));
          ARROW_CHECK_OK(dynamic_cast<arrow::Int32Builder*>(tpcHitQaTruth.value_builder())->Append(hit->qaTruth()));
        }
      }
    }
    int64_t megabytes_allocated = arrow::default_memory_pool()->bytes_allocated() / 1024. / 1024.;
    if (megabytes_allocated > 256 /* mb */) {
      write_table();
      std::cerr << megabytes_allocated << " mb\t" << num_events << std::endl;
    }
    num_events++;
  }

  write_table();
  PARQUET_THROW_NOT_OK(writer->Close());
  outfile.reset();

  return EXIT_SUCCESS;
}
