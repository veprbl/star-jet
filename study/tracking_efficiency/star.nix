{ nixpkgs /*? import <nixpkgs> {}*/, nixpkgs_src /*? <nixpkgs>*/, overlays /*? []*/ }:

let
  _eff_pkgs = nixpkgs.pkgsi686Linux;
  overlay = final: prev: {
    stdenv = (_eff_pkgs.overrideCC _eff_pkgs.stdenv _eff_pkgs.gcc48).override {
      fetchurlBoot = nixpkgs.fetchurl;
      overrides = final: prev: {
        perl = prev.perl.overrideAttrs (old: {
          hardeningDisable = [ "format" ];
        });
      };
    };
    inherit (nixpkgs) fetchurl;

    pam = _eff_pkgs.pam.override { stdenv = _eff_pkgs.stdenv; };
  };
  eff_pkgs = import nixpkgs_src { overlays = [ overlay ] ++ overlays; };
  stdenv = eff_pkgs.stdenv;
  root = (eff_pkgs.root5.override { inherit stdenv;
    python2 = null;
    libX11=null; libXpm=null; libXft=null; libXext=null; libGLU=null; libGL=null;
      }).overrideAttrs (old: rec {
    version = "5.34.30"; # no significant difference to STAR's "5.34.30a_2"
    name = "root-${version}";
    src = nixpkgs.fetchurl {
      url = "https://root.cern.ch/download/root_v${version}.source.tar.gz";
      sha256 = "1iahmri0vi1a44qkp2p0myrjb6izal97kf27ljfvpq0vb6c6vhw4";
    };
    cmakeFlags = old.cmakeFlags ++ [
      "-Dtable=ON"
      "-Dvc=ON"
      "-Dopengl=OFF"
      "-Dx11=OFF"
      ];
  });
in
  {
    inherit eff_pkgs stdenv root;
    inherit (eff_pkgs) libxml2;
    # C++ packages are to be rebuilt with gcc48
    log4cxx = eff_pkgs.log4cxx.override { inherit stdenv; };
    fastjet = eff_pkgs.fastjet.override { inherit stdenv; };
  }
