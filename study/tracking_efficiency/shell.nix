{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation {
  name = "parquet-shell";

  buildInputs = [
    python3
    python3Packages.pyarrow
    python3Packages.pyjet
    python3Packages.pandas
    python3Packages.matplotlib
    python3Packages.tqdm # optional
  ];
}
