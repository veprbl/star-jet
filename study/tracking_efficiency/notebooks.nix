{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  utils = lib.sourceByRegex ./. [
    "^utils(/.+\.py)?$"
  ];
  runNotebook = store: name: notebookfile: lists: runCommand name {
    inherit lists;
    nativeBuildInputs = [
      python3Packages.nbconvert
      #texlive.combined.scheme-small
      (texlive.combine { inherit (texlive) scheme-small type1cm; })
    ] ++ (import ./shell.nix { inherit pkgs; }).buildInputs;
    PYTHONPATH = utils;
  } ''
    mkdir "$out"
    cd "$out"

    # Prepeare inputs
    notebook_name="$(stripHash "${notebookfile}")"
    cp ${notebookfile} "$notebook_name"
    for list in $lists; do
      cp "$list" "$(stripHash "$list")"
    done
    ln -s ${store} store

    # Prepare output directories
    mkdir single_track_embedding-output
    mkdir data-driven-output

    # Execute the notebook
    jupyter-nbconvert \
      "$notebook_name" \
      --execute --ExecutePreprocessor.timeout=-1 \
      --to html --output "$out"/index.html

    rm store

    if [ ! -z "$(ls single_track_embedding-output)" ]; then
      mv single_track_embedding-output/* .
    fi
    if [ ! -z "$(ls data-driven-output)" ]; then
      mv data-driven-output/* .
    fi

    rmdir single_track_embedding-output
    rmdir data-driven-output
  '';
  PARQUET_STORE = "`cat ${builtins.getEnv "HOME"}/.parquet_store`";
in
  {

    single_track_embedding = runNotebook
      PARQUET_STORE
      "single_track_embedding"
      ./single_track_embedding.ipynb
      [
        ./pp200.sl13b_embed.data_single_track_multi_1b91fc62ed8f8245e3de078224a958ccb749a2bc.lst
        ./pp200.sl13b_embed.embed_single_track_multi_cff3d69a5315f98cd42ab7c4dd2c4eebecd7c7c2.lst
        ./pp200.sl13b_embed.reset_tpcrawdata.embed_single_track_multi_fafd13ddde05957f1faa0a80ebfe37221659369f.lst
        ./pp510.sl13b_embed.data_single_track_multi_c618ece1b5aab82599f1e4783de434038897eae3.lst
        ./pp510.sl13b_embed.embed_single_track_multi_d26a2e52c7f585aac97bf178fc4bbbb0c3946277.lst
        ./pp510.sl13b_embed.reset_tpcrawdata.embed_single_track_multi_4ca643c8b9de79585a564149a476d7dce7f63944.lst
        ./pp200.sl13b_embed.eplus.data_single_track_multi_47e96553d19003f3464c7c30612743181b347bbf.lst
        ./pp200.sl13b_embed.eplus.embed_single_track_multi_90765f07b8424c302f63e7d7151ec40e1ab2f07a.lst
      ]
    ;

    data-driven = runNotebook
      PARQUET_STORE
      "data-driven"
      ./data-driven.ipynb
      [
        ./pp200.sl13b_embed.data_multi_3dfcd2d517c79645eba0e42951a5273de50676fa.lst
        ./pp200.sl13b_embed.embed_multi_6a133b358ea9260c4e85ea77306f03e976d146d0.lst
        ./pp200.sl13b_embed.reset_tpcrawdata.embed_multi_bc4b847bcdeb4f82e4cec868d78486a30a0641c5.lst
        ./pp510.sl13b_embed.data_multi_824f66182992ccfa16797814b71366aad8fda719.lst
        ./pp510.sl13b_embed.embed_multi_50ac800ee420f26ff4d93092d72398c6b50472f4.lst
        ./pp510.sl13b_embed.reset_tpcrawdata.embed_multi_384dd51ead12a805323c20b3395c56b5fe497cb8.lst
      ]
    ;

  }
