import numpy as np
import pandas as pd
from .common import is_embedded_track_id


def calc_eff_uncertainty(sumX, sumY, sumX2=None, sumY2=None):
    if sumX2 is None:
        sumX2 = sumX
    if sumY2 is None:
        sumY2 = sumY
    assert np.all(sumX2 >= 0)
    assert np.all(sumY2 >= 0)
    assert np.all(sumX <= sumY)
    eff = sumX / sumY
    return np.sqrt(
        ((1 - 2 * eff) * sumX2 + (eff**2) * sumY2)
    ) / sumY


def calc_efficiency(pqs, cut_set_name='primaryTrackPassAllCuts'):
    def _check_tracks(row):
        good_tracks = row['primaryTrackIsFromHighestRankingVertex'] \
            & row[cut_set_name]
        is_the_embedded = is_embedded_track_id(row['primaryTrackIdTruth'])
        if not (good_tracks & ~is_the_embedded).any():
            # shortcut:
            # by construction no good tracks implies no good true tracks
            return (False, False)
        good_true_tracks = good_tracks & is_the_embedded
        # it shouldn't be possible for a single particle to produce more than one good primary track
        #assert good_true_tracks.sum() <= 1
        # yet it happens once in a while...
        has_good_true_tracks = good_true_tracks.any()
        return (True, has_good_true_tracks)
    
    track_counts = \
        pd.concat([
            pqs.tracks['primaryTrackIsFromHighestRankingVertex'],
            pqs.tracks['primaryTrackIdTruth'],
            pqs.cuts
        ], axis=1, sort=False, copy=False) \
          .apply(_check_tracks, axis=1, result_type='reduce') \
          .value_counts()
    
    n_good_true_track_found = track_counts.get((True, True), default=0)
    n_good_tracks_found = track_counts.get((True, False), default=0) + n_good_true_track_found
    if n_good_tracks_found > 0:
        eff = n_good_true_track_found / n_good_tracks_found
        delta_eff = calc_eff_uncertainty(n_good_true_track_found, n_good_tracks_found)
    else:
        eff = np.nan
        delta_eff = np.nan

    return (
        n_good_true_track_found,
        n_good_tracks_found,
        eff,
        delta_eff,
    )


def round_eff_tuple(eff_tuple):
    return (
        eff_tuple[0],
        eff_tuple[1],
        np.round(eff_tuple[2] * 1000) / 10,
        np.ceil(eff_tuple[3] * 1000) / 10,
    )


def apply_thrown_eta_cut(pqs, cond=lambda thrown_eta: np.abs(thrown_eta) < 0.7):
    thrown_eta = pqs.rs_geant['thrownEta'].values
    eta_cut = cond(thrown_eta)
    return pqs.iloc[eta_cut]


apply_eta_thrown_cut = apply_thrown_eta_cut # For backwards compatibility TODO: remove once notebooks updated


from .common import select_require_chg_jets
apply_charged_jet_cut = select_require_chg_jets # TODO: remove once notebooks updated


def apply_vz_cut(pqs, cond=lambda vz: np.abs(vz) < 60):
    vz_cut = cond(pqs.tracks['highestRankingVertexZ'].values)
    return pqs.iloc[vz_cut]
