from cycler import cycler
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors

from .constants import \
    TPC_LENGTH, TPC_INNER_RADIUS, TPC_OUTER_RADIUS
from .common import \
    geant3_particle, \
    is_mc_assoc_track_id, is_embedded_track_id


def setup_paper_style():
    mpl.rcParams.update(mpl.rcParamsDefault)

    # colors from ggplot
    ggplot_prop_cycle = cycler(color=[
        '#E24A33', # red
        '#348ABD', # blue
        '#988ED5', # purple
        '#777777', # gray
        '#FBC15E', # yellow
        '#8EBA42', # green
        '#EFA5A8', # pink
    ])

    plt.style.use('seaborn-paper')
    figsize_scale = 0.6
    plt.rcParams.update({
        'axes.grid': True,
        'axes.labelsize': 8,
        'axes.prop_cycle': ggplot_prop_cycle,
        'figure.figsize': (5 * figsize_scale, 5 * figsize_scale),
        'font.family': ['serif'],
        'grid.linestyle': ':',
        'grid.linewidth': 0.4,
        'legend.fontsize': 7,
        'legend.frameon': False,
        'xtick.bottom': True,
        'xtick.direction': 'in',
        'xtick.labelsize': 8,
        'xtick.major.pad': 2.5,
        'xtick.minor.pad': 2.4,
        'xtick.top': True,
        'ytick.direction': 'in',
        'ytick.labelsize': 8,
        'ytick.left': True,
        'ytick.major.pad': 2.5,
        'ytick.minor.pad': 2.4,
        'ytick.right': True,
        'pgf.rcfonts': False,
    })


def setup_presentation_style():
    mpl.rcParams.update(mpl.rcParamsDefault)
    plt.style.use('ggplot')
    plt.rcParams.update({
        'axes.labelsize': 8,
        'axes.titlesize': 9,
        'figure.titlesize': 9,
        'figure.figsize': (3, 3),
        'legend.fontsize': 7,
        'xtick.labelsize': 8,
        'ytick.labelsize': 8,
        'pgf.rcfonts': False,
    })


def make_profile(xs, ys, bins, **kwargs):
    a = np.digitize(xs, bins)
    pos = np.eye(len(bins) + 1)[a]
    sumy = pos.T.dot(ys)
    sumw = pos.sum(axis=0)
    with np.errstate(divide='ignore', invalid='ignore'):
        avg = (sumy / sumw)[1:-1]
    return avg


def plot_vector(ys, dys=None, bins=None, vlines=True, *args, **kwargs):
    if bins is None:
        bins = np.arange(len(ys) + 1)
    if vlines:
        lines = plt.plot(bins, np.concatenate((ys, [np.nan])), drawstyle="steps-post", *args, **kwargs)
        color = lines[0].get_color()
    else:
        if 'ls' in kwargs:
            kwargs['linestyles'] = kwargs['ls']
            del kwargs['ls']
        linecol = plt.hlines(ys, bins[:-1], bins[1:], *args, **kwargs)
        color = linecol.get_color()
    if dys is not None:
        _eb_kwargs = dict(kwargs)
        if 'label' in _eb_kwargs:
            del _eb_kwargs['label']
        # inherit the color of the line
        if 'color' not in _eb_kwargs:
            _eb_kwargs['color'] = color
        plt.errorbar((bins[:-1] + bins[1:]) / 2,
                     ys,
                     yerr=dys,
                     fmt='none',
                     barsabove=True,
                     **_eb_kwargs)
    if bins is not None:
        plt.autoscale(enable=True, axis='x', tight=True)


def plot_vectors_with_ratio(plots, bins=None, figsize=None):
    subplots_kwargs = {}
    if figsize is not None:
        subplots_kwargs['figsize'] = figsize
    f, ax = plt.subplots(2, sharex=True, gridspec_kw = { 'height_ratios': [3, 1] }, **subplots_kwargs)
    ref_plot = plots[0]
    for plot in plots:
        ys = plot['ys']
        dys = plot['dys']
        ref_ys = ref_plot['ys']
        vis_kwargs = plot['vis_kwargs']
        plt.sca(ax[0])
        plot_vector(ys, dys=dys, bins=bins, **vis_kwargs)
        plt.sca(ax[1])
        with np.errstate(divide='ignore', invalid='ignore'):
            rs = ys / ref_ys
            drs = dys / ref_ys
        plot_vector(rs, dys=drs, bins=bins, **vis_kwargs)
    return f, ax


def hist2d(x, y, **kwargs):
    histogram2d_kwargs = {}
    if 'bins' in kwargs:
        histogram2d_kwargs['bins'] = kwargs['bins']
    m, xs, ys = np.histogram2d(x, y, **histogram2d_kwargs)
    m = np.ma.array(m, mask=(m == 0))
    plt.pcolormesh(xs, ys, m.T)
    plt.colorbar()


def set_label(x=None, y=None, offset=1.0):
    if x is not None:
        plt.xlabel(x, horizontalalignment='right', x=offset, fontweight='semibold')
    if y is not None:
        plt.ylabel(y, horizontalalignment='right', y=offset, fontweight='semibold')


def setup_ratio_plot(ax, fine_agreement=False):
    """
    Setups the labeling, spacing and axes limits for ratio figures
    produced by plot_vectors_with_ratio().

    The idea is to always have the horizontal grid lines on the ratio
    plot every 0.05 of ratio units. So that whenever the scale of the
    ratio comparison changes, the grid lines give reader a clue about
    the change of the y-limits range.

    The fine_agreement=True preset is to be used for the ratio plots
    where agreement is already good and we are looking for small
    deviations.
    """
    plt.sca(ax[1])
    plt.ylabel("Ratio to data")
    if fine_agreement:
        plt.ylim(0.94, 1.06)
        plt.gca().yaxis.set_major_locator(mpl.ticker.MultipleLocator(base=0.05))
        plt.gca().yaxis.set_minor_locator(mpl.ticker.MultipleLocator(base=0.01))
    else:
        plt.ylim(0.5, 1.5)
        plt.gca().yaxis.set_minor_locator(mpl.ticker.MultipleLocator(base=0.05))
        plt.grid(b=True, which='minor', axis='y', alpha=0.4)
    plt.subplots_adjust(hspace=0.1)


def setup_tpc_sector_phi_plot(ax, eta_cut=None):
    """
    Sets major ticks at phi values proportional to 90 degrees, with no
    grid enabled. Places minor ticks and vertical grid lines on the
    sector boundaries. If *eta_cut* is specified, the sectors are
    labeled.
    """

    for _ax in ax:
        _ax.xaxis.set_major_locator(
            mpl.ticker.FixedLocator([-180, -90, 0, 90, 180])
        )
        _ax.grid(False, which='major', axis='x')
        # sector boundaries for minor ticks and vertical grid lines
        _ax.xaxis.set_minor_locator(
            mpl.ticker.FixedLocator(np.arange(-180 + 15, 180, 30))
        )
        _ax.grid(True, which='minor', axis='x')

    if eta_cut is not None:
        fontdict = {
            'color':  mpl.rcParams['grid.color'],
            'size': mpl.rcParams['xtick.labelsize'],
        }
        prev_ax = plt.gca()
        plt.sca(ax[0])
        for sector in range(1, 12 + 1):
            if eta_cut == 1:
                angle = 60 - 30 * (sector - 1)
                sector_id = sector
            elif eta_cut == -1:
                angle = 120 + 30 * (sector - 1)
                sector_id = sector + 12
            if angle < -180 + 15:
                angle += 360
            if angle > 180 + 15:
                angle -= 360
            y = np.mean(ax[0].get_yticks()[:2])
            plt.text(
                angle, y, f"{sector_id}",
                horizontalalignment='center', verticalalignment='center',
                fontdict=fontdict
            )
        plt.sca(prev_ax)


def get_radius(p):
    # p in GeV
    p *= 1000.0  # convert GeV to MeV
    B = 5  # kG
    return p / (0.3 * B)


def get_track_arc(o1x, o1y, p1x, p1y, o2x, o2y, p2x, p2y):
    p1conj = np.array([p1y, -p1x])
    p2conj = np.array([p2y, -p2x])
    o1 = np.array([o1x, o1y])
    o2 = np.array([o2x, o2y])
    k1, k2 = np.linalg.inv(
        np.array([[-p1conj[0], p2conj[0]], [-p1conj[1], p2conj[1]]])
    ).dot(o1 - o2)
    center = o1 + p1conj * k1
    center_alt = o2 + p2conj * k2
    assert np.sqrt(np.dot(center - center_alt, center - center_alt)) < 2.0  # cm
    # find arc parameters
    theta1 = np.arctan2((o1 - center)[1], (o1 - center)[0])
    theta2 = np.arctan2((o2 - center)[1], (o2 - center)[0])
    radius1 = np.sqrt(np.dot(o1 - center, o1 - center))
    radius2 = np.sqrt(np.dot(o2 - center, o2 - center))
    if radius1 - radius2 > 10.0:  # cm
        print(f"Warning: found radii don't agree: {radius1:.1f}, {radius2:.1f}")
    radius = np.mean([radius1, radius2])
    p = np.sqrt(p1x ** 2 + p1y ** 2)
    if theta1 < 0:
        theta1 += 2 * np.pi
    if theta2 < 0:
        theta2 += 2 * np.pi

    charge = np.sign(p1x * p2y - p2x * p1y)
    if charge < 0:
        theta1, theta2 = theta2, theta1

    theta1 *= 180.0 / np.pi
    theta2 *= 180.0 / np.pi

    return mpl.patches.Arc(
        center, 2 * radius, 2 * radius, theta1=theta1, theta2=theta2
    )


def plot_tpc_end_view_outline(ax):
    """
    Display end view of the TPC barrel
    """
    c1 = plt.Circle((0, 0), TPC_INNER_RADIUS, color="black", fill=False)
    c2 = plt.Circle((0, 0), TPC_OUTER_RADIUS, color="black", fill=False)
    ax.add_artist(c1)
    ax.add_artist(c2)
    for angle in np.linspace(np.pi / 12, 2 * np.pi + np.pi / 12, 12, endpoint=False):
        l = plt.Line2D(
            (TPC_INNER_RADIUS * np.cos(angle), TPC_OUTER_RADIUS * np.cos(angle)),
            (TPC_INNER_RADIUS * np.sin(angle), TPC_OUTER_RADIUS * np.sin(angle)),
            color="black",
        )
        l.set_linewidth(c1.get_linewidth())
        l.set_alpha(0.25)
        ax.add_artist(l)


def show_event(event_id, geant_event, simu_event, reco_event, ttype):
    has_geant = geant_event is not None

    if has_geant:
        f, axes = plt.subplots(2, 4, figsize=(21, 11))
        ax, ax2 = axes
        plt.subplots_adjust(hspace=0.25)
    else:
        f, ax2 = plt.subplots(1, 4, figsize=(21, 5))
        axes = (ax2,)
    figlegend = plt.figure(figsize=(3, 2))

    # Draw TPC outline
    for row_ax in axes:
        plot_tpc_end_view_outline(row_ax[1])

    # Display geant TPC hits
    track_color = {}
    cmap = plt.rcParams["axes.prop_cycle"].by_key()["color"]
    i = 0
    if has_geant:
        for track_id, ge_pid, track_ptot in zip(
            geant_event["g2t_track_id"],
            geant_event["g2t_track_ge_pid"],
            geant_event["g2t_track_ptot"],
        ):
            cond = geant_event["g2t_tpc_hit_track_p"] == track_id
            de = geant_event["g2t_tpc_hit_de"][cond]
            xs = geant_event["g2t_tpc_hit_x"][cond]
            ys = geant_event["g2t_tpc_hit_y"][cond]
            zs = geant_event["g2t_tpc_hit_z"][cond]
            if len(xs) < 10:
                continue
            c = cmap[i % 7]
            track_color[track_id] = c
            i += 1
            plt.sca(ax[1])
            particle_name = geant3_particle.get(ge_pid, "PID{}".format(ge_pid))
            nhits = len(xs)
            plt.scatter(
                xs,
                ys,
                s=8,
                c=c,
                marker=".",
                lw=0,
                label=f"{particle_name} track {track_id} of {nhits} hits, {track_ptot:.2f} GeV/c",
            )
            plt.sca(ax[2])
            plt.scatter(zs, xs, s=8, c=c, marker=".", lw=0)
            plt.sca(ax[3])
            plt.scatter(zs, ys, s=8, c=c, marker=".", lw=0)

    def lookup_track_color(id_truth):
        if is_mc_assoc_track_id(id_truth):
            if is_embedded_track_id(id_truth):
                default_color = "red"
            else:
                default_color = "blue"
        else:
            default_color = "black"
        return track_color.get(id_truth, default_color)

    if simu_event is not None:
        # Display TPC hits after full simulation
        xs = simu_event["tpcHitPositionX"]
        ys = simu_event["tpcHitPositionY"]
        zs = simu_event["tpcHitPositionZ"]

        def is_truth(id_truth):
            return (id_truth > 0) and (id_truth < 1000)

        c = [
            mcolors.to_rgba(lookup_track_color(id_truth))[:3]
            + (1.0 if is_truth(id_truth) else 0.1,)
            for id_truth in simu_event["tpcHitIdTruth"]
        ]
        plt.sca(ax2[1])
        plt.scatter(xs, ys, s=8, c=c, marker=".", lw=0)
        plt.sca(ax2[2])
        plt.scatter(zs, xs, s=8, c=c, marker=".", lw=0)
        plt.sca(ax2[3])
        plt.scatter(zs, ys, s=8, c=c, marker=".", lw=0)

    c = [
        lookup_track_color(id_truth) for id_truth in reco_event[f"{ttype}TrackIdTruth"]
    ]
    c = [mcolors.to_rgba(color)[:3] for color in c]
    c = [(color[0] * 0.7, color[1] * 0.7, color[2] * 0.7) for color in c]
    # Draw track origins as dots
    plt.sca(ax2[1])
    plt.scatter(
        reco_event[f"{ttype}TrackHelixOriginX"],
        reco_event[f"{ttype}TrackHelixOriginY"],
        s=128,
        c=c,
        marker=".",
        lw=0,
    )
    plt.sca(ax2[2])
    plt.scatter(
        reco_event[f"{ttype}TrackHelixOriginZ"],
        reco_event[f"{ttype}TrackHelixOriginX"],
        s=128,
        c=c,
        marker=".",
        lw=0,
    )
    plt.sca(ax2[3])
    plt.scatter(
        reco_event[f"{ttype}TrackHelixOriginZ"],
        reco_event[f"{ttype}TrackHelixOriginY"],
        s=128,
        c=c,
        marker=".",
        lw=0,
    )
    z = list(
        zip(
            reco_event[f"{ttype}TrackIdTruth"],
            reco_event[f"{ttype}TrackNHits"],
            reco_event[f"{ttype}TrackHelixOriginX"],
            reco_event[f"{ttype}TrackHelixOriginY"],
            reco_event[f"{ttype}TrackHelixOriginZ"],
            reco_event[f"{ttype}TrackHelixPX"],
            reco_event[f"{ttype}TrackHelixPY"],
            reco_event[f"{ttype}TrackHelixPZ"],
            reco_event[f"{ttype}TrackOuterHelixOriginX"],
            reco_event[f"{ttype}TrackOuterHelixOriginY"],
            reco_event[f"{ttype}TrackOuterHelixOriginZ"],
            reco_event[f"{ttype}TrackOuterHelixPX"],
            reco_event[f"{ttype}TrackOuterHelixPY"],
            reco_event[f"{ttype}TrackOuterHelixPZ"],
            c,
        )
    )
    z.sort(key=lambda t: t[0] if t[0] != 0 else 123_456)  # sort by IdTruth
    for (
        track_id,
        nhits,
        o1x,
        o1y,
        o1z,
        p1x,
        p1y,
        p1z,
        o2x,
        o2y,
        o2z,
        p2x,
        p2y,
        p2z,
        c,
    ) in z:
        # Draw tracks as arcs
        p1 = np.sqrt(p1x ** 2 + p1y ** 2 + p1z ** 2)
        plt.sca(ax2[1])
        arc = get_track_arc(o1x, o1y, p1x, p1y, o2x, o2y, p2x, p2y)
        arc.set_edgecolor(c)
        arc.set_linewidth(0.5)
        plt.gca().add_artist(arc)
        line_for_legend, = plt.plot(
            [],
            [],
            c=c,
            label=f"{ttype} track of {nhits} hits, IdTruth {track_id}, {p1:.2f} GeV/c",
        )
        line_for_legend.set_linewidth(arc.get_linewidth())

        # Function to determine arrow size
        MIN_PT = 0.5
        MAX_PT = 2
        MIN_HL = 6
        MAX_HL = 12
        if p1 < MIN_PT:
            head_length = MIN_HL
        elif p1 > MAX_PT:
            head_length = MAX_HL
        else:
            head_length = MIN_HL + (MAX_HL - MIN_HL) * (p1 - MIN_PT) / (MAX_PT - MIN_PT)

        arrow_param = dict(
            head_width=head_length / 2,
            head_length=head_length,
            facecolor=c,
            edgecolor=c,
            zorder=2 if is_mc_assoc_track_id(track_id) else 1
        )

        # Draw tracks as arrows
        plt.sca(ax2[1])
        plt.arrow(
            o1x,
            o1y,
            50 * p1x,
            50 * p1y,
            **arrow_param
        )
        plt.sca(ax2[2])
        plt.arrow(
            o1z,
            o1x,
            50 * p1z,
            50 * p1x,
            **arrow_param
        )
        plt.sca(ax2[3])
        plt.arrow(
            o1z,
            o1y,
            50 * p1z,
            50 * p1y,
            **arrow_param
        )

    # Setup axes limits, titles, labels
    for row_ax in axes:
        plt.sca(row_ax[1])
        plt.xlim(-TPC_OUTER_RADIUS - 10, TPC_OUTER_RADIUS + 10)
        plt.ylim(-TPC_OUTER_RADIUS - 10, TPC_OUTER_RADIUS + 10)
        plt.xticks(np.linspace(-TPC_OUTER_RADIUS, TPC_OUTER_RADIUS, 9))
        plt.yticks(np.linspace(-TPC_OUTER_RADIUS, TPC_OUTER_RADIUS, 9))
        plt.xlabel("$x$, cm")
        plt.ylabel("$y$, cm")
        plt.sca(row_ax[2])
        plt.xlim(-TPC_LENGTH / 2 - 10, TPC_LENGTH / 2 + 10)
        plt.ylim(-TPC_OUTER_RADIUS - 10, TPC_OUTER_RADIUS + 10)
        plt.xlabel("$z$, cm")
        plt.ylabel("$x$, cm")
        plt.sca(row_ax[3])
        plt.xlim(-TPC_LENGTH / 2 - 10, TPC_LENGTH / 2 + 10)
        plt.ylim(-TPC_OUTER_RADIUS - 10, TPC_OUTER_RADIUS + 10)
        plt.xlabel("$z$, cm")
        plt.ylabel("$y$, cm")

    if has_geant:
        plt.sca(ax[1])
        plt.title(f"event {event_id}: geant TPC hits", loc="left")

    plt.sca(ax2[1])
    plt.title(
        f"event {event_id}: TPC hits after full simulation and reconstructed tracks",
        loc="left",
    )
    for _ax in ax2:
        _ax.set_facecolor("#dddddd")

    if has_geant:
        plt.sca(ax[0])
        handles_labels = ax[1].get_legend_handles_labels()
        plt.legend(*handles_labels, loc="upper center", markerscale=6)

    plt.sca(ax2[0])
    handles_labels = ax2[1].get_legend_handles_labels()
    plt.legend(*handles_labels, loc="upper center", markerscale=6)

    for row_ax in axes:
        plt.sca(row_ax[0])
        plt.axis("off")

    plt.show()
