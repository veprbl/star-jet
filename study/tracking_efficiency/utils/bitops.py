import numpy as np


def popcount64(n):
    if type(n) is int:
        n = np.uint64(n)
    n = (n & np.uint64(0x5555555555555555)) + ((n & np.uint64(0xAAAAAAAAAAAAAAAA)) >> np.uint64(1))
    n = (n & np.uint64(0x3333333333333333)) + ((n & np.uint64(0xCCCCCCCCCCCCCCCC)) >> np.uint64(2))
    n = (n & np.uint64(0x0F0F0F0F0F0F0F0F)) + ((n & np.uint64(0xF0F0F0F0F0F0F0F0)) >> np.uint64(4))
    n = (n & np.uint64(0x00FF00FF00FF00FF)) + ((n & np.uint64(0xFF00FF00FF00FF00)) >> np.uint64(8))
    n = (n & np.uint64(0x0000FFFF0000FFFF)) + ((n & np.uint64(0xFFFF0000FFFF0000)) >> np.uint64(16))
    n = (n & np.uint64(0x00000000FFFFFFFF)) + (n >> np.uint64(32))
    return n

assert popcount64(0) == 0
assert popcount64(1) == 1
assert popcount64(2) == 1
assert popcount64(3) == 2
for i in range(63):
    assert popcount64((1 << i) - 1) == i
for i in range(32):
    assert popcount64(0xDEADBEEF << i) == popcount64(0xDEADBEEF)
assert popcount64(0xFFFFFFFFFFFFFFFF) == 64


def _bitscan64(x, test_masks):
    all64 = np.uint64(0xFFFFFFFFFFFFFFFF)
    mask = np.full_like(x, all64)
    res = np.zeros_like(x)
    for i, test_mask in enumerate(test_masks):
        no_bit = (x & test_mask & mask) != 0
        np.bitwise_or(res, 1 << (len(test_masks) - i - 1),
                      out=res, where=~no_bit)
        mask &= np.where(no_bit, test_mask, ~test_mask)
    return res

@np.vectorize
def _low_bit_pos64_slow(x):
    assert x != 0
    for index in range(64):
        if np.uint64(x) & np.uint64(1 << index):
            return index

def low_bit_pos64(x):
    assert (x != 0).all()
    test_masks = [
        np.uint64(0x00000000FFFFFFFF),
        np.uint64(0x0000FFFF0000FFFF),
        np.uint64(0x00FF00FF00FF00FF),
        np.uint64(0x0F0F0F0F0F0F0F0F),
        np.uint64(0x3333333333333333),
        np.uint64(0x5555555555555555),
    ]
    return _bitscan64(x, test_masks)


@np.vectorize
def _high_bit_pos64_slow(x):
    assert x != 0
    for index in range(63, -1, -1):
        if np.uint64(x) & np.uint64(1 << index):
            return index

def high_bit_pos64(x):
    assert (x != 0).all()
    test_masks = [
        np.uint64(0xFFFFFFFF00000000),
        np.uint64(0xFFFF0000FFFF0000),
        np.uint64(0xFF00FF00FF00FF00),
        np.uint64(0xF0F0F0F0F0F0F0F0),
        np.uint64(0xCCCCCCCCCCCCCCCC),
        np.uint64(0xAAAAAAAAAAAAAAAA),
    ]
    return 63 - _bitscan64(x, test_masks)

_test = np.random.randint(0xFFFFFFFFFFFFFFFF, size=1000000, dtype=np.uint64)
assert (_low_bit_pos64_slow(_test) == low_bit_pos64(_test)).all()
assert (_high_bit_pos64_slow(_test) == high_bit_pos64(_test)).all()
