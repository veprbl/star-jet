from functools import reduce

import numpy as np
import pandas as pd
import pyarrow.parquet as pq
from pyjet import cluster


# from https://www.star.bnl.gov/public/comp/simu/gstar/Manual/particle_id.html
geant3_particle = {
    1: r"$\gamma$",
    2: r"$e^+$",
    3: r"$e^-$",
    4: r"$\nu$",
    5: r"$\mu^+$",
    6: r"$\mu^-$",
    7: r"$\pi^0$",
    8: r"$\pi^+$",
    9: r"$\pi^-$",
    10: r"$K^0_L$",
    11: r"$K^+$",
    12: r"$K^-$",
    13: r"$n$",
    14: r"$p^+$",
    15: r"$p^-$",
    16: r"$K^0_S$",
    17: r"$\eta$",
    18: r"$\Lambda$",
    19: r"$\Sigma^+$",
    20: r"$\Sigma^0$",
    21: r"$\Sigma^-$",
    45: r"${}^2H$",
}


try:
    from tqdm import tqdm
    # remove time information from bar format to make the output reproducible
    bar_format = "{l_bar}{bar}| {n_fmt}/{total_fmt}"
    progressbar = lambda *args, **kwargs: tqdm(*args, **dict(kwargs, bar_format=bar_format))
except ImportError:
    progressbar = lambda x, *args, **kwargs: x


def is_mc_assoc_track_id(id_truth):
    """
    Returns whether this TPC track's IdTruth indicates an associated
    with an MC track. A naive check would be to just check if
    id_truth>0, but in recent libraries (e.g. SL18c) the data tracks
    IdTruth have assigned values above 10000.
    """
    return (id_truth > 0) & (id_truth < 10000)


def is_embedded_track_id(id_truth):
    """
    Returns whether this TPC track was associated with the embedded
    single track. The single track is simulated in a separate starsim
    and TpcRS run where it is initially an id of 1, but that is later
    shifted by 500 in a custom mixer.
    """
    return id_truth == 501


from .constants import \
    TRIG_RUN12PP200_VPDMB_NOBSMD, \
    TRIG_RUN12PP200_JP0, \
    TRIG_RUN12PP200_JP1, \
    TRIG_RUN12PP200_JP2, \
    TRIG_RUN12PP510_VPDMB_NOBSMD, \
    TRIG_RUN12PP510_JP0, \
    TRIG_RUN12PP510_JP1, \
    TRIG_RUN12PP510_JP2

TRIG_VPDMB_NOBSMD = [TRIG_RUN12PP200_VPDMB_NOBSMD, TRIG_RUN12PP510_VPDMB_NOBSMD]
TRIG_JP0 = [TRIG_RUN12PP200_JP0, TRIG_RUN12PP510_JP0]
TRIG_JP1 = [TRIG_RUN12PP200_JP1, TRIG_RUN12PP510_JP1]
TRIG_JP2 = [TRIG_RUN12PP200_JP2, TRIG_RUN12PP510_JP2]

def select_trig(pqs, trig_ids):
    cond_func = lambda event_trig_ids: any((trig_id in event_trig_ids) for trig_id in trig_ids)
    cond = pqs.tracks['eventTriggers'] \
              .map(cond_func) \
              .values
    return pqs.iloc[cond]


def select_require_chg_jets(pqs):
    has_jet = pqs.jets.map(lambda jets: len(jets) > 0).values
    return pqs.iloc[has_jet]


def flatten(series):
    """
    Flatten series with array elements

    >>> flatten(pd.Series([
    ...     np.array([1., 2., 3.], dtype=np.double),
    ...     np.array([], dtype=np.double),
    ...     np.array([4., 5., 6.], dtype=np.double),
    ... ]))
    array([1., 2., 3., 4., 5., 6.])
    >>> flatten(pd.Series([
    ...     np.array([1, 2, 3], dtype=np.int64),
    ...     np.array([], dtype=np.int64),
    ...     np.array([4, 5, 6], dtype=np.int64),
    ... ]))
    array([1, 2, 3, 4, 5, 6])
    >>> flatten(pd.Series([
    ...     np.array([1, 2, 3], dtype=np.int64),
    ...     np.array([], dtype=np.double),
    ...     np.array([4, 5, 6], dtype=np.int64),
    ... ]))
    array([1, 2, 3, 4, 5, 6])
    >>> flatten(pd.Series([
    ...     np.array([], dtype=np.double),
    ...     np.array([4., 5., 6.], dtype=np.double),
    ... ]))
    array([4., 5., 6.])
    >>> flatten(pd.Series([
    ...     np.array([], dtype=np.int64),
    ...     np.array([4, 5, 6], dtype=np.int64),
    ... ]))
    array([4, 5, 6])
    """
    assert isinstance(series, pd.Series)
    not_empty = series.map(len) != 0
    return np.concatenate(series[not_empty].values)


class PqSetLocator:
    def __init__(self, pqs):
        self.pqs = pqs

    def __getitem__(self, key):
        result = PqSet()
        result.jets = self.pqs.jets[key]
        result.tracks = self.pqs.tracks[key]
        result.events = None if self.pqs.events is None else self.pqs.events[key]
        result.geant = None if self.pqs.geant is None else self.pqs.geant[key]
        result.rs_geant = None if self.pqs.rs_geant is None else self.pqs.rs_geant[key]
        result.cuts = self.pqs.cuts[key]
        return result


class PqSet:
    @property
    def iloc(self):
        return PqSetLocator(self)


def apply_cuts(df):
    cut_df = pd.DataFrame()
    cut_df["primaryTrackPassVertexRankCut"] = df["primaryTrackVertexRank"].map(
        lambda rank: rank > 0
    )
    cut_df["primaryTrackPassFlagCut"] = df["primaryTrackFlag"].map(lambda fs: fs > 0)
    cut_df["primaryTrackPassNHitsCut"] = df["primaryTrackNHits"].map(lambda fs: fs > 12)
    cut_df["primaryTrackPassPossibleHitRatioCut"] = (
        df["primaryTrackNHits"] / df["primaryTrackNHitsPoss"]
    ).map(lambda fs: fs >= 0.51)
    pt1 = 0.5
    pt2 = 1.5
    dcaMax1 = 2.0
    dcaMax2 = 1.0

    def get_max_dca(pt):
        return (
            (pt < pt1) * dcaMax1
            + np.logical_and(pt >= pt1, pt < pt2)
            * (dcaMax1 + (dcaMax2 - dcaMax1) / (pt2 - pt1) * (pt - pt1))
            + (pt >= pt2) * dcaMax2
        )

    max_dca = df["primaryTrackPt"].map(get_max_dca)
    cut_df["primaryTrackPassDcaCut"] = (df["primaryTrackTdca"] - max_dca).map(
        lambda d: d < 0
    )

    cut_df["primaryTrackPassPtCut"] = df["primaryTrackPt"].map(
        lambda pt: np.logical_and(pt > 0.2, pt < 200.0)
    )

    ETA_MIN = -2.5
    ETA_MAX = 2.5
    cut_df["primaryTrackPassEtaCut"] = df["primaryTrackEta"].map(
        lambda eta: np.logical_and(eta >= ETA_MIN, eta <= ETA_MAX)
    )

    # http://www.star.bnl.gov/HyperNews-star/protected/get/jetfinding/992/1.html
    LAST_POINT_MIN_DISTANCE = 125  # cm
    cut_df["primaryTrackPassLastPointCut"] = (
        df["primaryTrackOuterHelixOriginX"] ** 2
        + df["primaryTrackOuterHelixOriginY"] ** 2
    ).map(lambda dsq: np.sqrt(dsq) > LAST_POINT_MIN_DISTANCE)

    cut_df["primaryTrackPassAllCuts"] = [
        reduce(np.logical_and, row, True) for row in cut_df.itertuples(index=False)
    ]

    cut_AuAu_df = pd.DataFrame()
    cut_AuAu_df["primaryTrackPassDcaCut"] = df["primaryTrackTdca"].map(lambda tdca: tdca < 3) # unit: cm
    cut_AuAu_df["primaryTrackPassNHitsCut"] = df["primaryTrackNHits"].map(lambda nhits: nhits > 15)
    cut_AuAu_df["primaryTrackPassPossibleHitRatioCut"] = (
        df["primaryTrackNHits"] / df["primaryTrackNHitsPoss"]
    ).map(lambda ratio: ratio >= 0.52)
    cut_AuAu_df["primaryTrackPassPtCut"] = df["primaryTrackPt"].map(
        lambda pt: np.logical_and(pt > 0.2, pt < 200.0) # unit: GeV
    )
    cut_df["primaryTrackPassAllAuAuCuts"] = [
        reduce(np.logical_and, row, True) for row in cut_AuAu_df.itertuples(index=False)
    ]

    if "primaryTrackNHitsdEdx" in df.columns:
        cut_df["primaryTrackPassNHitsdEdx15Cut"] = df["primaryTrackNHitsdEdx"].map(lambda fs: fs > 15)

    return cut_df


def populate_track_vertex_info(tracks_df):
    assert "primaryTrackVertexRank" not in tracks_df.columns  # Old format

    tracks_df["primaryTrackVertexRank"] = [
        ranks[vertex_ids]
        for vertex_ids, ranks in zip(
            tracks_df["primaryTrackVertexId"], tracks_df["primaryVertexRank"]
        )
    ]
    tracks_df["primaryTrackVertexX"] = [
        xs[vertex_ids]
        for vertex_ids, xs in zip(
            tracks_df["primaryTrackVertexId"], tracks_df["primaryVertexX"]
        )
    ]
    tracks_df["primaryTrackVertexY"] = [
        ys[vertex_ids]
        for vertex_ids, ys in zip(
            tracks_df["primaryTrackVertexId"], tracks_df["primaryVertexY"]
        )
    ]
    tracks_df["primaryTrackVertexZ"] = [
        zs[vertex_ids]
        for vertex_ids, zs in zip(
            tracks_df["primaryTrackVertexId"], tracks_df["primaryVertexZ"]
        )
    ]


def _get_highest_ranking_vertex_id(row):
    """
    >>> _get_highest_ranking_vertex_id(pd.Series([np.array([1e5, -1e5, -1e5])], index = ['primaryVertexRank']))
    0
    >>> _get_highest_ranking_vertex_id(pd.Series([np.array([-1e5, 1e5, -1e5])], index = ['primaryVertexRank']))
    1
    """
    vs = list(enumerate(row["primaryVertexRank"]))
    vertex_id, max_rank = max(vs, key=lambda v: v[1], default=(-1, None))  # sort by rank
    return vertex_id


def _get_highest_ranking_vertex_z(row):
    """
    >>> _get_highest_ranking_vertex_z(pd.Series(
    ...     [1, np.array([-33.0, 42.1, -3.3])],
    ...     index=['highestRankingVertexId', 'primaryVertexZ']
    ... ))
    42.1
    >>> _get_highest_ranking_vertex_z(pd.Series(
    ...     [-1, np.array([])],
    ...     index=['highestRankingVertexId', 'primaryVertexZ']
    ... ))
    nan
    """
    if row["highestRankingVertexId"] < 0:
        return np.NAN
    return row["primaryVertexZ"][row["highestRankingVertexId"]]


def fill_highest_ranking_vertex_info(tracks_df):
    tracks_df["highestRankingVertexId"] = tracks_df.apply(
        _get_highest_ranking_vertex_id, axis=1, result_type="reduce"
    )
    tracks_df["highestRankingVertexZ"] = tracks_df.apply(
        _get_highest_ranking_vertex_z, axis=1, result_type="reduce"
    )
    tracks_df["primaryTrackIsFromHighestRankingVertex"] = tracks_df.apply(
        lambda row: row['primaryTrackVertexId'] == row['highestRankingVertexId'],
        axis=1, result_type="reduce"
    )


def _get_number_of_tracks_for_vertex(row):
    """
    >>> list(_get_number_of_tracks_for_vertex(pd.Series(
    ...     map(np.array, [[np.nan] * 5, [1, 1, 2, 2, 3, 0, 0, 0, 0, 0]]),
    ...     index=['primaryVertexRank', 'primaryTrackVertexId']
    ...     )))
    [5, 2, 2, 1, 0]
    >>> list(_get_number_of_tracks_for_vertex(pd.Series(
    ...     map(np.array, [[], []]),
    ...     index=['primaryVertexRank', 'primaryTrackVertexId']
    ...     )))
    []
    """
    num_vertices = len(row["primaryVertexRank"])
    vertex_ids = np.arange(num_vertices).reshape((num_vertices, 1))
    return np.sum(vertex_ids == row["primaryTrackVertexId"], axis=1)


def _get_sum_track_pt_for_vertex(row):
    """
    >>> list(_get_sum_track_pt_for_vertex(pd.Series(
    ...     map(np.array, [[np.nan] * 5, [1, 1, 2, 2, 3, 0, 0, 0, 0, 0], [10, 10, 20, 20, 30, 0, 0, 0, 0, 0]]),
    ...     index=['primaryVertexRank', 'primaryTrackVertexId', 'primaryTrackPt']
    ...     )))
    [0, 20, 40, 30, 0]
    >>> list(_get_sum_track_pt_for_vertex(pd.Series(
    ...     map(np.array, [[], [], []]),
    ...     index=['primaryVertexRank', 'primaryTrackVertexId', 'primaryTrackPt']
    ...     )))
    []
    """
    num_vertices = len(row["primaryVertexRank"])
    vertex_ids = np.arange(num_vertices).reshape((num_vertices, 1))
    return np.sum((vertex_ids == row["primaryTrackVertexId"]) * row["primaryTrackPt"], axis=1)


def fill_track_information_for_vertices(tracks_df):
    tracks_df["primaryVertexNumTracks"] = tracks_df.apply(
        _get_number_of_tracks_for_vertex, axis=1, result_type="reduce"
    )
    tracks_df["primaryVertexSumTrackPt"] = tracks_df.apply(
        _get_sum_track_pt_for_vertex, axis=1, result_type="reduce"
    )


def _get_thrown_particle(key):
    """
    This is to be applied on the rs geant dataframe, where the single
    thrown particle still has id 1 (it turns into 501 when embedding
    is done)

    >>> _get_thrown_particle('g2t_track_eta')(pd.Series(
    ...     [
    ...       np.array([1, 2, 3]),
    ...       np.array([123.0, -999.0, -999.0]),
    ...     ],
    ...     index=['g2t_track_id', 'g2t_track_eta']
    ...     ))
    123.0
    >>> _get_thrown_particle('g2t_track_pt')(pd.Series(
    ...     [
    ...       np.array([1]),
    ...       np.array([321.0]),
    ...       np.array([123.0]),
    ...     ],
    ...     index=['g2t_track_id', 'g2t_track_eta', 'g2t_track_pt']
    ...     ))
    123.0
    """
    def f(row):
        single_track_thrown_cond = row['g2t_track_id'] == 1
        assert len(single_track_thrown_cond) >= 1
        assert single_track_thrown_cond[0]
        assert not single_track_thrown_cond[1:].any()
        return row[key][0]
    return f


def fill_thrown_particle(df):
    df['thrownPt'] = df.apply(_get_thrown_particle('g2t_track_pt'), axis=1, result_type="reduce")
    df['thrownEta'] = df.apply(_get_thrown_particle('g2t_track_eta'), axis=1, result_type="reduce")
    # not implemented for all datasets yet
    if 'g2t_track_phi' in df.columns:
        df['thrownPhi'] = df.apply(_get_thrown_particle('g2t_track_phi'), axis=1, result_type="reduce")


def find_jets(row, R=0.6, ptmin=3.0):
    assert (row["primaryTrackPt"] >= 0).all()
    good_tracks = (
        row["primaryTrackIsFromHighestRankingVertex"]
        & row["primaryTrackPassAllCuts"]
    )
    vectors = np.array(
        list(
            zip(
                row["primaryTrackPt"][good_tracks],
                row["primaryTrackEta"][good_tracks],
                row["primaryTrackPhi"][good_tracks],
                np.ones_like(row["primaryTrackPt"][good_tracks])
                * 0.140,  # pion mass, GeV
            )
        ),
        dtype=np.dtype([("pT", "f8"), ("eta", "f8"), ("phi", "f8"), ("mass", "f8")]),
    )
    sequence = cluster(vectors, algo="antikt", R=R)
    return sequence.inclusive_jets(ptmin=ptmin)


from .memoization import _loader_memoized


def make_pq_loader(load_events=False, load_geant=False, load_helix=False, load_dedx=False, load_thrown_particle=False, load_globals=False):
    def load_pq(path):
        global _loader_memoized
        key = (path, load_events, load_geant, load_thrown_particle, load_globals)
        if key in _loader_memoized:
            print(f"Not loading {path}")
            return _loader_memoized[key]
        import os.path

        pqs = PqSet()
        tracks_filepath = os.path.join(path, "track.parquet")
        columns = [field.name for field in pq.read_schema(tracks_filepath)]
        if not load_globals:
            columns = filter(lambda col_name: col_name.find("globalTrack") == -1, columns)
        if not load_helix:
            columns = filter(lambda col_name: col_name.find("TrackHelix") == -1, columns)
            # Exception for primaryTrackOuterHelixOrigin[XY] that is needed
            columns = filter(lambda col_name: col_name.find("TrackOuterHelix") == -1
                             or col_name.startswith("primaryTrackOuterHelixOrigin"), columns)
        if not load_dedx:
            columns = filter(lambda col_name: col_name.find("TrackdEdx") == -1, columns)
            columns = filter(lambda col_name: col_name.find("TracknSigma") == -1, columns)
        read_table_kwargs = dict(
            columns=columns,
        )
        pqs.tracks = pq.read_table(tracks_filepath, **read_table_kwargs).to_pandas()
        pqs.tracks["numPrimaryVertices"] = pqs.tracks["primaryVertexRank"].map(len)
        pqs.tracks["numPrimaryTracks"] = pqs.tracks["primaryTrackPt"].map(len)
        populate_track_vertex_info(pqs.tracks)
        fill_highest_ranking_vertex_info(pqs.tracks)
        fill_track_information_for_vertices(pqs.tracks)
        pqs.tracks.index = pqs.tracks.apply(
            lambda row: (row["eventRunNumber"], row["eventNumber"]), axis=1
        )

        if load_events:
            pqs.events = pq.read_table(os.path.join(path, "event.parquet")).to_pandas()
            pqs.events.index = pqs.events.apply(
                lambda row: (row["eventRunNumber"], row["eventNumber"]), axis=1
            )
            assert len(pqs.tracks) == len(pqs.events)
            assert (pqs.tracks.index == pqs.events.index).all()
        else:
            pqs.events = None

        pqs.geant = None
        if load_geant:
            filename_geant = os.path.join(path, "geant.parquet")
            if os.path.isfile(filename_geant):
                pqs.geant = pq.read_table(filename_geant).to_pandas()
                assert len(pqs.tracks) == len(pqs.geant)
                assert (
                    pqs.tracks["eventRunNumber"].values
                    == pqs.geant["eventRunNumber"].values
                ).all()
                # event numbers will not match

        pqs.rs_geant = None
        if load_geant or load_thrown_particle:
            filename_rs_geant = os.path.join(path, "rs.geant.parquet")
            if os.path.isfile(filename_rs_geant):
                read_table_kwargs = dict()
                if not load_geant:
                    # read column list to only include the thrown particle
                    tracks_schema = pq.read_schema(filename_rs_geant)
                    read_table_kwargs = dict(
                        columns=['eventRunNumber', 'eventNumber', 'g2t_track_id', 'g2t_track_pt', 'g2t_track_eta'],
                    )
                pqs.rs_geant = pq.read_table(filename_rs_geant, **read_table_kwargs).to_pandas()
                assert len(pqs.tracks) == len(pqs.rs_geant)
                assert (
                    pqs.tracks["eventRunNumber"].values
                    == pqs.rs_geant["eventRunNumber"].values
                ).all()
                # event numbers will not match

                fill_thrown_particle(pqs.rs_geant)

        pqs.cuts = apply_cuts(pqs.tracks)
        pqs.jets = (
            pd.concat([pqs.tracks, pqs.cuts], axis=1, sort=False)
            .apply(find_jets, axis=1, result_type="reduce")
            .rename("jet")
        )
        _loader_memoized[key] = pqs
        return pqs

    return load_pq


def concatenate(pqs_list):
    result = PqSet()
    result.jets = pd.concat([pqs.jets for pqs in pqs_list], axis=0)
    result.tracks = pd.concat([pqs.tracks for pqs in pqs_list], axis=0)
    if all([pqs.events is not None for pqs in pqs_list]):
        result.events = pd.concat([pqs.events for pqs in pqs_list], axis=0)
    else:
        result.events = None
    if all(pqs.geant is not None for pqs in pqs_list):
        result.geant = pd.concat([pqs.geant for pqs in pqs_list], axis=0)
    else:
        result.geant = None
    if all(pqs.rs_geant is not None for pqs in pqs_list):
        result.rs_geant = pd.concat([pqs.rs_geant for pqs in pqs_list], axis=0)
    else:
        result.rs_geant = None
    result.cuts = pd.concat([pqs.cuts for pqs in pqs_list], axis=0)
    return result


def eq_values(val1, val2):
    """
    Compare two values, return True if and only if they are equal

    >>> eq_values(int(3), int(3))
    True
    >>> eq_values(np.array([1, 2, 3]), np.array([4]))
    False
    >>> eq_values(np.array([1, 2, 3]), np.array([1, 2, 3]))
    True
    >>> eq_values(np.array([1, 2, 3]), np.array([1, 2, 3, 4]))
    False
    >>> eq_values(np.array([1, 2, 3]), np.array([3, 2, 1]))
    False
    """
    if isinstance(val1, np.ndarray):
        if len(val1) != len(val2):
            return False
        return np.allclose(val1, val2)
    else:
        return val1 == val2


def eq_series(s1, s2):
    """
    Compare two pandas Series, return True if and only if they are equal

    >>> eq_series(pd.Series([1, 2, 3]), pd.Series([1, 2, 3]))
    True
    >>> eq_series(pd.Series([1, 2, 3]), pd.Series([1, 2, 3, 4]))
    False
    >>> eq_series(pd.Series([1, 2, 3]), pd.Series([3, 2, 1]))
    False
    """
    assert isinstance(s1, pd.Series)
    assert isinstance(s2, pd.Series)
    return (
        pd.concat([s1, s2], axis=1)
        .apply(lambda row: eq_values(*row.values), axis=1)
        .all()
    )


def diff_df(df1, df2):
    """
    Compare two dataframes, return names of the columns that differ

    >>> diff_df(pd.DataFrame(), pd.DataFrame())
    []
    >>> diff_df(
    ...     pd.DataFrame.from_dict({ 'a' : [1, 2, 3] }),
    ...     pd.DataFrame.from_dict({ 'a' : [1, 2, 3] }),
    ...     )
    []
    >>> diff_df(
    ...     pd.DataFrame.from_dict({ 'a' : [1, 2, 3] }),
    ...     pd.DataFrame.from_dict({ 'a' : [4] }),
    ...     )
    ['a']
    >>> diff_df(
    ...     pd.DataFrame.from_dict({ 'a' : [1, 2, 3] }),
    ...     pd.DataFrame.from_dict({ 'b' : [1, 2, 3] }),
    ...     )
    ['a', 'b']
    """
    assert isinstance(df1, pd.DataFrame)
    assert isinstance(df2, pd.DataFrame)
    res = []
    if list(df1.columns) != list(df2.columns):
        res += set(df1.columns).symmetric_difference(set(df2.columns))
    for col_name in set(df1.columns).intersection(set(df2.columns)):
        if not eq_series(df1[col_name], df2[col_name]):
            res.append(col_name)
    return res


def diff_pq(pq1, pq2):
    assert isinstance(pq1, PqSet)
    assert isinstance(pq2, PqSet)
    res = {}
    if (pq1.events is None) != (pq2.events is None):
        res["events"] = []
    if (pq1.events is not None) and (pq2.events is not None):
        d1 = diff_df(pq1.events, pq2.events)
        if d1:
            res["events"] = d1
    if not eq_series(pq1.jets, pq2.jets):
        res["jets"] = ["jets"]
    d2 = diff_df(pq1.tracks, pq2.tracks)
    if d2:
        res["tracks"] = d2
    return res
