{ nixpkgs ? import <nixpkgs> {} }:

let
  inherit (import ../../default.nix { inherit nixpkgs; nobatch = true; }) bits;
  bits' = bits.override (final: prev: {
    etabin_cut_1 = "0.4";
    etabin_cut_2 = "0.8";
  });
in
rec {
  star-jet-ref = nixpkgs.fetchFromSourcehut {
    owner = "~veprbl";
    repo = "star-jet-ref";
    rev = "v19";
    sha256 = "1xbfjkj4w252270pjdzdrxribm1igx2xsadbd6gwxzx8w1k2wpmj";
  };

  ref-data = star-jet-ref;

  result_jp0 = bits'.unfold' {
    jp = 0;
    bins = [ 6.7 8.9 11.9 15.8 21.1 28.1 37.5 ];
    use_eta_bins = true;
  }
    (ref-data + "/485759cc76843179a9c8fea5a9fe92bf6c13268a/lk8s4glp2qn1x3192vv1hlcq17i668hp-embed_yoda")
    (ref-data + "/485759cc76843179a9c8fea5a9fe92bf6c13268a/6c4izz1pwnd7bgi3x23k6qnmvpj4skg5-merge");

  result_jp0_ref = ref-data + "/485759cc76843179a9c8fea5a9fe92bf6c13268a/output_jp0.yoda.gz";

  diff_jp0 = nixpkgs.runCommandNoCC "jp0.diff" {} ''
    mkdir a
    mkdir b
    gunzip -c ${result_jp0_ref} > a/output_jp0.yoda
    gunzip -c ${result_jp0}/output.yoda.gz > b/output_jp0.yoda
    ( diff -u {a,b}/output_jp0.yoda || true ) >> $out
  '';

  test_jp0 = nixpkgs.runCommandNoCC "jp0-test" {} ''
    if ! diff -u <(zcat ${result_jp0_ref}) <(zcat ${result_jp0}/output.yoda.gz); then
      echo "Output mismatch. See the diff at '${diff_jp0}'."
      exit 1
    fi
    touch "$out"
  '';
}
