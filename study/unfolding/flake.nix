{
  description = "Unfolding analysis environment flake";

  outputs = { self, nixpkgs }:
    let

      inherit (nixpkgs) lib;
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" ];

    in
    {
      devShells = lib.genAttrs supportedSystems (system:
        with import nixpkgs
          {
            inherit system;
          };
        {
          default = mkShell {
            buildInputs = [
              python3
              python3Packages.matplotlib
              python3Packages.notebook
              python3Packages.numpy
              python3Packages.scipy
              python3Packages.yoda
            ];
          };
        }
      );
    };
}
