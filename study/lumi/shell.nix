{ pkgs ? import ./nixpkgs {} }:

with pkgs;

mkShell {
  buildInputs = [
    python3
    python3.pkgs.matplotlib
    python3.pkgs.notebook
    python3.pkgs.numpy
    python3.pkgs.scipy
    python3.pkgs.yoda
    python3.pkgs.sdds
    (texlive.combine { inherit (texlive) scheme-small type1cm; })
  ];
}
