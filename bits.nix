{ nixpkgs /*? import <nixpkgs> {}*/ }:

with nixpkgs;
with import ./common.nix { inherit (nixpkgs) lib; };

let
  split = n: xs: if builtins.length xs <= n then
      [xs]
    else
      [(lib.take n xs)] ++ (split n (lib.drop n xs));
  enumerate = lst: builtins.genList
    (i: { fst = i; snd = builtins.elemAt lst i; })
    (builtins.length lst);
  find = val: lst: (lib.findFirst (p: p.snd == val) { fst = -1; } (enumerate lst)).fst;

  this = final: with final; {
    nobatch = false;
    submit_as_job = if nobatch then x: x else wrap_condor;
    LOCK_DIRECTORY = "."; # don't enforce locks by default

    to_output = file: runCommandNoCC (builtins.baseNameOf file) { inherit file; } ''
      install -Dm444 $file $out/output.yoda.gz
    '';
    remove_dot = builtins.replaceStrings [ "." ] [ "" ];

    # A newer version of yodamerge version is needed as it doesn't load
    # all input files into the memory at the same time. This helps to do
    # merging of the new embedding histograms within a reasonable memory
    # constraint.
    yoda_for_yodamerge =
      if lib.versionOlder pkgs.yoda.version "1.9.1" then
        pkgs.yoda.overrideAttrs (_: rec {
          name = "yoda-${version}";
          version = "1.9.1";
          src = fetchurl {
            url = "https://yoda.hepforge.org/downloads?f=YODA-1.9.1.tar.bz2";
            sha256 = "0k0xfm9shl671814136yx38nl653l4w5ja5xr0d5pgk5cmda05n6";
          };
          patches = [
            (fetchpatch {
              url = "https://gitlab.com/hepcedar/yoda/-/commit/fc47b2add37f57455a1b8739e06a13ed0aa70b10.diff";
              sha256 = "1dlqvaai3f26vfcpv3xjajlxzcji2jkiwrf31pgwz11g7gzjmfcr";
            })
            # fix for use in yoda_reset_bins
            (fetchpatch {
              url = "https://gitlab.com/hepcedar/yoda/-/commit/ba1275033522c66bc473dfeffae1a7971e985611.diff";
              sha256 = "sha256-/8UJuypiQzywarE+o3BEMtqM+f+YzkHylugi+xTJf+w=";
              excludes = [ "ChangeLog" ];
            })
          ];
          # Workaround for https://gitlab.com/hepcedar/yoda/-/merge_requests/49
          preInstallCheck = ''
            cp tests/test{1,}.yoda
            gzip -c tests/test.yoda > tests/test.yoda.gz
          '';
        })
      else
        pkgs.yoda;

    yoda_for_yodascale =
      if lib.versionOlder pkgs.yoda.version "1.9.9" then
        pkgs.yoda.overrideAttrs (_: {
          patches = [
            (fetchpatch {
              url = "https://gitlab.com/hepcedar/yoda/-/commit/35559c05ec966fe3d5f6c5438c2247e7e1f37dd3.diff";
              sha256 = "1hgrdh6p4d2hzf1a7nkn5k311qdj2yzbpgv49fsva10l0ldh9jg2";
            })
          ];
        })
      else
        pkgs.yoda;

    possiblePhases = [ "buildPhase" "installPhase" ];
    mkDerivation = attrs: stdenv.mkDerivation ((
      lib.optionalAttrs (!(attrs.dontSetPhases or false)) {
        # Determine phases based on attributes provided. This disables all implicit phases from stdenv.
        phases = builtins.filter (lib.flip builtins.hasAttr attrs) possiblePhases;
      } // attrs) // {
      preferLocalBuild = true;
      allowSubstitutes = false;
    });
    HOME = builtins.getEnv "HOME";
    inherited_titles = drv: { inherit (drv) title; } // lib.optionalAttrs (drv ? short_title) { inherit (drv) short_title; };
    set_short_title = short_title: drv: (drv // { inherit short_title; });
    set_title = title: drv: (drv // { inherit title; });
    set_title' = title: short_title: drv: set_title title (set_short_title short_title drv);
    add_title_suffix = suffix: drv: set_title (drv.title + suffix) drv;
    pt_bins = [
      "pt2_3"
      "pt3_4"
      "pt4_5"
      "pt5_7"
      "pt7_9"
      "pt9_11"
      "pt11_15"
      "pt15_20"
      "pt20_25"
      "pt25_35"
      "pt35_45"
      "pt45_55"
      "pt55_-1"
      ];
    make_jet_plots = "${make_jet_plots_build}/bin/make_jet_plots";
    make_jet_plots_build = stdenv.mkDerivation rec {
      name = "make_jet_plots_build";
      src = lib.sourceByRegex ./. [
        "^wscript$"
        "^StJetPlots(/.+)?$"
        "^StSpinPool(/.+)?$"
        "^src(/common(/.+)?)?$"
        ];
      buildInputs = [ lhapdf root yoda python3 wafHook ];
      wafConfigureFlags = [
        "--with-pdf-reweighting"
        "--without-jet2parquet"
      ];
    };
    jet_r = "0.5";
    jet_r_branch = {
      "0.5" = "R050";
      "0.6" = "R060";
      "0.7" = "R070";
    }.${jet_r};
    process_overrides = {
      detector_jet_branch_name = "AntiKt${jet_r_branch}NHits12";
      detector_ue_branch_name = "AntiKt${jet_r_branch}NHits12OffAxisConesR050";
      inherit etabin_cut_1 etabin_cut_2;
    };
    process_data_overrides = {};
    process_embed_overrides = {
      particle_jet_branch_name = "AntiKt${jet_r_branch}Particle";
      particle_ue_branch_name = "AntiKt${jet_r_branch}ParticleOffAxisConesR050";
    };
    process_do_pdf_reweight = false;
    reference_pdf = "cteq6l1";
    target_pdf = null;
    process = extra_overrides: name: inputs: mkDerivation ({
      inherit name;
      passthru = {
        title = builtins.replaceStrings ["_"] ["\\_"] name;
      };
      nativeBuildInputs = [ gzip ]
      ++ lib.optionals process_do_pdf_reweight (assert target_pdf != null; [
        lhapdf.pdf_sets.${reference_pdf}
        lhapdf.pdf_sets.${target_pdf}
      ]);
      buildPhase = ''
        set -o noglob
        inputs=( ${join inputs} )
        # see "Parameter Expansion" in "man bash"
        inputs_gpfs=( ''${inputs[@]%%:*} )
        inputs_gpfs=( ''${inputs_gpfs[@]} ''${inputs_gpfs[@]/.jets.root/.skim.root} ''${inputs_gpfs[@]/.jets.root/.ueCones.root} )
        inputs_args=( ''${inputs[@]##*/} )
        set +o noglob
        cp -v ''${inputs_gpfs[@]} .
        set -o noglob
        ${time}/bin/time -f "%C\n%Uuser %Ssystem %eelapsed %PCPU (%Xtext+%Ddata %Mmax)\n%Iinputs+%Ooutputs (%Fmajor+%Rminor)pagefaults %Wswaps\ncontext switches: %cinvoluntarily %wwaits" -o time ${make_jet_plots} ${lib.optionalString process_do_pdf_reweight "--pdf-reweight ${reference_pdf} ${target_pdf}"} ''${inputs_args[@]}
        set +o noglob
        cat time
        gzip output.yoda
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        mkdir "$out_data" || true
        CA_HASH="$(sha256sum output.yoda.gz | cut -d " " -f 1)"
        DEST="$out_data/$CA_HASH"
        ( time; env; /usr/bin/hostname ) >> "$DEST".info
        cp output.yoda.gz "$DEST"
        mkdir "$out" || true
        if [ -L "$out"/output.yoda.gz ]; then
          if [ "$(readlink "$out"/output.yoda.gz)" != "$DEST" ]; then
            echo Irreproducible build detected!
            exit 1
          fi
        else
          ln -s "$DEST" "$out"/output.yoda.gz
        fi
        cp time "$out"/time
      '';
      __slurm_job_timelimit = "0-2";
      __htcondor_request_memory = "2G";
    } // process_overrides // extra_overrides);
    batch_process = extra_overrides: name: inputs: submit_as_job (process extra_overrides name inputs);
    merge = outputs: submit_as_job (mkDerivation {
      name = "merge";
      yodagz_files = map (output: "${output}/output.yoda.gz") outputs;
      nativeBuildInputs = [ gzip utillinux yoda_for_yodamerge ];
      buildPhase = ''
        yodastack -o output.yoda ''${yodagz_files[@]}
        gzip output.yoda
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        mkdir "$out_data"
        cp output.yoda.gz "$out_data"/output.yoda.gz
        mkdir "$out"
        ln -s "$out_data"/output.yoda.gz "$out"/output.yoda.gz
      '';
      passthru = inherited_titles (builtins.head outputs);
      __htcondor_request_memory = "4G";
    });
    scale = scale_spec: input: let
      scale_specs =
        if builtins.isList scale_spec then
          scale_spec
        else
          [ scale_spec ];
      yodascale_args = join (map (s: "-c '${s}'") scale_specs);
    in mkDerivation {
      name = "${input.name}-scaled";
      nativeBuildInputs = [ gzip utillinux yoda_for_yodascale ];
      buildPhase = ''
        cp ${input}/output.yoda.gz .
        gunzip output.yoda.gz
        flock -x ${LOCK_DIRECTORY}/scale.lock \
        yodascale ${yodascale_args} output.yoda
        gzip output-scaled.yoda
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        mkdir "$out_data"
        cp output-scaled.yoda.gz "$out_data"/output.yoda.gz
        mkdir "$out"
        ln -s "$out_data"/output.yoda.gz "$out"/output.yoda.gz
      '';
      passthru = inherited_titles input;
    };
    yoda_combine_uncertainties = inputs: mkDerivation {
      name = "${(builtins.elemAt inputs 0).name}-combine-uncertainties";
      nativeBuildInputs = [
        utillinux
        python3
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/yoda-combine-uncertainties.py} ./yoda-combine-uncertainties.py
        flock -x ${LOCK_DIRECTORY}/yoda_combine_uncertainties.lock \
        ./yoda-combine-uncertainties.py -o output.yoda \
          ${join (map (input: ''"${input}/output.yoda.gz"'') inputs)}
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    yoda_multiply = mult1: mult2: mkDerivation {
      name = "${mult1.name}-times-${mult2.name}";
      nativeBuildInputs = [
        utillinux
        python3
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/yoda-multiply.py} ./yoda-multiply.py
        flock -x ${LOCK_DIRECTORY}/yoda_multiply.lock \
        ./yoda-multiply.py ${mult1}/output.yoda.gz ${mult2}/output.yoda.gz output.yoda
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    yoda_ratio = numerator: denominator: mkDerivation {
      name = "${numerator.name}-${denominator.name}";
      nativeBuildInputs = [
        utillinux
        python3
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/yoda-ratio.py} ./yoda-ratio.py
        flock -x ${LOCK_DIRECTORY}/yoda_ratio.lock \
        ./yoda-ratio.py ${numerator}/output.yoda.gz ${denominator}/output.yoda.gz output.yoda
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    yoda_rebin = input: regex: bin_edges: mkDerivation {
      name = "${input.name}-rebin";
      nativeBuildInputs = [
        utillinux
        python3
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/yoda-rebin.py} ./yoda-rebin.py
        flock -x ${LOCK_DIRECTORY}/yoda_rebin.lock \
        ./yoda-rebin.py ${input}/output.yoda.gz output.yoda \
          --spec '${regex}' '${join (map toString bin_edges)}'
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      '';
      passthru = inherited_titles input;
    };
    yoda_reset_bins = input: specs: mkDerivation {
      name = "${input.name}-rebin";
      nativeBuildInputs = [
        utillinux
        python3
        yoda_for_yodamerge # includes fix for Histo1D.addBins
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/yoda-reset-bins.py} ./yoda-reset-bins.py
        flock -x ${LOCK_DIRECTORY}/yoda_reset_bins.lock \
        ./yoda-reset-bins.py ${input}/output.yoda.gz output.yoda \
          ${join (map (spec: "--spec '${spec.regex}' '${spec.cond}'") specs)}
        gzip output.yoda
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        mkdir "$out_data"
        cp output.yoda.gz "$out_data"/output.yoda.gz
        mkdir "$out"
        ln -s "$out_data"/output.yoda.gz "$out"/output.yoda.gz
      '';
      passthru = inherited_titles input;
    };
    yoda_envelope = central: others: mkDerivation {
      name = "${central.name}-envelope";
      nativeBuildInputs = [
        utillinux
        python3
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/yoda-envelope.py} ./yoda-envelope.py
        flock -x ${LOCK_DIRECTORY}/yoda_envelope.lock \
        ./yoda-envelope.py -o output.yoda -c "${central}/output.yoda.gz" ${join (map (input: ''"${input}/output.yoda.gz"'') others)}
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    yoda_variance' = central: inputs: mkDerivation {
      name = "${(builtins.elemAt inputs 0).name}-variance";
      nativeBuildInputs = [
        utillinux
        python3
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/yoda-variance.py} ./yoda-variance.py
        flock -x ${LOCK_DIRECTORY}/yoda_variance.lock \
        ./yoda-variance.py -o output.yoda \
          ${lib.optionalString (central != null) "--central ${central}/output.yoda.gz"} \
          ${join (map (input: ''"${input}/output.yoda.gz"'') inputs)}
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    yoda_variance = yoda_variance' null;
    combine_results = inputs: mkDerivation {
      name = "combined-${toString (builtins.length inputs)}";
      src = ./misc/combine-results.py;
      nativeBuildInputs = [
        utillinux
        python3
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp "$src" ./combine-results.py
        flock -x ${LOCK_DIRECTORY}/combine_results.lock \
        ./combine-results.py \
          ${join (map (input: ''--input "${input.input}/output.yoda.gz" "${input.reference_uncertainty or input.input}/output.yoda.gz"'') inputs)} \
          output.yoda
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    stack = inputs: let
      inputspecs = map
        (i: ''${i.output}/output.yoda.gz:${toString i.xmin}${if i ? xmax then ":${toString i.xmax}" else ""}'')
        inputs;
    in mkDerivation {
      name = "stack";
      nativeBuildInputs = [
        python3
        python3Packages.tqdm
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/yodastack.py} ./yodastack.py
        ./yodastack.py ${join inputspecs} -o output.yoda \
          -m '^/jet_quantities.*' \
          -m '^/triggers.*' \
          -M 'tower_id' \
          -M 'vz_raw'
        gzip output.yoda
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        mkdir "$out_data"
        cp output.yoda.gz "$out_data"/output.yoda.gz
        mkdir "$out"
        ln -s "$out_data"/output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    mkhtml_config_stack = mkDerivation {
      name = "JET_PLOTS_stack.plot";
      orig_conf = ./JET_PLOTS.plot;
      script = builtins.toFile "to_stack.sed" ''
        s#^YLabel=#ZLabel=#
        s#^XLabel=#YLabel=#
        s#^YMin=#ZMin=#
        s#^XMin=#YMin=#
        s#^LogY=#LogZ=#
        s#^LogX=#LogY=#

        # Broken for 2D histograms
        s/Rebin=/#Rebin=/
        s/NormalizeToIntegral=1/#NormalizeToIntegral=1/
      '';
      buildPhase = ''
        cat > output.plot <<EOF
# BEGIN PLOT .*
XLabel=run index
# END PLOT
# BEGIN PLOT .*/(jet_pt.*|jet_charged_pt|jet_neutral_pt|jet_tower_mult|jet_tower_z|jet_track_mult|jet_track_z)
LogZ=1
ZMin=1e-5
# END PLOT
EOF
        sed -f $script "$orig_conf" >> output.plot
      '';
      installPhase = ''
        cp output.plot "$out"
      '';
    };
    embed_disable_ff = false;
    embed_scale = pt_bin: input: mkDerivation {
      name = "${input.name}-embed-scaled";
      nativeBuildInputs = [ gzip utillinux yoda_for_yodascale ];
      buildPhase = ''
        cp ${input}/output.yoda.gz .
        gunzip output.yoda.gz
        flock -x ${LOCK_DIRECTORY}/embed_scale.lock \
        yodascale -c ".* $(cat ${embed_factor pt_bin input})x" output.yoda
        gzip output-scaled.yoda
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        mkdir "$out_data"
        cp output-scaled.yoda.gz "$out_data"/output.yoda.gz
        mkdir "$out"
        ln -s "$out_data"/output.yoda.gz "$out"/output.yoda.gz
      '';
      passthru = inherited_titles input;
    };
    embed_factor_script = ./misc/embed_factor.py;
    embed_factor_prefix = "pp200_";
    embed_factor = pt_bin: input: mkDerivation {
      name = "${input.name}-embed-factor-${pt_bin}";
      nativeBuildInputs = [
        python3
        python3Packages.yoda
        utillinux
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${embed_factor_script} ./embed_factor.py
        flock -x ${LOCK_DIRECTORY}/embed_factor.lock \
        ./embed_factor.py \
          ${lib.optionalString embed_disable_ff "--disable-ff"} \
          ${embed_factor_prefix}${pt_bin} ${input}/output.yoda.gz > factor
      '';
      installPhase = ''
        cp factor "$out"
      '';
    };
    embed_merge = with lib; inputs: let
      input_list = join (mapAttrsToList (pt_bin: input: "${input}/output.yoda.gz:$(cat ${embed_factor pt_bin input})") inputs);
    in
    mkDerivation {
      name = "embed-merge";
      nativeBuildInputs = [ gzip utillinux yoda_for_yodamerge ];
      buildPhase = ''
        flock -x ${LOCK_DIRECTORY}/embed_merge.lock \
        yodastack -o output.yoda ${input_list}
        gzip output.yoda
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        mkdir "$out_data"
        cp output.yoda.gz "$out_data"/output.yoda.gz
        mkdir "$out"
        ln -s "$out_data"/output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    rivet-mkhtml = stdenv.mkDerivation {
      pname = "rivet-mkhtml";
      inherit (rivet) latex src version;
      patches = rivet.patches ++ lib.optionals (lib.versionOlder rivet.version "3.1.7") [
        (fetchpatch {
          url = "https://gitlab.com/hepcedar/rivet/-/commit/78e137505ba3ceb51aa0de5a97943850b130ab38.diff";
          sha256 = "1gqvssvjqdsgvyzggl6x1d7bpy1lgym97wk26vb0ay97bw1zxsnr";
        })
        (fetchpatch {
          url = "https://gitlab.com/hepcedar/rivet/-/commit/021c387e1c79b4e17ea449e9b8a3b63763f379ca.diff";
          excludes = [ "ChangeLog" ];
          sha256 = "0cn2cdihrjpz3p50b7lrj5pd4g26h6qw91affhr0kbg7f6l597jp";
        })
      ];
      buildInputs = [
        (python3.withPackages (ps: with ps; [ rivet yoda ]))
      ];
      postPatch = ''
        substituteInPlace bin/make-plots \
          --replace '"which"' '"${which}/bin/which"' \
          --replace '"latex"' '"'$latex'/bin/latex"' \
          --replace '"dvips"' '"'$latex'/bin/dvips"' \
          --replace '"gs"' '"${ghostscript}/bin/gs"' \
          --replace '"ps2pdf"' '"${ghostscript}/bin/ps2pdf"' \
          --replace '"ps2eps"' '"${ghostscript}/bin/ps2eps"' \
          --replace '"kpsewhich"' '"'$latex'/bin/kpsewhich"' \
          --replace '"convert"' '"${graphicsmagick-imagemagick-compat.out}/bin/convert"' \
          --replace "title += ' [%s]' % opt" "title += ' [%s]' % opt.replace('_', r'\\_')"
        substituteInPlace bin/rivet-cmphistos \
          --replace ".append('Title=%s'" "#"
        substituteInPlace bin/rivet-mkhtml \
          --replace '"make-plots"' \"$out/bin/make-plots\" \
          --replace '"rivet-cmphistos"' \"$out/bin/rivet-cmphistos\"
      '';
      dontConfigure = true;
      dontBuild = true;
      installPhase = ''
        runHook preInstall

        mkdir -p "$out"/bin
        mv bin/{make-plots,rivet-cmphistos,rivet-mkhtml} "$out"/bin

        runHook postInstall
      '';
    };
    etabin_cut_1_nodot = remove_dot etabin_cut_1;
    etabin_cut_2_nodot = remove_dot etabin_cut_2;
    extra_custom_legend = ''\\\raisebox{-0.66\baselineskip}{$5\%$ luminosity uncertainty not shown}'';
    ptjet_ue_corr_label = "";
    sigma_label = "\\sigma";
    sigma_unit = "pb"; # picobarn
    mkhtml_config = substituteAll {
      src = ./JET_PLOTS.plot;

      inherit
        jet_r
        etabin_cut_1
        etabin_cut_1_nodot
        etabin_cut_2
        etabin_cut_2_nodot
        extra_custom_legend
        ptjet_ue_corr_label
        sigma_label
        sigma_unit
        ;
    };
    rivet_mkhtml_font = "helvetica";
    mkhtml = yoda_files: extra_args: let
      title_option = d:
        lib.optionalString ((d.title or null) != null)
          ":'${builtins.replaceStrings [ "'" ] [ "'\\''" ] d.title}'";
    in mkDerivation {
      name = "plots";
      nativeBuildInputs = [ findutils rivet-mkhtml utillinux ];
      buildPhase = ''
        export HOME=$(mktemp -d)
        flock -x ${LOCK_DIRECTORY}/mkhtml.lock \
        rivet-mkhtml --font=${rivet_mkhtml_font} --num-threads 4 --config ${mkhtml_config} -o ./out \
          ${extra_args} \
          ${join (map (d: "${d}/output.yoda.gz${title_option d}${d.plot_options or ""}") yoda_files)} \
          -M ".*/events_.*" \
          -M ".*/jets_.*" \
          -M ".*/vertex_[xy]" \
          -M ".*/jet_[be]emc_tower_id" \
          -M ".*_fine$" \
          -M "^/pt.*"
        find ./out -type f -name "*.dat" -exec gzip {} \;
      '';
      installPhase = ''
        mv ./out "$out"
      '';
      passthru = {
        inherit yoda_files extra_args;
      };
    };
    plot_options_to_string =
      let
        escape = builtins.replaceStrings [ "'" ] [ "'\\''" ];
        to_string = name: value: ":${escape name}=${escape (toString value)}";
      in
        options: "'${builtins.concatStringsSep "" (lib.mapAttrsToList to_string options)}'";
    mkhtml' =
      extra_args: global_plot_options: yoda_files:
      let
        yoda_files' = map
          (input:
            input
            // lib.optionalAttrs (builtins.isAttrs input.plot_options or null) {
              plot_options = plot_options_to_string input.plot_options;
            }) yoda_files;
        reference_short_title = (builtins.elemAt yoda_files 0).short_title or null;
        global_plot_options' =
          {
            RatioPlotDrawReferenceFirst = 0;
          }
          // lib.optionalAttrs (reference_short_title != null) {
            RatioPlotYLabel = "Ratio to ${reference_short_title}";
          }
          // global_plot_options;
        extra_args' =
          extra_args
          + (lib.optionalString
              (global_plot_options' != {})
              " PLOT${plot_options_to_string global_plot_options'}");
      in
        mkhtml yoda_files' extra_args' // { inherit extra_args global_plot_options yoda_files; };
    rescale_ref = input: ref: mkDerivation {
      name = "${input.name}-rescaled";
      nativeBuildInputs = [ gzip utillinux yoda_for_yodascale ];
      buildPhase = ''
        cp ${input}/output.yoda.gz .
        cp ${ref}/output.yoda.gz ref.yoda.gz
        gunzip output.yoda.gz
        gunzip ref.yoda.gz
        flock -x ${LOCK_DIRECTORY}/rescale_ref.lock \
        yodascale -v -c 'vs_vertex_z$ 1x' -c '^/.*/jet_.* REF' -c '^/.*/vertex_.* REF' -c '.* 1x' output.yoda -r ref.yoda
        gzip output-scaled.yoda
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        mkdir "$out_data"
        cp output-scaled.yoda.gz "$out_data"/output.yoda.gz
        mkdir "$out"
        ln -s "$out_data"/output.yoda.gz "$out"/output.yoda.gz
      '';
      passthru = inherited_titles input;
    };
    post_process = input: mkDerivation {
      name = "${input.name}-post-process";
      nativeBuildInputs = [
        python3
        python3Packages.yoda
        utillinux
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/post-processing.py} ./post-processing.py
        flock -x ${LOCK_DIRECTORY}/post_process.lock \
        ./post-processing.py ${input}/output.yoda.gz
        gzip output.yoda
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        mkdir "$out_data"
        cp output.yoda.gz "$out_data"/output.yoda.gz
        mkdir "$out"
        ln -s "$out_data"/output.yoda.gz "$out"/output.yoda.gz
      '';
      passthru = inherited_titles input;
    };
    rename = input: what: _with: mkDerivation {
      name = "${input.name}-rename";
      nativeBuildInputs = [
        python3
        python3Packages.yoda
        utillinux
        ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/rename.py} ./rename.py
        flock -x ${LOCK_DIRECTORY}/rename.lock \
        ./rename.py '${input}/output.yoda.gz' '${what}' '${_with}'
        gzip output.yoda
      '';
      installPhase = ''
        out_data="${OUTPUT_DIRECTORY}/$(basename $out)"
        mkdir "$out_data"
        cp output.yoda.gz "$out_data"/output.yoda.gz
        mkdir "$out"
        ln -s "$out_data"/output.yoda.gz "$out"/output.yoda.gz
      '';
      passthru = inherited_titles input;
    };
    reweight2d_ratio = numerator: denominator: mkDerivation {
      name = "reweight2d_ratio";
      nativeBuildInputs = [
        utillinux
        python3
        python3Packages.numpy
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp ${./misc/reweight2d_ratio.py} ./reweight2d_ratio.py
        flock -x ${LOCK_DIRECTORY}/reweight2d_ratio.lock \
        ./reweight2d_ratio.py \
          --numerator "${numerator}/output.yoda.gz" \
          --denominator "${denominator}/output.yoda.gz" \
          --output output.yoda
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    unfold_notebook = study/unfolding/unfolding.org;
    unfold' = { jp ? 0, bins ? [ 5.0 6.7 8.9 11.9 15.8 21.1 28.1 37.5 50.0 ], use_eta_bins ? false, use_response_filtering ? false }: response: measurement: submit_as_job (mkDerivation {
      name = "unfold";
      nativeBuildInputs = [
        python3Packages.numpy
        python3Packages.matplotlib
        python3Packages.scipy
        python3Packages.yoda
      ];
      JP=jp;
      BINS=join (map toString bins);
      PT_BINS=pt_bins;
      ETA_BINS=if use_eta_bins then "1" else "0";
      RESPONSE_FILTERING=if use_response_filtering then "1" else "0";
      DEFAULT_SAVE_FIG_FMT="pdf";
      MATPLOTLIBRC=writeTextFile {
        name = "unfold-mpl-config";
        text = ''
          # interactive mode prevents plt.show() from blocking
          interactive : True
        '';
      };
      buildPhase = ''
        ln -s ${response} ./input_response
        ln -s ${measurement} ./input_measurement
        mkdir "$out"
        awk -f ${misc/org2py.awk} ${unfold_notebook} > unfolding.py
        sed -i unfolding.py -e 's/_00_04/_00_${remove_dot etabin_cut_1}/g' -e 's/_04_08/_${remove_dot etabin_cut_1}_${remove_dot etabin_cut_2}/g'
        mkdir plots
        python ./unfolding.py
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
        mv plots "$out"/plots
      '';
      passthru = {
        title = "Unfolded";
      };
      __htcondor_request_memory = "8G";
    });
    unfold = unfold' {};
    vertex_fit = input: mkDerivation {
      name = "${input.name}-vertex-fit";
      nativeBuildInputs = [
        python3
        python3Packages.pandas
        python3Packages.numpy
        python3Packages.matplotlib
        python3Packages.scipy
        python3Packages.yoda
        ];
      buildPhase = ''
        cp ${./misc/plot.py} ./plot.py
        cp ${./misc/vertex_fit.py} ./vertex_fit.py
        cp ${input}/output.yoda.gz input.yoda.gz
        ./vertex_fit.py input.yoda.gz
      '';
      installPhase = ''
        mkdir "$out"
        mv *.pdf *.png "$out"/
        gzip output.yoda
        mv output.yoda.gz "$out"/
      '';
      passthru = inherited_titles input;
    };
    uncert_breakdown = { unc_stat, unc_emb, unc_emc }: mkDerivation {
      name = "${unc_stat.name}-uncert-breakdown";
      src = ./misc/uncert-breakdown.py;
      nativeBuildInputs = [
        python3
        python3Packages.yoda
        python3Packages.pyyaml
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp "$src" ./uncert-breakdown.py
        ./uncert-breakdown.py \
          --unc_stat "${unc_stat}"/output.yoda.gz \
          --unc_emb "${unc_emb}"/output.yoda.gz \
          --unc_emc "${unc_emc}"/output.yoda.gz \
          -o output.yoda
        gzip output.yoda
      '';
      installPhase = ''
        install -Dm444 output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    to_latex = { unc_stat, unc_syst }: mkDerivation {
      name = "${unc_stat.name}-table.tex";
      src = ./misc/to_latex.py;
      nativeBuildInputs = [
        python3
        python3Packages.yoda
      ];
      buildPhase = ''
        # copy to prevent python from hammering the store during imports
        cp "$src" ./to_latex.py
        ./to_latex.py \
          --unc_stat "${unc_stat}"/output.yoda.gz \
          --unc_syst "${unc_syst}"/output.yoda.gz \
          > table.tex
      '';
      installPhase = ''
        install -m444 table.tex "$out"
      '';
    };
    run_info_file = misc/run_info.json;
    run_info = builtins.fromJSON (builtins.readFile run_info_file);
    run_lumi_file = misc/run_lumi_fromZilong.json;
    run_lumi = builtins.fromJSON (builtins.readFile run_lumi_file);
    read_list_file = file: builtins.filter (e: (e != "") && (!builtins.isList e)) (builtins.split "\n" (builtins.readFile file));
    kevin_runlist = read_list_file misc/2012_200GeV_FinalResultRunlist_fromKevin.txt;
    remove_runs = runs: runlist: builtins.filter (r: !(builtins.elem r runs)) runlist;
    runlist_global = kevin_runlist;
    runindex_from_runid = runid: find runid runlist_global; # get position of the run in the runlist
    runlist_blacklist = [
      "13050011" # missing files in data
      "13059087" # missing files in embedding
      "13055015" # funny run
      "13069004" # empty files in new production
      "13066101" # big DCA in data
      "13066102" # big DCA in data
      "13066104" # big DCA in data
      "13066109" # big DCA in data
    ];
    runlist = remove_runs runlist_blacklist runlist_global;
    data_files = runid: ''"${HOME}/run12_pp200_data_v2/${runid}/*.jets.root:${toString (runindex_from_runid runid)}"'';
    data_run = runid: batch_process process_data_overrides "data" [ (data_files runid) ];

    zerobias_definition_ps = 524289; # zerobias bit was firing every 524289 bunch crossings

    sum_floats = builtins.foldl' builtins.add 0.0;
    jp0_luminosity = sum_floats (map (runid: run_lumi.${runid}.jp0_luminosity) runlist);
    jp1_luminosity = sum_floats (map (runid: run_lumi.${runid}.jp1_luminosity) runlist);
    jp2_luminosity = sum_floats (map (runid: run_lumi.${runid}.jp2_luminosity) runlist);
    vpdmb_nobsmd_luminosity = sum_floats (map (runid: run_lumi.${runid}.vpdmb_nobsmd_luminosity) runlist);
    zerobias_luminosity = sum_floats (map (runid: run_lumi.${runid}.zerobias_luminosity) runlist);
    jp0_luminosity_no_ps = sum_floats (map (runid: run_lumi.${runid}.jp0_luminosity * run_lumi.${runid}.jp0_ps) runlist);
    jp1_luminosity_no_ps = sum_floats (map (runid: run_lumi.${runid}.jp1_luminosity * run_lumi.${runid}.jp1_ps) runlist);
    jp2_luminosity_no_ps = sum_floats (map (runid: run_lumi.${runid}.jp2_luminosity * run_lumi.${runid}.jp2_ps) runlist);
    vpdmb_nobsmd_luminosity_no_ps = sum_floats (map (runid: run_lumi.${runid}.vpdmb_nobsmd_luminosity * run_lumi.${runid}.vpdmb_nobsmd_ps) runlist);
    scale_lumi = scale [
      "/.*_jp0_promoted/.* ${toString (1 / jp0_luminosity_no_ps)}x"
      "/.*_jp1_promoted/.* ${toString (1 / jp1_luminosity_no_ps)}x"
      "/.*_jp2_promoted/.* ${toString (1 / jp2_luminosity_no_ps)}x"
      "/.*_jp0_demoted/.* ${toString (1 / jp0_luminosity_no_ps)}x"
      "/.*_jp1_demoted/.* ${toString (1 / jp1_luminosity_no_ps)}x"
      "/.*_jp2_demoted/.* ${toString (1 / jp2_luminosity_no_ps)}x"
      "/.*_jp0(_.+)?/.* ${toString (1 / jp0_luminosity)}x"
      "/.*_jp1(_.+)?/.* ${toString (1 / jp1_luminosity)}x"
      "/.*_jp2(_.+)?/.* ${toString (1 / jp2_luminosity)}x"
      "/.*_vpdmb_nobsmd(_jp0_simu)?/.* ${toString (1 / vpdmb_nobsmd_luminosity)}x"
      "/.*_zerobias/.* ${toString (1 / zerobias_luminosity * zerobias_definition_ps)}x"
    ];
    no_scale = x: x;
    unscale_lumi = scale [
      "/.*_jp0(_.+)?/.* ${toString (jp0_luminosity_no_ps)}x"
      "/.*_jp1(_.+)?/.* ${toString (jp1_luminosity_no_ps)}x"
      "/.*_jp2(_.+)?/.* ${toString (jp2_luminosity_no_ps)}x"
      "/.*_vpdmb_nobsmd(_jp0_simu)?/.* ${toString (vpdmb_nobsmd_luminosity_no_ps)}x"
      "/.*_zerobias/.* ${toString (zerobias_luminosity)}x"
    ];
    scale_for_data = scale_lumi;
    scale_for_embed = no_scale;

    apply_post_cuts = input: yoda_reset_bins input [
      {
        regex = "jp0_(demoted|promoted).*/(detector_jet_pt|response)";
        cond = "x < ${toString (lib.last (builtins.filter (pt: pt < 25.0) unfold_bins))}";
      }
      {
        regex = "jp1_(demoted|promoted).*/(detector_jet_pt|response)";
        cond = "x > ${toString (lib.head (builtins.filter (pt: pt >= 8.0) unfold_bins))}";
      }
      {
        regex = "jp2_(demoted|promoted)/(detector_jet_pt|response)";
        cond = "x > ${toString (lib.head (builtins.filter (pt: pt >= 9.5) unfold_bins))}";
      }
    ];

    data_yoda' = merge (map data_run runlist);
    data_yoda = scale_for_data (apply_post_cuts data_yoda');
    data_qa_yoda' = stack (map
      (runnumber: {
        output = data_run runnumber;
        xmin = runindex_from_runid runnumber;
      })
      runlist);
    data_qa_yoda = scale_for_data data_qa_yoda';
    data_qa_norm_yoda' = stack (map
      (runnumber: {
        output = scale ".* 1" (data_run runnumber);
        xmin = runindex_from_runid runnumber;
      })
      runlist);
    data_qa_norm_yoda = scale_for_data data_qa_norm_yoda';

    embed_files = pt_bin: runid: ''"${HOME}/run12_pp200_embed_v3/${pt_bin}/st_zerobias_adc_${runid}_*_r*.jets.root:${toString (runindex_from_runid runid)}:${toString run_info.${runid}.jp0_prescaler}:${toString run_info.${runid}.jp1_prescaler}"'';
    embed_num_batches = pt_bin: 5 * ({
      # number of repeats
      "pt2_3" = 3;
      "pt3_4" = 3;
      "pt4_5" = 3;
      "pt5_7" = 3;
      "pt7_9" = 3;
      "pt9_11" = 3;
      "pt11_15" = 3;
      "pt15_20" = 3;
      "pt20_25" = 3;
      "pt25_35" = 2;
      "pt35_45" = 2;
      "pt45_55" = 1;
      "pt55_-1" = 1;
    }.${pt_bin});
    embed_batch_size = pt_bin: ((builtins.length runlist) / (embed_num_batches pt_bin)) + 1;
    embed_bin = pt_bin: let
        run_files = map (embed_files pt_bin) runlist;
        run_batches = split (embed_batch_size pt_bin) run_files;
      in
        merge (map (batch_process process_embed_overrides "${pt_bin}") run_batches);
    embed_bins_unscaled = lib.mapAttrs (pt_bin: apply_post_cuts) (lib.genAttrs pt_bins embed_bin);
    embed_bins = lib.mapAttrs (pt_bin: scale_for_embed) (lib.mapAttrs embed_scale embed_bins_unscaled);
    embed_run = runnumber: embed_merge (lib.genAttrs pt_bins
      (pt_bin: let
        files = (embed_files pt_bin runnumber);
      in
        batch_process process_embed_overrides "${runnumber}-${pt_bin}" [ files ]));
    embed_yoda = pkgs.symlinkJoin {
      name = "embed_yoda";
      paths = [
        (post_process (scale_for_embed (embed_merge embed_bins_unscaled)))
        embed_bins_set
      ];
    };
    embed_factors = lib.mapAttrsToList embed_factor embed_bins_unscaled;
    embed_qa_yoda = stack (map
      (runnumber: {
        output = embed_run runnumber;
        xmin = runindex_from_runid runnumber;
      })
      runlist);
    embed_qa_norm_yoda = stack (map
      (runnumber: {
        output = scale ".* 1" (embed_run runnumber);
        xmin = runindex_from_runid runnumber;
      })
      runlist);
    embed_bins_set = mkDerivation {
      name = "embed-bins-set";
      installPhase = ''
        mkdir "$out"
      ''
      + builtins.concatStringsSep "\n"
          (lib.mapAttrsToList (pt_bin: input: ''ln -s "${input}"/output.yoda.gz "$out"/${pt_bin}.yoda.gz'') embed_bins);
    };
  };
in
  enableOverride this
