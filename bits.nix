with import <nixpkgs> {};
with import ./common.nix;

{ overrides ? self: super: {} }:

let
  split = n: xs: if builtins.length xs <= n then
      [xs]
    else
      [(lib.take n xs)] ++ (split n (lib.drop n xs));
  bits = self: with self; {
    possiblePhases = [ "buildPhase" "installPhase" ];
    mkDerivation = attrs: stdenv.mkDerivation ((
      lib.optionalAttrs (!(attrs.dontSetPhases or false)) {
        # Determine phases based on attributes provided. This disables all implicit phases from stdenv.
        phases = builtins.filter (lib.flip builtins.hasAttr attrs) possiblePhases;
      } // attrs) // {
      preferLocalBuild = true;
      allowSubstitutes = false;
    });
    HOME = builtins.getEnv "HOME";
    set_title = title: drv: (drv // { inherit title; });
    pt_bins = [
      "pt2_3"
      "pt3_4"
      "pt4_5"
      "pt5_7"
      "pt7_9"
      "pt9_11"
      "pt11_15"
      "pt15_20"
      "pt20_25"
      "pt25_35"
      "pt35_-1"
      ];
    make_jet_plots = "${make_jet_plots_build}/bin/make_jet_plots";
    make_jet_plots_build = stdenv.mkDerivation rec {
      name = "make_jet_plots_build";
      src = lib.sourceByRegex ./. [
        "^wscript$"
        "^StJetPlots(|/.+)$"
        "^StSpinPool(|/.+)$"
        ];
      buildInputs = [ root yoda ];
      buildPhase = ''
        ${waf} configure build install --prefix=$out
      '';
      installPhase = "true";
    };
    process_overrides = {};
    process = name: inputs: stdenv.mkDerivation ({
      inherit name;
      passthru = {
        title = builtins.replaceStrings ["_"] ["\\_"] name;
      };
      nativeBuildInputs = [ gzip ];
      phases = [ "buildPhase" "installPhase" ];
      buildPhase = ''
        ${time}/bin/time -f "%C\n%Uuser %Ssystem %eelapsed %PCPU (%Xtext+%Ddata %Mmax)\n%Iinputs+%Ooutputs (%Fmajor+%Rminor)pagefaults %Wswaps\ncontext switches: %cinvoluntarily %wwaits" -o time ${make_jet_plots} ${join inputs}
        cat time
      '';
      installPhase = ''
        install -Dm644 output.root $out/output.root
        gzip output.yoda
        install -Dm644 output.yoda.gz $out/output.yoda.gz
        install -Dm644 time $out/time
      '';
      allowSubstitutes = false;
      __slurm_job_timelimit = "0-2";
    } // process_overrides);
    batch_process = name: inputs: wrap_condor (process name inputs);
    merge = outputs: mkDerivation {
      name = "merge";
      passthru = {
        title = (builtins.head outputs).title;
      };
      root_files = map (output: "${output}/output.root") outputs;
      yodagz_files = map (output: "${output}/output.yoda.gz") outputs;
      time_files = map (output: "${output}/time") outputs;
      nativeBuildInputs = [ root yoda gzip ];
      buildPhase = ''
        mkdir -p $out
        hadd $out/output.root ''${root_files[@]}
        yodamerge --add -o output.yoda ''${yodagz_files[@]}
        gzip output.yoda
        install -Dm644 output.yoda.gz $out/output.yoda.gz
        cat ''${time_files[@]} > $out/time_summary
      '';
    };
    scale = scale_spec: input: mkDerivation {
      name = "${input.name}-scaled";
      passthru = {
        title = input.title;
      };
      nativeBuildInputs = [ gzip yoda ];
      buildPhase = ''
        yodascale -c "${scale_spec}" ${input}/output.yoda
        gzip output-scaled.yoda
        install -Dm644 output-scaled.yoda.gz $out/output.yoda.gz
      '';
    };
    embed_disable_ff = false;
    embed_factor = "${./embed_factor.py} ${lib.optionalString embed_disable_ff "--disable-ff"}";
    embed_scale = pt_bin: input: mkDerivation {
      name = "${input.name}-embed-scaled";
      passthru = {
        title = input.title;
      };
      nativeBuildInputs = [ gzip yoda ];
      buildPhase = ''
        cp ${input}/output.yoda.gz .
        gunzip output.yoda.gz
        yodascale -c ".* $(${embed_factor} pp200_${pt_bin} ${input}/output.yoda.gz)x" output.yoda
        gzip output-scaled.yoda
        install -Dm644 output-scaled.yoda.gz $out/output.yoda.gz
      '';
    };
    embed_merge = with stdenv.lib; inputs: let
      input_list = filename: join (mapAttrsToList (pt_bin: input: "${input}/${filename}:$(${embed_factor} pp200_${pt_bin} ${input}/output.yoda.gz 2>> $out/factors)") inputs);
    in
    mkDerivation {
      name = "embed-merge";
      nativeBuildInputs = [ gzip python2 root python2Packages.rootpy yoda ];
      buildPhase = ''
        mkdir -p $out
        ${misc/rootmerge.py} --add -o $out/output.root ${input_list "output.root"}
        yodamerge --add -o output.yoda ${input_list "output.yoda.gz"}
        gzip output.yoda
        install -Dm644 output.yoda.gz $out/output.yoda.gz
      '';
    };
    mkhtml_config = ./JET_PLOTS.plot;
    mkhtml = yoda_files: extra_args: mkDerivation {
      name = "plots";
      nativeBuildInputs = [ rivet ];
      buildPhase = ''
        rivet-mkhtml ${extra_args} --num-threads 4 --config ${mkhtml_config} -o $out \
          ${join (map (d: "${d}/output.yoda.gz:\"${builtins.replaceStrings ["\\" "$"] ["\\\\" "\\$"] d.title}\"${d.plot_options or ""}") yoda_files)} \
          -M ".*/vertex_[xy]" \
          -M ".*_fine$" \
          -M "^/pt.*"
      '';
    };
    rescale_ref = input: ref: mkDerivation {
      name = "${input.name}-rescaled";
      passthru = {
        title = input.title;
      };
      nativeBuildInputs = [ gzip yoda ];
      buildPhase = ''
        cp ${input}/output.yoda.gz .
        cp ${ref}/output.yoda.gz ref.yoda.gz
        gunzip output.yoda.gz
        gunzip ref.yoda.gz
        yodascale -v -c 'vs_vertex_z$ 1x' -c '^/.*/jet_.* REF' -c '^/.*/vertex_.* REF' -c '.* 1x' output.yoda -r ref.yoda
        gzip output-scaled.yoda
        install -Dm644 output-scaled.yoda.gz $out/output.yoda.gz
      '';
    };
    post_process = input: mkDerivation {
      name = "${input.name}-post-process";
      passthru = {
        title = input.title;
      };
      nativeBuildInputs = [ yoda ];
      buildPhase = ''
        ${misc/post-processing.py} ${input}/output.yoda.gz
        gzip output.yoda
        mkdir -p $out
        cp ${input}/* $out/
        install -Dm644 output.yoda.gz $out/output.yoda.gz
      '';
    };
    rename = input: what: _with: mkDerivation {
      name = "${input.name}-rename";
      passthru = {
        title = input.title;
      };
      nativeBuildInputs = [ yoda ];
      buildPhase = ''
        ${misc/rename.py} ${input}/output.yoda.gz ${what} ${_with}
        gzip output.yoda
        mkdir -p $out
        cp ${input}/* $out/
        install -Dm644 output.yoda.gz $out/output.yoda.gz
      '';
    };
    partonic_bin_copy = tag: input: mkDerivation {
      name = input.name + "-" + tag;
      passthru = {
        title = input.title;
      };
      nativeBuildInputs = [ yoda python2 ];
      buildPhase = ''
        ${misc/partonic_bin_copy.py} ${input}/output.yoda.gz ${tag}
        gzip output.yoda
        mkdir -p $out
        cp ${input}/* $out/
        install -Dm644 output.yoda.gz $out/output.yoda.gz
      '';
    };
    unfold = response: measurement: mkDerivation {
      name = "unfold";
      passthru = {
        title = "Unfolded";
      };
      nativeBuildInputs =
        lib.optional (builtins.compareVersions python2Packages.numpy.version "1.13.0" < 0) python2Packages.numpy_1_13
        ++ [
          python2Packages.nbconvert python2Packages.scipy yoda jq
        ];
      buildPhase = ''
        ln -s ${response}/output.yoda.gz ./input_embed.yoda.gz
        ln -s ${measurement}/output.yoda.gz ./input_data.yoda.gz
        # replace python3 metadata with python2 one
        jq -s "(.[0] | del(.metadata)) * .[1]" ${misc/unfolding.ipynb} ${misc/python2_nb_meta.json} > unfolding.ipynb
        mkdir "$out"
        HOME=$(pwd) jupyter-nbconvert unfolding.ipynb --execute --ExecutePreprocessor.timeout=-1 --to html --output "$out"/index.html
        gzip output.yoda
        install -Dm644 output.yoda.gz "$out"/output.yoda.gz
      '';
    };
    kevin_runlist_file = misc/2012_200GeV_FinalResultRunlist_fromKevin.txt;
    kevin_runlist = builtins.filter (s: s != "") (lib.splitString "\n" (builtins.readFile kevin_runlist_file));
    remove_runs = runs: runlist: builtins.filter (r: !(builtins.elem r runs)) runlist;
    runlist = remove_runs [
      "13050011" # missing files in data
      "13059087" # missing files in embedding
      "13055015" # funny run
      ] kevin_runlist;
    data_files = runid: ''"${HOME}/pwg/${runid}/*.jets.root"'';
    data_run = runid: batch_process "data" [ (data_files runid) ];
    data_yoda = merge (map data_run runlist);

    embed_files = pt_bin: runid: ''"${HOME}/pwg/run12_200GeVEmbedJets/${pt_bin}/${pt_bin}_${runid}_1.jets.root"'';
    embed_batch_size = 100;
    embed_bin = pt_bin: let
        run_files = map (embed_files pt_bin) runlist;
        run_batches = split embed_batch_size run_files;
      in
        merge (map (batch_process "${pt_bin}") run_batches);
    embed_bins_unscaled = stdenv.lib.genAttrs pt_bins embed_bin;
    embed_bins = stdenv.lib.mapAttrs embed_scale embed_bins_unscaled;
    embed_yoda = post_process (embed_merge embed_bins_unscaled);
  };
in
  lib.fix (lib.extends overrides bits)
